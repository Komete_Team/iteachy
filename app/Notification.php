<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
   	use SoftDeletes;

   	protected $fillable = ['user_id', 'priority_id', 'heading', 'description', 'send_email', 'notification_source', 'is_read', 'created_at', 'created_by', 'lecture_id'];
}
