<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //

    protected $table = 'e_wallet';

      public $fillable = [
        'paid_id',
        'user_id',
    	'amount',
    	'comment'
    ];



}
