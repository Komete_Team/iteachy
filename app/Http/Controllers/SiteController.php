<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Categories;
use App\AvailabilityDays;
use App\TeacherAvailability;
use App\TeacherLeave;

use Mail;
use App\TeacherReviews;
use App\User;
use App\Role;
use App\Permission;
use App\UserCategories;
use App\TeacherLessons;
use Validator;
class SiteController extends Controller {
    public function login() {
        if (Auth::check() || Auth::viaRemember()) // User is already logged-in or remembered
        {
            return redirect('teacher/dashboard');
        } else
        // User is not logged-in, show the login page
        {
            return view('login');
        }
    }
    /**
     * Function to return register page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function register() {
        return view('register');
    }
    public function registerTeacher() {
        // Get the serialized form data
        $frmData = Input::get('frmData');
        // Parse the serialize form data to an array
        parse_str($frmData, $teacherDetails);
        $response = array();
        // Server Side Validation
        $validation = Validator::make(array('first_name' => $teacherDetails['first_name'], 'last_name' => $teacherDetails['last_name'], 'email_id' => $teacherDetails['email_id'], 'password' => $teacherDetails['password']), array('first_name' => array('required'), 'last_name' => array('required'), 'email_id' => array('required', 'email'), 'password' => array('required')), array('first_name.required' => 'Please enter school name', 'last_name.required' => 'Please enter school name', 'email_id.required' => 'Please enter email id', 'email_id.email' => 'Please enter a valid email id', 'password.required' => 'Please enter password'));
        if ($validation->fails()) // Some data is not valid as per the defined validation rules
        {
            $error = $validation->errors()->first();
            if (isset($error) && !empty($error)) {
                $response['resCode'] = 1;
                $response['resMsg'] = $error;
            }
        } else
        // The data is valid, go ahead and save it
        {
            $user = User::where(['email' => $teacherDetails['email_id']])->first();
            if (is_null($user)) // No user with the same email id exist, save the data
            {
                try {
                    // Begin transaction
                    DB::beginTransaction();
                    $user = new User;
                    $user->first_name = $teacherDetails['first_name'];
                    $user->last_name = $teacherDetails['last_name'];
                    $user->email = $teacherDetails['email_id'];
                    $user->password = Hash::make($teacherDetails['password']);
                    $user->role = 2;
                    if ($user->save()) {
                        $response['resCode'] = 0;
                        $response['resMsg'] = 'Teacher registered successfully. You can access your account after verification.';
                    } else {
                        // Transaction failed
                        DB::rollback();
                        $response['resCode'] = 6;
                        $response['resMsg'] = 'Server error!';
                    }
                }
                catch(\Exception $e) {
                    // Transaction failed
                    DB::rollback();
                    $response['resCode'] = 7;
                    $response['resMsg'] = 'Server error!';
                }
            } else {
                $response['resCode'] = 2;
                $response['resMsg'] = 'Teacher with the same email id already exist';
            }
        }
        return response()->json($response);
    }
    /**
     * Function to save role details
     * @param void
     * @return array
     */
    public function registerSchool() {
        // Get the serialized form data
        $frmData = Input::get('frmData');
        // Parse the serialize form data to an array
        parse_str($frmData, $schoolDetails);
        $response = array();
        // Server Side Validation
        $validation = Validator::make(array('school_name' => $schoolDetails['school_name'], 'full_name' => $schoolDetails['full_name'], 'email_id' => $schoolDetails['email_id'], 'password' => $schoolDetails['password']), array('school_name' => array('required'), 'full_name' => array('required'), 'email_id' => array('required', 'email'), 'password' => array('required')), array('school_name.required' => 'Please enter school name', 'full_name.required' => 'Please enter school name', 'email_id.required' => 'Please enter email id', 'email_id.email' => 'Please enter a valid email id', 'password.required' => 'Please enter password'));
        if ($validation->fails()) // Some data is not valid as per the defined validation rules
        {
            $error = $validation->errors()->first();
            if (isset($error) && !empty($error)) {
                $response['resCode'] = 1;
                $response['resMsg'] = $error;
            }
        } else
        // The data is valid, go ahead and save it
        {
            // Check if the school with the same name already exist
            $school = School::where(['school_name' => $schoolDetails['school_name']])->first();
            if (is_null($school)) // No school with the same name exist, save the data
            {
                // Check if an user exist with the same email id
                $user = User::where(['email' => $schoolDetails['email_id']])->first();
                if (is_null($user)) // No user with the same email id exist, save the data
                {
                    try {
                        // Begin transaction
                        DB::beginTransaction();
                        $user = new User;
                        $user->name = $schoolDetails['full_name'];
                        $user->email = $schoolDetails['email_id'];
                        $user->password = Hash::make($schoolDetails['password']);
                        if ($user->save()) {
                            // Attach the role "ali_user" to this newly created user
                            $userRole = Role::where(['name' => 'ali_user'])->first();
                            if ($user->attachRole($userRole)) {
                                $school = new School;
                                $school->user_id = $user->id;
                                $school->school_name = $schoolDetails['school_name'];
                                $school->status = '1';
                                $school->verified = '0';
                                $school->created_at = date('Y-m-d H:i:s');
                                if ($school->save()) {
                                    // Transaction successful
                                    DB::commit();
                                    $response['resCode'] = 0;
                                    $response['resMsg'] = 'School registered successfully. You can access your account after verification.';
                                } else {
                                    // Transaction failed
                                    DB::rollback();
                                    $response['resCode'] = 4;
                                    $response['resMsg'] = 'Server error!';
                                }
                            } else {
                                // Transaction failed
                                DB::rollback();
                                $response['resCode'] = 5;
                                $response['resMsg'] = 'Server error!';
                            }
                        } else {
                            // Transaction failed
                            DB::rollback();
                            $response['resCode'] = 6;
                            $response['resMsg'] = 'Server error!';
                        }
                    }
                    catch(\Exception $e) {
                        // Transaction failed
                        DB::rollback();
                        $response['resCode'] = 7;
                        $response['resMsg'] = 'Server error!';
                    }
                } else {
                    $response['resCode'] = 2;
                    $response['resMsg'] = 'User with the same email id already exist';
                }
            } else {
                $response['resCode'] = 3;
                $response['resMsg'] = 'School with the same name already exist';
            }
        }
        return response()->json($response);
    }
    /**
     * Function to return login page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = Categories::where('parent_id', '==', 0)->get();
        $subcategories = Categories::where('parent_id', '!=', 0)->get();
        $teachers = DB::table('users')->join('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->select('users.id', 'users.first_name', 'users.last_name', 'users.profile_image', 'teacher_infos.title', 'teacher_infos.description')->where(['users.role' => 2, 'status' => '1'])->limit(8)->get();
        //echo '<pre>'; print_r($teachers); die;
        return view('home', ['categories' => $categories, 'subcategories' => $subcategories, 'teachers' => $teachers]);
    }
    public function getTeachers(Request $request) {
        //echo '<pre>'; print_r($_POST); die;
        $categories = Categories::where('parent_id', '==', 0)->get();
        $allteachers = User::where(['role' => 2])->get();
        $subcategories = Categories::where('parent_id', '!=', 0)->get();
        $teachers = array();
        //$totalreviews = TeacherReviews::where('parent_id', '!=', 0)->get();
        $teachers1 = !empty($request->input('teachers')) ? $request->input('teachers') : '';
        if (!empty($teachers1)) {
            $teachers = DB::table('users')->leftJoin('user_categories', 'users.id', '=', 'user_categories.teacher_id')->leftJoin('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->leftJoin('categories', 'user_categories.category_id', '=', 'categories.id')->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id')->select('users.id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'categories.category_name')->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")->groupBy('users.id')->whereIn('users.id', $teachers1)->where('users.is_accridited', 1)->paginate(15);
        }
        //echo '<pre>'; print_r($teachers); die;
        return view('teacher_list', ['categories' => $categories, 'subcategories' => $subcategories, 'teachers' => $teachers, 'allteachers' => $allteachers]);
    }
    public function searchTeacher(Request $request) {
        DB::enableQueryLog();
        $recentSelectedCategories = array();
        if (isset($_COOKIE['recent_selected'])) {
            $recent_selected = explode(',', $_COOKIE['recent_selected']);
            $recentSelectedCategories = Categories::whereIn('id', $recent_selected)->get();   
        }
        $categories = Categories::where('parent_id', '==', 0)->get();
        $allteachers = User::where(['role' => 2])->get();
        $subcategories = Categories::where('parent_id', '!=', 0)->get();
        $category = !empty($request->input('categories')) ? $request->input('categories') : '';
        if (!empty($request->input('dtp_input'))) {
            $dtp_input[] = date('D', strtotime($request->input('dtp_input')));
			$searchdate = date('Y-m-d', strtotime($request->input('dtp_input')));
        } else {
            $dtp_input = array(1=>'Mon',2=>'Tue',3=>'Wed',4=>'Thu',5=>'Fri',6=>'Sat',7=>'Sun');
			$searchdate = '';
        }
        if (!empty($request->input('accridited'))) {
            $acr = $request->input('accridited');
        } else {
            $acr = 0;
        }
        if (!empty($request->input('max-value'))) {
            $minval = $request->input('min-value');
            $maxval = $request->input('max-value');
        } else {
            $minval = 0;
            $maxval = 0;
        }
		
		 $getNumDay = AvailabilityDays::whereIn('days', $dtp_input)->get();
		 $numdaysarr = array();
		 foreach($getNumDay as $numdayval){
			$numdaysarr[] = $numdayval->num_days;
		 }
		 
		 $getAvailableTeacher = TeacherAvailability::whereIn('num_days', $numdaysarr)->where('status', 1)->groupBy('teacher_id')->get();
		$getLeaves = array();
		$unavailableTeachers = array();
		if(!empty($getAvailableTeacher)){
			foreach($getAvailableTeacher as $k => $getAvailableTeacherVal){
				$getLeaves = TeacherLeave::where(['teacher_id'=>$getAvailableTeacherVal->teacher_id, 'status'=>1])->get();
				foreach($getLeaves as $val){
					if(!empty($searchdate)){
						if($val->leave_date == $searchdate)
						{
							$unavailableTeachers[] = $val->teacher_id;
						}
					}
				}
			}
		}
		//echo '<pre>'; print_r($getAvailableTeacher); die('bob');
		/* if(!empty($getAvailableTeacher)){
			foreach($getAvailableTeacher as $k => $getAvailableTeacherVal1){
				foreach($getLeaves as $val){
					if(!empty($searchdate)){
						if($val->leave_date == $searchdate)
						{
							unset($getAvailableTeacher[$k]);
						}
					}
				}
			}
		} */
		//echo '<pre>'; print_r($unavailableTeachers); die;
		
        if (!empty($category)) {
            $teachers = DB::table('users')->leftJoin('user_categories', 'users.id', '=', 'user_categories.teacher_id')->leftJoin('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->leftJoin('categories', 'user_categories.category_id', '=', 'categories.id')->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id')->leftJoin('teacher_leaves', 'teacher_leaves.teacher_id', '=', 'users.id')->select('users.id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.file_name', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'categories.category_name', 'teacher_leaves.leave_date')->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")->groupBy('users.id')->whereBetween('teacher_infos.price', [$minval, $maxval])->whereIn('user_categories.category_id', $category)->where('users.is_accridited', $acr)->whereNotIn('users.id', $unavailableTeachers)->paginate(15);
        } else {
            $teachers = DB::table('users')->leftJoin('user_categories', 'users.id', '=', 'user_categories.teacher_id')->leftJoin('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->leftJoin('categories', 'user_categories.category_id', '=', 'categories.id')->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id')->leftJoin('teacher_leaves', 'teacher_leaves.teacher_id', '=', 'users.id')->select('users.id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.file_name', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'categories.category_name', 'teacher_leaves.leave_date')->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")->groupBy('users.id')->whereBetween('teacher_infos.price', [$minval, $maxval])->where('users.is_accridited', $acr)->whereNotIn('users.id', $unavailableTeachers)->paginate(15);
        }
		foreach($teachers as $k => $val){
			//echo $val->id; die;
			$tid = $val->id;
			 //$teachers[$k]->price = TeacherLessons::where(['teacher_id'=>$tid,'type'=>1])->where('price_per_hour', '>', 0)->orderBy('price_per_hour', 'ASC')->limit(1)->first();
			 $price = DB::table('teacher_lessons')->select('price_per_hour')->where(['teacher_id'=>$tid,'type'=>1])->where('price_per_hour', '>', 0)->orderBy('price_per_hour', 'ASC')->limit(1)->first();
			 //echo '<pre>'; print_r($price); die;
			 if(!empty($price)){
				$teachers[$k]->price_per_hour = $price->price_per_hour;
			 }else{
				$teachers[$k]->price_per_hour = 0;
			 }
			 
			 $trial_price = DB::table('teacher_lessons')->select('price_per_hour')->where(['teacher_id'=>$tid,'type'=>0])->where('price_per_hour', '>', 0)->orderBy('price_per_hour', 'ASC')->limit(1)->first();
			 
			 if(!empty($trial_price)){
				$teachers[$k]->trial_price = $trial_price->price_per_hour;
			 }else{
				$teachers[$k]->trial_price = 0;
			 }
			 
			 
		}
		//echo '<pre>'; print_r($teachers); die;
        $query = DB::getQueryLog();
        return view('teacher_list', ['categories' => $categories, 'subcategories' => $subcategories, 'teachers' => $teachers, 'allteachers' => $allteachers, 'recentSelectedCategories' => $recentSelectedCategories]);
    }
    public function getTeacherSubcategory(Request $request) {
        // Get the serialized form data
        //echo '<pre>'; print_r($request->input('frmData')); die('bob');
        $response = array();
        // Check if the school with the same name already exist
        $cats = $request->input('frmData');
        $subcategories = Categories::whereIn('parent_id', $cats)->get();
        $output = '';
        foreach ($subcategories as $row) {
            $output.= '<option value="' . $row["id"] . '">' . $row["category_name"] . '</option>';
        }
        return $output;
    }
    public function getTeacherByFilter(Request $request) {
        // Get the serialized form data
        $frmData = Input::get('frmData');
        // Parse the serialize form data to an array
        parse_str($frmData, $search);
        //echo '<pre>'; print_r($search); die;
        if (!empty($search['categories'])) {
            $cat = $search['categories'];
        } else {
            $cat = array();
        }
        if (!empty($search['subcategories'])) {
            $subcat = $search['subcategories'];
        } else {
            $subcat = $cat;
        }
        if (!empty($search['accridited'])) {
            $acr = $search['accridited'];
        } else {
            $acr = 0;
        }
        if (!empty($search['min-value']) && !empty($search['max-value'])) {
            $minval = $search['min-value'];
            $maxval = $search['max-value'];
        } else {
            $minval = 0;
            $maxval = 1000000000;
        }
        if (!empty($search['review'])) {
            $review = $search['review'];
            DB::enableQueryLog();
            $allteachers = DB::table('users')->join('user_categories', 'users.id', '=', 'user_categories.teacher_id')->join('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->join('categories', 'user_categories.category_id', '=', 'categories.id')->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id')->select('users.id', 'user_categories.teacher_id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.file_name', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'categories.category_name', DB::raw('round(AVG(teacher_reviews.rate),0) as rate_avg'))->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")->where('users.is_accridited', $acr)->whereBetween('teacher_infos.price', [$minval, $maxval])->whereIn('category_id', $cat)->whereIn('category_id', $subcat)->groupBy('user_categories.teacher_id')->orderBy('rate_avg', 'DESC')->paginate(15);
        } else {
            $review = 0;
            $allteachers = DB::table('users')->join('user_categories', 'users.id', '=', 'user_categories.teacher_id')->join('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->join('categories', 'user_categories.category_id', '=', 'categories.id')->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id')->select('users.id', 'user_categories.teacher_id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.file_name', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'categories.category_name')->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")->where('users.is_accridited', $acr)->whereBetween('teacher_infos.price', [$minval, $maxval])->whereIn('category_id', $cat)->whereIn('category_id', $subcat)->groupBy('user_categories.teacher_id')->paginate(15);
        }
        //$query = DB::getQueryLog();
        //echo '<pre>'; print_r($allteachers); die;
        $response['allteachers'] = $allteachers;
        return view('ajax_teacher_list', ['teachers' => $allteachers]);
    }
    public function teacherDetail(Request $request, $tid = null) {
        //echo $tid; die;
		$recentSelectedCategories = array();
        if (isset($_COOKIE['recent_selected'])) {
            $recent_selected = explode(',', $_COOKIE['recent_selected']);
            $recentSelectedCategories = Categories::whereIn('id', $recent_selected)->get();   
        }
        $categories = Categories::where('parent_id', '==', 0)->get();
        $allteachers = User::where(['role' => 2])->get();
        $subcategories = Categories::where('parent_id', '!=', 0)->get();
        $teacherInfo = array();
		$teachertotalreviews = DB::table('teacher_reviews')->select('teacher_reviews.*', DB::raw('AVG(teacher_reviews.rate) as rates'), DB::raw('COUNT(teacher_reviews.user_id) as totalusers'))
		->where('teacher_reviews.teacher_id', $tid)
		->where('teacher_reviews.user_id', '!=', NULL)
		->first();
		//echo '<pre>'; print_r($teachertotalreviews); die;
        $teacherreviews = DB::table('users')->join('teacher_reviews', 'users.id', '=', 'teacher_reviews.user_id')->select('users.*', 'teacher_reviews.title', 'teacher_reviews.rate', 'teacher_reviews.review_message')->where('teacher_reviews.teacher_id', $tid)->orderBy('teacher_reviews.rate', 'DESC')->first();
        $teacherLessons = TeacherLessons::where(['teacher_id' => $tid])->orderBy('id', 'ASC')->get();
        if (!empty($tid)) {
            $teacherInfo = DB::table('users')->join('user_categories', 'users.id', '=', 'user_categories.teacher_id')->join('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id')->join('categories', 'user_categories.category_id', '=', 'categories.id')->select('users.id', 'user_categories.teacher_id', 'users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.address', 'users.city', 'users.province', 'users.country', 'users.contact_no', 'users.profile_image', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.price', 'teacher_infos.price_per_hour', 'teacher_infos.file_name', 'categories.category_name')->selectRaw("GROUP_CONCAT(categories.category_name SEPARATOR '$') as categories")->where('users.id', $tid)->groupBy('users.id')->first();
            return view('teacher_detail', ['teacherInfo' => $teacherInfo, 'teacherLessons' => $teacherLessons, 'teacherreviews' => $teacherreviews, 'categories' => $categories, 'subcategories' => $subcategories, 'allteachers' => $allteachers, 'recentSelectedCategories' => $recentSelectedCategories, 'teachertotalreviews'=>$teachertotalreviews]);
        } else {
            return redirect('/')->with('danger', 'something_wrong.');
        }
        return view('teacher_detail', ['teacherInfo' => $teacherInfo, 'teacherreviews' => $teacherreviews, 'categories' => $categories, 'subcategories' => $subcategories, 'allteachers' => $allteachers, 'recentSelectedCategories' => $recentSelectedCategories, 'teachertotalreviews'=>$teachertotalreviews]);
    }
    public function getTeacherCategory(Request $request) {
        // Get the serialized form data
        $frmData = Input::get('frmData');
        // Parse the serialize form data to an array
        parse_str($frmData, $search);
        echo '<pre>';
        print_r($search);
        die;
        $response = array();
        // Check if the school with the same name already exist
        $school = School::where(['school_name' => $schoolDetails['school_name']])->first();
        if (is_null($school)) // No school with the same name exist, save the data
        {
            // Check if an user exist with the same email id
            $user = User::where(['email' => $schoolDetails['email_id']])->first();
            if (is_null($user)) // No user with the same email id exist, save the data
            {
                try {
                    // Begin transaction
                    DB::beginTransaction();
                    $user = new User;
                    $user->name = $schoolDetails['full_name'];
                    $user->email = $schoolDetails['email_id'];
                    $user->password = Hash::make($schoolDetails['password']);
                    if ($user->save()) {
                        // Attach the role "ali_user" to this newly created user
                        $userRole = Role::where(['name' => 'ali_user'])->first();
                        if ($user->attachRole($userRole)) {
                            $school = new School;
                            $school->user_id = $user->id;
                            $school->school_name = $schoolDetails['school_name'];
                            $school->status = '1';
                            $school->verified = '0';
                            $school->created_at = date('Y-m-d H:i:s');
                            if ($school->save()) {
                                // Transaction successful
                                DB::commit();
                                $response['resCode'] = 0;
                                $response['resMsg'] = 'School registered successfully. You can access your account after verification.';
                            } else {
                                // Transaction failed
                                DB::rollback();
                                $response['resCode'] = 4;
                                $response['resMsg'] = 'Server error!';
                            }
                        } else {
                            // Transaction failed
                            DB::rollback();
                            $response['resCode'] = 5;
                            $response['resMsg'] = 'Server error!';
                        }
                    } else {
                        // Transaction failed
                        DB::rollback();
                        $response['resCode'] = 6;
                        $response['resMsg'] = 'Server error!';
                    }
                }
                catch(\Exception $e) {
                    // Transaction failed
                    DB::rollback();
                    $response['resCode'] = 7;
                    $response['resMsg'] = 'Server error!';
                }
            } else {
                $response['resCode'] = 2;
                $response['resMsg'] = 'User with the same email id already exist';
            }
        } else {
            $response['resCode'] = 3;
            $response['resMsg'] = 'School with the same name already exist';
        }
        return response()->json($response);
    }
    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {
        return view('school/dashboard');
    }
    public function aboutUs() {
        return view('aboutus');
    }
    public function contactUs() {
        return view('contactus');
    }
    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function logout() {
        Auth::logout();
        return redirect('school');
    }
	
	
	public function userForgotpassword(Request $request)
    {

      if(isset($_POST['forgotpassword']))
      {

                $rules=[
                         'email' => 'required|string|email|max:255',                               
                      ];

              $message["email.required"] = 'Il campo email è obbligatorio';
              
               $this->validate($request, $rules,$message); 
     

          $email = !empty($request->input('email'))?$request->input('email'):'';

           $token= !empty($request->input('_token'))?$request->input('_token'):'';

          $checkEmailExists= User::where('email','=',$email)->first();
             
       
          if(!empty($checkEmailExists))
          { 

              if($checkEmailExists->is_email_verify==1)
              {

                    $updateToken= array(
                            'remember_token'=>$token
                        );

                     User::where(['id' => $checkEmailExists->id])->update($updateToken);

                      $subject = 'Forgot Password Confirmation';

                     /* $header = "From:bklic@bklic.komete.it \r\n";
                     $header.= 'MIME-Version: 1.0' . "\r\n";
                     $header.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                     $message ='Hello '.$checkEmailExists->first_name.' '.$checkEmailExists->last_name.'<br/>'; 
                     $message.='You can reset your password bellow link . <br/>'; 
                     $message.= '<a href="'.url('user-resetpassword/'.$token).'">Please click on link to verify your email address</a>';

                     mail($email,$subject,$message,$header);   */
					 
					 Mail::send('password_reset', ['user' => $checkEmailExists, 'token'=>$token], function ($m) use ($checkEmailExists) {
						$m->from('info@iteachy.com', 'Iteachy Plateform');

						$m->to($checkEmailExists->email, $checkEmailExists->first_name)->subject('Forgot Password Confirmation');
					}); 

              }else{

                  return redirect('user-forgotpassword')->with('danger','Si prega di verificare prima e-mail.');
              }

          }else{

            return redirect('user-forgotpassword')->with('danger','indirizzo email non corrisponde a noi.');
          }

      }
      return view('auth/passwords/email');

    }
	
	public function ResetPassword(Request $request,$token)
    {
        //echo $token;die;


          $checkTokenCorrect= User::where('remember_token','=',$token)->first();

          if(!empty($checkTokenCorrect))
          {
              if(isset($_POST['reset']))
              {
                 
                     $rules=[
                         'password' => 'required|string|min:6|confirmed',                               
                      ];

                      $message["password.required"] = 'Il campo della password è obbligatorio';
                      $message["password.min"] = 'La password deve contenere almeno 6 caratteri';

                     $message["password.confirmed"] = 'La conferma della password non corrisponde';

                      $this->validate($request, $rules,$message); 


                   $password= !empty($request->input('password'))?$request->input('password'):'';

                    $updatepassword= array(
                            'password'=>bcrypt($password)
                        );


                     User::where(['id' => $checkTokenCorrect->id])->update($updatepassword); 

                   return redirect('/')->with('success','La tua password cambia con successo');  
             }
             
          }else{

            return redirect('/')->with('danger','Mancata corrispondenza dei token con noi.');
          }

        return view('auth.passwords.reset',['token'=>$token]);

    }
	
	public function registerUser()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $teacherDetails);
		// echo '<pre>'; print_r($teacherDetails); die;
        $response =array(); 
        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'first_name'	=> $teacherDetails['first_name'],
		        'last_name'		=> $teacherDetails['last_name'],
		        'email_id'		=> $teacherDetails['email_id'],
		        'password' 		=> $teacherDetails['password'],
				'categories'		=> $teacherDetails['categories'],
				
		    ),
		    array(
		        'first_name'	=> array('required'),
		        'last_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required'),
				'categories' 		=> array('required')
		    ),
		    array(
		        'first_name.required' 	=> 'Si prega di inserire il nome',
		        'last_name.required' 	=> 'Per favore inserisci il cognome',
		        'email_id.required'		=> 'Si prega di inserire l ID e-mail',
		        'email_id.email'		=> 'Si prega di inserire un ID e-mail valido',
		        'password.required'		=> 'Per favore, inserisci la password',
				'categories.required' 	=> 'si prega di selezionare la categoria',
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$user = User::where(['email' => $teacherDetails['email_id']])->first();
			if( is_null( $user ) )	// No user with the same email id exist, save the data
			{

				$user = new User;

				$user->first_name 	= $teacherDetails['first_name'];
				$user->last_name 	= $teacherDetails['last_name'];
				$user->email 	= $teacherDetails['email_id'];
				$user->password = Hash::make($teacherDetails['password']);
				//$user->role = 2;
				$user->remember_token = $teacherDetails['_token'];
				$user->is_online  = !empty($teacherDetails['online'])  ?  $teacherDetails['online'] : '';
				$user->role = $teacherDetails['account_type'];
				if($teacherDetails['account_type'] == 3){
					$user->status = 1;
				}
				
				if( $user->save() )
				{
					$cats = $teacherDetails['categories'];
					//echo '<pre>'; print_r($_POST); die;
					foreach($cats as $catval){
						$catarray = array();
						$catarray = explode("-",$catval);
						foreach($catarray as $catarrayval){
							$usercats = UserCategories::where(['teacher_id' => $user->id, 'category_id' => $catarrayval])->first();
							if( is_null( $usercats ) ){
								$usercat = new UserCategories;
								$usercat->teacher_id 	= $user->id;
								$usercat->category_id 	= $catarrayval;
								$usercat->save();
								
							}	
						}
					}
										   
					 Mail::send('teacher.varifyemail', ['user' => $teacherDetails], function ($m) use ($teacherDetails) {
						$m->from('hello@app.com', 'Your Application');

						$m->to($teacherDetails['email_id'], $teacherDetails['first_name'])->subject('Your Reminder!');
					}); 
					$response['resCode']    = 0;
					$response['resMsg']     = 'Insegnante registrato con successo. Puoi accedere al tuo account dopo la verifica.';
				}
				else
				{
					// Transaction failed
					//DB::rollback();

					$response['resCode']    = 6;
					$response['resMsg']     = 'Errore del server!';
				}
				
			}
			else
			{
				$response['resCode']    = 2;
				$response['resMsg']     = "L'insegnante con lo stesso ID e-mail esiste già";
			}
			
		}

		return response()->json($response);
	}
}
