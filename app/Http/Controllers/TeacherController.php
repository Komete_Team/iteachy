<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use App\Questionnaire;
use App\StudentQuestionnaire;
use File;
use App\TeacherLeave;
use App\TeacherDocument;
use App\UserCategories;
use App\AvailabilityDays;
use App\Categories; 
use App\TeacherInfo; 
use App\TeacherReviews; 
use App\BookedLectures; 
use App\User;
use App\Role;
use App\Notification;
use App\Permission;
use App\AppointmentBooking;
use App\TeacherAvailability;
use App\TeacherLessons;
use App\UserPaymentInfo;
use Mail;

use Validator;

class TeacherController extends Controller
{
	
	
	public function index($token=null)
    {
		echo Auth::role(); die;
        if(!empty($token))
        {
             $checkvarify = User::where(['remember_token'=>$token,'is_email_verify'=>0])->first();

              if(!empty($checkvarify))
              {
                    $updateisVerify= array(
                                    'is_email_verify'=>1
                                     );
                    User::where(['id' => $checkvarify->id])->update($updateisVerify);

                    return redirect('teacher')->with('success','Il tuo indirizzo email è verificato ora puoi effettuare il login.');

              }else{

                return redirect('teacher')->with('danger','Mancata corrispondenza dei token con noi.');
              }

                  //echo '<pre>';print_r($checkReferralCodeParent);die;

        }

    	if( Auth::check() || Auth::viaRemember() )	// User is already logged-in or remembered
    	{
    		return redirect('teacher/dashboard');
    	}
    	else 										// User is not logged-in, show the login page
    	{
    		return view('login');
    	}
    }
	
	public function testemail()
    {
		$user = array(0=>'123');
//echo '<pre>'; print_r($user); die;
    	Mail::send('teacher.varifyemail', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to('bobbysharma@virtualemployee.com', 'bobby')->subject('Your Reminder!');
        });
    }
	
	public function profile(Request $request)
    {
			$userId = Auth::id();
			$users = User::where(['id' => $userId])->first();
			$categories = Categories::where(['parent_id' => 0])->get();
			$subcategories = Categories::where('parent_id', '!=', 0)->get();
			$subctarray = array();
			$ctarray = array();
			if(!empty($_POST)){
				//echo '<pre>'; print_r($_POST); die;	
				$rules = [
				'email' => 'required|email',             
				'first_name' => 'required',
				'last_name' => 'required',
				'contact_no' => 'required',
				'address' => 'required',
				'city' => 'required',
				'province' => 'required',
				'country' => 'required',
				];

				$message["email.required"] = trans('translation.required');
				$message["first_name.required"] = trans('translation.required');
				$message["last_name.required"] = trans('translation.required');
				$message["contact_no.required"] = trans('translation.required');
				$message["address.required"] = trans('translation.required');
				$message["city.required"] = trans('translation.required');
				$message["province.required"] = trans('translation.required');
				$message["country.required"] = trans('translation.required');
					
				$this->validate($request, $rules,$message);   
				$cats = $request->input('categories');
				//echo '<pre>'; print_r($_POST); die;
				foreach($cats as $catval){
					$catarray = array();
					$catarray = explode("-",$catval);
					if(!in_array($catarray[0], $ctarray)){
						$ctarray[] = $catarray[0];
					}
					if(!in_array($catarray[1], $subctarray)){
						$subctarray[] = $catarray[1];
					}
				}
				
				$catesarray['category'] = $ctarray;
				$catesarray['subcategory'] = $subctarray;
				//echo '<pre>'; print_r($ctarray); die;
				$catjson = json_encode($catesarray);
				//echo '<pre>'; print_r($catjson); die;
				if(!empty($request->file('teacher_logo'))){


                     $image       =         !empty($request->file('teacher_logo'))  ?  $request->file('teacher_logo') : '';
                     $fileext     =         $image->getClientOriginalExtension();

                     // file move to folder//
                     $imagNmae = time().'.'.$image->getClientOriginalExtension();
                     $destinationPath = public_path('/images/teacher_logos');
                     $image->move($destinationPath, $imagNmae);
                 }else{
					$imagNmae = !empty($request->input('old_image'))  ?  $request->input('old_image') : '';
				 }	
				$first_name        =            !empty($request->input('first_name')) ?    $request->input('first_name') :'';
				$last_name     =          !empty($request->input('last_name')) ?     $request->input('last_name'):'';
				$contact_no  =    !empty($request->input('contact_no'))  ?  $request->input('contact_no'):'';
				$address   =         !empty($request->input('address'))    ?  $request->input('address'):'';
				$city        =            !empty($request->input('city')) ?    $request->input('city') :'';
				$province     =          !empty($request->input('province')) ?     $request->input('province'):'';
				$country   =         !empty($request->input('country'))    ?  $request->input('country'):'';
				

				
				
				//echo '<pre>'; print_r(json_encode($catesarray)); die;
				
				$updateUserDetail = array(
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'contact_no'=>$contact_no,
				'address' =>$address,
				'city'=>$city,
				'province'=>$province,
				'country'=>$country,
				'profile_image' => $imagNmae,
				'categories' => $catjson
				);

				User::where(['id'=>$userId])->update($updateUserDetail);


				$request->session()->flash('success', trans('translation.updated'));         
				return redirect('/teacher/profile');		
				
			}
    		return view('teacher.profile',['teacherDetails'=>$users, 'categories' => $categories, 'subcategories'=>$subcategories]);
    	
    }
	
	
	
	
	public function teacherLeave(Request $request)
    {
		$userId = Auth::id();
		$teacherLeaveDetails = TeacherLeave::where(['teacher_id' => $userId, 'status'=>1])->get();
		if(!empty($_POST)){
			$rules = [      
			'dtp_input' => 'required'
			];

			$message["dtp_input.required"] = trans('translation.required');
			$this->validate($request, $rules,$message);
			
			$date        =            !empty($request->input('dtp_input')) ?    $request->input('dtp_input') :'';
			
			$updateLeaveDetail = array(
			'dtp_input'=>$date
			);
			
			$teacherLeave = new TeacherLeave;

			$teacherLeave->teacher_id 	= $userId;
			$teacherLeave->leave_date 	= $date;
			$teacherLeave->status 	= 1;

			if( $teacherLeave->save() )
			{
				$request->session()->flash('success', trans('translation.updated'));         
				return redirect('/teacher/teacher_leave');	
				
			}else{
				$request->session()->flash('danger', trans('translation.not_updated'));         
				return redirect('/teacher/teacher_leave');	
			}
		}
		
    	return view('teacher/teacher_leave', ['teacherLeaveDetails'=>$teacherLeaveDetails]); 
    }
	
	public function teacherAccreditedProfile(Request $request)
    {
		$userId = Auth::id();
		$teacherDocDetails = TeacherDocument::where(['teacher_id' => $userId])->get(); 
    	return view('teacher/teacher_accredited_profile', ['teacherDocDetails'=>$teacherDocDetails, 'user_id'=>$userId]); 
    }
	
	public function teacherAddDoc(Request $request)
    {
		$userId = Auth::id();
		$teacherDocDetails = TeacherDocument::where(['teacher_id' => $userId])->get(); 
		if(!empty($_POST)){
			//echo '<pre>'; print_r($_POST); die;
			$rules = [      
			'file' => 'required',
			'teacher_description' => 'required',
			'doc_name' => 'required',
			];
			$message["doc_name.required"] = trans('translation.required');
			$message["file.required"] = trans('translation.required');
			$message["teacher_description.required"] = trans('translation.required');
			
			$this->validate($request, $rules,$message);
			$doc_name        =            !empty($request->input('doc_name')) ?    $request->input('doc_name') :'';
			$files        =            !empty($request->input('files')) ?    $request->input('files') :'';
			$note        =            !empty($request->input('teacher_description')) ?    $request->input('teacher_description') :'';
			
			foreach($files as $filesval){
				$teacherLeave = new TeacherDocument;
				$teacherLeave->teacher_id 	= $userId;
				$teacherLeave->doc_name  	= $doc_name;
				$teacherLeave->files 	= $filesval;
				$teacherLeave->teacher_note 	= $note;
				$teacherLeave->save();
			}
			
				$request->session()->flash('success', 'Files successfully uploaded!');         
				return redirect('/teacher/teacher_accredited_profile');	
				
			
		}
    	return view('teacher/teacher_add_docs', ['teacherDocDetails'=>$teacherDocDetails, 'user_id'=>$userId]); 
    }
	
	public function uploadDocs(Request $request, $id)
    {
		if(!empty($_POST)){
			
			
			$file        =            !empty($request->input('request')) ?    $request->input('request') :'';
			
			$request = $file;

			// Upload file
			if($request == 1){
					$doc = Input::file('file');
					if( !is_null( $doc ) && ( $doc->getSize() > 0 ) )
					{
					    // Image destination folder
					    $destinationPath = public_path('/uploads/teachers/documents');
					    if( $doc->isValid() )  // If the file is valid or not
					    {
					        $fileExt  = $doc->getClientOriginalExtension();
							$fileType = $doc->getMimeType();
					        $fileSize = $doc->getSize();
							
							if( ( $fileType == 'image/jpeg' || $fileType == 'image/jpg' || $fileType == 'image/png' || $fileType == 'application/pdf' || $fileType == 'application/msword' ) ) // 2 MB
					        {
					            // Rename the file
					            $logoFileName = str_random(20) . '.' . $fileExt;

					            if($doc->move( $destinationPath, $logoFileName )){
									echo $logoFileName;
								 }else{
									 echo 0;
								 }
					    	}else{
								$uploadOk = 0;
							}

					        
					    }
					}
				
				exit;
			}

			// Remove file
			if($request == 2){

				$path = $_POST['path'];
				$return_text = 0;

				// Check file exist or not
				if (file_exists(public_path().'/uploads/teachers/documents/'.$path))
				{
				   // Remove file
				   File::delete(public_path().'/uploads/teachers/documents/'.$path);
				 
				   // Set status
				   $return_text = 1;
				}else{
				   // Set status
				   $return_text = 0;
				}

				// Return status
				echo $return_text;
				exit;
			}
		}
	}
	
    /**
     * Function to delete teacher document
     * @param void
     * @return array
     */
    public function deleteDoc()
    {
    	$docid = Input::get('docid');

    	$docid = TeacherDocument::find($docid);

    	if( $docid->delete() )
    	{
    		$response['resCode']    = 0;
    		$response['resMsg']     = trans('translation.doc_deleted');
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	/**
     * Function to delete teacher document
     * @param void
     * @return array
     */
    public function deleteAppointment()
    {
    	$appt_id = Input::get('appt_id');

    	$appt_id = AppointmentBooking::find($appt_id);

    	if( $appt_id->delete() )
    	{
    		$response['resCode']    = 0;
    		$response['resMsg']     = trans('translation.appointment_deleted');
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	
	
	
	
	public function getTeacherLeaveDetail(Request $request)
    {

        $teacherleaveid = Input::get('teacherleaveid');

    	$teacherLeaveDetails = TeacherLeave::find($teacherleaveid);

    	return response()->json($teacherLeaveDetails);
    }
	
	public function saveTeacherLeaveDetail(Request $request)
    {
		// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $teacherLeaveDetails);
        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'leave_date'	=> $teacherLeaveDetails['leave_date']
		    ),
		    array(
		        'leave_date'	=> array('required')
		    ),
		    array(
		        'leave_date.required' 	=> 'Please enter leave date'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			try
			{
				$TeacherLeave = TeacherLeave::where(['id' => $teacherLeaveDetails['leave_id']])->first();

		        $TeacherLeave->leave_date 	= $teacherLeaveDetails['leave_date'];

		        if( $TeacherLeave->update() )
		        {
		        	$response['resCode']    = 0;
		        	$response['resMsg']     = trans('translation.updated');
		        }
		        else
		        {
		        	$response['resCode']    = 2;
		        	$response['resMsg']     = trans('translation.error');
		        }
			}
			catch(\Exception $e)
			{
				$response['resCode']    = 7;
				$response['resMsg']     = trans('translation.error');
			}
		}
			

		return response()->json($response); 
    }
	
	public function cancelLeave()
    {
    	$leaveid = Input::get('leaveid');

    	$leave = TeacherLeave::find($leaveid);

		$TeacherLeave = TeacherLeave::where(['id' => $leaveid])->first();

		$TeacherLeave->status 	= 0;

		if( $TeacherLeave->update() )
		{
    		$response['resCode']    = 0;
    		$response['resMsg']     = 'Leave has been canceled successfully';
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	
	public function teacherInfo(Request $request)
    {
		$userId = Auth::id();
		$teacherDetails = TeacherInfo::where(['teacher_id' => $userId])->first();
		if(!empty($_POST)){
			$rules = [      
				'title' => 'required',
				'description' => 'required',
				'video' => 'required'
				
				];

				$message["title.required"] = trans('translation.required');
				$message["description.required"] = trans('translation.required');
				//$message["price.required"] = trans('translation.required');
				//$message["price_per_hour.required"] = trans('translation.required');
				
				$message["video.required"] = trans('translation.required');

				$this->validate($request, $rules,$message);
				
				$title        =            !empty($request->input('title')) ?    $request->input('title') :'';
				$description     =          !empty($request->input('description')) ?     $request->input('description'):'';
				//$price  =    !empty($request->input('price'))  ?  $request->input('price'):'';
				//$price_per_hour  =    !empty($request->input('price_per_hour'))  ?  $request->input('price_per_hour'):'';
				
				if(!empty($request->file('video'))){
                     $video       =         !empty($request->file('video'))  ?  $request->file('video') : '';
                     $fileext     =         $video->getClientOriginalExtension();

                     // file move to folder//
                     $videoNmae = time().'.'.$video->getClientOriginalExtension();
                     $destinationPath = public_path('/videos/teacher_videos');
                     $video->move($destinationPath, $videoNmae);
                 }else{
					$videoNmae = !empty($request->input('old_video'))  ?  $request->input('old_video') : '';
				 }

				$updateUserDetail = array(
				'title'=>$title,
				'description'=>$description,
				'file_name'=>$videoNmae
				);

				$teacherinfo = TeacherInfo::where(['teacher_id'=>$userId])->first();
				if(empty($teacherinfo)){
					$teacher = new TeacherInfo;

					$teacher->teacher_id 	= $userId;
					$teacher->title 	= $title;
					$teacher->description 	= $description;
					//$teacher->price = $price;
					//$teacher->price_per_hour = $price_per_hour;

					if( $teacher->save() )
					{
						$request->session()->flash('success', trans('translation.updated'));         
						return redirect('/teacher/teacher_info');	
						
					}
				}else{
					TeacherInfo::where(['teacher_id'=>$userId])->update($updateUserDetail);
				}


				$request->session()->flash('success', trans('translation.updated'));         
				return redirect('/teacher/teacher_info');		
		}
		
    	return view('teacher/teacher_info',['teacherDetails'=>$teacherDetails]); 
    }
	
	
	
	/**
     * Function to return register page
     * @param void
     * @return \Illuminate\Http\Response
     */
	 
	public function register()
	{
		$categories = Categories::where('parent_id', '==', 0)->get();
		$subcategories = Categories::where('parent_id', '!=', 0)->get();
		$catarr = array();
		/* foreach($categories as $catid){
			//echo $catid->id; die;
			$subcategories = array();
			$subcategories = Categories::where(['parent_id' => $catid->id])->get();
				foreach($subcategories as $subcat){
				//echo '<pre>'; print_r($subcategories); die;
				if(!empty($subcat->id)){
					
					$catarr[$catid->id][] = array('name'=>$subcat->category_name,'id'=>$subcat->id);
				}
			}
		} */
		//echo '<pre>'; print_r($catarr); die;
		return view('register',['categories' => $categories, 'subcategories' => $subcategories]);
	}

	 
    public function registerTeacher()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $teacherDetails);
		// echo '<pre>'; print_r($teacherDetails); die;
        $response =array(); 
        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'first_name'	=> $teacherDetails['first_name'],
		        'last_name'		=> $teacherDetails['last_name'],
		        'email_id'		=> $teacherDetails['email_id'],
		        'password' 		=> $teacherDetails['password'],
				'categories'		=> $teacherDetails['categories'],
				
		    ),
		    array(
		        'first_name'	=> array('required'),
		        'last_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required'),
				'categories' 		=> array('required')
		    ),
		    array(
		        'first_name.required' 	=> 'Si prega di inserire il nome',
		        'last_name.required' 	=> 'Per favore inserisci il cognome',
		        'email_id.required'		=> 'Si prega di inserire l ID e-mail',
		        'email_id.email'		=> 'Si prega di inserire un ID e-mail valido',
		        'password.required'		=> 'Per favore, inserisci la password',
				'categories.required' 	=> 'si prega di selezionare la categoria',
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$user = User::where(['email' => $teacherDetails['email_id']])->first();
			if( is_null( $user ) )	// No user with the same email id exist, save the data
			{

				$user = new User;

				$user->first_name 	= $teacherDetails['first_name'];
				$user->last_name 	= $teacherDetails['last_name'];
				$user->email 	= $teacherDetails['email_id'];
				$user->password = Hash::make($teacherDetails['password']);
				$user->role = 2;
				$user->remember_token = $teacherDetails['_token'];
				$user->is_online  = $teacherDetails['online'];

				if( $user->save() )
				{
					$cats = $teacherDetails['categories'];
					//echo '<pre>'; print_r($_POST); die;
					foreach($cats as $catval){
						$catarray = array();
						$catarray = explode("-",$catval);
						foreach($catarray as $catarrayval){
							$usercats = UserCategories::where(['teacher_id' => $user->id, 'category_id' => $catarrayval])->first();
							if( is_null( $usercats ) ){
								$usercat = new UserCategories;
								$usercat->teacher_id 	= $user->id;
								$usercat->category_id 	= $catarrayval;
								$usercat->save();
								
							}	
						}
					}
										   
					 Mail::send('teacher.varifyemail', ['user' => $teacherDetails], function ($m) use ($teacherDetails) {
						$m->from('hello@app.com', 'Your Application');

						$m->to($teacherDetails['email_id'], $teacherDetails['first_name'])->subject('Your Reminder!');
					}); 
					$response['resCode']    = 0;
					$response['resMsg']     = 'Insegnante registrato con successo. Puoi accedere al tuo account dopo la verifica.';
				}
				else
				{
					// Transaction failed
					//DB::rollback();

					$response['resCode']    = 6;
					$response['resMsg']     = 'Errore del server!';
				}
				
			}
			else
			{
				$response['resCode']    = 2;
				$response['resMsg']     = "L'insegnante con lo stesso ID e-mail esiste già";
			}
			
		}

		return response()->json($response);
	}

    /**
     * Function to save role details
     * @param void
     * @return array
     */
    public function registerSchool()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $schoolDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'school_name'	=> $schoolDetails['school_name'],
		        'full_name'		=> $schoolDetails['full_name'],
		        'email_id'		=> $schoolDetails['email_id'],
		        'password' 		=> $schoolDetails['password']
		    ),
		    array(
		        'school_name'	=> array('required'),
		        'full_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required')
		    ),
		    array(
		        'school_name.required' 	=> 'Please enter school name',
		        'full_name.required' 	=> 'Please enter school name',
		        'email_id.required'		=> 'Please enter email id',
		        'email_id.email'		=> 'Please enter a valid email id',
		        'password.required'		=> 'Please enter password'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			// Check if the school with the same name already exist
			$school = School::where(['school_name' => $schoolDetails['school_name']])->first();

			if( is_null( $school ) )	// No school with the same name exist, save the data
			{
				// Check if an user exist with the same email id
				$user = User::where(['email' => $schoolDetails['email_id']])->first();

				if( is_null( $user ) )	// No user with the same email id exist, save the data
				{
					try
					{
						// Begin transaction
					    DB::beginTransaction();

						$user = new User;

						$user->name 	= $schoolDetails['full_name'];
						$user->email 	= $schoolDetails['email_id'];
						$user->password = Hash::make($schoolDetails['password']);

						if( $user->save() )
						{
							// Attach the role "ali_user" to this newly created user
							$userRole = Role::where(['name' => 'ali_user'])->first();
							if( $user->attachRole($userRole) )
							{
								$school = new School;

								$school->user_id 	= $user->id;
								$school->school_name= $schoolDetails['school_name'];
								$school->status 	= '1';
								$school->verified 	= '0';
								$school->created_at = date('Y-m-d H:i:s');

								if( $school->save() )
								{
									// Transaction successful
							    	DB::commit();

							    	$response['resCode']    = 0;
							    	$response['resMsg']     = 'School registered successfully. You can access your account after verification.';
								}
								else
								{
									// Transaction failed
								    DB::rollback();

								    $response['resCode']    = 4;
								    $response['resMsg']     = trans('translation.error');
								}
							}
							else
							{
								// Transaction failed
							    DB::rollback();

							    $response['resCode']    = 5;
							    $response['resMsg']     = trans('translation.error');
							}
						}
						else
						{
							// Transaction failed
						    DB::rollback();

						    $response['resCode']    = 6;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						// Transaction failed
					    DB::rollback();

					    $response['resCode']    = 7;
					    $response['resMsg']     = trans('translation.error');
					}
				}
				else
				{
					$response['resCode']    = 2;
					$response['resMsg']     = 'User with the same email id already exist';
				}
			}
			else
			{
				$response['resCode']    = 3;
				$response['resMsg']     = 'School with the same name already exist';
			}
		}

		return response()->json($response);
	}

	/**
     * Function to return login page
     * @param void
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
		$userId = Auth::id();
		$teacherAvailable= TeacherAvailability::where(['teacher_id' => $userId])->get();
		$teacherreviews = DB::table('users')->join('teacher_reviews', 'users.id', '=', 'teacher_reviews.user_id')->select('users.*', 'teacher_reviews.title', 'teacher_reviews.rate', 'teacher_reviews.review_message')->where('teacher_reviews.teacher_id', $userId)->orderBy('teacher_reviews.rate', 'DESC')->first();
		
		$totalrecords = DB::table('appointement_booking')->where(['teacher_id'=>$userId,'is_cancel'=>1, 'is_booked'=>0])->select('id', DB::raw('DATE(updated_at) as date_only'),DB::raw('COUNT(is_cancel) as cancel_record'))
		->groupBy('date_only')
		->get();
		//echo '<pre>'; print_r($totalrecords); die;
    	return view('teacher/dashboard',['teacherAvailable'=>$teacherAvailable, 'teacherreviews'=>$teacherreviews, 'totalrecords'=>$totalrecords]);
    }

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }
	
	public function teacherAvailability(Request $request)
    {
		$userId = Auth::id();
		$teacherDetails = TeacherInfo::where(['teacher_id' => $userId])->first();
		$availability = AvailabilityDays::get();
		$teacherAvailabilities = TeacherAvailability::get();
		if(!empty($_POST)){
				$days        =            !empty($request->input('days')) ?    $request->input('days') :'';
				$start_time     =          !empty($request->input('start_time')) ?     $request->input('start_time'):'';
				$end_time  =    !empty($request->input('end_time'))  ?  $request->input('end_time'):'';
			if(!empty($days)){	
				foreach($days as $days_val){
					$updateavailability = array();
					$updateavailability = array(
					'status'=>1,
					'num_days'=>$days_val,
					'start_time'=>$start_time[$days_val-1],
					'end_time'=>$end_time[$days_val-1]
					);
					$teacherAvail = TeacherAvailability::where(['teacher_id' => $userId,'num_days' => $days_val])->get();
					//echo '<pre>'; print_r(count($teacherAvail)); die;
					if(count($teacherAvail) > 0){
						TeacherAvailability::where(['teacher_id' => $userId,'num_days' => $days_val])->update($updateavailability);
						$updateavailability1 = array(
						'status'=>0
						);
						TeacherAvailability::whereNotIn('num_days',$days)->where(['teacher_id'=>$userId])->update($updateavailability1);
					}else{
						$saveTeacherAvail = new TeacherAvailability;

						$saveTeacherAvail->teacher_id 	= $userId;
						$saveTeacherAvail->num_days= $days_val;
						$saveTeacherAvail->status 	= 1;
						$saveTeacherAvail->start_time  	= $start_time[$days_val-1];
						$saveTeacherAvail->end_time= $end_time[$days_val-1];

						if($saveTeacherAvail->save()){
							/* $saveAppointment = new AppointmentBooking;
							$saveAppointment->teacher_id = $userId;
							$saveAppointment->appointment_color = '#62c208';
							$saveAppointment->save(); */
						}
					}
				}
			}else{
				$updateavailability = array(
					'status'=>0
				);
				TeacherAvailability::query()->where(['teacher_id'=>$userId])->update($updateavailability);
			}
			$request->session()->flash('success', trans('translation.updated'));         
			return redirect('/teacher/teacher_availability');		
		}
		//echo '<pre>'; print_r($_POST); die;
		
    	return view('teacher/teacher_availability',['teacherDetails'=>$teacherDetails, 'availability'=>$availability, 'teacherAvailabilities'=>$teacherAvailabilities]); 
    }
	
    public function teacherAppointment()
    {
		$tid = Auth::id();
        $dataAvailable=[];
		//$teacherLesson= TeacherLessons::where(['id' => $lid])->first();
		
		$lecturetime = 30;
		
		
        $teacherBooked= AppointmentBooking::where(['teacher_id' => $tid, 'is_booked'=>1])->get();
		
		$teacherCancel= AppointmentBooking::where(['teacher_id' => $tid, 'is_cancel'=>1])->get();
		$teacherAvailable= TeacherAvailability::where(['teacher_id' => $tid])->get();
		$teacherLeave= TeacherLeave::where(['teacher_id' => $tid,'status' => 1])->get();
		//$userTempDetail= TempAppointmentBooking::where(['teacher_id' => $tid, 'user_id' => $userId])->get();
        $dataBooked =[];
		$dataTempSelected = [];
		$dataLeave = [];
		$allbookedid = [];
		$allcanceledid = [];
		
		//echo '<pre>'; print_r($teacherAvailable); die;
        
		$start = $_GET['start'];
		$end = $_GET['end'];
		
		$alldatesInWeekArray = array(); 

		$Variable1 = strtotime($start); 
		$Variable2 = strtotime($end); 
		$temparr = array();
		// Use for loop to store dates into array 
		// 86400 sec = 24 hrs = 60*60*24 = 1 day 
		for ($currentDate = $Variable1; $currentDate <= $Variable2;  
				$currentDate += (86400)) { 
					  
		$Store = date('Y-m-d', $currentDate); 
		$alldatesInWeekArray[] = $Store; 
		} 

		
		foreach($teacherCancel as $row)
        {
			$allcanceledid[] = date('Y-m-d H:i',strtotime($row->start_date));
            $dataBooked[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d H:i',strtotime($row->start_date)),
                              'end'   => date('Y-m-d H:i',strtotime($row->end_date)),
							  'className'=> 'calendar-cell-canceled'
                         );
        }
		
		foreach($teacherBooked as $row)
        {
			$allbookedid[] = date('Y-m-d H:i',strtotime($row->start_date));
			if(in_array(date('Y-m-d H:i',strtotime($row->start_date)), $allcanceledid)){
			}else{
				if(!in_array($row->start_date, $temparr)){
					$temparr[] = $row->start_date;
					$dataBooked[] = array(
								  'id'   => $row->id,
								  'start'   => date('Y-m-d H:i',strtotime($row->start_date)),
								  'end'   => date('Y-m-d H:i',strtotime($row->end_date)),
								  'className'=> 'calendar-cell-booked'
							 );
				}
			}
        }
		//$dataBooked = array_unique($dataBooked);
		/* foreach($userTempDetail as $row)
		{
			$dataTempSelected[] = array(
			'id'   => $row->id,
			'start'   => date('Y-m-d H:i',strtotime($row->start_date.' '.$row->starteTime )),
			'end'   => date('Y-m-d H:i',strtotime($row->end_date.' '.$row->endtime)),
			'className'=> ['calendar-cell-tempselected', 'calendar-cell-tempselected'.$row->id]
			);
		}  */
		//echo '<pre>'; print_r($teacherAvailable); die;
		 foreach($teacherAvailable as $k => $row)
        {
			//$time1 = new DateTime($row->start_time);
			//$time2 = new DateTime($row->end_time);
			//$interval = $time1->diff($time2);

			$from_time1 = $row->start_time;
			$to_time = strtotime($row->end_time);
			$from_time = strtotime($row->start_time);
			$timediff = round(abs($to_time - $from_time) / 60,2);
			$totaltimediff = $timediff/$lecturetime;
			$timearr = array();
			for($i=0; $i<$totaltimediff; $i++){
				if($i>0){
					$from_time1 = date('H:i', strtotime("+$lecturetime minutes", strtotime($from_time1)));
				}
				$timearr[$i]['start_time'] = $from_time1;
				
				$timearr[$i]['end_time'] = date('H:i', strtotime("+$lecturetime minutes", strtotime($from_time1)));
				
			}
			
			foreach($timearr as $timearrval){
			//echo '<pre>'; print_r($timearr); die;
			//echo $row->end_time->diff($row->start_time)->format('%H:%I:%S'); die;
			//echo $interval->format('%s second(s)'); die;
			foreach($alldatesInWeekArray as $dateval){
				
				$day = date('D',strtotime($dateval));
				 //echo $row->num_days.'=='.$day; die;
				$daterange = '';
				$startdaterange = '';
				$enddaterange = '';
				if($row->num_days == 7 && $day == 'Sun'){
					
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 1 && $day == 'Mon'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 2 && $day == 'Tue'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 3 && $day == 'Wed'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 4 && $day == 'Thu'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 5 && $day == 'Fri'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 6 && $day == 'Sat'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				
				
				
			if(in_array(date('Y-m-d H:i',strtotime($startdaterange)), $allbookedid)){
			}else{
			
            $dataAvailable[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d H:i',strtotime($startdaterange)),
                              'end'   => date('Y-m-d H:i',strtotime($enddaterange)),
							  'className'=> ['calendar-cell-available', 'avail_selected'.$row->id]
                         );
			}
			}
		}
			
        }   
		//echo '<pre>'; print_r($allbookedid); die;
		
		foreach($teacherLeave as $row)
        {
            $dataLeave[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d 00:00',strtotime($row->leave_date)),
                              'end'   => date('Y-m-d 24:00',strtotime($row->leave_date)),
							  'className'=> 'calendar-cell-leave'
                         );
        } 
		//echo '<pre>'; print_r($dataTempSelected); die;
		$data = array_merge($dataBooked, $dataAvailable, $dataLeave);
        echo  json_encode($data);   
    }
	
	public function Notification(Request $request,$id)
	{
		$notifications= Notification::where(['notifications.id'=>$id])->leftJoin('users', 'users.id', '=', 'notifications.created_by')->select('users.first_name','users.last_name','users.email','users.profile_image','users.role', 'notifications.*')->get();
		if(!empty($notifications)){
		$updateIsRead= array(
					'is_read'=>'1'
				   );
		Notification::where(['id' => $id])->update($updateIsRead);
		}     
		return view('teacher/notification-view',['notifications'=>$notifications]);
	}
	
	public function AllNotification(Request $request) 
	{
		$userId = Auth::id();
		$notifications= Notification::where(['notifications.user_id'=>$userId])->leftJoin('users', 'users.id', '=', 'notifications.created_by')->select('users.first_name','users.last_name','users.email','users.profile_image','users.role', 'notifications.*')->get();
		//echo '<pre>'; print_r($notifications); die;
		if(!empty($notifications)){
		$updateIsRead= array(
					'is_read'=>'1'
				   );
		foreach($notifications as $val){	
			$notid = $val->id;		
			Notification::where(['id' => $notid])->update($updateIsRead);
		}
		}     
		return view('teacher/notification-view',['notifications'=>$notifications]);
	}
	
	public function teacherLesson(Request $request)
	{
		$userId = Auth::id();
		$teacherlessons= TeacherLessons::where(['teacher_id' => $userId])->get();
		return view('teacher/teacher_lessons',['lessons'=>$teacherlessons]);
	}
	
	public function saveLessons()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $lessonDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'lesson_name' 		=> $lessonDetails['lesson_name'],
		        'total_lessons' 	=> $lessonDetails['total_lessons'],
				'lesson_time' 	=> $lessonDetails['lesson_time'],
				'price_per_hour' => $lessonDetails['price_per_hour'],
				'lecture_type' => $lessonDetails['lecture_type'],
		    ),
		    array(
		        'lesson_name' 		=> array('required'),
		        'total_lessons' 	=> array('required'),
				'lesson_time' 	=> array('required'),
				'price_per_hour' 	=> array('required'),
				'lecture_type' 	=> array('required'),
		    ),
		    array(
		        'lesson_name.required'			=> 'Inserisci il nome della lezione',
		        'total_lessons.required'		=> 'Inserisci il numero della lezione',
				'lesson_time.required'		=> "Inserisci l'ora in multipli di 30",
				'price_per_hour.required'		=> 'Inserisci il prezzo della lezione',
				'lecture_type.required'			=> 'Seleziona il tipo di lezione'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();

			if( $lessonDetails['lesson_id'] == '' )	// Event id is not available, save the data
			{
				$teacherLessons = new TeacherLessons();

				$teacherLessons->lesson_name 		= $lessonDetails['lesson_name'];
				$teacherLessons->total_lessons 		= $lessonDetails['total_lessons'];
				$teacherLessons->lesson_time 		= $lessonDetails['lesson_time'];
				$teacherLessons->price_per_hour 		= $lessonDetails['price_per_hour'];
				$teacherLessons->type 		= $lessonDetails['lecture_type'];
				
				$teacherLessons->teacher_id 		= $loggedInUserId;
				

				try
				{
					if( $teacherLessons->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Lezione salvata con successo';
						//$response['calendarEvents'] = $teacherLessons;
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
			else
			{
				$teacherLessons = TeacherLessons::where(['id' => $lessonDetails['lesson_id']])->first();

				$teacherLessons->lesson_name 		= $lessonDetails['lesson_name'];
				$teacherLessons->total_lessons 		= $lessonDetails['total_lessons'];
				$teacherLessons->lesson_time 		= $lessonDetails['lesson_time'];
				$teacherLessons->price_per_hour 		= $lessonDetails['price_per_hour'];
				$teacherLessons->type 		= $lessonDetails['lecture_type'];
				
				
				

				try
				{
					if( $teacherLessons->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Dettagli della lezione aggiornati correttamente';
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
		}

		return response()->json($response);
	}
	
	public function getLessonDetails()
    {
    	$lessonid = Input::get('lessonid');

    	$lessondetail = TeacherLessons::where(['id' => $lessonid])->first();
    	return response()->json($lessondetail);
    }
	
	public function deleteLesson()
    {
    	$lesson_id = Input::get('lesson_id');

    	$lessondetails = TeacherLessons::find($lesson_id);

    	try
    	{
    		if( $lessondetails->delete() )
    		{
    			$response['resCode']    	= 0;
    			$response['resMsg']     	= 'Lezione cancellata con successo';
    		}
    		else
    		{
    			$response['resCode']    = 1;
    		    $response['resMsg']     = trans('translation.error');
    		}
    	}
    	catch(\Exception $e)
    	{
    		$response['resCode']    = 2;
    	    $response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	public function saveTeacherReview()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $reviewDetails);
        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'headline' 		=> $reviewDetails['headline'],
		        'review' 	=> $reviewDetails['review']
		    ),
		    array(
		        'headline' 		=> array('required'),
		        'review' 	=> array('required')
		    ),
		    array(
		        'headline.required'			=> 'Please enter title for review',
		        'review.required'		=> 'Please write message for review'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();
			$teacherreviews = new TeacherReviews();

			$teacherreviews->title 		= $reviewDetails['headline'];
			$teacherreviews->review_message = $reviewDetails['review'];
			$teacherreviews->user_id 		= $loggedInUserId;
			$teacherreviews->teacher_id 		= $reviewDetails['teacher_id'];
			$teacherreviews->rate 		= $reviewDetails['rate'];
			if( $teacherreviews->save() )
			{
				$response['resCode']    	= 0;
				$response['resMsg']     	= 'Review saved successfully';
				//$response['calendarEvents'] = $teacherLessons;
			}
			else
			{
				$response['resCode']    = 1;
				$response['resMsg']     = 'Server error!';
			}
		}

		return response()->json($response);
	}
	
	public function settings(Request $request)
	{
		//echo '<pre>'; print_r($_POST); die;
		$userId = Auth::id();
		if(!empty($_POST)){
			//echo '<pre>'; print_r($_POST); die;	
			$rules = [
			'pwd' => 'required',             
			'pwd2' => 'required'
			];

			$message["pwd.required"] = trans('translation.required');
			$message["pwd2.required"] = trans('translation.required');
				
			$this->validate($request, $rules,$message);   
				
			$pwd        =            !empty($request->input('pwd')) ?    $request->input('pwd') :'';
			$pwd2     =          !empty($request->input('pwd2')) ?     $request->input('pwd2'):'';
			if($pwd != $pwd2){
				$request->session()->flash('danger', trans('translation.password_not_match'));
				return redirect('/teacher/settings');
			}
			$userdetail = User::where(['id' => $userId])->first();
			//echo '<pre>'; print_r($userdetail->password); die;
			
			
			//echo '<pre>'; print_r(json_encode($catesarray)); die;
			if(Hash::check($pwd, $userdetail->password) == 1){
				$updateUserDetail = array(
				'status'=>0
				);
				User::where(['id'=>$userId])->update($updateUserDetail);
				Auth::logout();
				$request->session()->flash('success', trans('translation.account_deleted')); 
			}else{
				$request->session()->flash('success', trans('translation.account_not_deleted')); 
			}				
			return redirect('/');		
			
		}
		return view('teacher/teacher_setting');
	}
	
	
	public function getNotDetails()
    {
    	$notId = Input::get('notId');

    	$notDetails = BookedLectures::find($notId);

    	return response()->json($notDetails);
    }
	
	public function getAppointments(Request $request)
	{
		$userId = Auth::id();
		$teacherappointments= AppointmentBooking::where(['appointement_booking.teacher_id' => $userId])->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')->leftJoin('users', 'users.id', '=', 'appointement_booking.student_id')->select('appointement_booking.id as aptid', 'users.first_name','users.last_name','users.email','users.profile_image','users.role', 'appointement_booking.start_date', 'appointement_booking.end_date', 'booked_lectures.skype_id', 'booked_lectures.meeting_place', 'booked_lectures.knowledge_level', 'booked_lectures.message')->get();
		
		
		
		return view('teacher/teacher_appointments',['teacherappointments'=>$teacherappointments]);
	}
	
	
	
	public function getAppointment()
    {
    	$appointmentid = Input::get('appointmentid');

    	$teacherappointments= AppointmentBooking::where(['appointement_booking.id' => $appointmentid])->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')->leftJoin('users', 'users.id', '=', 'appointement_booking.student_id')->select('appointement_booking.id as aptid', 'users.first_name','users.last_name','users.email','users.profile_image','users.role', 'appointement_booking.start_date', 'appointement_booking.end_date', 'booked_lectures.skype_id', 'booked_lectures.meeting_place', 'booked_lectures.knowledge_level', 'booked_lectures.message')->first();
    	return response()->json($teacherappointments);
    }
	
	
	public function saveTeacherAppointment()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $appointmentDetails);
        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'appointment_start_date' 		=> $appointmentDetails['appointment_start_date'],
		        'appointment_end_date' 	=> $appointmentDetails['appointment_end_date'],
				'skype_id' 		=> $appointmentDetails['skype_id'],
		        'meeting_place' 	=> $appointmentDetails['meeting_place'],
				'knowledge_level' 		=> $appointmentDetails['knowledge_level']
		    ),
		    array(
		        'appointment_start_date' 		=> array('required'),
		        'appointment_end_date' 	=> array('required'),
				'skype_id' 		=> array('required'),
		        'meeting_place' 	=> array('required'),
				'knowledge_level' 		=> array('required')
		    ),
		    array(
		        'appointment_start_date.required'			=> 'Please enter appointment start date',
		        'appointment_end_date.required'		=> 'Please enter appointment end date',
				'skype_id.required'			=> 'Please enter skype id',
		        'meeting_place.required'		=> 'Please enter place for meeting',
				'knowledge_level.required'			=> 'Please enter knowledge level',
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();
			$AppointmentBooking = AppointmentBooking::where(['id'=>$appointmentDetails['appointment_id']])->first();
			$AppointmentBooking->starteTime = date('H:i', strtotime($appointmentDetails['appointment_start_date']));
			$AppointmentBooking->endtime = date('H:i', strtotime($appointmentDetails['appointment_start_date']));
			$AppointmentBooking->start_date 		= $appointmentDetails['appointment_start_date'];
			$AppointmentBooking->end_date = $appointmentDetails['appointment_end_date'];
			
			
			if( $AppointmentBooking->save() )
			{
				$BookedLectures = BookedLectures::where(['appointment_id'=>$appointmentDetails['appointment_id']])->first();
				$BookedLectures->skype_id 		= $appointmentDetails['skype_id'];
				$BookedLectures->meeting_place 		= $appointmentDetails['meeting_place'];
				$BookedLectures->knowledge_level 		= $appointmentDetails['knowledge_level'];
				$BookedLectures->message 		= $appointmentDetails['message'];
				$BookedLectures->save();
				$response['resCode']    	= 0;
				$response['resMsg']     	= 'Appointment saved successfully';
				//$response['calendarEvents'] = $teacherLessons;
			}
			else
			{
				$response['resCode']    = 1;
				$response['resMsg']     = 'Server error!';
			}
		}

		return response()->json($response);
	}
	
	public function paymentinfo(Request $request) {
		$user = auth()->user();
		if($request->method() == 'POST') {
            $userPayInfo = UserPaymentInfo::firstOrNew(array('user_id' => $user->id));
			$userPayInfo->payment_type  =  Input::get('type_of_payment');
			if(!empty(Input::get('card'))) {
				$userPayInfo->pay_info = json_encode(Input::get('card'));
			}
			if($userPayInfo->save()){
				$request->session()->flash('success', 'Le informazioni di pagamento sono state salvate correttamente.'); 
			}
			return redirect('student/paymentinfo');
		}
		$payinfo = UserPaymentInfo::where(['user_id'=>$user->id])->first();
		return view('teacher/payment_info',['payinfo'=>$payinfo]);
	}
	
	
	
	
	
	
	
	
	
	
	
}