<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Questionnaire;
use App\StudentQuestionnaire;


class QuestionnaireController extends Controller
{
    public function index(Request $request)
    {
        $questionnaires = Questionnaire::all();
        return view('admin.questionnaire',['questionnaires'=>$questionnaires]);
    }
	
	public function getAnswers(Request $request)
    {
        $questionnaires = Questionnaire::all();
		$teachers = DB::table('student_questionnaires_answer')->leftJoin('appointement_booking', 'student_questionnaires_answer.appointment_id', '=', 'appointement_booking.id')->leftJoin('users', 'users.id', '=', 'appointement_booking.student_id')->select('users.first_name', 'users.profile_image', 'users.last_name', 'users.email', 'users.contact_no', 'student_questionnaires_answer.*')->paginate(15);
		//echo '<pre>'; print_r($teachers); die;
        return view('admin.questionnaire_reply',['questionnaires'=>$questionnaires, 'teachers'=>$teachers]);
    }

    public function questionnaire(Request $request,$id) {
        $questionnaires = Questionnaire::all();
        if($request->method() == 'POST') {
           $studentQuestionnaire = new StudentQuestionnaire();
           $studentQuestionnaire->appointment_id  =  $id;
           $studentQuestionnaire->answer  =  json_encode(Input::get('answer'));

           if($studentQuestionnaire->save()) {
                return redirect('/student/questionnaire/'.$id)->with('success',"Le tue risposte sono state inviate correttamente."); 
           }
        }
        return view('student.questionnaire',['questionnaires'=>$questionnaires]);
    }

    public function add(Request $request) {
        if($request->method() == 'POST') {
            $questionnaire = new Questionnaire();
            $questionnaire->question  =  Input::get('question');
            $questionnaire->answer   =  json_encode(Input::get('answer'));
            if($questionnaire->save()){
                return redirect('/admin/questionnaires')->with('success',"Il piano di abbonamento è stato aggiunto correttamenteLa domanda è stata aggiunta correttamente."); 
            } else{
                return redirect('/admin/questionnaires')->with('error',"Si è verificato un errore. Per favore riprova più tardi."); 
            }
        }
        return view('admin.addquestionnaire');
    }

    public function edit(Request $request,$qid) {
        $questionnaireData = Questionnaire::where(['id'=>$qid])->first();
        if($request->method() == 'POST') {
            $questionnaire = Questionnaire::find($qid);
            $questionnaire->question  =  Input::get('question');
            $questionnaire->answer   =  json_encode(Input::get('answer'));
            if($questionnaire->save()){
                return redirect('/admin/questionnaires')->with('success',"La domanda è stata aggiornata correttamente."); 
            } else{
                return redirect('/admin/questionnaires')->with('error',"Si è verificato un errore. Per favore riprova più tardi."); 
            }
        }
        return view('admin.editquestionnaire', ['questionnaireData'=>$questionnaireData]);
    }

    public function delete(Request $request,$qid) {
        if(!empty($qid)) {
            DB::table('questionnaires')->where('id', '=', $qid)->delete();
            $request->session()->flash('success', "Eliminato con successo.");
        }
        echo 'success';exit;
    }
}