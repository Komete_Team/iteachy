<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use App\User;
use App\Role;
use App\Permission;

use App\School;
use App\SchoolDocument;
use App\DocumentType;

use Validator;

class SchoolController extends Controller
{
	/**
     * Function to return register page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
    	return view('school/register');
    }

    /**
     * Function to save role details
     * @param void
     * @return array
     */
    public function registerSchool()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $schoolDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'school_name'	=> $schoolDetails['school_name'],
		        'full_name'		=> $schoolDetails['full_name'],
		        'email_id'		=> $schoolDetails['email_id'],
		        'password' 		=> $schoolDetails['password']
		    ),
		    array(
		        'school_name'	=> array('required'),
		        'full_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required')
		    ),
		    array(
		        'school_name.required' 	=> 'Please enter school name',
		        'full_name.required' 	=> 'Please enter school name',
		        'email_id.required'		=> 'Please enter email id',
		        'email_id.email'		=> 'Please enter a valid email id',
		        'password.required'		=> 'Please enter password'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			// Check if the school with the same name already exist
			$school = School::where(['school_name' => $schoolDetails['school_name']])->first();

			if( is_null( $school ) )	// No school with the same name exist, save the data
			{
				// Check if an user exist with the same email id
				$user = User::where(['email' => $schoolDetails['email_id']])->first();

				if( is_null( $user ) )	// No user with the same email id exist, save the data
				{
					try
					{
						// Begin transaction
					    DB::beginTransaction();

						$user = new User;

						$user->name 	= $schoolDetails['full_name'];
						$user->email 	= $schoolDetails['email_id'];
						$user->password = Hash::make($schoolDetails['password']);

						if( $user->save() )
						{
							// Attach the role "ali_user" to this newly created user
							$userRole = Role::where(['name' => 'ali_user'])->first();
							if( $user->attachRole($userRole) )
							{
								$school = new School;

								$school->user_id 	= $user->id;
								$school->school_name= $schoolDetails['school_name'];
								$school->status 	= '1';
								$school->verified 	= '0';
								$school->created_at = date('Y-m-d H:i:s');

								if( $school->save() )
								{
									// Transaction successful
							    	DB::commit();

							    	$response['resCode']    = 0;
							    	$response['resMsg']     = 'School registered successfully. You can access your account after verification.';
								}
								else
								{
									// Transaction failed
								    DB::rollback();

								    $response['resCode']    = 4;
								    $response['resMsg']     = 'Server error!';
								}
							}
							else
							{
								// Transaction failed
							    DB::rollback();

							    $response['resCode']    = 5;
							    $response['resMsg']     = 'Server error!';
							}
						}
						else
						{
							// Transaction failed
						    DB::rollback();

						    $response['resCode']    = 6;
						    $response['resMsg']     = 'Server error!';
						}
					}
					catch(\Exception $e)
					{
						// Transaction failed
					    DB::rollback();

					    $response['resCode']    = 7;
					    $response['resMsg']     = 'Server error!';
					}
				}
				else
				{
					$response['resCode']    = 2;
					$response['resMsg']     = 'User with the same email id already exist';
				}
			}
			else
			{
				$response['resCode']    = 3;
				$response['resMsg']     = 'School with the same name already exist';
			}
		}

		return response()->json($response);
	}

	/**
     * Function to return login page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if( Auth::check() || Auth::viaRemember() )	// User is already logged-in or remembered
    	{
    		return redirect('school/dashboard');
    	}
    	else 										// User is not logged-in, show the login page
    	{
    		return view('school/login');
    	}
    }

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
    	return view('school/dashboard');
    }

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
    	Auth::logout();
    	return redirect('school');
    }
}