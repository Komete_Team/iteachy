<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use App\User;
use App\Role;
use App\Permission;

use Validator;

class LoginController extends Controller
{
    /**
     * Function to return login view
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if( Auth::check() || Auth::viaRemember() )	// User is already logged-in or remembered
        {
        	return redirect('admin/dashboard');
        }
        else 										// User is not logged-in, show the login page
        {
        	return view('admin/index');
        }
    }

    /**
     * Function for admin login
     * @param void
     * @return array
     */
    public function login()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $loginData);

        $remember = false;
        if( isset( $loginData['remember'] ) )
        {
        	$remember = true;
        }

        // Server Side Validation
        $response =array();

		$validation = Validator::make(
		    array(
		        'username'	=> $loginData['username'],
		        'password' 	=> $loginData['password']
		    ),
		    array(
		        'username' 	=> array('required', 'email'),
		        'password'	=> array('required'),
		        // 'password'	=> array('required', 'min:6'),
		    ),
		    array(
		        'username.required' => 'Please enter email',
		        'username.email'   	=> 'Please enter valid email',
		        'password.required'	=> 'Please enter password',
		        // 'password.min'    	=> 'Password must contain atleat 6 characters',
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and check the login credentials and do login
		{
			// Check for the credential and role.
				
			$user = User::where(['email' => $loginData['username']])->first();

			if( isset( $user ) )
			{
				//$userRole = $user->roles->first();
				$role = $user->role;
				$status = $user->status;
				$is_varify = $user->is_email_verify;
		        /* if( $userRole->name == $loginData['source'] )	// If the source role and the role name matched then go ahead
		        { */
				$redirectionUrl = array(
		        		1		=> url('admin/dashboard'),
		        		2 		=> url('teacher/dashboard'),
						3 		=> url('student/dashboard')
		        	);
				if($role == 2 && $status == 2){
					if(Auth::attempt(['email' => $loginData['username'], 'password' => $loginData['password'], 'status' => 2, 'is_email_verify' => 1], $remember))
		            {
		                // Get the logged-in user id
		                $userId = Auth::id();

		                // If user credentials are valid, update the last_login time in users table.
		                $user = User::find($userId);
		                $user->last_login = date('Y-m-d H:i:s');
		                $user->update();

		                $response['resCode']    = 0;
		                $response['resMsg']     = 'Successful login';
		                $response['redirectUrl']= url('teacher/addteacherdoc');
		            }
		            else
		            {
		                $response['resCode']    = 2;
		                $response['resMsg']     = 'Invalid user credentials';
		            }
				}else{
		            if(Auth::attempt(['email' => $loginData['username'], 'password' => $loginData['password'], 'status' => 1, 'is_email_verify' => 1], $remember))
		            {
		                // Get the logged-in user id
		                $userId = Auth::id();

		                // If user credentials are valid, update the last_login time in users table.
		                $user = User::find($userId);
		                $user->last_login = date('Y-m-d H:i:s');
		                $user->update();

		                $response['resCode']    = 0;
		                $response['resMsg']     = 'Successful login';
		                $response['redirectUrl']= $redirectionUrl[$user->role];
		            }
		            else
		            {
		                $response['resCode']    = 2;
		                $response['resMsg']     = 'Invalid user credentials';
		            }
				}
		       /*  }
		        else
		        {
		        	$response['resCode']    = 3;
		           	$response['resMsg']     = 'Invalid user';
		        } */
			}
			else
	        {
	        	$response['resCode']    = 4;
	           	$response['resMsg']     = 'Invalid user credentials';
	        }
		}

		return response()->json($response);
    }
}