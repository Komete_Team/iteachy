<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Subscription;
use App\PaymentHistory; 
use Helper; 
use App\Wallet;
use App\User;
use App\Role;
use App\Notification;

class SubscriptionController extends Controller
{
	public function index(Request $request)
    {
    	$subscriptions = Subscription::all();
        return view('admin.subscription',['subscriptions'=>$subscriptions]);
    }

    public function subscription(Request $request) {
    	$subscriptions = Subscription::all();
    	return view('teacher.subscription',['subscriptions'=>$subscriptions]);
    }
	
	public function paymentcancel(Request $request) {
		 $taxId= isset($_GET['tx'])?$_GET['tx']:'';
		 if(!empty($taxId)){
			return redirect('/teacher/subscription')->with('success',"Grazie per iscriverti."); 
		 }else{
			return redirect('/teacher/subscription')->with('danger',"Il pagamento è stato annullato.");
		 }
    }
	
	public function paymentsuccess(Request $request) {
		$taxId= isset($_GET['tx'])?$_GET['tx']:'';
		 if(!empty($taxId)){
			$paymentto = isset($_GET['cm'])?$_GET['cm']:'';
			$status= $_GET['st'];
			$userName= $_GET['item_name'];
			$userId= $_GET['item_number'];
			$currencyCode= $_GET['cc'];
			$paidAmount= $_GET['amt'];

			$paymentData= new PaymentHistory();

			$paymentData->txn_id= $taxId;
			$paymentData->user_id= $userId;
			$paymentData->userName = $userName;
			$paymentData->currencyCode= $currencyCode;
			$paymentData->paidAmount= $paidAmount;
			$paymentData->status= $status;
			$paymentData->teacher_id= $paymentto;
			$paymentData->payment_level= 'T';


			$paymentData->save();
			$insertId= $paymentData->id;

			if(!empty($insertId)) {
				$wallet= new Wallet;
				$wallet->paid_id= $insertId;
				$wallet->user_id= $paymentto;
				$wallet->amount= $paidAmount;
				$wallet->paid_status = 1;
			if($wallet->save()){
				$notMessage = 'Subscription Payment';
				$notHeading = 'Subscription Payment';
				$type = 5;
				$notificationdetail= Helper::saveNotification($userId, $paymentto, $notMessage, $notHeading);
				//$totalnotifications = count($notificationdetail); 

			}
			}
			return redirect('/teacher/subscription')->with('success',"Grazie per iscriverti."); 
		 }else{
			return redirect('/teacher/subscription')->with('danger',"Il pagamento è stato annullato.");
		 }
    }
	
	 

    public function add(Request $request) {
    	if($request->method() == 'POST') {
    		$subscription = new Subscription();
            $subscription->title  =  Input::get('title');
            $subscription->base	  =  Input::get('base');
            $subscription->standard  =  Input::get('standard');
            $subscription->premium  =  Input::get('premium');
            if($subscription->save()){
            	return redirect('/admin/subscriptions')->with('success',"Il piano di abbonamento è stato aggiunto correttamente."); 
            } else{
            	return redirect('/admin/subscriptions')->with('error',"Si è verificato un errore. Per favore riprova più tardi."); 
            }
    	}
    	return view('admin.addsubscription');
    }

    public function edit(Request $request,$subid) {
    	$subscriptionData = Subscription::where(['id'=>$subid])->first();
    	if($request->method() == 'POST') {
    		$subscription = Subscription::find($subid);
            $subscription->title  =  Input::get('title');
            $subscription->base	  =  Input::get('base');
            $subscription->standard  =  Input::get('standard');
            $subscription->premium  =  Input::get('premium');
            if($subscription->save()){
            	return redirect('/admin/subscriptions')->with('success',"il piano di abbonamento è stato aggiornato con successo."); 
            } else{
            	return redirect('/admin/subscriptions')->with('error',"Si è verificato un errore. Per favore riprova più tardi."); 
            }
    	}
    	return view('admin.editsubscription', ['subscriptionData'=>$subscriptionData]);
    }

    public function delete(Request $request,$subid) {
    	if(!empty($subid)) {
    		DB::table('subscriptions')->where('id', '=', $subid)->delete();
    		$request->session()->flash('success', "il piano di abbonamento è stato aggiornato con successo.");
    	}
    	echo 'success';exit;
    }
}