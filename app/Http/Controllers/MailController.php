<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller {
	public function basic_email() {
		$data = array('name'=> "Mayank Paney");

		Mail::send(['text'=>'emails.mail'], $data, function($message) {
			$message->to('mayankpandey@virtualemployee.com', 'Mayank Pandey')->subject
			('Laravel Basic Testing Mail');
			$message->from('testvirtual8@gmail.com', 'Test Virtual');
		});
		echo "Basic Email Sent. Check your inbox.";
	}
	
	public function html_email() {
		$data = array('name'=> "Mayank Paney");

		Mail::send('emails.mail', $data, function($message) {
			$message->to('mayankpandey@virtualemployee.com', 'Mayank Pandey')->subject
			('Laravel HTML Testing Mail');
			$message->from('testvirtual8@gmail.com', 'Test Virtual');
		});
		echo "HTML Email Sent. Check your inbox.";
	}
}