<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Chat;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Notification;
use App\BookedLectures;


class ChatController extends Controller
{
	public function index(Request $request, $lectureId)
    {
		if (!Auth::check()) {
			return redirect('/');
		}
    	$lectureData = BookedLectures::where(['id' => $lectureId])->first();
    	$chats = Chat::where(['appointment_id' => $lectureId])->orderBy('date_time','ASC')->get();
        $chatUsers=[];
        if(!empty($lectureData->teacher_id) && !empty($lectureData->student_id)) {
            $chatUs = User::whereIn('id', array($lectureData->teacher_id, $lectureData->student_id))->get();
            foreach ($chatUs as $cuser) {
                $chatUsers[$cuser->id] = [
                    'first_name' => $cuser->first_name,
                    'last_name' => $cuser->last_name,
                    'profile_image' => $cuser->profile_image, 
					'role' => $cuser->role
                ];
            }
        }
    	$user = auth()->user();
    	if(isset($lectureData->lecture_id)) {
    		if($user->role == 3){
    			if($user->id != $lectureData->student_id) {
    				return redirect('/');
    			}
    		} else {
    			if($user->id != $lectureData->teacher_id) {
    				return redirect('/');
    			}
    		}
    		
    	} else {
    		return redirect('/');
    	}
		if($user->role == 3){
			return view('student.chat',['user'=>$user, 'lectureData'=> $lectureData, 'chats'=>$chats,'chatUsers'=>$chatUsers]);
		}else if($user->role == 2){
			return view('teacher.chat',['user'=>$user, 'lectureData'=> $lectureData, 'chats'=>$chats,'chatUsers'=>$chatUsers]);
		}else{
			return redirect('/');
		}
    }

    public function savedata(Request $request)
    {
    	$user = auth()->user();
    	$chat = new Chat();
        $chat->appointment_id = Input::get('appid');
        $chat->message_from = Input::get('fromuser');
        $chat->message_to = Input::get('touser');
        $chat->message = Input::get('message');
        $chat->date_time = date('Y-m-d H:i:s', time());
        if($chat->save()) {
            $notificationData = [
                'user_id' => Input::get('touser'),
                'created_by' => Input::get('fromuser'),
                'priority_id'=>1,
                'heading' =>'Chat Notification',
                'description' => Input::get('message'),
                'send_email' => 0,
                'notification_source' => 2,
				'lecture_id' => Input::get('appid')
            ];
            Notification::firstOrCreate(['user_id' => Input::get('touser'),'created_by'=>Input::get('fromuser'),'is_read'=>0], $notificationData);
        	echo 'success';
        }
        exit;
    }
}