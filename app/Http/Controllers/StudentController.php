<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use Helper; 
use File;
use App\TeacherLeave;
use App\TeacherDocument;
use App\UserCategories;
use App\AvailabilityDays;
use App\Categories; 
use App\TeacherInfo; 
use App\TeacherReviews; 
use App\BookedLectures; 
use App\PaymentHistory; 

use App\Wallet;
use App\User;
use App\Role;
use App\Notification;
use App\Permission;
use App\AppointmentBooking;
use App\TempAppointmentBooking;
use App\TeacherAvailability;
use App\TeacherLessons;
use App\UserPaymentInfo;

use Mail;

use Validator;
use Session;
use Stripe;

class StudentController extends Controller
{
	
	
	public function index($token=null)
    {
        if(!empty($token))
        {
             $checkvarify = User::where(['remember_token'=>$token,'is_email_verify'=>0])->first();

              if(!empty($checkvarify))
              {
                    $updateisVerify= array(
                                    'is_email_verify'=>1
                                     );
                    User::where(['id' => $checkvarify->id])->update($updateisVerify);

                    return redirect('student')->with('success','Il tuo indirizzo email è verificato ora puoi effettuare il login.');

              }else{

                return redirect('student')->with('danger','Mancata corrispondenza dei token con noi.');
              }

                  //echo '<pre>';print_r($checkReferralCodeParent);die;

        }

    	if( Auth::check() || Auth::viaRemember() )	// User is already logged-in or remembered
    	{
    		return redirect('student/dashboard');
    	}
    	else 										// User is not logged-in, show the login page
    	{
    		return view('login');
    	}
    }
	
	public function testemail()
    {
		$user = array(0=>'123');
//echo '<pre>'; print_r($user); die;
    	Mail::send('teacher.varifyemail', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to('bobbysharma@virtualemployee.com', 'bobby')->subject('Your Reminder!');
        });
    }
	
	public function profile(Request $request)
    {
			$userId = Auth::id();
			$users = User::where(['id' => $userId])->first();
			$categories = Categories::where(['parent_id' => 0])->get();
			$subcategories = Categories::where('parent_id', '!=', 0)->get();
			$userinterest=UserCategories::where('teacher_id', $userId)->get();
			$subctarray = array();
			$ctarray = array();
			if(!empty($_POST)){
				$rules = [         
					'first_name' => 'required',
					'last_name' => 'required',
					'contact_no' => 'required',
					'address' => 'required',
					'city' => 'required',
					'province' => 'required',
					'country' => 'required',
				];

				$message["first_name.required"] = trans('translation.required');
				$message["last_name.required"] = trans('translation.required');
				$message["contact_no.required"] = trans('translation.required');
				$message["address.required"] = trans('translation.required');
				$message["city.required"] = trans('translation.required');
				$message["province.required"] = trans('translation.required');
				$message["country.required"] = trans('translation.required');
				$this->validate($request, $rules,$message);
				if (isset($request->validator) && $request->validator->fails()) {
					$request->session()->flash('error', trans('translation.errormessage'));
					return redirect('/student/profile');
			    } else {	
					if(!empty($request->file('student_image'))){
	                    $image       =         !empty($request->file('student_image'))  ?  $request->file('student_image') : '';
	                    $fileext     =         $image->getClientOriginalExtension();

	                    // file move to folder//
	                    $imgName = time().'.'.$image->getClientOriginalExtension();
	                    $destinationPath = public_path('/images/student_profileimage');
	                    $image->move($destinationPath, $imgName);
	                }else{
						$imgName = !empty($request->input('old_image'))  ?  $request->input('old_image') : '';
					}
					$first_name        =            !empty($request->input('first_name')) ?    $request->input('first_name') :'';
					$last_name     =          !empty($request->input('last_name')) ?     $request->input('last_name'):'';
					$contact_no  =    !empty($request->input('contact_no'))  ?  $request->input('contact_no'):'';
					$address   =         !empty($request->input('address'))    ?  $request->input('address'):'';
					$city        =            !empty($request->input('city')) ?    $request->input('city') :'';
					$province     =          !empty($request->input('province')) ?     $request->input('province'):'';
					$country   =         !empty($request->input('country'))    ?  $request->input('country'):'';
					
					$updateUserDetail = array(
						'first_name'=>$first_name,
						'last_name'=>$last_name,
						'contact_no'=>$contact_no,
						'address' =>$address,
						'city'=>$city,
						'province'=>$province,
						'country'=>$country,
						'profile_image' => $imgName
					);
					if(!empty($request->input('communication_type')) && !empty($request->input('communication_id'))) {
						$social   =  $request->input('communication_type').':'. $request->input('communication_id');
						$updateUserDetail['social_profile'] = $social;
					}
					$catData = [];
					if(!empty($request->input('category'))){
						foreach ($request->input('category') as $cat) {
							$catData[]=[
								'teacher_id' => $userId,
								'category_id' => $cat
							];
						}
					}
					if(User::where(['id'=>$userId])->update($updateUserDetail)){
						UserCategories::where('teacher_id', $userId)->delete();
						if(!empty($catData)) {
							UserCategories::insert($catData);
						}
					}
					$request->session()->flash('success', trans('translation.updated'));    
					return redirect('/student/profile');
				}
			}
    		return view('student.profile',['studentDetails'=>$users, 'categories' => $categories, 'subcategories'=>$subcategories,'userinterest' => $userinterest]);
    	
    }
	
	
	public function teacherLeave(Request $request)
    {
		$userId = Auth::id();
		$teacherLeaveDetails = TeacherLeave::where(['teacher_id' => $userId, 'status'=>1])->get();
		if(!empty($_POST)){
			$rules = [      
			'dtp_input' => 'required'
			];

			$message["dtp_input.required"] = trans('translation.required');
			$this->validate($request, $rules,$message);
			
			$date        =            !empty($request->input('dtp_input')) ?    $request->input('dtp_input') :'';
			
			$updateLeaveDetail = array(
			'dtp_input'=>$date
			);
			
			$teacherLeave = new TeacherLeave;

			$teacherLeave->teacher_id 	= $userId;
			$teacherLeave->leave_date 	= $date;
			$teacherLeave->status 	= 1;

			if( $teacherLeave->save() )
			{
				$request->session()->flash('success', trans('translation.updated'));         
				return redirect('/teacher/teacher_leave');	
				
			}else{
				$request->session()->flash('danger', trans('translation.not_updated'));         
				return redirect('/teacher/teacher_leave');	
			}
		}
		
    	return view('teacher/teacher_leave', ['teacherLeaveDetails'=>$teacherLeaveDetails]); 
    }
	
	public function teacherAccreditedProfile(Request $request)
    {
		$userId = Auth::id();
		$teacherDocDetails = TeacherDocument::where(['teacher_id' => $userId])->get(); 
    	return view('teacher/teacher_accredited_profile', ['teacherDocDetails'=>$teacherDocDetails, 'user_id'=>$userId]); 
    }
	
	public function teacherAddDoc(Request $request)
    {
		$userId = Auth::id();
		$teacherDocDetails = TeacherDocument::where(['teacher_id' => $userId])->get(); 
		if(!empty($_POST)){
			//echo '<pre>'; print_r($_POST); die;
			$rules = [      
			'file' => 'required',
			'teacher_description' => 'required',
			'doc_name' => 'required',
			];
			$message["doc_name.required"] = trans('translation.required');
			$message["file.required"] = trans('translation.required');
			$message["teacher_description.required"] = trans('translation.required');
			
			$this->validate($request, $rules,$message);
			$doc_name        =            !empty($request->input('doc_name')) ?    $request->input('doc_name') :'';
			$files        =            !empty($request->input('files')) ?    $request->input('files') :'';
			$note        =            !empty($request->input('teacher_description')) ?    $request->input('teacher_description') :'';
			
			foreach($files as $filesval){
				$teacherLeave = new TeacherDocument;
				$teacherLeave->teacher_id 	= $userId;
				$teacherLeave->doc_name  	= $doc_name;
				$teacherLeave->files 	= $filesval;
				$teacherLeave->teacher_note 	= $note;
				$teacherLeave->save();
			}
			
				$request->session()->flash('success', 'Files successfully uploaded!');         
				return redirect('/teacher/teacher_accredited_profile');	
				
			
		}
    	return view('teacher/teacher_add_docs', ['teacherDocDetails'=>$teacherDocDetails, 'user_id'=>$userId]); 
    }
	
	public function uploadDocs(Request $request, $id)
    {
		if(!empty($_POST)){
			
			
			$file        =            !empty($request->input('request')) ?    $request->input('request') :'';
			
			$request = $file;

			// Upload file
			if($request == 1){
					$doc = Input::file('file');
					if( !is_null( $doc ) && ( $doc->getSize() > 0 ) )
					{
					    // Image destination folder
					    $destinationPath = public_path('/uploads/teachers/documents');
					    if( $doc->isValid() )  // If the file is valid or not
					    {
					        $fileExt  = $doc->getClientOriginalExtension();
							$fileType = $doc->getMimeType();
					        $fileSize = $doc->getSize();
							
							if( ( $fileType == 'image/jpeg' || $fileType == 'image/jpg' || $fileType == 'image/png' || $fileType == 'application/pdf' || $fileType == 'application/msword' ) ) // 2 MB
					        {
					            // Rename the file
					            $logoFileName = str_random(20) . '.' . $fileExt;

					            if($doc->move( $destinationPath, $logoFileName )){
									echo $logoFileName;
								 }else{
									 echo 0;
								 }
					    	}else{
								$uploadOk = 0;
							}

					        
					    }
					}
				
				exit;
			}

			// Remove file
			if($request == 2){

				$path = $_POST['path'];
				$return_text = 0;

				// Check file exist or not
				if (file_exists(public_path().'/uploads/teachers/documents/'.$path))
				{
				   // Remove file
				   File::delete(public_path().'/uploads/teachers/documents/'.$path);
				 
				   // Set status
				   $return_text = 1;
				}else{
				   // Set status
				   $return_text = 0;
				}

				// Return status
				echo $return_text;
				exit;
			}
		}
	}
	
    /**
     * Function to delete teacher document
     * @param void
     * @return array
     */
    public function deleteDoc()
    {
    	$docid = Input::get('docid');

    	$docid = TeacherDocument::find($docid);

    	if( $docid->delete() )
    	{
    		$response['resCode']    = 0;
    		$response['resMsg']     = trans('translation.doc_deleted');
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	
	
	
	
	public function getTeacherLeaveDetail(Request $request)
    {

        $teacherleaveid = Input::get('teacherleaveid');

    	$teacherLeaveDetails = TeacherLeave::find($teacherleaveid);

    	return response()->json($teacherLeaveDetails);
    }
	
	public function saveTeacherLeaveDetail(Request $request)
    {
		// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $teacherLeaveDetails);
        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'leave_date'	=> $teacherLeaveDetails['leave_date']
		    ),
		    array(
		        'leave_date'	=> array('required')
		    ),
		    array(
		        'leave_date.required' 	=> 'Please enter leave date'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			try
			{
				$TeacherLeave = TeacherLeave::where(['id' => $teacherLeaveDetails['leave_id']])->first();

		        $TeacherLeave->leave_date 	= $teacherLeaveDetails['leave_date'];

		        if( $TeacherLeave->update() )
		        {
		        	$response['resCode']    = 0;
		        	$response['resMsg']     = trans('translation.updated');
		        }
		        else
		        {
		        	$response['resCode']    = 2;
		        	$response['resMsg']     = trans('translation.error');
		        }
			}
			catch(\Exception $e)
			{
				$response['resCode']    = 7;
				$response['resMsg']     = trans('translation.error');
			}
		}
			

		return response()->json($response); 
    }
	
	public function cancelLeave()
    {
    	$leaveid = Input::get('leaveid');

    	$leave = TeacherLeave::find($leaveid);

		$TeacherLeave = TeacherLeave::where(['id' => $leaveid])->first();

		$TeacherLeave->status 	= 0;

		if( $TeacherLeave->update() )
		{
    		$response['resCode']    = 0;
    		$response['resMsg']     = 'Leave has been canceled successfully';
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	
	public function teacherInfo(Request $request)
    {
		$userId = Auth::id();
		$teacherDetails = TeacherInfo::where(['teacher_id' => $userId])->first();
		if(!empty($_POST)){
			$rules = [      
				'title' => 'required',
				'description' => 'required',
				'price' => 'required',
				'video' => 'required'
				];

				$message["title.required"] = trans('translation.required');
				$message["description.required"] = trans('translation.required');
				$message["price.required"] = trans('translation.required');
				$message["video.required"] = trans('translation.required');

				$this->validate($request, $rules,$message);
				
				$title        =            !empty($request->input('title')) ?    $request->input('title') :'';
				$description     =          !empty($request->input('description')) ?     $request->input('description'):'';
				$price  =    !empty($request->input('price'))  ?  $request->input('price'):'';
				
				if(!empty($request->file('video'))){
                     $video       =         !empty($request->file('video'))  ?  $request->file('video') : '';
                     $fileext     =         $video->getClientOriginalExtension();

                     // file move to folder//
                     $videoNmae = time().'.'.$video->getClientOriginalExtension();
                     $destinationPath = public_path('/videos/teacher_videos');
                     $video->move($destinationPath, $videoNmae);
                 }else{
					$videoNmae = !empty($request->input('old_video'))  ?  $request->input('old_video') : '';
				 }

				$updateUserDetail = array(
				'title'=>$title,
				'description'=>$description,
				'price'=>$price,
				'file_name'=>$videoNmae
				);

				$teacherinfo = TeacherInfo::where(['teacher_id'=>$userId])->first();
				if(empty($teacherinfo)){
					$teacher = new TeacherInfo;

					$teacher->teacher_id 	= $userId;
					$teacher->title 	= $title;
					$teacher->description 	= $description;
					$teacher->price = $price;

					if( $teacher->save() )
					{
						$request->session()->flash('success', trans('translation.updated'));         
						return redirect('/teacher/teacher_info');	
						
					}
				}else{
					TeacherInfo::where(['teacher_id'=>$userId])->update($updateUserDetail);
				}


				$request->session()->flash('success', trans('translation.updated'));         
				return redirect('/teacher/teacher_info');		
		}
		
    	return view('teacher/teacher_info',['teacherDetails'=>$teacherDetails]); 
    }
	
	
	
	/**
     * Function to return register page
     * @param void
     * @return \Illuminate\Http\Response
     */
	 
	public function register()
	{
		$categories = Categories::where('parent_id', '==', 0)->get();
		$subcategories = Categories::where('parent_id', '!=', 0)->get();
		$catarr = array();
		/* foreach($categories as $catid){
			//echo $catid->id; die;
			$subcategories = array();
			$subcategories = Categories::where(['parent_id' => $catid->id])->get();
				foreach($subcategories as $subcat){
				//echo '<pre>'; print_r($subcategories); die;
				if(!empty($subcat->id)){
					
					$catarr[$catid->id][] = array('name'=>$subcat->category_name,'id'=>$subcat->id);
				}
			}
		} */
		//echo '<pre>'; print_r($catarr); die;
		return view('student/register',['categories' => $categories, 'subcategories' => $subcategories]);
	}

	 
    public function registerStudent()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $studentDetails);
		// echo '<pre>'; print_r($teacherDetails); die;
        $response =array(); 
        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'first_name'	=> $studentDetails['first_name'],
		        'last_name'		=> $studentDetails['last_name'],
		        'email_id'		=> $studentDetails['email_id'],
		        'password' 		=> $studentDetails['password'],
				
		    ),
		    array(
		        'first_name'	=> array('required'),
		        'last_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required')
		    ),
		    array(
		        'first_name.required' 	=> 'Si prega di inserire il nome',
		        'last_name.required' 	=> 'Per favore inserisci il cognome',
		        'email_id.required'		=> 'Si prega di inserire l ID e-mail',
		        'email_id.email'		=> 'Si prega di inserire un ID e-mail valido',
		        'password.required'		=> 'Per favore, inserisci la password'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$user = User::where(['email' => $studentDetails['email_id']])->first();
			if( is_null( $user ) )	// No user with the same email id exist, save the data
			{

				$user = new User;

				$user->first_name 	= $studentDetails['first_name'];
				$user->last_name 	= $studentDetails['last_name'];
				$user->email 	= $studentDetails['email_id'];
				$user->password = Hash::make($studentDetails['password']);
				$user->role = 3;
				$user->status = 1;
				$user->remember_token = $studentDetails['_token'];

				if( $user->save() )
				{
					//echo '<pre>'; print_r($_POST); die;
								   
					 Mail::send('teacher.varifyemail', ['user' => $studentDetails], function ($m) use ($studentDetails) {
						$m->from('hello@app.com', 'Your Application');

						$m->to($studentDetails['email_id'], $studentDetails['first_name'])->subject('Your Reminder!');
					}); 
					$response['resCode']    = 0;
					$response['resMsg']     = 'Insegnante registrato con successo. Puoi accedere al tuo account dopo la verifica.';
				}
				else
				{
					// Transaction failed
					//DB::rollback();

					$response['resCode']    = 6;
					$response['resMsg']     = 'Errore del server!';
				}
				
			}
			else
			{
				$response['resCode']    = 2;
				$response['resMsg']     = "Lo studente con lo stesso ID e-mail esiste già";
			}
			
		}

		return response()->json($response);
	}

    /**
     * Function to save role details
     * @param void
     * @return array
     */
    public function registerSchool()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $schoolDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'school_name'	=> $schoolDetails['school_name'],
		        'full_name'		=> $schoolDetails['full_name'],
		        'email_id'		=> $schoolDetails['email_id'],
		        'password' 		=> $schoolDetails['password']
		    ),
		    array(
		        'school_name'	=> array('required'),
		        'full_name'		=> array('required'),
		        'email_id'		=> array('required', 'email'),
		        'password' 		=> array('required')
		    ),
		    array(
		        'school_name.required' 	=> 'Please enter school name',
		        'full_name.required' 	=> 'Please enter school name',
		        'email_id.required'		=> 'Please enter email id',
		        'email_id.email'		=> 'Please enter a valid email id',
		        'password.required'		=> 'Please enter password'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined validation rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			// Check if the school with the same name already exist
			$school = School::where(['school_name' => $schoolDetails['school_name']])->first();

			if( is_null( $school ) )	// No school with the same name exist, save the data
			{
				// Check if an user exist with the same email id
				$user = User::where(['email' => $schoolDetails['email_id']])->first();

				if( is_null( $user ) )	// No user with the same email id exist, save the data
				{
					try
					{
						// Begin transaction
					    DB::beginTransaction();

						$user = new User;

						$user->name 	= $schoolDetails['full_name'];
						$user->email 	= $schoolDetails['email_id'];
						$user->password = Hash::make($schoolDetails['password']);

						if( $user->save() )
						{
							// Attach the role "ali_user" to this newly created user
							$userRole = Role::where(['name' => 'ali_user'])->first();
							if( $user->attachRole($userRole) )
							{
								$school = new School;

								$school->user_id 	= $user->id;
								$school->school_name= $schoolDetails['school_name'];
								$school->status 	= '1';
								$school->verified 	= '0';
								$school->created_at = date('Y-m-d H:i:s');

								if( $school->save() )
								{
									// Transaction successful
							    	DB::commit();

							    	$response['resCode']    = 0;
							    	$response['resMsg']     = 'School registered successfully. You can access your account after verification.';
								}
								else
								{
									// Transaction failed
								    DB::rollback();

								    $response['resCode']    = 4;
								    $response['resMsg']     = trans('translation.error');
								}
							}
							else
							{
								// Transaction failed
							    DB::rollback();

							    $response['resCode']    = 5;
							    $response['resMsg']     = trans('translation.error');
							}
						}
						else
						{
							// Transaction failed
						    DB::rollback();

						    $response['resCode']    = 6;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						// Transaction failed
					    DB::rollback();

					    $response['resCode']    = 7;
					    $response['resMsg']     = trans('translation.error');
					}
				}
				else
				{
					$response['resCode']    = 2;
					$response['resMsg']     = 'User with the same email id already exist';
				}
			}
			else
			{
				$response['resCode']    = 3;
				$response['resMsg']     = 'School with the same name already exist';
			}
		}

		return response()->json($response);
	}

	/**
     * Function to return login page
     * @param void
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
		$userId = Auth::id();
		$now = date('Y-m-d H:i:s');
		$categories = Categories::where('parent_id', '==', 0)->get();
		$subcategories = Categories::where('parent_id', '!=', 0)->get();
		$teacherFuttureBooked= AppointmentBooking::where(['appointement_booking.student_id' => $userId, 'appointement_booking.is_booked'=>1,'appointement_booking.is_cancel'=>0])
		->leftJoin('users', 'users.id', '=', 'appointement_booking.teacher_id')
		->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')
		->leftJoin('teacher_lessons', 'teacher_lessons.id', '=', 'booked_lectures.lecture_id')
		->where('appointement_booking.start_date', '>', $now)
		->select('users.first_name','users.last_name','users.email','users.profile_image','teacher_lessons.lesson_name', 'appointement_booking.start_date', 'appointement_booking.end_date', 'appointement_booking.id')->get();
		
		$teachers =  DB::table('users')
                  ->leftJoin('user_categories', 'users.id', '=', 'user_categories.teacher_id') 
				  ->leftJoin('teacher_infos', 'users.id', '=', 'teacher_infos.teacher_id') 
				  ->leftJoin('categories', 'user_categories.category_id', '=', 'categories.id')
				   ->leftJoin('teacher_reviews', 'teacher_reviews.teacher_id', '=', 'users.id') 	
                  ->select('users.id','users.first_name','users.profile_image','users.last_name','users.email','users.address','users.city','users.province','users.country','users.contact_no', 'user_categories.category_id', 'teacher_infos.title', 'teacher_infos.description', 'teacher_infos.price','teacher_infos.price_per_hour', 'categories.category_name')
				  ->selectRaw("GROUP_CONCAT(teacher_reviews.rate SEPARATOR '$') as ratings")->selectRaw("GROUP_CONCAT(teacher_reviews.user_id SEPARATOR '$') as users")
				  ->groupBy('users.id')
				  ->where('users.is_accridited',1)
                  ->paginate(15); 
				  
		$teacherInfo= User::where(['id' => $userId])->first();
    	return view('student/dashboard',['categories'=>$categories, 'subcategories'=>$subcategories, 'studentInfo'=>$teacherInfo,'teachers'=>$teachers, 'total_booked_appt'=>count($teacherFuttureBooked)]);
    }

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }
	
	public function teacherAvailability(Request $request)
    {
		$userId = Auth::id();
		$teacherDetails = TeacherInfo::where(['teacher_id' => $userId])->first();
		$availability = AvailabilityDays::get();
		$teacherAvailabilities = TeacherAvailability::get();
		if(!empty($_POST)){
				$days        =            !empty($request->input('days')) ?    $request->input('days') :'';
				$start_time     =          !empty($request->input('start_time')) ?     $request->input('start_time'):'';
				$end_time  =    !empty($request->input('end_time'))  ?  $request->input('end_time'):'';
			if(!empty($days)){	
				foreach($days as $days_val){
					$updateavailability = array();
					$updateavailability = array(
					'status'=>1,
					'num_days'=>$days_val,
					'start_time'=>$start_time[$days_val-1],
					'end_time'=>$end_time[$days_val-1]
					);
					$teacherAvail = TeacherAvailability::where(['teacher_id' => $userId,'num_days' => $days_val])->get();
					//echo '<pre>'; print_r(count($teacherAvail)); die;
					if(count($teacherAvail) > 0){
						TeacherAvailability::where(['teacher_id' => $userId,'num_days' => $days_val])->update($updateavailability);
						$updateavailability1 = array(
						'status'=>0
						);
						TeacherAvailability::whereNotIn('num_days',$days)->where(['teacher_id'=>$userId])->update($updateavailability1);
					}else{
						$saveTeacherAvail = new TeacherAvailability;

						$saveTeacherAvail->teacher_id 	= $userId;
						$saveTeacherAvail->num_days= $days_val;
						$saveTeacherAvail->status 	= 1;
						$saveTeacherAvail->start_time  	= $start_time[$days_val-1];
						$saveTeacherAvail->end_time= $end_time[$days_val-1];

						if($saveTeacherAvail->save()){
							/* $saveAppointment = new AppointmentBooking;
							$saveAppointment->teacher_id = $userId;
							$saveAppointment->appointment_color = '#62c208';
							$saveAppointment->save(); */
						}
					}
				}
			}else{
				$updateavailability = array(
					'status'=>0
				);
				TeacherAvailability::query()->where(['teacher_id'=>$userId])->update($updateavailability);
			}
			$request->session()->flash('success', trans('translation.updated'));         
			return redirect('/teacher/teacher_availability');		
		}
		//echo '<pre>'; print_r($_POST); die;
		
    	return view('teacher/teacher_availability',['teacherDetails'=>$teacherDetails, 'availability'=>$availability, 'teacherAvailabilities'=>$teacherAvailabilities]); 
    }
	
    
	
	public function Notification(Request $request) 
	{
		$userId = Auth::id();
		$notifications= Notification::where(['notifications.user_id'=>$userId, 'is_read'=>0])->leftJoin('users', 'users.id', '=', 'notifications.created_by')->select('users.first_name','users.last_name','users.email','users.profile_image','users.role', 'notifications.*')->get();
		//echo '<pre>'; print_r($notifications); die;
		if(!empty($notifications)){
		$updateIsRead= array(
					'is_read'=>'1'
				   );
		foreach($notifications as $val){	
			$notid = $val->id;		
			Notification::where(['id' => $notid])->update($updateIsRead);
		}
		}     
		return view('student/notification-view',['notifications'=>$notifications]);
	}
	
	public function teacherLesson(Request $request)
	{
		$userId = Auth::id();
		$teacherlessons= TeacherLessons::where(['teacher_id' => $userId])->get();
		return view('teacher/teacher_lessons',['lessons'=>$teacherlessons]);
	}
	
	public function saveLessons()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $lessonDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'lesson_name' 		=> $lessonDetails['lesson_name'],
		        'total_lessons' 	=> $lessonDetails['total_lessons']
		    ),
		    array(
		        'lesson_name' 		=> array('required'),
		        'total_lessons' 	=> array('required')
		    ),
		    array(
		        'lesson_name.required'			=> 'Please enter lesson name',
		        'total_lessons.required'		=> 'Please enter number of lesson'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();

			if( $lessonDetails['lesson_id'] == '' )	// Event id is not available, save the data
			{
				$teacherLessons = new TeacherLessons();

				$teacherLessons->lesson_name 		= $lessonDetails['lesson_name'];
				$teacherLessons->total_lessons 		= $lessonDetails['total_lessons'];
				$teacherLessons->teacher_id 		= $loggedInUserId;
				

				try
				{
					if( $teacherLessons->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Lesson saved successfully';
						//$response['calendarEvents'] = $teacherLessons;
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
			else
			{
				$teacherLessons = TeacherLessons::where(['id' => $lessonDetails['lesson_id']])->first();

				$teacherLessons->lesson_name 		= $lessonDetails['lesson_name'];
				$teacherLessons->total_lessons 		= $lessonDetails['total_lessons'];
				

				try
				{
					if( $teacherLessons->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Lesson details updated successfully';
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
		}

		return response()->json($response);
	}
	
	public function getLessonDetails()
    {
    	$lessonid = Input::get('lessonid');

    	$lessondetail = TeacherLessons::where(['id' => $lessonid])->first();
    	return response()->json($lessondetail);
    }
	
	public function deleteLesson()
    {
    	$lesson_id = Input::get('lesson_id');

    	$lessondetails = TeacherLessons::find($lesson_id);

    	try
    	{
    		if( $lessondetails->delete() )
    		{
    			$response['resCode']    	= 0;
    			$response['resMsg']     	= 'Lesson deleted successfully';
    		}
    		else
    		{
    			$response['resCode']    = 1;
    		    $response['resMsg']     = trans('translation.error');
    		}
    	}
    	catch(\Exception $e)
    	{
    		$response['resCode']    = 2;
    	    $response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	public function saveTeacherReview()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $reviewDetails);
        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'headline' 		=> $reviewDetails['headline'],
		        'review' 	=> $reviewDetails['review']
		    ),
		    array(
		        'headline' 		=> array('required'),
		        'review' 	=> array('required')
		    ),
		    array(
		        'headline.required'			=> 'Please enter title for review',
		        'review.required'		=> 'Please write message for review'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();
			$teacherreviews = new TeacherReviews();

			$teacherreviews->title 		= $reviewDetails['headline'];
			$teacherreviews->review_message = $reviewDetails['review'];
			$teacherreviews->user_id 		= $loggedInUserId;
			$teacherreviews->teacher_id 		= $reviewDetails['teacher_id'];
			$teacherreviews->rate 		= $reviewDetails['rate'];
			if( $teacherreviews->save() )
			{
				$response['resCode']    	= 0;
				$response['resMsg']     	= 'Review saved successfully';
				//$response['calendarEvents'] = $teacherLessons;
			}
			else
			{
				$response['resCode']    = 1;
				$response['resMsg']     = 'Server error!';
			}
		}

		return response()->json($response);
	}
	
	public function settings(Request $request)
	{
		//echo '<pre>'; print_r($_POST); die;
		$userId = Auth::id();
		if(!empty($_POST)){
			//echo '<pre>'; print_r($_POST); die;	
			$rules = [
			'pwd' => 'required',             
			'pwd2' => 'required'
			];

			$message["pwd.required"] = trans('translation.required');
			$message["pwd2.required"] = trans('translation.required');
				
			$this->validate($request, $rules,$message);   
				
			$pwd        =            !empty($request->input('pwd')) ?    $request->input('pwd') :'';
			$pwd2     =          !empty($request->input('pwd2')) ?     $request->input('pwd2'):'';
			if($pwd != $pwd2){
				$request->session()->flash('danger', trans('translation.password_not_match'));
				return redirect('/teacher/settings');
			}
			$userdetail = User::where(['id' => $userId])->first();
			//echo '<pre>'; print_r($userdetail->password); die;
			
			
			//echo '<pre>'; print_r(json_encode($catesarray)); die;
			if(Hash::check($pwd, $userdetail->password) == 1){
				$updateUserDetail = array(
				'status'=>0
				);
				User::where(['id'=>$userId])->update($updateUserDetail);
				Auth::logout();
				$request->session()->flash('success', trans('translation.account_deleted')); 
			}else{
				$request->session()->flash('success', trans('translation.account_not_deleted')); 
			}				
			return redirect('/');		
			
		}
		return view('teacher/teacher_setting');
	}
	
	public function bookLecture($id=null)
    {
		$userId = Auth::id();
		if(empty($userId)){
			return redirect('/login');
		}
		$teacherAvailable= TeacherAvailability::where(['teacher_id' => $id])->get();
		$userTempDetail= TempAppointmentBooking::where(['teacher_id' => $id, 'user_id' => $userId])->get();
		
		$teacherlectures= TeacherLessons::where(['teacher_id' => $id])->get();
		$user_detail = User::where(['id' => $userId])->get();
		$teacher_detail = TeacherInfo::where(['teacher_id' => $id])->first();
    	return view('student/booking_form',['user_detail'=>$user_detail, 'teacherlectures'=>$teacherlectures, 'teacher_detail'=>$teacher_detail, 'tid' => $id,'userId' => $userId, 'teacherAvailable'=>$teacherAvailable, 'userTempDetail'=>$userTempDetail]);
    }
	
	public function saveTimeSlot()
    {
		$start_time = Input::get('start_time');
		$end_time = Input::get('end_time');
		$user_id = Input::get('user_id');
		$teacher_id = Input::get('teacher_id');
		$start_date = Input::get('start_date');
		$end_date = Input::get('end_date');
		
		$userAlreadySlot = TempAppointmentBooking::where(['teacher_id' => $teacher_id, 'user_id' => $user_id])->first();
		if(!empty($userAlreadySlot)){
			$userAlreadySlot->delete();
		}
		//$loggedInUserId = Auth::id();
		$userTempDetail = new TempAppointmentBooking();

		$userTempDetail->starteTime 		= $start_time;
		$userTempDetail->endtime = $end_time;
		$userTempDetail->start_date 		= date('Y-m-d', strtotime($start_date));
		$userTempDetail->end_date = date('Y-m-d', strtotime($end_date));
		$userTempDetail->user_id 		= $user_id;
		$userTempDetail->teacher_id 		= $teacher_id;
		$userTempDetail->save();
			
    }
	
	public function saveAppointment()
    {
		//echo '<pre>'; print_r($_POST); die;
		$lectureid = Input::get('lectureids');
		$lectureprice = Input::get('lectureprice');
		$studentid = Input::get('studentid');
		$teacherid = Input::get('teacherid');
		$skype = Input::get('skype');
		$skype_id = Input::get('skype_id');
		$place = Input::get('place');
		$knowledgelevel = Input::get('knowledgelevel');
		$message = Input::get('message');
		
		$start_time = Input::get('start_time');
		$end_time = Input::get('end_time');
		$start_date = Input::get('start_date');
		$end_date = Input::get('end_date');
		
		//$userAlreadySlot = TempAppointmentBooking::where(['teacher_id' => $teacherid, 'user_id' => $studentid])->first();
		//echo '<pre>'; print_r($userAlreadySlot); die;
		//$loggedInUserId = Auth::id();
		$userAppointmentDetail = new AppointmentBooking();

		$userAppointmentDetail->starteTime 		= $start_time;
		$userAppointmentDetail->endtime = $end_time;
		$userAppointmentDetail->start_date 		= $start_date.' '.$start_time;
		$userAppointmentDetail->end_date = $end_date.' '.$end_time;
		$userAppointmentDetail->student_id 		= $studentid;
		$userAppointmentDetail->teacher_id 		= $teacherid;
		//$userAppointmentDetail->save();
		if($userAppointmentDetail->save()){
			$bookedlectures = new BookedLectures();
			$bookedlectures->appointment_id = $userAppointmentDetail->id;
			$bookedlectures->teacher_id 		= $teacherid;
			$bookedlectures->student_id = $studentid;
			if($skype == 'yes'){
				$bookedlectures->skype_id 		= $skype_id ?? '';
			}
			$bookedlectures->meeting_place = $place;
			$bookedlectures->knowledge_level 		= $knowledgelevel;
			$bookedlectures->message 		= $message;
			$bookedlectures->lecture_id 		= $lectureid;
			$bookedlectures->save();
			$teacherlectures= TeacherLessons::where(['id' => $lectureid])->first();
			$user_detail = User::where(['id' => $studentid])->first();
			$teacher_detail = User::where(['id' => $teacherid])->first();
			$lectdetail = array('start_time'=>$start_time, 'end_time'=> $end_time, 'start_date'=>$start_date, 'end_date'=>$end_date);
			return view('student/checkout_form',['user_detail'=>$user_detail, 'teacherlectures'=>$teacherlectures, 'teacher_detail'=>$teacher_detail, 'tid' => $teacherid,'userId' => $studentid, 'skype_id'=>$skype_id, 'lectdetail'=>$lectdetail, 'price'=>$lectureprice, 'appt_id' => $userAppointmentDetail->id]);
			
		}
			
    }
	
	
	
	
	public function deleteTimeSlot()
    {
		$tempeventid = Input::get('tempeventid');
		
		$tempeventid = TempAppointmentBooking::find($tempeventid);

    	if( $tempeventid->delete() )
    	{
    		$response['resCode']    = 0;
    		//$response['resMsg']     = trans('translation.doc_deleted');
    	}
		
			
    }
	
	
	
	
	public function teacherAppointment(Request $request,$tid=null, $lid=null)
    {
		$visible = $request->input('visible');
		$userId = Auth::id();
		$dataAvailable=[];
		$teacherLesson= TeacherLessons::where(['id' => $lid])->first();
		if(isset($visible)) {
			$lecturetime = 180;
		}else{
			if(!empty($teacherLesson->lesson_time)){
				$lecturetime = $teacherLesson->lesson_time; 
			}else{
				$lecturetime = 30;
			}
		}
		
        $teacherBooked= AppointmentBooking::where(['teacher_id' => $tid, 'is_booked'=>1])->get();
		
		$teacherCancel= AppointmentBooking::where(['teacher_id' => $tid, 'is_cancel'=>1])->get();
		$teacherAvailable= TeacherAvailability::where(['teacher_id' => $tid])->get();
		$teacherLeave= TeacherLeave::where(['teacher_id' => $tid,'status' => 1])->get();
		//$userTempDetail= TempAppointmentBooking::where(['teacher_id' => $tid, 'user_id' => $userId])->get();
        $dataBooked =[];
		$dataTempSelected = [];
		$dataLeave = [];
		$allbookedid = [];
		$allcanceledid = [];
		
		//echo '<pre>'; print_r($teacherAvailable); die;
        
		$start = $_GET['start'];
		$end = $_GET['end'];
		
		$alldatesInWeekArray = array(); 

		$Variable1 = strtotime($start); 
		$Variable2 = strtotime($end); 
		$temparr = array();
		// Use for loop to store dates into array 
		// 86400 sec = 24 hrs = 60*60*24 = 1 day 
		for ($currentDate = $Variable1; $currentDate <= $Variable2;  
				$currentDate += (86400)) { 
					  
		$Store = date('Y-m-d', $currentDate); 
		$alldatesInWeekArray[] = $Store; 
		} 

		
		foreach($teacherCancel as $row)
        {
			$allcanceledid[] = date('Y-m-d H:i',strtotime($row->start_date));
            $dataBooked[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d H:i',strtotime($row->start_date)),
                              'end'   => date('Y-m-d H:i',strtotime($row->end_date)),
							  'className'=> 'calendar-cell-canceled'
                         );
        }
		
		foreach($teacherBooked as $row)
        {
			$allbookedid[] = date('Y-m-d H:i',strtotime($row->start_date));
			if(in_array(date('Y-m-d H:i',strtotime($row->start_date)), $allcanceledid)){
			}else{
				if(!in_array($row->start_date, $temparr)){
					$temparr[] = $row->start_date;
					$dataBooked[] = array(
								  'id'   => $row->id,
								  'start'   => date('Y-m-d H:i',strtotime($row->start_date)),
								  'end'   => date('Y-m-d H:i',strtotime($row->end_date)),
								  'className'=> 'calendar-cell-booked'
							 );
				}
			}
        }
		//$dataBooked = array_unique($dataBooked);
		/* foreach($userTempDetail as $row)
		{
			$dataTempSelected[] = array(
			'id'   => $row->id,
			'start'   => date('Y-m-d H:i',strtotime($row->start_date.' '.$row->starteTime )),
			'end'   => date('Y-m-d H:i',strtotime($row->end_date.' '.$row->endtime)),
			'className'=> ['calendar-cell-tempselected', 'calendar-cell-tempselected'.$row->id]
			);
		}  */
		//echo '<pre>'; print_r($teacherAvailable); die;
		 foreach($teacherAvailable as $k => $row)
        {
			//$time1 = new DateTime($row->start_time);
			//$time2 = new DateTime($row->end_time);
			//$interval = $time1->diff($time2);

			$from_time1 = $row->start_time;
			$to_time = strtotime($row->end_time);
			$from_time = strtotime($row->start_time);
			$timediff = round(abs($to_time - $from_time) / 60,2);
			$totaltimediff = $timediff/$lecturetime;
			$timearr = array();
			for($i=0; $i<$totaltimediff; $i++){
				if($i>0){
					$from_time1 = date('H:i', strtotime("+$lecturetime minutes", strtotime($from_time1)));
				}
				$timearr[$i]['start_time'] = $from_time1;
				
				$timearr[$i]['end_time'] = date('H:i', strtotime("+$lecturetime minutes", strtotime($from_time1)));
				
			}
			
			foreach($timearr as $timearrval){
			//echo '<pre>'; print_r($timearr); die;
			//echo $row->end_time->diff($row->start_time)->format('%H:%I:%S'); die;
			//echo $interval->format('%s second(s)'); die;
			foreach($alldatesInWeekArray as $dateval){
				
				$day = date('D',strtotime($dateval));
				 //echo $row->num_days.'=='.$day; die;
				$daterange = '';
				$startdaterange = '';
				$enddaterange = '';
				if($row->num_days == 7 && $day == 'Sun'){
					
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 1 && $day == 'Mon'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 2 && $day == 'Tue'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 3 && $day == 'Wed'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 4 && $day == 'Thu'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 5 && $day == 'Fri'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				if($row->num_days == 6 && $day == 'Sat'){
					$startdaterange = $dateval.' '.$timearrval['start_time'];
					$enddaterange = $dateval.' '.$timearrval['end_time'];
				}
				
				
				
			if(in_array(date('Y-m-d H:i',strtotime($startdaterange)), $allbookedid)){
			}else{
			
            $dataAvailable[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d H:i',strtotime($startdaterange)),
                              'end'   => date('Y-m-d H:i',strtotime($enddaterange)),
							  'className'=> ['calendar-cell-available', 'avail_selected'.$row->id]
                         );
			}
			}
		}
			
        }   
		//echo '<pre>'; print_r($allbookedid); die;
		
		foreach($teacherLeave as $row)
        {
            $dataLeave[] = array(
                              'id'   => $row->id,
                              'start'   => date('Y-m-d 00:00',strtotime($row->leave_date)),
                              'end'   => date('Y-m-d 24:00',strtotime($row->leave_date)),
							  'className'=> 'calendar-cell-leave'
                         );
        } 
		//echo '<pre>'; print_r($dataTempSelected); die;
		$data = array_merge($dataBooked, $dataAvailable, $dataLeave);
        echo  json_encode($data);       
    }
	
	public function CancelStatus()
    {

       return view('payment.cancel');  
    }

    public function SuccessStatus($txid)
    {

      return view('payment.success',['taxId'=>$txid]);

    }


    public function GetPaymentResponse(Request $request)
    {           
       $taxId= isset($_GET['tx'])?$_GET['tx']:'';
	   $cmarr = array();
	   $cmarr = explode("$",$_GET['cm']);

         if(!empty($taxId)){

                    $checkTxnId= PaymentHistory::where(['txn_id'=>$taxId])->first();

                   
                    if(!empty($checkTxnId)){

                         //return redirect('paypal/success/'.$taxId);
                       $request->session()->flash('success', 'Il tuo pagamento è andato a buon fine');
                       return redirect('student/booking_listing');

                    }else{
                          
                            $status= $_GET['st'];
                            $userName= $_GET['item_name'];
                            $userId= $_GET['item_number'];
							$teacher_id= $cmarr[0];
							$apt_id= $cmarr[1];
                            $currencyCode= $_GET['cc'];
                            $paidAmount= $_GET['amt'];

                           $paymentData= new PaymentHistory();

                           $paymentData->txn_id= $taxId;
                           $paymentData->user_id= $userId;
                           $paymentData->userName = $userName;
                           $paymentData->currencyCode= $currencyCode;
                           $paymentData->paidAmount= $paidAmount;
                           $paymentData->status= $status;
						   $paymentData->teacher_id= $teacher_id;
                            $paymentData->payment_level= 'S';
							$paymentData->appointment_id= $apt_id;
							

                           $paymentData->save();
                           $insertId= $paymentData->id;

                          if(!empty($insertId)) {
                              $wallet= new Wallet;

                              $wallet->paid_id= $insertId;
                              $wallet->user_id= $teacher_id;
                              $wallet->amount= $paidAmount;
							  $wallet->paid_status = 1;
                              if($wallet->save()){
								$updateAppointment= array(
                                    'is_booked'=>1
                                     );
								AppointmentBooking::where(['id' => $apt_id])->update($updateAppointment);
								
								//$tempApptavailable= TempAppointmentBooking::where(['teacher_id' => $teacher_id, 'user_id'=>$userId])->first();

								//$tempApptavailable->delete();
								$studentResponse = BookedLectures::where(['appointment_id' => $apt_id])->first();
								
								
								 if(!empty($studentResponse->message)){
									 $notMessage = $studentResponse->message;
								 }else{
									$notMessage = 'User Has booked the lecture';
								 }
								$notHeading = 'Lecture Booked';
								$type = 2;
								$lectid = $studentResponse->id;
								$notificationdetail= Helper::saveNotification($userId, $teacher_id, $notMessage, $notHeading, $lectid, $type);
								
								$notificationdetail= Helper::saveNotification($userId, $teacher_id, 'Il pagamento e stato effettuato', 'Payment', $lectid, 6);
								//$totalnotifications = count($notificationdetail); 
    	
							  }
                          }
                        
                          $request->session()->flash('success', 'Il tuo pagamento è andato a buon fine');
                       return redirect('student/booking_listing');
                    }                  

            }else{ 

              return redirect('paypal/cancel');  

            }
    }
	
	public function booking_listing()
    {
	  $userId = Auth::id();
	  $now = date('Y-m-d H:i:s');
	  $teacherFuttureBooked= AppointmentBooking::where(['appointement_booking.student_id' => $userId, 'appointement_booking.is_booked'=>1,'appointement_booking.is_cancel'=>0])
	  ->leftJoin('users', 'users.id', '=', 'appointement_booking.teacher_id')
	  ->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')
	  ->leftJoin('teacher_lessons', 'teacher_lessons.id', '=', 'booked_lectures.lecture_id')
	  ->where('appointement_booking.start_date', '>', $now)
	  ->select('users.first_name','users.last_name','users.email','users.profile_image','teacher_lessons.lesson_name', 'appointement_booking.start_date', 'appointement_booking.end_date', 'appointement_booking.id', 'booked_lectures.id as lid')->get();
	  
	  $teacherPastBooked= AppointmentBooking::where(['appointement_booking.student_id' => $userId, 'appointement_booking.is_booked'=>1, 'appointement_booking.is_cancel'=>0])
	  ->leftJoin('users', 'users.id', '=', 'appointement_booking.teacher_id')
	  ->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')
	  ->leftJoin('teacher_lessons', 'teacher_lessons.id', '=', 'booked_lectures.lecture_id')
	  ->where('appointement_booking.start_date', '<=', $now)
	  ->select('users.first_name','users.last_name','users.email','users.profile_image','teacher_lessons.lesson_name', 'appointement_booking.start_date', 'appointement_booking.end_date', 'appointement_booking.id', 'booked_lectures.id as lid')->get();
	  
	  //echo '<pre>'; print_r($teacherBooked); die;
      return view('student/booking_listing',['teacherBooked'=>$teacherFuttureBooked, 'teacherPastBooked'=>$teacherPastBooked]);

    }
	
	public function lesson_finish()
    {
	  $userId = Auth::id();
      return view('student/lesson_finish');

    }
	
	
	/* public function deleteAppointment()
    {
    	$appointmentid = Input::get('appointmentid');

    	$appointmentid = AppointmentBooking::find($appointmentid);

    	if( $appointmentid->delete() )
    	{
    		$response['resCode']    = 0;
    		$response['resMsg']     = trans('translation.doc_deleted');
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    } */
	
	public function deleteAppointment(Request $request, $id)
    {
    	//$appointmentid = Input::get('appointmentid');
		$updateisCancel= array(
						'is_cancel'=>1
						 );
		AppointmentBooking::where(['id' => $id])->update($updateisCancel);
    	
		$request->session()->flash('success', 'Il tuo pagamento è andato a buon fine');
		return redirect('student/booking_listing');
    	
    	
    }
	
	
	public function getAppointment()
    {
    	$appointmentid = Input::get('appointmentid');
		$userId = Auth::id();
	  $now = date('Y-m-d H:i:s');
	  $teacherFuttureBooked= AppointmentBooking::where(['appointement_booking.id' => $appointmentid])
	  ->leftJoin('users', 'users.id', '=', 'appointement_booking.teacher_id')
	  ->leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')
	  ->leftJoin('teacher_lessons', 'teacher_lessons.id', '=', 'booked_lectures.lecture_id')
	  ->select('users.first_name','users.last_name','users.email','users.profile_image','teacher_lessons.lesson_name', 'appointement_booking.start_date', 'appointement_booking.end_date', 'appointement_booking.id')->first();
	  
		
	  
    	if(!empty($teacherFuttureBooked))
    	{
			$lecturetime = '';
			$to_time = strtotime($teacherFuttureBooked->end_date);
			$from_time = strtotime($teacherFuttureBooked->start_date);
			$lecturetime = round(abs($to_time - $from_time) / 60,2). " minute";
			$imgUrl = url('/images/avatar5.png');

			if( isset( $teacherFuttureBooked->profile_image ))
			{

				$imgUrl = url('/images/teacher_logos/' . $teacherFuttureBooked->profile_image);

			}
			
			$html = '';
			
			$html .= '<form name="frm_delete_appt" id="frm_delete_appt" action="'.url('/student/deleteappointment/' . $teacherFuttureBooked->id).'" autocomplete="off" method="post">'.csrf_field().'<div class="col-sm-2">
					  <p><img style="max-width: 100px;" src="'.$imgUrl.'" /></p>
					</div>
					<div class="col-sm-2">
					  <p>'.$teacherFuttureBooked->lesson_name.'</p>
					</div>
					<div class="col-sm-3">
					  <p>'.date("H:i",strtotime($teacherFuttureBooked->start_date)).'</p>
					  <span>'.$lecturetime.'</span>
					</div>
					<div class="col-sm-3">
					  <p>'.date("d M Y",strtotime($teacherFuttureBooked->start_date)).'</p>
					</div>
					<div class="col-sm-2">
					  <p><a href="javascript:void(0);" id="'.$teacherFuttureBooked->id.'" class="delete_appt1"><span class="glyphicon glyphicon-trash"></span></a></p>
					</div>';
			
    		$response['resCode']    = 0;
    		$response['resMsg']     = $html;
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }

    public function IpnStatus()
    {
      echo 'mukesh';

    }
	
	public function paymentinfo(Request $request) {
		$user = auth()->user();
		if($request->method() == 'POST') {
            $userPayInfo = UserPaymentInfo::firstOrNew(array('user_id' => $user->id));
			$userPayInfo->payment_type  =  Input::get('type_of_payment');
			if(!empty(Input::get('card'))) {
				$userPayInfo->pay_info = json_encode(Input::get('card'));
			}
			if($userPayInfo->save()){
				$request->session()->flash('success', 'Le informazioni di pagamento sono state salvate correttamente.'); 
			}
			return redirect('student/paymentinfo');
		}
		$payinfo = UserPaymentInfo::where(['user_id'=>$user->id])->first();
		return view('student/payment_info',['payinfo'=>$payinfo]);
	}

	public function deleteprofile(Request $request) {
		$user = auth()->user();
		if($request->method() == 'POST') {
			if(Hash::check(Input::get('password'), $user->password)) {
				User::where('id', $user->id)->update(['status' => '0']);
				$request->session()->flash('success', trans('Il profilo è stato cancellato correttamente.'));
				Auth::logout();
				return redirect('login');
			} else {
				$request->session()->flash('danger', trans('Password immessa donot match. Per favore, riprova.'));
				return redirect('student/deleteprofile');
			}
		}
		return view('student/delete_profile');
	}

	public function becomeexpert(Request $request) {
		$user = auth()->user();
		if($user->role == 2) {
			return redirect('student/profile');
		}
		$categories = Categories::where(['parent_id' => 0])->get();
		$userinterest=UserCategories::where('teacher_id', $user->id)->get();
		if($request->method() == 'POST') {
			$catData = [];
			if(!empty($request->input('category'))){
				foreach ($request->input('category') as $cat) {
					$catData[]=[
						'teacher_id' => $user->id,
						'category_id' => $cat
					];
				}
			}
			$updateUserDetail = array(
				'role'=>2
			);
			if(User::where(['id'=>$user->id])->update($updateUserDetail)){
				UserCategories::where('teacher_id', $user->id)->delete();
				if(!empty($catData)) {
					UserCategories::insert($catData);
				}
			}
			$request->session()->flash('success', 'La tua richiesta è stata elaborata correttamente.');
			return redirect('student/profile');
		}
		return view('student/become_expert', ['categories'=>$categories,'userinterest'=>$userinterest]);
	}
	
	public function changePassword(Request $request) {
		$user = auth()->user();
		if($request->method() == 'POST') {
			//echo Input::get('password'); die;
			if(Hash::check(Input::get('password'), $user->password)) {
				User::where('id', $user->id)->update(['password' => Hash::make(Input::get('new_password'))]);
				$request->session()->flash('success', trans('La password è stata aggiornata con successo.'));
				Auth::logout();
				return redirect('/');
			} else {
				$request->session()->flash('danger', trans('Password immessa donot match. Per favore, riprova.'));
				return redirect('student/changepassword');
			}
		}
		return view('student/change_password');
	}
	
	
	
	public function stripePost(Request $request)
    {
		$data=$request->all();

	//	print_r($data); exit;
		$first_amount=$data['amount']*100;
		$second_amount=($first_amount*86)/100;

		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		
		$charge=  Stripe\Charge::create ([
		"amount" => $first_amount,
		"currency" => "eur",
		"source" => $request->stripeToken,
		"description" => "Test payment from Vipin Pandey." 
		]);

		$payment = \Stripe\Charge::create([
		"amount" => $second_amount,
		"currency" => "eur",
		"source" => "tok_visa",
		], ["stripe_account" => "acct_1FNGbODsHoIheCsH"]);

	  
	 $paymentJson = $payment->jsonSerialize();
	 $chargeJson = $charge->jsonSerialize();
	//  print_r($chargeJson);
	// echo "<br>";
	// print_r($paymentJson);
//	exit;

	// for teacher
			Session::flash('success', 'Payment successful!');
			$userId = $data['item_number'];
			$paymentData= new PaymentHistory();
			$paymentData->txn_id= $paymentJson['balance_transaction'];
			$paymentData->user_id= $userId;
			$paymentData->userName = $data['item_name'];
			$paymentData->currencyCode= $paymentJson['currency'];
			$paymentData->paidAmount= $paymentJson['amount']/100;
			$paymentData->status= $paymentJson['status'];
			$paymentData->teacher_id= $data['teacher_id'];
			$paymentData->payment_level= 'S';
			$paymentData->appointment_id= $data['appointment_id'];
			$paymentData->save();
			$insertId= $paymentData->id;
// end for teacher


//start for admin

			$paymentData1= new PaymentHistory();
			$paymentData1->txn_id= $chargeJson['balance_transaction'];
			$paymentData1->user_id= $userId;
			$paymentData1->userName = $data['item_name'];
			$paymentData1->currencyCode= $chargeJson['currency'];
			$paymentData1->paidAmount= ($first_amount*14/100)/100;
			$paymentData1->status= $chargeJson['status'];
			$paymentData1->teacher_id=0;
			$paymentData1->payment_level= 'S';
			$paymentData1->appointment_id= $data['appointment_id'];
			$paymentData1->save();
			$insertId1= $paymentData1->id;
			if(!empty($insertId1)) {
			$wallet= new Wallet;
			$wallet->paid_id= $insertId;
			$wallet->user_id=0;
			$wallet->amount= ($first_amount*14/100)/100;
			$wallet->paid_status = 1;
			$wallet->save();
			}

// end for admin

			if(!empty($insertId)) {
				$wallet= new Wallet;
				$wallet->paid_id= $insertId;
				$wallet->user_id= $data['teacher_id'];
				$wallet->amount=$paymentJson['amount']/100;
				$wallet->paid_status = 1;
				if($wallet->save()){
					$updateAppointment= array(
					'is_booked'=>1
					);
					AppointmentBooking::where(['id' => $data['appointment_id']])->update($updateAppointment);
					$studentResponse = BookedLectures::where(['appointment_id' =>  $data['appointment_id']])->first();
					if(!empty($studentResponse->message)){
						$notMessage = $studentResponse->message;
					}else{
						$notMessage = 'User Has booked the lecture';
					}
						$notHeading = 'Lecture Booked';
						$type = 2;
						$lectid = $studentResponse->id;
						$notificationdetail= Helper::saveNotification($userId, $data['teacher_id'], $notMessage, $notHeading, $lectid, $type);
						$notificationdetail= Helper::saveNotification($userId, $data['teacher_id'], 'Il pagamento e stato effettuato', 'Payment', $lectid, 6);
				}
			}
		//	$request->session()->flash('success', 'Il tuo pagamento è andato a buon fine');
			return redirect('student/booking_listing');


       exit;
     
    }
	
	
}