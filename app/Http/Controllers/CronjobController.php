<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Notification;
use App\AppointmentBooking;
use DateTime;
use DatePeriod;
use DateInterval;

class CronjobController extends Controller
{
	public function lectureReminderNotification(Request $request, $reminderTime)
    {
    	$lecturesForToday = AppointmentBooking::whereRaw("DATE_FORMAT(start_date,'%Y-%m-%d') = '".date('Y-m-d',time())."'")->get();
    	switch ($reminderTime) {
    		case 'onehour':
    			$notificationTime=60;
    			break;
    		case 'thirtyminute':
    			$notificationTime=30;
    			break;
    		case 'tenminute':
    			$notificationTime=10;
    			break;
    	}
    	foreach ($lecturesForToday as $lecture) {
    		$datetime1 = new DateTime();
			$datetime2 = new DateTime($lecture->start_date);
			if(strtotime($lecture->start_date) > time()) {
				$interval = $datetime1->diff($datetime2);
				$minutesRemaining = ($interval->h*60) + $interval->i;
				if($minutesRemaining == $notificationTime) {
					$notification = new Notification();
					$notification->user_id = $lecture->student_id;
					$notification->priority_id = 1;
					$notification->notification_source = 2;
					$notification->heading = 'Promemoria della conferenza';
					$notification->description = $notificationTime.' minuti rimanenti per iniziare la lezione.';
					$notification->save();
				}
			}
    	}
    	exit;
    }
}