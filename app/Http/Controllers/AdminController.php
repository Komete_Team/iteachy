<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\AppointmentBooking;
use App\User;
use App\Categories; 
use App\Role;
use App\Permission;
use App\Questionnaire;
use App\StudentQuestionnaire;
use App\School;
use App\SchoolDocument;
use App\DocumentType;
use App\TeacherDocument;
use App\CalendarEvent;
use App\NotificationPriority;
use App\Notification;

use Validator;
use Mail;

class AdminController extends Controller
{
    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
	
    	return view('admin/dashboard');
    }

    /**
     * Function to return dashboard page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
    	Auth::logout();
    	return redirect('admin');
    }

    /**
     * Function to return user role page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
		return view('admin/roles');
    }

    /**
     * Function to fetch the roles listing
     * @param void
     * @return array
     */
    public function fetchRoles()
    {
    	$start      = Input::get('iDisplayStart');      // Offset
    	$length     = Input::get('iDisplayLength');     // Limit
    	$sSearch    = Input::get('sSearch');            // Search string
    	$col        = Input::get('iSortCol_0');         // Column number for sorting
    	$sortType   = Input::get('sSortDir_0');         // Sort type

    	// Datatable column number to table column name mapping
        $arr = array(
            0 => 'id',
            1 => 'name',
            2 => 'display_name',
            3 => 'description'
        );

        // Map the sorting column index to the column name
        $sortBy = $arr[$col];

        // Get the records after applying the datatable filters
        $roles = Role::where('name','like', '%'.$sSearch.'%')
        			->orWhere('display_name','like', '%'.$sSearch.'%')
                    ->orderBy($sortBy, $sortType)
                    ->limit($length)
                    ->offset($start)
                    ->get();

        $iTotal = Role::where('name','like', '%'.$sSearch.'%')->orWhere('display_name','like', '%'.$sSearch.'%')->count();

        // Create the datatable response array
        $response = array(
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iTotal,
            'aaData' => array()
        );

        $k=0;
        if ( count( $roles ) > 0 )
        {
            foreach ($roles as $role)
            {
            	$response['aaData'][$k] = array(
                    0 => $role->id,
                    1 => $role->name,
                    2 => $role->display_name,
                    3 => $role->description,
                    4 => '<a href="javascript:void(0);" id="'. $role->id .'" class="edit_role"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> | <a href="javascript:void(0);" id="'. $role->id .'" class="delete_role"><i class="fa fa-trash-o"></i></a>'
                );
                $k++;
            }
        }

    	return response()->json($response);
    }

    /**
     * Function to save role details
     * @param void
     * @return array
     */
    public function saveRoleDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $roleDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'name'			=> $roleDetails['name'],
		        'display_name' 	=> $roleDetails['display_name']
		    ),
		    array(
		        'name' 			=> array('required'),
		        'display_name'	=> array('required'),
		    ),
		    array(
		        'name.required' 		=> 'Please enter keyword',
		        'display_name.required'	=> 'Please enter name',
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			if( $roleDetails['role_id'] == '' )		// New record, save it
			{
				$role = new Role;

		        $role->name 		= $roleDetails['name'];
		        $role->display_name = $roleDetails['display_name'];
		        $role->description 	= $roleDetails['description'];
		        $role->created_at 	= date('Y-m-d H:i:s');

		        if( $role->save() )
		        {
		        	$response['resCode']    = 0;
		        	$response['resMsg']     = 'Role added successfully';
		        }
		        else
		        {
		        	$response['resCode']    = 2;
		        	$response['resMsg']     = 'Internal server error';
		        }
			}
			else 									// Existing record, update it
			{
				$role = Role::where(['id' => $roleDetails['role_id']])->first();

		        $role->name 		= $roleDetails['name'];
		        $role->display_name = $roleDetails['display_name'];
		        $role->description 	= $roleDetails['description'];
		        $role->created_at 	= date('Y-m-d H:i:s');

		        if( $role->update() )
		        {
		        	$response['resCode']    = 0;
		        	$response['resMsg']     = 'Role details updated successfully';
		        }
		        else
		        {
		        	$response['resCode']    = 2;
		        	$response['resMsg']     = 'Internal server error';
		        }
			}
		}

		return response()->json($response);
	}

	/**
     * Function to fetch the selected role details
     * @param void
     * @return array
     */
    public function getRoleDetails()
    {
    	$roleId = Input::get('roleId');

    	$roleDetails = Role::find($roleId);

    	return response()->json($roleDetails);
    }

    /**
     * Function to delete role
     * @param void
     * @return array
     */
    public function deleteRole()
    {
    	$roleId = Input::get('roleId');

    	$role = Role::find($roleId);

    	if( $role->delete() )
    	{
    		$response['resCode']    = 0;
    		$response['resMsg']     = 'Role deleted successfully';
    	}
    	else
    	{
    		$response['resCode']    = 2;
    		$response['resMsg']     = 'Internal server error';
    	}

    	return response()->json($response);
    }

    /**
     * Function to return school listing page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function schools()
    {
		return view('admin/schools');
    }
	
	public function categories()
    {
		// Get the language list
    	$categories = Categories::where(['parent_id' => 0])->get();
		return view('admin/categories', ['categories' => $categories]);
    }
	
	public function subCategories($id)
    {
		// Get the subcategories list
    	$subcategories = Categories::where(['parent_id' => $id])->get();
		$categories = Categories::where(['parent_id' => 0])->get();
		return view('admin/subcategories', ['subcategories' => $subcategories, 'categories' => $categories]);
    }
	
	

    /**
     * Function to return add/edit school page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function addSchool($schoolId=null) 
    {
    	$schoolDetails 	= array();
    	$schoolDocuments= array();
		$docAttributes= array();

    	if( !is_null( $schoolId ) )
    	{
	    	$schoolId = base64_decode($schoolId);

	    	// Get the school details if the schoolId is available
	    	$schoolDetails 	= School::find($schoolId);

	    	// Get the school documents (if available)
	    	$schoolDocuments= $schoolDetails->documents()->get();

	    	// Get the list of document types and their icons
	    	$docAttributes = array();
	    	$documentTypes = DocumentType::get();
	    	if( !is_null( $documentTypes ) && ( $documentTypes->count() > 0 ) )
	    	{
	    		foreach ($documentTypes as $documentType)
	    		{
	    			$docAttributes[$documentType->document_type] = $documentType->document_icon;
	    		}
	    	}
    	}

		return view('admin/addschool', ['schoolDetails' => $schoolDetails, 'schoolDocuments' => $schoolDocuments, 'docAttributes' => $docAttributes]);
    }
	
	/**
     * Function to return add/edit language page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function addLanguages($schoolId=null)
    {
    	$schoolDetails 	= array();
    	$schoolDocuments= array();

    	if( !is_null( $schoolId ) )
    	{
	    	$schoolId = base64_decode($schoolId);

	    	// Get the school details if the schoolId is available
	    	$schoolDetails 	= School::find($schoolId);

	    	// Get the school documents (if available)
	    	$schoolDocuments= $schoolDetails->documents()->get();

	    	// Get the list of document types and their icons
	    	$docAttributes = array();
	    	$documentTypes = DocumentType::get();
	    	if( !is_null( $documentTypes ) && ( $documentTypes->count() > 0 ) )
	    	{
	    		foreach ($documentTypes as $documentType)
	    		{
	    			$docAttributes[$documentType->document_type] = $documentType->document_icon;
	    		}
	    	}
    	}

		return view('admin/addschool', ['schoolDetails' => $schoolDetails, 'schoolDocuments' => $schoolDocuments, 'docAttributes' => $docAttributes]);
    }
	
	

    /**
     * Function to save school details
     * @param void
     * @return array
     */
    public function saveSchoolDetails()
    {
    	// Get the serialized form data
        $formData = Input::get();

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'school_name' 				=> $formData['school_name'],
		        'enrollment_no'				=> $formData['enrollment_no'],
		        'address' 					=> $formData['address'],
		        'city' 						=> $formData['city'],
		        'province' 					=> $formData['province'],
		        'country' 					=> $formData['country'],
		        'principal_fname' 			=> $formData['principal_fname'],
		        'principal_lname' 			=> $formData['principal_lname'],
		        'principal_email' 			=> $formData['principal_email'],
		        'principal_contact_no' 		=> $formData['principal_contact_no'],
		        'contact_person_fname' 		=> $formData['contact_person_fname'],
		        'contact_person_lname' 		=> $formData['contact_person_lname'],
		        'contact_person_email' 		=> $formData['contact_person_email'],
		        'contact_person_contact_no' => $formData['contact_person_contact_no'],
		        'status' 					=> $formData['status']
		    ),
		    array(
		        'school_name' 				=> array('required'),
		        'enrollment_no' 			=> array('required'),
		        'address' 					=> array('required'),
		        'city' 						=> array('required'),
		        'province' 					=> array('required'),
		        'country' 					=> array('required'),
		        'principal_fname' 			=> array('required'),
		        'principal_lname' 			=> array('required'),
		        'principal_email' 			=> array('required'),
		        'principal_contact_no' 		=> array('required'),
		        'contact_person_fname' 		=> array('required'),
		        'contact_person_lname' 		=> array('required'),
		        'contact_person_email' 		=> array('required'),
		        'contact_person_contact_no' => array('required'),
		        'status' 					=> array('required'),
		    ),
		    array(
		        'school_name.required' 				=> 'Please enter school name',
		        'enrollment_no.required' 			=> 'Please enter enrollment no',
		        'address.required' 					=> 'Please enter address',
		        'city.required' 					=> 'Please enter city',
		        'province.required' 				=> 'Please enter province/state',
		        'country.required' 					=> 'Please enter country',
		        'principal_fname.required' 			=> 'Please enter principal first name',
		        'principal_lname.required' 			=> 'Please enter principal last name',
		        'principal_email.required' 			=> 'Please enter principal email',
		        'principal_contact_no.required' 	=> 'Please enter principal contact number',
		        'contact_person_fname.required' 	=> 'Please enter contact person first name',
		        'contact_person_lname.required' 	=> 'Please enter contact person last name',
		        'contact_person_email.required' 	=> 'Please enter contact person email',
		        'contact_person_contact_no.required'=> 'Please enter contact person contact number',
		        'status.required' 					=> 'Please select status'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			if( $formData['school_id'] == '' )	// Add the school
			{
				// Check if the principal & contact person email is unique or not
				$uniqueEmail =  School::where(['principal_email' => $formData['principal_email']])
								->orWhere(['contact_person_email' => $formData['contact_person_email']])
								->first();
				
				if( !is_null( $uniqueEmail ) )
				{
					if( isset( $uniqueEmail->principal_email ) && ( $uniqueEmail->principal_email == $formData['principal_email'] ) )
					{
						$response['resCode']    = 3;
			        	$response['resMsg']     = 'The entered email id for principal already exist';
					}
					else if( isset( $uniqueEmail->contact_person_email ) && ( $uniqueEmail->contact_person_email == $formData['contact_person_email'] ) )
					{
						$response['resCode']    = 4;
			        	$response['resMsg']     = 'The entered email id for contact person already exist';
					}
				}
				else
				{
					$schoolLogo = Input::file('school_logo');
					$documents 	= Input::file('documents');

					// For school logo
					if( !is_null( $schoolLogo ) && ( $schoolLogo->getSize() > 0 ) )
					{
					    // Image destination folder
					    $destinationPath = storage_path() . '/uploads/schools/logos';
					    if( $schoolLogo->isValid() )  // If the file is valid or not
					    {
					        $fileExt  = $schoolLogo->getClientOriginalExtension();
					        $fileType = $schoolLogo->getMimeType();
					        $fileSize = $schoolLogo->getSize();

					        if( ( $fileType == 'image/jpeg' || $fileType == 'image/jpg' || $fileType == 'image/png' ) && $fileSize <= 2000000 ) // 2 MB
					        {
					            // Rename the file
					            $logoFileName = str_random(20) . '.' . $fileExt;

					            $schoolLogo->move( $destinationPath, $logoFileName );
					    	}
					    }
					}

					$loggedInUserId = Auth::id();

					$school = new School;

					$school->school_name 		= $formData['school_name'];
					$school->enrollment_number 	= $formData['enrollment_no'];
					$school->address 			= $formData['address'];
					$school->city 				= $formData['city'];
					$school->province 			= $formData['province'];
					$school->country 			= $formData['country'];
					
					if( !is_null( Input::file('school_logo') ) )
					{
						$school->logo_image_name 	= $logoFileName;
					}
					
					$school->principal_fname 		= $formData['principal_fname'];
					$school->principal_lname 		= $formData['principal_lname'];
					$school->principal_contact_no 	= $formData['principal_contact_no'];
					$school->principal_email 		= $formData['principal_email'];

					$school->contact_person_fname 		= $formData['contact_person_fname'];
					$school->contact_person_lname 		= $formData['contact_person_lname'];
					$school->contact_person_contact_no 	= $formData['contact_person_contact_no'];
					$school->contact_person_email 		= $formData['contact_person_email'];

					$school->status = $formData['status'];
					$school->created_at = date('Y-m-d H:i:s');
					$school->created_by = $loggedInUserId;

					if( $school->save() )
					{
						// For school documents
						if( !is_null( $documents ) )
						{
						    $schoolDocuments = array();

						    foreach( $documents as $document )
						    {
						    	if( !is_null( $document ) && ( $document->getSize() > 0 ) )
						    	{
						    	    // Image destination folder
						    	    $destinationPath = storage_path() . '/uploads/schools/documents';
						    	    if( $document->isValid() )  // If the file is valid or not
						    	    {
						    	        $fileExt  = $document->getClientOriginalExtension();
						    	        $fileType = $document->getMimeType();
						    	        $fileSize = $document->getSize();

						    	        $allowedFileType = DocumentType::get();
						    	        $allowedFileType = $allowedFileType->toArray();
						    	        $documentTypes = array_column($allowedFileType, 'document_type');

						    	        if( in_array($fileType, $documentTypes) && $fileSize <= 2000000 ) // 2 MB
						    	        {
						    	            // Rename the file
						    	            $docName = str_random(20) . '.' . $fileExt;

						    	            if( $document->move( $destinationPath, $docName ) )
						    	            {
						    	            	$schoolDocuments[] = array(
						    	            		'document_name' => $docName,
						    	            		'document_type' => $fileType,
						    	            		'uploaded_at'	=> date('Y-m-d H:i:s'),
						    	            		'uploaded_by' 	=> $loggedInUserId
						    	            	);
						    	            }
						    	    	}
						    	    }
						    	}
						    }

						    // Save school related documents
						    $school->documents()->createMany($schoolDocuments);
						}

						$response['resCode']    = 0;
						$response['resMsg']     = 'School details saved successfully';
					}
					else
					{
						$response['resCode']    = 2;
						$response['resMsg']     = 'Server error';
					}
				}
			}
			else
			{
				$schoolLogo = Input::file('school_logo');
				$documents 	= Input::file('documents');

				// For school logo
				if( !is_null( $schoolLogo ) && ( $schoolLogo->getSize() > 0 ) )
				{
				    // Image destination folder
				    $destinationPath = storage_path() . '/uploads/schools/logos';
				    if( $schoolLogo->isValid() )  // If the file is valid or not
				    {
				        $fileExt  = $schoolLogo->getClientOriginalExtension();
				        $fileType = $schoolLogo->getMimeType();
				        $fileSize = $schoolLogo->getSize();

				        if( ( $fileType == 'image/jpeg' || $fileType == 'image/jpg' || $fileType == 'image/png' ) && $fileSize <= 2000000 ) // 2 MB
				        {
				            // Rename the file
				            $logoFileName = str_random(20) . '.' . $fileExt;

				            $schoolLogo->move( $destinationPath, $logoFileName );
				    	}
				    }
				}

				$loggedInUserId = Auth::id();

				$school = School::find($formData['school_id']);

				$school->school_name 		= $formData['school_name'];
				$school->enrollment_number 	= $formData['enrollment_no'];
				$school->address 			= $formData['address'];
				$school->city 				= $formData['city'];
				$school->province 			= $formData['province'];
				$school->country 			= $formData['country'];
				
				if( !is_null( Input::file('school_logo') ) )
				{
					$school->logo_image_name 	= $logoFileName;
				}
				
				$school->principal_fname 		= $formData['principal_fname'];
				$school->principal_lname 		= $formData['principal_lname'];
				$school->principal_contact_no 	= $formData['principal_contact_no'];
				$school->principal_email 		= $formData['principal_email'];

				$school->contact_person_fname 		= $formData['contact_person_fname'];
				$school->contact_person_lname 		= $formData['contact_person_lname'];
				$school->contact_person_contact_no 	= $formData['contact_person_contact_no'];
				$school->contact_person_email 		= $formData['contact_person_email'];

				$school->status = $formData['status'];
				$school->created_at = date('Y-m-d H:i:s');
				$school->created_by = $loggedInUserId;

				if( $school->save() )
				{
					// For school documents
					if( !is_null( $documents ) )
					{
					    $schoolDocuments = array();

					    foreach( $documents as $document )
					    {
					    	if( !is_null( $document ) && ( $document->getSize() > 0 ) )
					    	{
					    	    // Image destination folder
					    	    $destinationPath = storage_path() . '/uploads/schools/documents';
					    	    if( $document->isValid() )  // If the file is valid or not
					    	    {
					    	        $fileExt  = $document->getClientOriginalExtension();
					    	        $fileType = $document->getMimeType();
					    	        $fileSize = $document->getSize();

					    	        $allowedFileType = DocumentType::get();
					    	        $allowedFileType = $allowedFileType->toArray();
					    	        $documentTypes = array_column($allowedFileType, 'document_type');

					    	        if( in_array($fileType, $documentTypes) && $fileSize <= 2000000 ) // 2 MB
					    	        {
					    	            // Rename the file
					    	            $docName = str_random(20) . '.' . $fileExt;

					    	            if( $document->move( $destinationPath, $docName ) )
					    	            {
					    	            	$schoolDocuments[] = array(
					    	            		'document_name' => $docName,
					    	            		'document_type' => $fileType,
					    	            		'uploaded_at'	=> date('Y-m-d H:i:s'),
					    	            		'uploaded_by' 	=> $loggedInUserId
					    	            	);
					    	            }
					    	    	}
					    	    }
					    	}
					    }

					    // Save school related documents
					    $school->documents()->createMany($schoolDocuments);
					}

					$response['resCode']    = 0;
					$response['resMsg']     = 'School details saved successfully';
				}
			}
		}

		return response()->json($response);
	}

	/**
	 * Function to fetch the roles listing
	 * @param void
	 * @return array
	 */
	public function fetchSchools()
	{
		$start      = Input::get('iDisplayStart');      // Offset
		$length     = Input::get('iDisplayLength');     // Limit
		$sSearch    = Input::get('sSearch');            // Search string
		$col        = Input::get('iSortCol_0');         // Column number for sorting
		$sortType   = Input::get('sSortDir_0');         // Sort type

		// Datatable column number to table column name mapping
	    $arr = array(
	        0 => 'id',
	        1 => 'school_name',
	        2 => 'enrollment_number',
	        3 => 'address',
	        4 => 'city',
	        5 => 'province',
	        6 => 'country'
	    );

	    // Map the sorting column index to the column name
	    $sortBy = $arr[$col];

	    // Get the records after applying the datatable filters
	    $schools = School::where('school_name','like', '%'.$sSearch.'%')
	    			->orWhere('city','like', '%'.$sSearch.'%')
	    			->orWhere('province','like', '%'.$sSearch.'%')
	    			->orWhere('country','like', '%'.$sSearch.'%')
	                ->orderBy($sortBy, $sortType)
	                ->limit($length)
	                ->offset($start)
	                ->get();

	    $iTotal = School::where('school_name','like', '%'.$sSearch.'%')
	    		->orWhere('city','like', '%'.$sSearch.'%')
	    		->orWhere('province','like', '%'.$sSearch.'%')
	    		->orWhere('country','like', '%'.$sSearch.'%')
	    		->count();

	    // Create the datatable response array
	    $response = array(
	        'iTotalRecords' => $iTotal,
	        'iTotalDisplayRecords' => $iTotal,
	        'aaData' => array()
	    );

	    $k=0;
	    if ( count( $schools ) > 0 )
	    {
	        foreach ($schools as $school)
	        {
	        	$checked = '';
	        	$verificationText = 'Pending';
	        	if( $school->verified == '1' )
	        	{
	        		$verificationText = 'Verified';
	        		$checked = 'checked';
	        	}

	        	$response['aaData'][$k] = array(
	                0 => $school->id,
	                1 => $school->school_name,
	                2 => $school->enrollment_number,
	                3 => $school->address,
	                4 => $school->city,
	                5 => $school->province,
	                6 => $school->country,
	                7 => '<label class="switch" title="'. $verificationText .'"><input id="'. $school->id .'" class="toggle_verification" type="checkbox" '. $checked .'><span class="slider round"></span></label>',
	                8 => '<a href="'. url('/admin/school/' . base64_encode($school->id)) .'" id="'. $school->id .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> | <a href="javascript:void(0);" id="'. $school->id .'" class="delete_school"><i class="fa fa-trash-o"></i></a>'
	            );
	            $k++;
	        }
	    }

		return response()->json($response);
	}

	/**
	 * Function to update the school verification status
	 * @param void
	 * @return array
	 */
	public function verifySchool()
	{
		$schoolId = Input::get('schoolId');
		$verificationStatus = Input::get('verificationStatus');

		$schoolDetails 	= School::find($schoolId);
		$userDetails 	= $schoolDetails->user()->first();

		$response = array();
		try
		{
			if( School::where(['id' => $schoolId])->update(['verified' => $verificationStatus]) )
			{
				if( $verificationStatus == '1' )
				{
					// Send the email when verification is successful
					Mail::send('emails.schoolVerified', ['userDetails' => $userDetails], function($message) use ($userDetails) {
						$message->to($userDetails->email, $userDetails->name)->subject('School Verified Successfully');
						$message->from('testvirtual8@gmail.com', 'OAKLAND NATIVES GIVE BACK');
					});
				}

				$response['resCode']    = 0;
			    $response['resMsg']     = ( $verificationStatus ) ? 'Verified Successfully': 'Unverified Successfully';
			}
			else
			{
				$response['resCode']    = 1;
			    $response['resMsg']     = 'Server error!';
			}
		}
		catch(\Exception $e)
		{
			$response['resCode']    = 2;
		    $response['resMsg']     = 'Server error!';
		}

		return response()->json($response);
	}

	/**
     * Function to return calender events page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function calendarEvents()
    {
    	// Get all the existing calender events
    	$calendarEvents = CalendarEvent::get();

    	return view('admin/calendarEvents', ['calendarEvents' => $calendarEvents]);
    }

    /**
     * Function to save calender event details
     * @param void
     * @return array
     */
    public function saveEventDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $eventDetails);

        $response =array();

        // Server Side Validation
		$validation = Validator::make(
		    array(
		        'event_name' 		=> $eventDetails['event_name'],
		        'event_start_hour' 	=> $eventDetails['event_start_hour'],
		        'event_start_minute'=> $eventDetails['event_start_minute'],
		        'event_description' => $eventDetails['event_description']
		    ),
		    array(
		        'event_name' 		=> array('required'),
		        'event_start_hour' 	=> array('required'),
		        'event_start_minute'=> array('required'),
		        'event_description' => array('required')
		    ),
		    array(
		        'event_name.required'			=> 'Please enter event name',
		        'event_start_hour.required'		=> 'Please select time',
		        'event_start_minute.required'	=> 'Please select time',
		        'event_description.required'	=> 'Please enter event description'
		    )
		);

		if ( $validation->fails() )		// Some data is not valid as per the defined rules
		{
			$error = $validation->errors()->first();

		    if( isset( $error ) && !empty( $error ) )
		    {
		        $response['resCode']    = 1;
		        $response['resMsg']     = $error;
		    }
		}
		else 							// The data is valid, go ahead and save it
		{
			$loggedInUserId = Auth::id();

			if( $eventDetails['event_id'] == '' )	// Event id is not available, save the data
			{
				$calendarEvents = new CalendarEvent();

				$calendarEvents->event_name 		= $eventDetails['event_name'];
				$calendarEvents->event_date 		= date('Y-m-d', strtotime($eventDetails['event_date']));
				$calendarEvents->event_start_time 	= date('H:i:s', strtotime($eventDetails['event_start_hour'] . ':' . $eventDetails['event_start_minute'] . ':00'));
				$calendarEvents->event_description 	= $eventDetails['event_description'];
				$calendarEvents->event_source 		= '1';
				$calendarEvents->created_at 		= date('Y-m-d H:i:s');
				$calendarEvents->created_by 		= $loggedInUserId;

				try
				{
					if( $calendarEvents->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Events details saved successfully';
						$response['calendarEvents'] = $calendarEvents;
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
			else
			{
				$calendarEvents = CalendarEvent::where(['id' => $eventDetails['event_id']])->first();

				$calendarEvents->event_name 		= $eventDetails['event_name'];
				$calendarEvents->event_start_time 	= date('H:i:s', strtotime($eventDetails['event_start_hour'] . ':' . $eventDetails['event_start_minute'] . ':00'));
				$calendarEvents->event_description 	= $eventDetails['event_description'];
				$calendarEvents->updated_at 		= date('Y-m-d H:i:s');
				$calendarEvents->updated_by 		= $loggedInUserId;

				try
				{
					if( $calendarEvents->save() )
					{
						$response['resCode']    	= 0;
						$response['resMsg']     	= 'Events details updated successfully';
					}
					else
					{
						$response['resCode']    = 1;
					    $response['resMsg']     = 'Server error!';
					}
				}
				catch(\Exception $e)
				{
					$response['resCode']    = 2;
				    $response['resMsg']     = 'Server error!';
				}
			}
		}

		return response()->json($response);
	}

	/**
     * Function to return event details
     * @param void
     * @return array
     */
    public function getEventDetails()
    {
    	$eventId = Input::get('eventId');

    	$eventDetails = CalendarEvent::where(['id' => $eventId])->first();

    	$response = array();
    	if( !is_null( $eventDetails ) && ( $eventDetails->count() > 0 ) )
    	{
    		$response = array(
    			'id' 			=> $eventDetails->id,
    			'name' 			=> $eventDetails->event_name,
    			'date' 			=> $eventDetails->event_date,
    			'description' 	=> $eventDetails->event_description,
    			'source' 		=> $eventDetails->event_source,
    			'start_hour' 	=> date('H', strtotime($eventDetails->event_start_time)),
    			'start_minute' 	=> date('i', strtotime($eventDetails->event_start_time))
    		);
    	}

    	return response()->json($response);
    }

    /**
     * Function to return notification page
     * @param void
     * @return \Illuminate\Http\Response
     */
    public function notifications()
    {
    	// Get the priority list
    	$priorities = NotificationPriority::get();
		$notifications= Notification::leftJoin('users', 'users.id', '=', 'notifications.user_id')->select('users.first_name','users.last_name','users.email','users.profile_image','users.role', 'notifications.*')->get();
    	// Get the ALI user's list
    	$users = User::where('role', '!=', 1)->get();
		//echo '<pre>'; print_r($users); die;
    	return view('admin/notifications', ['priorities' => $priorities, 'users' => $users, 'notifications'=>$notifications]);
    }
	
	public function teachers()
    {
    	// Get the priority list
    	$teachers = User::where(['role' => 2])->get();

    	return view('admin/teachers', ['teachers' => $teachers]);
    }
	
	
	
	
	/**
     * Function to save sub category details
     * @param void
     * @return array
     */
    public function saveSubCategoryDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $categoryDetails);

        $response =array();
		
		//echo '<pre>'; print_r($categoryDetails); die('bob');

		if( isset( $categoryDetails['categories'] ) )
        {
	        // Server Side Validation
			$validation = Validator::make(
			    array(
			        'sub_category_name' 		=> $categoryDetails['sub_category_name'],
			    ),
			    array(
			        'sub_category_name' 		=> array('required')
			    ),
			    array(
			        'sub_category_name.required'		=> 'Please enter subcategory name'
			    )
			);

			if ( $validation->fails() )		// Some data is not valid as per the defined rules
			{
				$error = $validation->errors()->first();

			    if( isset( $error ) && !empty( $error ) )
			    {
			        $response['resCode']    = 1;
			        $response['resMsg']     = $error;
			    }
			}
			else 							// The data is valid, go ahead and save it
			{
				$loggedInUserId = Auth::id();

				if($categoryDetails['sub_category_id'] == ''){
					$subcategories = [];
					
					foreach( $categoryDetails['categories'] as $subcat )
					{
						$insert= new Categories();
						$insert->category_name = $categoryDetails['sub_category_name'];
						$insert->parent_id = $subcat;
						
						
						try
						{
							if( $insert->save() )
							{
								$response['resCode']    	= 0;
								$response['resMsg']     	= trans('translation.subcategory_saved');
							}
							else
							{
								$response['resCode']    = 2;
								$response['resMsg']     = trans('translation.error');
							}
						}
						catch(\Exception $e)
						{
							$response['resCode']    = 3;
							$response['resMsg']     = trans('translation.error');
						}
					}
//echo '<pre>'; print_r($subcategories); die;
					
				}else{
					$subcategories = [];
					
					foreach( $categoryDetails['categories'] as $subcat )
					{
						$category = Categories::where(['id' => $categoryDetails['sub_category_id']])->first();
						//echo '<pre>'; print_r($teacher); die;
						$category->category_name 	= $categoryDetails['sub_category_name'];
						$category->parent_id 	= $subcat;
						//$teacher->send_email 	= isset( $notificationDetails['email_notification'] ) ? '1': '0';
					
						try
						{
							if( $category->save() )
							{
								$response['resCode']    	= 0;
								$response['resMsg']     	= trans('translation.updated');
							}
							else
							{
								$response['resCode']    = 2;
								$response['resMsg']     = trans('translation.error');
							}
						}
						catch(\Exception $e)
						{
							$response['resCode']    = 3;
							$response['resMsg']     = trans('translation.error');
						}
					}
				}
				
			}
        }
        else
        {
        	$response['resCode']    = 1;
			$response['resMsg']     = trans('translation.enter_subcategory');
        }

		return response()->json($response); 
	}
	
	
	/**
     * Function to save category details
     * @param void
     * @return array
     */
    public function saveCategoryDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $categoryDetails);

        $response =array();
		
		//echo '<pre>'; print_r($teacherDetails); die('bob');

         if( !empty( $categoryDetails ) )
        {
	        // Server Side Validation
			$validation = Validator::make(
			    array(
			        'category_name' 		=> $categoryDetails['category_name'],
			    ),
			    array(
			        'category_name' 		=> array('required')
			    ),
			    array(
			        'category_name.required'		=> trans('translation.category_required')
			    )
			);

			if ( $validation->fails() )		// Some data is not valid as per the defined rules
			{
				$error = $validation->errors()->first();

			    if( isset( $error ) && !empty( $error ) )
			    {
			        $response['resCode']    = 1;
			        $response['resMsg']     = $error;
			    }
			}
			else 							// The data is valid, go ahead and save it
			{
				$loggedInUserId = Auth::id();

				if($categoryDetails['category_id'] == ''){

						$insert= new Categories();
						$insert->category_name= $categoryDetails['category_name'];
						
						
					try
					{
						if( $insert->save() )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= trans('translation.category_saved');
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}else{
					$category = Categories::where(['id' => $categoryDetails['category_id']])->first();
					//echo '<pre>'; print_r($teacher); die;
					$category->category_name 	= $categoryDetails['category_name'];
					//$teacher->send_email 	= isset( $notificationDetails['email_notification'] ) ? '1': '0';
				

					try
					{
						if( $category->save() )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= trans('translation.updated');
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}
				
			}
        }
        else
        {
        	$response['resCode']    = 1;
			$response['resMsg']     = trans('translation.category_required');
        }

		return response()->json($response); 
	}
	
	
	/**
     * Function to save teacher details
     * @param void
     * @return array
     */
    public function saveTeacherDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $teacherDetails);

        $response =array();
		
		//echo '<pre>'; print_r($teacherDetails); die('bob');

         if( !empty( $teacherDetails ) )
        {
	        // Server Side Validation
			$validation = Validator::make(
			    array(
			        'first_name' 		=> $teacherDetails['first_name'],
			        'last_name'		=> $teacherDetails['last_name'],
			        'email' 	=> $teacherDetails['email'],
					'password' 	=> $teacherDetails['password']
			    ),
			    array(
			        'first_name' 		=> array('required'),
			        'last_name' 		=> array('required'),
			        'email'	=> array('required'),
					'password' 		=> array('required'),
			    ),
			    array(
			        'first_name.required'		=> trans('translation.required'),
			        'last_name.required'		=> trans('translation.required'),
			        'email.required'	=> trans('translation.required'),
					'password.required'	=> trans('translation.required')
			    )
			);

			if ( $validation->fails() )		// Some data is not valid as per the defined rules
			{
				$error = $validation->errors()->first();

			    if( isset( $error ) && !empty( $error ) )
			    {
			        $response['resCode']    = 1;
			        $response['resMsg']     = $error;
			    }
			}
			else 							// The data is valid, go ahead and save it
			{
				$loggedInUserId = Auth::id();

				if($teacherDetails['teacher_id'] == ''){

						$insert= new User();
						$insert->first_name= $teacherDetails['first_name'];
						$insert->last_name= $teacherDetails['last_name'];
						$insert->email= $teacherDetails['email'];
						$insert->status= $teacherDetails['status'];
						$insert->password= Hash::make($teacherDetails['password']);
						$insert->role= 2;
						
						
					try
					{
						if( $insert->save() )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= trans('translation.saved');
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}else{
					$teacher = User::where(['id' => $teacherDetails['teacher_id']])->first();
					//echo '<pre>'; print_r($teacher); die;
					$teacher->first_name 	= $teacherDetails['first_name'];
					$teacher->last_name = $teacherDetails['last_name'];
					$teacher->email 	= $teacherDetails['email'];
					$teacher->status 	= $teacherDetails['status'];
					$teacher->password 	= Hash::make($teacherDetails['password']);
					//$teacher->send_email 	= isset( $notificationDetails['email_notification'] ) ? '1': '0';
				

					try
					{
						if( $teacher->save() )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= trans('translation.updated');
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}
				
			}
        }
        else
        {
        	$response['resCode']    = 1;
			$response['resMsg']     = 'Please select atleast one user';
        }

		return response()->json($response); 
	}

	
	

    /**
     * Function to save notification details
     * @param void
     * @return array
     */
    public function saveNotificationDetails()
    {
    	// Get the serialized form data
        $frmData = Input::get('frmData');

        // Parse the serialize form data to an array
        parse_str($frmData, $notificationDetails);

        $response =array();

        if( isset( $notificationDetails['users'] ) )
        {
	        // Server Side Validation
			$validation = Validator::make(
			    array(
			        'priority' 		=> $notificationDetails['priority'],
			        'heading'		=> $notificationDetails['heading'],
			        'description' 	=> $notificationDetails['description']
			    ),
			    array(
			        'priority' 		=> array('required'),
			        'heading' 		=> array('required'),
			        'description'	=> array('required')
			    ),
			    array(
			        'priority.required'		=> 'Please select priority',
			        'heading.required'		=> 'Please enter heading',
			        'description.required'	=> 'Please enter description'
			    )
			);

			if ( $validation->fails() )		// Some data is not valid as per the defined rules
			{
				$error = $validation->errors()->first();

			    if( isset( $error ) && !empty( $error ) )
			    {
			        $response['resCode']    = 1;
			        $response['resMsg']     = $error;
			    }
			}
			else 							// The data is valid, go ahead and save it
			{
				$loggedInUserId = Auth::id();

				if( $notificationDetails['notification_id'] == '' )	// If notification id is not available, save the data
				{
					$notifications = array();

					foreach( $notificationDetails['users'] as $user )
					{
						$notifications[] = array(
							'user_id' 				=> $user,
							'priority_id' 			=> $notificationDetails['priority'],
							'heading' 				=> $notificationDetails['heading'],
							'description' 			=> $notificationDetails['description'],
							'send_email' 			=> isset( $notificationDetails['email_notification'] ) ? '1': '0',
							'notification_source' 	=> '1',
							'created_at' 			=> date('Y-m-d H:i:s'),
							'created_by' 			=> $loggedInUserId
						);
					}

					try
					{
						if( Notification::insert( $notifications ) )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= 'Notification saved successfully';
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}
				else 	// Upadte the data
				{
					$notification = Notification::where(['id' => $notificationDetails['notification_id']])->first();

					$notification->user_id 		= $notificationDetails['users'][0];
					$notification->priority_id 	= $notificationDetails['priority'];
					$notification->heading 		= $notificationDetails['heading'];
					$notification->description 	= $notificationDetails['description'];
					$notification->send_email 	= isset( $notificationDetails['email_notification'] ) ? '1': '0';
					$notification->updated_by 	= $loggedInUserId;

					try
					{
						if( $notification->save() )
						{
							$response['resCode']    	= 0;
							$response['resMsg']     	= 'Notification details updated successfully';
						}
						else
						{
							$response['resCode']    = 2;
						    $response['resMsg']     = trans('translation.error');
						}
					}
					catch(\Exception $e)
					{
						$response['resCode']    = 3;
					    $response['resMsg']     = trans('translation.error');
					}
				}
			}
        }
        else
        {
        	$response['resCode']    = 1;
			$response['resMsg']     = 'Please select at least one user';
        }

		return response()->json($response);
	}

	/**
	 * Function to fetch notification listing
	 * @param void
	 * @return array
	 */
	public function fetchNotifications()
	{
		$start      = Input::get('iDisplayStart');      // Offset
		$length     = Input::get('iDisplayLength');     // Limit
		$sSearch    = Input::get('sSearch');            // Search string
		$col        = Input::get('iSortCol_0');         // Column number for sorting
		$sortType   = Input::get('sSortDir_0');         // Sort type

		// Datatable column number to table column name mapping
	    $arr = array(
	        0 => 't1.id',
	        1 => 't2.name',
	        2 => 't1.heading',
	        3 => 't3.priority_type',
	        4 => 't1.send_email',
	        5 => 't1.is_read',
	    );

	    // Map the sorting column index to the column name
	    $sortBy = $arr[$col];

	    // Get the records after applying the datatable filters
	    $notifications = DB::select(
	    	DB::raw(
	    		"SELECT t1.id, t1.heading, t1.send_email, t1.is_read, t2.name as username, t3.priority_type FROM notifications AS t1 
	    		LEFT JOIN users as t2 ON t1.user_id = t2.id
	    		LEFT JOIN notification_priorities as t3 ON t1.priority_id = t3.id
	    		WHERE ( ( t1.heading LIKE ('%". $sSearch ."%') or t2.name LIKE ('%". $sSearch ."%') ) AND t1.deleted_at IS NULL )
	    		ORDER BY " . $sortBy . " " . $sortType ." LIMIT " . $start . ", ". $length
	    	)
		);

	    $notificationCount = DB::select(
	    	DB::raw(
	    		"SELECT t1.heading FROM notifications AS t1 
	    		LEFT JOIN users as t2 ON t1.user_id = t2.id
	    		LEFT JOIN notification_priorities as t3 ON t1.priority_id = t3.id
	    		WHERE ( ( t1.heading LIKE ('%". $sSearch ."%') or t2.name LIKE ('%". $sSearch ."%') ) AND t1.deleted_at IS NULL )
	    		ORDER BY " . $sortBy . " " . $sortType ." LIMIT " . $start . ", ". $length
	    	)
		);

		$iTotal = count($notificationCount);

	    // Create the datatable response array
	    $response = array(
	        'iTotalRecords' => $iTotal,
	        'iTotalDisplayRecords' => $iTotal,
	        'aaData' => array()
	    );

	    $k=0;
	    if ( count( $notifications ) > 0 )
	    {
	        foreach ($notifications as $notification)
	        {
	        	$response['aaData'][$k] = array(
	                0 => $notification->id,
	                1 => ucwords( strtolower( $notification->username ) ),
	                2 => ucfirst( strtolower($notification->heading ) ),
	                3 => ucwords( strtolower( $notification->priority_type ) ),
	                4 => ( $notification->send_email == '1' ) ? 'Yes': 'No',
	                5 => ( $notification->is_read == '1' ) ? 'Read': 'Not Opened',
	                6 => '<a href="javascript:void(0);" id="'. $notification->id .'" class="edit_teacher"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="'. $notification->id .'" class="delete_teacher"><i class="fa fa-trash-o"></i></a>'
	            );
	            $k++;
	        }
	    }

		return response()->json($response);
	}

	/**
     * Function to get notification details
     * @param void
     * @return array
     */
    public function getNotificationDetails()
    {
    	$notificationId = Input::get('notificationId');

    	$notificationDetails = Notification::where(['id' => $notificationId])->first();

    	return response()->json($notificationDetails);
    }
	
	public function getTeacherDetails()
    {
    	$teacherid = Input::get('teacherid');

    	$teacherDetails = User::where(['id' => $teacherid])->first();
    	return response()->json($teacherDetails);
    }
	
	public function getCategoryDetails()
    {
    	$catid = Input::get('catid');

    	$categorydetail = Categories::where(['id' => $catid])->first();
    	return response()->json($categorydetail);
    }
	
	public function getSubCategoryDetails()
    {
    	$catid = Input::get('catid');

    	$categorydetail = Categories::where(['id' => $catid])->first();
    	return response()->json($categorydetail);
    }
	
	
	
	/**
     * Function to delete subcategory
     * @param void
     * @return array
     */
    public function deleteSubCategory()
    {
    	$catid = Input::get('catid');

    	$categoriesdetails = Categories::find($catid);

    	try
    	{
    		if( $categoriesdetails->delete() )
    		{
    			$response['resCode']    	= 0;
    			$response['resMsg']     	= 'Subcategory deleted successfully';
    		}
    		else
    		{
    			$response['resCode']    = 1;
    		    $response['resMsg']     = trans('translation.error');
    		}
    	}
    	catch(\Exception $e)
    	{
    		$response['resCode']    = 2;
    	    $response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }
	
	
	/**
     * Function to delete category
     * @param void
     * @return array
     */
    public function deleteCategory()
    {
    	$catid = Input::get('catid');

    	$categoriesdetails = Categories::find($catid);

    	try
    	{
    		if( $categoriesdetails->delete() )
    		{
    			$response['resCode']    	= 0;
    			$response['resMsg']     	= 'Category deleted successfully';
    		}
    		else
    		{
    			$response['resCode']    = 1;
    		    $response['resMsg']     = trans('translation.error');
    		}
    	}
    	catch(\Exception $e)
    	{
    		$response['resCode']    = 2;
    	    $response['resMsg']     = trans('translation.error');
    	}

    	return response()->json($response);
    }

    /**
     * Function to delete notification
     * @param void
     * @return array
     */
    public function deleteNotification()
    {
    	$notificationId = Input::get('notificationId');

    	$notificationDetails = Notification::find($notificationId);

    	try
    	{
    		if( $notificationDetails->delete() )
    		{
    			$response['resCode']    	= 0;
    			$response['resMsg']     	= 'Notification deleted successfully';
    		}
    		else
    		{
    			$response['resCode']    = 1;
    		    $response['resMsg']     = 'Server error!';
    		}
    	}
    	catch(\Exception $e)
    	{
    		$response['resCode']    = 2;
    	    $response['resMsg']     = 'Server error!';
    	}

    	return response()->json($response);
    }
	public function viewTeacher($id)
    {
		$teacherDocs = TeacherDocument::where(['teacher_id'=>$id])->get();
    	$teacher = User::where(['role' => 2, 'id'=>$id])->first();
		return view('admin/view_teacher', ['teacherDetails' => $teacher, 'teacherDocs'=>$teacherDocs]);
    }
	public function setTeacherStatus(Request $request)
    {
		$status = !empty($request->input('status')) ?    $request->input('status') :'';
		$is_accridited = !empty($request->input('acr')) ?    $request->input('acr') :'';
		$userid = !empty($request->input('userid')) ?    $request->input('userid') :'';
		$updatestatus = array(
		 'status'=>$status,
		 'is_accridited'=>$is_accridited
		);
		User::where(['id'=>$userid])->update($updatestatus);
	}
	
	public function questionnaires(Request $request)
	{
		$userId = Auth::id();
		$appointments = [];
		$teacherappointment= AppointmentBooking::where(['teacher_id' => $userId])->get();
		foreach($teacherappointment as $val){
			$appointments[] = $val->id;
		}
		$questionnaires = DB::table('student_questionnaires_answer')
						  ->whereIn('appointment_id', $appointments)
						  ->get();
		//echo '<pre>'; print_r($questionnaires); die;
		
		return view('admin/student_feedbacks',['questionnaires'=>$questionnaires]);
	}
	
	public function getAppointments(Request $request)
	{
		$userId = Auth::id();
		$teacherappointments= AppointmentBooking::leftJoin('booked_lectures', 'booked_lectures.appointment_id', '=', 'appointement_booking.id')->leftJoin('users', 'users.id', '=', 'appointement_booking.student_id')->select('appointement_booking.id as aptid', 'users.first_name','users.last_name','users.email','users.profile_image','users.role', 'appointement_booking.start_date', 'appointement_booking.end_date', 'booked_lectures.skype_id', 'booked_lectures.meeting_place', 'booked_lectures.knowledge_level', 'booked_lectures.message')->get();
		return view('admin/teacher_appointments',['teacherappointments'=>$teacherappointments]);
	}
	
	
}