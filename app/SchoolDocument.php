<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolDocument extends Model
{
    public $timestamps = false;

    protected $fillable = ['school_id', 'document_name', 'document_type', 'uploaded_at', 'uploaded_by'];
}
