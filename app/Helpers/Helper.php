<?php
// Custom helper class that contains the common function to be used in entire application

namespace App\Helpers;

use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Notification;
use App\Categories;
use App\AppointmentBooking;
use Illuminate\Support\Str;
class Helper
{
    /**
     * Function to get trip addon ariline list
     * @param void
     * @return array
     */ 
       

   public static function saveNotification($notFrom,$notTo,$notMessage,$heading, $apptid=null, $type=1)
	{
		$notifications = new Notification();
		$notifications->user_id = $notTo;
		$notifications->heading = $heading;
		$notifications->description = $notMessage;
		$notifications->notification_source = '2';
		$notifications->send_email = '0';
		$notifications->created_at = date('Y-m-d H:i:s');
		$notifications->created_by = $notFrom;
		$notifications->lecture_id = $apptid;
		$notifications->type = $type;
		
		$notifications->save();

	}  

	public static function updateNotification($nid)
	{

		DB::table('notifications')->where('id', $nid)->update(['is_read' => 1]);

	}
	
	
	public static function newPaymentNotification($userId)
	{

		return Notification::where(['user_id'=>$userId,'is_read'=>0, 'type'=>6])->orderBy('id','DESC')->get();           

	}

	public static function notificationDetail($userId)
	{

		return Notification::where(['user_id'=>$userId,'is_read'=>'0'])->orderBy('id','DESC')->limit(5)->get();           

	} 
	  
	public static function totalNotification($userId)
	{

		return Notification::where(['user_id'=>$userId,'is_read'=>'0'])->orderBy('id','DESC')->get();           

	}
	 
	public static function notificationSentDetail($userId)
	{

		return Notification::where(['created_by'=>$userId,'is_read'=>'0'])->orderBy('id','DESC')->get();           
		
	}

	public static function notificationArchiveDetail($userId)
	{

		return Notification::where(['user_id'=>$userId,'is_read'=>'1'])->orderBy('id','DESC')->get();           

	}	  


	public static function notificationDetailForAdmin()
	{
		return Notification::where(['is_read'=>'0'])->orderBy('id','DESC')->limit(5)->get();

	}

	public static function categories()
	{
		return Categories::where('parent_id', '==', 0)->get();

	}
	public static function subcategories()
	{
		return Categories::where('parent_id', '!=', 0)->get();

	}
	public static function todayLessons($userId)
	{
		$todaydate = date('Y-m-d');
		return DB::table('appointement_booking')->where(['teacher_id'=>$userId,'is_cancel'=>0, 'is_booked'=>1])->select('id', 'start_date')
		->whereRaw('Date(start_date) = CURDATE()')
		->get();

	}
	public static function nextLecture($userId)
	{
		$currenthour = date('Y-m-d H:i:s');
		return DB::table('appointement_booking')->where(['teacher_id'=>$userId,'is_cancel'=>0, 'is_booked'=>1])->select('id', 'start_date')
		->where('start_date', '>=', $currenthour)
		->Orderby('start_date', 'asc')
		->limit(1)
		->first();

	}
	
	public static function sub_string($text, $start, $end)
	{
		return Str::substr($text, $start, $end);
	}
	
	
	  
	   
	  
	  
	  
	  
   

}