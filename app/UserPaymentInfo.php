<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentInfo extends Model
{
    protected $fillable = ['user_id', 'payment_type', 'pay_info'];
}
