<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    /**
     * Get the documents for the school.
     */
    public function documents()
    {
        return $this->hasMany('App\SchoolDocument');
    }

    /**
     * Get the user that associatd with the school.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
