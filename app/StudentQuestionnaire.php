<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentQuestionnaire extends Model
{
	protected $table = 'student_questionnaires_answer';
}
