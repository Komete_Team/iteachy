<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    protected $fillable = ['event_name', 'event_date', 'event_start_time', 'event_description', 'event_source', 'created_at'];
}
