<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="route" content="{{ url('/') }}">
	<!-- font Awesome -->
	<!--<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />-->
	<link rel="stylesheet" href="{{URL::asset('website/scripts/rateit.css')}}">
	<!-- bootstrap 3.0.2 -->
	 <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::asset('front/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/custom.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/multiple-select.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('js/plugins/chosen_v1.8.7/chosen.css') }}" />
	<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ URL::asset('front/css/bootstrap.min.css') }}">
	 
	<!-- fullCalendar -->
      
	<link href="{{ URL::asset('css/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- jquery here -->
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script src="{{ URL::asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
	<script src="{{ URL::asset('js/multiple-select.js') }}"></script>
	<script src="{{ URL::asset('front/js/owl.carousel.min.js') }}"></script>
	<script src="{{ URL::asset('front/js/custom.js') }}"></script>
	<script type="text/javascript" src="{{URL::asset('website/scripts/jquery.rateit.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('website/scripts/jquery.rateit.min.js')}}"></script>
	<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
	
	 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('front/js/price_range_script.js') }}"></script>
	@if (Route::currentRouteAction() == 'App\Http\Controllers\ChatController@index')
		<script src="{{ URL::asset('front/js/chat.js') }}"></script>
	@endif
	<script type="text/javascript">
		var base_url = '<?php echo url('/') ?>';
	</script>
		<!-- fullCalendar -->
	<script>
	
	$(document).ready(function(){
		// Initialize the functionality
		loginObj1.init();
	});

	var loginObj1 = {
		init: function() {
			loginObj1.holdFormSubmit1();
			
			loginObj1.formValidation1();
			
			loginObj1.loginFunction1();
		},
		holdFormSubmit1: function() {
			// Form validations
			$('#frm_teacher_registration').submit(function(e){
		        e.preventDefault();
		    });
		},
		formValidation1: function(){
			$('#frm_teacher_registration').validate({
				rules: {
					first_name: {
						required: true
					},
					last_name: {
						required: true
					},
					email_id: {
						required: true,
						email: true
					},
					password: {
						required: true
					},
					categories: {
						required: true
					},
					account_type: {
						required: true
					}
				},
				messages: {
					first_name: {
						required: 'Si prega di inserire il nome'
					},
					last_name: {
						required: 'Per favore inserisci il cognome'
					},
					email_id: {
						required: 'Si prega di inserire l ID e-mail',
						email: 'Si prega di inserire un ID e-mail valido'
					},
					password: {
						required: 'Per favore, inserisci la password'
					},
					categories: {
						required: 'si prega di selezionare la categoria'
					},
					account_type: {
						required: 'Seleziona il tipo di account'
					}
					
				},
				
			});
		},
		loginFunction1: function() {
			// Login functionality
		    $('#btn_teacher_registration').click(function(){
		    	// Check the validation
		    	if( $('#frm_teacher_registration').valid() )
		    	{
		    		// Hold the button reference
		    		var btn = $(this);
					console.log($('#frm_teacher_registration').serialize());
					$('#server_resposne').hide();
		    		$('#server_resposne_msg').html('');
		    		$.ajax({
		    			url: $('meta[name="route"]').attr('content') + '/registeruser',
		    			method: 'post',
		    			data: {
		    				frmData: $('#frm_teacher_registration').serialize()
		    			},
		    			beforeSend: function() {
		    				// Disable the button
					        $(btn).attr('disabled', true);
					        
					        $('#loading_spinner').show();
					    },
		    			headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    complete: function()
					    {
					    	// Enable the button
					    	$(btn).attr('disabled', false);

					    	$('#loading_spinner').hide();
					    },
					    success: function(response){
					    	if( response.resCode == 0 )
					    	{
					    		// Reset the form
					    		$('#frm_teacher_registration')[0].reset();

					    		$('#server_resposne_res').removeClass('alert-danger').addClass('alert-success');
					    		$('#server_resposne_msg1').html(response.resMsg);
					    		$('#server_resposne_res').show();
					    	}
					    	else
					    	{
					    		$('#server_resposne_res').removeClass('alert-success').addClass('alert-danger');
					    		$('#server_resposne_msg1').html(response.resMsg);
					    		$('#server_resposne_res').show();
					    	}
					    }
		    		});
		    	}
		    });
		}
	}
	
	$(document).ready(function(){
		// Initialize the functionality
		loginObj.init();
	});

	var loginObj = {
		init: function() {
			loginObj.holdFormSubmit();
			
			loginObj.formValidation();
			
			loginObj.loginFunction();
		},
		holdFormSubmit: function() {
			// Form validations
			$('#frm_teacher_login').submit(function(e){
		        e.preventDefault();
		    });
		},
		formValidation: function(){
			$('#frm_teacher_login').validate({
				rules: {
					username: {
						required: true,
						email: true
					},
					password: {
						required: true
					}
				},
				messages: {
					username: {
						required: 'Si prega di inserire il nome utente',
						email: 'per favore inserisci UN nome utente valido'
					},
					password: {
						required: 'Per favore, inserisci la password'
					}
				}
			});
		},
		loginFunction: function() {
			// Login functionality
		    $('#btn_admin_login').click(function(){
		    	// Check the validation
		    	if( $('#frm_teacher_login').valid() )
		    	{
		    		// Hold the button reference
		    		var btn = $(this);

					$('#server_resposne').hide();
		    		$('#server_resposne_msg').html('');

		    		$.ajax({
		    			url: $('meta[name="route"]').attr('content') + '/login',
		    			method: 'post',
		    			data: {
		    				frmData: $('#frm_teacher_login').serialize()
		    			},
		    			beforeSend: function() {
		    				// Disable the button
					        $(btn).attr('disabled', true);
					        
					        $('#loading_spinner').show();
					    },
		    			headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    complete: function()
					    {
					    	// Enable the button
					    	$(btn).attr('disabled', false);

					    	$('#loading_spinner').hide();
					    },
					    success: function(response){
					    	if( response.resCode == 0 )
					    	{
					    		 document.location.href = response.redirectUrl;
					    	}
					    	else
					    	{
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    }
		    		});
		    	}
		    });
		}
	}
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#member-persons').owlCarousel({
			loop:true,
			margin:15,
			responsiveClass:true,
			autoplay:true,
			autoplayTimeout:3000,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:2,
					nav:true
				},
				1000:{
					items:4,
					nav:true,
					loop:true
				}
			}
		});

		$('.owl-carousel').owlCarousel({
			loop:true,
			margin:15,
			responsiveClass:true,
			autoplay:true,
			autoplayTimeout:3000,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:2,
					nav:true
				},
				1000:{
					items:3,
					nav:true,
					loop:true
				}
			}
		});
	
		  
		  $('#testimonial').owlCarousel({
				loop:true,
				margin:10,
				nav:false,
				dots:true,
				responsive:{
					0:{
						items:1
					},
					600:{
						items:2
					},
					1000:{
						items:3
					}
				}
			})
	});
	</script>
	

	<style type="text/css">
	
	label.error {
		color: red;
	}
    
	.calendar-cell-leave {
		background-color: #ffffff !important;
		width:100%;
		z-index: 999999 !important;
	}
	.calendar-cell-booked{background:#cccccc !important;
		width:100%;
		z-index: 99 !important;}
		
	.calendar-cell-available{background:#62c208 !important;
		
		z-index: 9 !important;}	
	.calendar-cell-available-selected{background:#00B4FF !important;}	
		
	.calendar-cell-canceled {
		width:100%;
		background-color: #FF0000 !important;
		z-index: 9999 !important;
	}
	</style>
</head>
<body>
    <div id="app">
	@php $user = Auth::user(); @endphp
	<div class="top_header">
        <nav class="navbar navbar-default">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
             <a class="navbar-brand" href="{{url('/')}}"><img src="{{ URL::asset('front/images/logo.png') }}" alt=""></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
				<li><a href="{{ url('/') }}">Cerca un insegnante</a></li> 
				<li><a href="{{ url('student/booking_listing') }}">Le mie lezioni</a></li>
				<li><a href="{{ url('contact-us') }}">Contattaci</a></li>
				@if (Auth::guest())
				<li><a href="#"  class="login-registration-btn" data-toggle="modal" data-target="#myModal">Accedi / Registrati</a></li>	
				<!--<li><a href="{{ url('login') }}">Accedi</a></li> 
				<li><a href="{{ url('register') }}">Registrazione insegnanti</a></li>
				<li><a href="{{ url('student/register') }}">Registrazione dello studente</a></li>-->
				
				@else
				<li>
				<a href="{{ url('student/dashboard') }}" role="button" aria-expanded="false">
					{{ Auth::user()->first_name }} <span class="caret"></span>
				</a>

				</li>
				
				
				<?php 
					$notificationdetail= Helper::notificationDetail($user->id);
					$totalnotifications = count($notificationdetail); 
				?>
				 <li>
					 <a href="{{ url('student/notifications')}}">
						<i class="fa fa-bell-o" aria-hidden="true"></i>
						@if($totalnotifications>0)<span class="label label-warning">{{$totalnotifications}}</span> @endif
					</a> 
					<ul class="dropdown-menu">
						<?php /* <li class="header">You have 10 notifications</li> */
						?>
							@if(!empty($notificationdetail) && count($notificationdetail)> 0)
							  @foreach($notificationdetail as $k => $notval )
											  <li class="unread"><a href="{{ url('teacher/notification').'/'.$notval->id}}">{{$notval->description}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
							  @endforeach
							@else
							<li class="read"><a href="javascript:void(0);">nessuna notifica</a></li>  
							@endif
							
						<!--<li class="footer"><a href="#">View all</a></li>-->
					</ul>
				</li>
				<li> <a href="{{ url('student/logout') }}">{{ __('translation.Sign out') }}</a></li>
				@endif
                </ul>
              </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </div>
	
	
        @yield('content')
	<div class="footer"> 
        <div class="container"> 
            <div class="row"> 
              <div class="col-md-3"> 
              <div class="foot_1"> 
                 <a href="{{url('/')}}"><img src="{{ URL::asset('front/images/logo.png') }}" alt=""></a>
              </div>
              </div>
              <div class="col-md-2 text-left"> 
              <div class="foot_2"> 
                   <ul> 
                      <li>Cerca lezioni</li>
                      <li>perche</li>
                      <li>Cerca lezioni</li>
                      <li>chi siamo</li>
                      <li>Cerca lezioni</li>
                   </ul>
              </div>
              </div>
              <div class="col-md-2 text-left"> 
              <div class="foot_3"> 
                  <ul> 
                      <li>Assistenza</li>
                      <li>Notelegali</li>
                      <li>pravicy</li>
                      <li>contattaci</li>
                   </ul>
              </div>
              </div>
              <div class="col-md-5 text-left"> 
              <div class="foot_4 "> 
                  <ul> 
                        <li class="social_heading"> <p> FOLLOW US</p></li>
                      <li class="circle_2"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                      <li class="circle_1"><i class="fa fa-facebook" aria-hidden="true"></i></li>
                      <li class="circle_3"><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                      
                   </ul>
              </div>
            </div>
            </div>
        </div>
    </div>
	</div>
	<script src="{{ URL::asset('js/plugins/chosen_v1.8.7/chosen.jquery.js') }}"></script>
	<script src="{{ URL::asset('js/plugins/chosen_v1.8.7/docsupport/prism.js') }}" type="text/javascript" charset="utf-8"></script>
	
	  <!-- Modal LOGIN PART-->
  <div class="modal login-form fade" id="myModal" role="dialog">
    <div class="modal-dialog login-popup">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><img src="{{ URL::asset('front/images/close-icon.jpg') }}"></button>
          <h4 class="modal-title">ACCEDI</h4>
        </div>
        <div class="modal-body"> 
          <form autocomplete="off" name="frm_teacher_login" id="frm_teacher_login" class="form-vertical">
           
           <input type="email" name="username" id="username" class="form-control form-group" placeholder="Inserisci la tua email" /> 
            <div class="input-group">
              <input type="password" class="form-control pwd" name="password" id="password" value="****">
              <span class="input-group-btn">
                <button class="btn btn-default reveal" type="button"><img src="{{ URL::asset('front/images/eye.jpg') }}"></button>
              </span>          
            </div>
            <div class="checkbox">
               <label> <input type="checkbox" value=""> <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Ricordami </label> <div class="pull-right">
                <a href="{{url('user-forgotpassword')}}">Password dimenticata?</a>
              </div>
            </div>
                <button type="submit" id="btn_admin_login" class="btn btn-primary btn-block custom-blue"><span class="text-left pull-left ln32">Accedi</span> <span class="text-right pull-right"><i class="fa fa-angle-right fa-2x"></i></span></button>
                  <hr />
                <a class="btn btn-block btn-social btn-facebook" href="">
					<span class="fa fa-facebook-square pull-left fb-icon"></span> Accedi con Facebook
				</a>
			<!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-danger alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>	
          </form>
        </div>
        <div class="modal-footer hide">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
        <br />
        <div class="clearfix"></div>
       <div class="modal-content"><div class="custom-link-registration"><strong>NON HAI UN ACCOUNT?</strong> <a href="#" class="text-right pull-right" data-toggle="modal" data-target="#myModalregistartion">Registrati </a></div></div>
      
    </div>
  </div>
   <!-- Modal LOGIN PART ENDED-->
	
    <!-- Modal REGISTRATION PART started-->
   <div class="modal fade login-form registration-form" id="myModalregistartion" role="dialog">
    <div class="moda-dialog reg-popup">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><img src="{{ URL::asset('front/images/close-icon.jpg') }}"></button>
          <h4 class="modal-title">REGISTRATI</h4>
        </div>
        <div class="modal-body"> 
          <form autocomplete="off" name="frm_teacher_registration" id="frm_teacher_registration" class="form-vertical">
           {{ csrf_field() }}
           <input type="text" class="form-control form-group" placeholder="{{ __('translation.First Name') }}" name="first_name" id="first_name" /> 
           <input type="text" class="form-control form-group" placeholder="{{ __('translation.Last Name') }}" name="last_name" id="last_name" /> 
           <input type="email" class="form-control form-group" placeholder="{{ __('translation.Email Id') }}" name="email_id" id="email_id" /> 
            <div class="form-group input-group">
              <input type="password" class="form-control pwd" value="{{ __('translation.Password') }}" name="password" id="password">
              <span class="input-group-btn">
                <button class="btn btn-default reveal" type="button"><img src="{{ URL::asset('front/images/eye.jpg') }}"></button>
              </span>          
            </div>
			<div class="form-group input-group">
				<div class="col-sm-12">
					<label for="input-type">Account Type *:</label>
					<div id="input-type" class="row">
						<div class="col-sm-6">
							<label class="radio-inline">
								<input name="account_type" id="input-type-student" class="account-type" value="3" type="radio" />Student
							</label>
						</div>
						<div class="col-sm-6">
							<label class="radio-inline">
								<input name="account_type" class="account-type" id="input-type-tutor" value="2" type="radio" />Teacher
							</label>
						</div>
					</div>
					<label class="customerror error"></label>
				</div>
			</div>
				<?php 
					$categories= Helper::categories();
					$subcategories= Helper::subcategories();
				?>
				@if(count($categories) > 0)
					<select name="categories[]" id="categories"  class="form-control form-group" multiple="multiple" placeholder="{{ __('translation.Category') }}">
					@foreach($categories as $cat)
						 <optgroup label="{{$cat->category_name}}">
						@foreach($subcategories as $subcat)
							@if($cat->id == $subcat->parent_id)
								<option value="{{$cat->id}}-{{$subcat->id}}">{{$subcat->category_name}}</option>
							@endif
						@endforeach
						</optgroup>
					@endforeach
					</select>
				@endif
           
			<div class="form-group">
            	<label for="online">{{ __('translation.online') }}:</label>
                <input style="vertical-align: middle;width:auto; display:inline-block;margin-bottom: 5px;" type="radio" class="form-control" placeholder="" name="online" id="online" value="1">
            </div>
			
            <div class="checkbox">
               <label> <input type="checkbox" value=""> <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>  Accetto i termini </label>  
               
              <div class="pull-right">
                <a href="#"> del servizio e della privacy</a>
              </div>
            </div>
			
			<div class="checkbox">
               <label> <input type="checkbox" value=""> <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>  termini di condizioni </label>  
               
              <div class="pull-right">
                <a href="#"> del servizio e della privacy</a>
              </div>
            </div>
			
			<div class="checkbox">
               <label> <input type="checkbox" value=""> <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>  Accetto i termini </label>  
               
              <div class="pull-right">
                <a href="#"> del servizio e della privacy</a>
              </div>
            </div>
              
			<button type="submit" id="btn_teacher_registration" class="btn btn-danger btn-block custom-color-red"><span class="text-left pull-left ln32">Registrati</span> <span class="text-right pull-right"><i class="fa fa-angle-right fa-2x"></i></span></button>
				
			<!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-dismissible text-center" id="server_resposne_res" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg1"></span>
          	</div>	
                  <hr />
                <a class="btn btn-block btn-social btn-facebook" href="">
            <span class="fa fa-facebook-square pull-left fb-icon"></span> Accedi con Facebook
          </a>
            
          </form>
        </div>
        <div class="modal-footer hide">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
        <br />
        <div class="clearfix"></div>
       <div class="modal-content"><div class="custom-link-registration"><strong>HAI GIÀ UN ACCOUNT?</strong> <a href="#" class="text-right pull-right login-registration-btn1" data-toggle="modal" data-target="#myModal">Accedi </a></div></div>
      
    </div>
  </div>
	

  <!-- Modal REGISTRATION PART ENDED-->
<script>
$(document).ready(function(){
  $(".custom-link-registration").click(function(){
    $(".login-popup").hide();
  });
  $(".login-registration-btn").click(function(){
    $(".login-popup").show();
	
	
  });
  
  $(".login-registration-btn1").click(function(){
	 //$(".fade").hide();
   // $(".login-registration-btn").click();
	
	
  });
  
  
  $(".close").click(function(){
    $(".fade").hide();
  });

});
</script>
<script type="text/javascript">
 $(document).ready(function(){
  $(".reveal").on('click',function() {
    var $pwd = $(".pwd");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
  });
 });
 
 $(".account-type") // select the radio by its id
	.change(function(){ // bind a function to the change event
		if( $(this).is(":checked") ){ // check if the radio is checked
			var val = $(this).val(); // retrieve the value
			alert(val);
		}
	});
 
 
</script>
	
</body>
</html>
