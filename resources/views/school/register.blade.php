<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="route" content="{{ url('/') }}">

	<title>School Registration</title>

	<!-- css here -->
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />

	<!-- jquery here -->
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>

	<script>
	$(document).ready(function(){
		// Initialize the functionality
		loginObj.init();
	});

	var loginObj = {
		init: function() {
			loginObj.holdFormSubmit();
			
			loginObj.formValidation();
			
			loginObj.loginFunction();
		},
		holdFormSubmit: function() {
			// Form validations
			$('#frm_school_registration').submit(function(e){
		        e.preventDefault();
		    });
		},
		formValidation: function(){
			$('#frm_school_registration').validate({
				rules: {
					full_name: {
						required: true
					},
					school_name: {
						required: true
					},
					email_id: {
						required: true,
						email: true
					},
					password: {
						required: true
					}
				},
				messages: {
					full_name: {
						required: 'Please enter name'
					},
					school_name: {
						required: 'Please enter school name'
					},
					email_id: {
						required: 'Please enter email id',
						email: 'Please enter a valid email id'
					},
					password: {
						required: 'Please enter password'
					}
				}
			});
		},
		loginFunction: function() {
			// Login functionality
		    $('#btn_school_registration').click(function(){
		    	// Check the validation
		    	if( $('#frm_school_registration').valid() )
		    	{
		    		// Hold the button reference
		    		var btn = $(this);

					$('#server_resposne').hide();
		    		$('#server_resposne_msg').html('');

		    		$.ajax({
		    			url: $('meta[name="route"]').attr('content') + '/school/register',
		    			method: 'post',
		    			data: {
		    				frmData: $('#frm_school_registration').serialize()
		    			},
		    			beforeSend: function() {
		    				// Disable the button
					        $(btn).attr('disabled', true);
					        
					        $('#loading_spinner').show();
					    },
		    			headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    complete: function()
					    {
					    	// Enable the button
					    	$(btn).attr('disabled', false);

					    	$('#loading_spinner').hide();
					    },
					    success: function(response){
					    	if( response.resCode == 0 )
					    	{
					    		// Reset the form
					    		$('#frm_school_registration')[0].reset();

					    		$('#server_resposne').removeClass('alert-danger').addClass('alert-success');
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    	else
					    	{
					    		$('#server_resposne').removeClass('alert-success').addClass('alert-danger');
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    }
		    		});
		    	}
		    });
		}
	}
	</script>

	<style type="text/css">
	.login-form {
		width: 35%;
    	margin: 10% auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
    label.error {
        color: red;
    }
	</style>
</head>
<body>
	<div class="login-form">
        <form autocomplete="off" name="frm_school_registration" id="frm_school_registration">
            <h2 class="text-center">School Registration</h2>
            <div class="form-group">
            	<label for="school_name">School Name:</label>
                <input type="text" class="form-control" placeholder="Please enter school name" name="school_name" id="school_name">
            </div>
            <div class="form-group text-center">
            	<h4>ALI User Details</h4>
            </div>
            <div class="form-group">
            	<label for="full_name">Name:</label>
                <input type="text" class="form-control" placeholder="Please enter name" name="full_name" id="full_name">
            </div>
            <div class="form-group">
            	<label for="email_id">Email:</label>
                <input type="text" class="form-control" placeholder="Please enter email id" name="email_id" id="email_id">
            </div>
            <div class="form-group">
            	<label for="password">Password:</label>
                <input type="password" class="form-control" placeholder="Please enter password" name="password" id="password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" id="btn_school_registration">Registration</button>
            </div>

            <!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>
        </form>
    </div>

</body>
</html>