@extends('admin.layouts.app')
@section('title', 'Role Management')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Role Management</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row">
	        	<div class="col-lg-12">
	        		<div>
	        			<button type="button" class="btn btn-primary" id="btn_show_role_modal">Add Role</button>
	        		</div>
	        		<div class="table-responsive" style="margin-top: 10px;">
			        	<table class="table table-striped" id="datatable_roles">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Keyword</th>
			        				<th>Name</th>
			        				<th>Description</th>
			        				<th>{{ __('translation.Action') }}</th>
			        			</tr>
			        		</thead>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	    <!-- Modal to add / edit role -->
	    <div id="modal_role" class="modal fade">
	    	<div class="modal-dialog">
	    		<!-- Modal content-->
	    		<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal">&times;</button>
	    				<h4 class="modal-title">Role Details</h4>
	    			</div>
	    			<div class="modal-body">
	    				<form name="frm_add_role" id="frm_add_role" autocomplete="off">
	    					<div class="form-group">
	    						<label for="text">Role Keyword:</label>
	    						<input type="text" class="form-control" id="name" name="name">
	    						<input type="hidden" id="role_id" name="role_id" value="">
	    					</div>
	    					<div class="form-group">
	    						<label for="text">Role Name:</label>
	    						<input type="text" class="form-control" id="display_name" name="display_name">
	    					</div>
	    					<div class="form-group">
	    						<label for="text">Role Description:</label>
	    						<textarea class="form-control" id="description" name="description"></textarea>
	    					</div>
	    					<button type="submit" class="btn btn-primary" id="btn_save_role">Submit</button>
	    				</form>
	    			</div>
	    			<!-- <div class="modal-footer">
	    				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    			</div> -->
	    		</div>
	    	</div>
	    </div>

	</aside>
	<!-- /.right-side -->

@endsection