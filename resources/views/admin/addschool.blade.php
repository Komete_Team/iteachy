@extends('admin.layouts.app')
@section('title', 'School Management')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Add School</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>
	        	<form name="frm_add_school" id="frm_add_school" autocomplete="off">
					<div class="row">
						<div class="col-lg-11"></div>
						<div class="col-lg-1">
							<label for="school_logo">
							    <!-- <img src="{{ url('/images/school_default_logo.jpg') }}" height="100px" width="100px"> -->
							    <?php
							    $imgUrl = url('/images/school_default_logo.jpg');
							    if( isset( $schoolDetails->logo_image_name ) && !is_null( $schoolDetails->logo_image_name ) && ( $schoolDetails->logo_image_name != '' ) )
							    {
							    	if( file_exists( storage_path() . '/uploads/schools/logos/' . $schoolDetails->logo_image_name ) )
							    	{
							    		$imgUrl = url('/images/uploads-schools-logos/' . $schoolDetails->logo_image_name);
							    	}
							    }
							    ?>
							    <img src="{{ $imgUrl }}" height="100px" width="100px">
							    <input type="file" id="school_logo" name="school_logo" style="display:none;">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12"><h4>School Information</h4></div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">School Name: <span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="school_name" name="school_name" value="{{ $schoolDetails->school_name ?? '' }}">
	    						<input type="hidden" id="school_id" name="school_id" value="{{ $schoolDetails->id ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Enrollment Number:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="enrollment_no" name="enrollment_no" value="{{ $schoolDetails->enrollment_number ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Address:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="address" name="address" value="{{ $schoolDetails->address ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
	    					<div class="form-group">
	    						<label for="text">City:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="city" name="city" value="{{ $schoolDetails->city ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Province/State:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="province" name="province" value="{{ $schoolDetails->province ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Country:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="country" name="country" value="{{ $schoolDetails->country ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12"><h4>Principal Information</h4></div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">First Name:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="principal_fname" name="principal_fname" value="{{ $schoolDetails->principal_fname ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Last Name:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="principal_lname" name="principal_lname" value="{{ $schoolDetails->principal_lname ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Email Id:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="principal_email" name="principal_email" value="{{ $schoolDetails->principal_email ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Contact No:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="principal_contact_no" name="principal_contact_no" value="{{ $schoolDetails->principal_contact_no ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12"><h4>Contact Person Information</h4></div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">First Name:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_person_fname" name="contact_person_fname" value="{{ $schoolDetails->contact_person_fname ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Last Name:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_person_lname" name="contact_person_lname" value="{{ $schoolDetails->contact_person_lname ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Email Id:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_person_email" name="contact_person_email" value="{{ $schoolDetails->contact_person_email ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">Contact No:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_person_contact_no" name="contact_person_contact_no" value="{{ $schoolDetails->contact_person_contact_no ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="text">Status:</label>
								<div class="row">
		    						<label class="radio-inline">
		    						    <input type="radio" name="status" value="1" <?php echo ( isset( $schoolDetails->status ) && ( $schoolDetails->status == '1' ) ) ? 'checked' : '' ?>> Active
		    						</label>
		    						<label class="radio-inline">
		    						    <input type="radio" name="status" value="0" <?php echo ( isset( $schoolDetails->status ) && ( $schoolDetails->status == '0' ) ) ? 'checked' : '' ?>> Inactive
		    						</label>
		    					</div>
		    					<label id="status-error" class="error" for="status"></label>
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="text">Documents:</label>
	    						<input type="file" class="form-control" id="documents" name="documents[]" multiple="">
	    					</div>
						</div>
					</div>

					<div>
						@if( isset( $schoolDocuments ) && !is_null( $schoolDocuments ))
			        		<h4>Document List</h4>
			        		<div>
			        			@foreach( $schoolDocuments as $schoolDocument )
			        				<p>
			        					<a href="javascript:void(0);">
			        						<img src="{{ url('/images/document_types/' . $docAttributes[$schoolDocument->document_type]) }}">
			        					</a>
			        				</p>
			        			@endforeach
			        		</div>
						@endif
			        </div>

					<button type="submit" class="btn btn-primary" id="btn_save_school">Submit</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

@endsection