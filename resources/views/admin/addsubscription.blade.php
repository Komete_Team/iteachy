@extends('admin.layouts.app')
@section('title', 'School Management')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('subscription.Add Subscription Plan') }}</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>
	        	<form method="post" id="add-subscription" autocomplete="off">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">{{ __('subscription.Title') }} : <span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" name="title">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">{{ __('subscription.Base') }} : <span class="mandatory_field">*</span></label>
	    						<input type="number" class="form-control" name="base">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">{{ __('subscription.Standard') }} : <span class="mandatory_field">*</span></label>
	    						<input type="number" class="form-control" name="standard">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">{{ __('subscription.Premium') }} : <span class="mandatory_field">*</span></label>
	    						<input type="number" class="form-control" name="premium">
	    					</div>
						</div>
					</div>
					{!! csrf_field() !!}
					<button type="submit" class="btn btn-primary" id="btn_save_school">Invia</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#add-subscription").validate({
			rules: {
				title: "required",
				base: "required",
				standard: "required",
				premium: "required"
			},
			messages: {
				title: "Per favore, inserisci il titolo.",
				base: "Inserisci il prezzo del piano di base.",
				standard: "Inserisci il prezzo del piano di standard.",
				premium: "Inserisci il prezzo del piano di premium."
			}
		});
	});
</script>
@endsection