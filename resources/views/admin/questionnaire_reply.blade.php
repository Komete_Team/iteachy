@extends('admin.layouts.app')
@section('title', 'Questionnaires')

@section('content')
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Questionnaires Answers</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			@if (Session::has('success'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('success') }}</strong>
							</div>
						@endif
						@if (Session::has('error'))
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('error') }}</strong>
							</div>
						@endif
						<h3>Questionnarie answers</h3>
			        	<table class="table table-striped" id="datatable_category">
							<tbody>      
								<?php $i=1; //echo '<pre>'; print_r($questionnaires); die; ?>
								@foreach($teachers as $val)
								 @php 
								 $answers = $val->answer; 
								 $answersarr = json_decode($answers, true);
								 //echo '<pre>'; print_r($answersarr); die;
								 @endphp
								<tr class="odd gradeX">
									<td>
										<div class="accordion" id="accordionExample">
										  <div class="card">
											<div class="card-header" id="headingOne">
											  <h2 class="mb-0">
												<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#{{$val->appointment_id}}" aria-expanded="true" aria-controls="collapseOne">
												 Appointment #{{$val->appointment_id}}
												</button>
											  </h2>
											</div>

											<div id="{{$val->appointment_id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
											  <div class="card-body">
												@php $i=1; @endphp
												@foreach($questionnaires as $ques)
													 <h3>Q{{$i}} {{$ques->question}}</h3>
													 @foreach($answersarr as $k => $ansval)
														@if($ques->id == $k)
															{{$ansval}}
														@endif	
													 @endforeach
														<p></p>
												@php $i++ @endphp	
												@endforeach
											  </div>
											</div>
										  </div>
										</div>
									</td>
									 
								  
								</tr>
								<?php 
									$i++;
								?>
                                       @endforeach
                                       
                                      
                                    </tbody>
			        	</table>
						<div class="center" style="text-align: center;">{{ $teachers->links() }}</div>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_ques',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "{{ url('admin/deletequestionnaire') }}"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
@endsection