@extends('admin.layouts.app')
@section('title', 'Teachers')

@section('content')
<?php //echo '<pre>'; print_r($teachers); die; ?>
	<!-- Multiselect dropdown -->
	<link href="{{ URL::asset('/css/multiple-select.css') }}" rel="stylesheet" />
	<script src="{{ URL::asset('/js/multiple-select.js') }}"></script>

	<script>
	$(document).ready(function(){
		$('#users').multipleSelect('destroy');
		$('#users').multipleSelect({
			width: '100%'
		});
		$("#defaultOpen").click();
	});
	</script>
	<style>

	/* Style the tab */
	.tab {
	  overflow: hidden;
	  border: 1px solid #ccc;
	  background-color: #f1f1f1;
	}

	/* Style the buttons inside the tab */
	.tab button {
	  background-color: inherit;
	  float: left;
	  border: none;
	  outline: none;
	  cursor: pointer;
	  padding: 14px 16px;
	  transition: 0.3s;
	  font-size: 17px;
	}

	/* Change background color of buttons on hover */
	.tab button:hover {
	  background-color: #ddd;
	}

	/* Create an active/current tablink class */
	.tab button.active {
	  background-color: #ccc;
	}

	/* Style the tab content */
	.tabcontent {
	  display: none;
	  padding: 6px 12px;
	
	}
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
	</style>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('translation.teacher') }}</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<?php //echo '<pre>'; print_r($teacher); die; ?>
						
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="text-align: center;">
					<?php
					$imgUrl = url('/images/avatar.png');
					if( $teacherDetails->profile_image != '' )
					{
						
							$imgUrl = url('/images/teacher_logos/' . $teacherDetails->profile_image);
						
					}
					?>
					<img src="{{ $imgUrl }}" height="100px" width="100px">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
					{{$teacherDetails->first_name.' '.$teacherDetails->last_name}}
					</div>
					<div class="profile-usertitle-job">
						Teacher
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
				
                <div id="basic-switch" data-id="{{$teacherDetails->id}}"></div>

                      
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="javascript:void(0);" class="tablinks" id="defaultOpen" onclick="openTab(event, 'overview')">
							<i class="glyphicon glyphicon-home"></i>
							Overview </a>
						</li>
						<li>
							<a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'doc')">
							<i class="glyphicon glyphicon-user"></i>
							Documents </a>
						</li>
					</ul>
				</div>
				
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <div id="overview" class="tabcontent">
				  <h3>Overview</h3>
				  <div>
					{{ __('translation.First Name') }}: {{$teacherDetails->first_name ?? ''}} <br/>
					{{ __('translation.Last Name') }}: {{$teacherDetails->last_name ?? ''}}  <br/>
					{{ __('translation.Email Id') }}: {{$teacherDetails->email ?? ''}}   <br/>
					{{ __('translation.Status') }}: 
					<?php if($teacherDetails->status == 0){
						echo __('translation.Rejected'); 
					}
					else if($teacherDetails->status == 1){
						echo __('translation.Approved');
					}
					else{
						echo __('translation.Pending'); } 
					?>
					
				  </div>
				</div>

				<div id="doc" class="tabcontent">
				  <h3>Documents</h3>
				  <?php //echo '<pre>'; print_r($teacherDocs); die; ?>
				  @foreach($teacherDocs as $val)
				  <div style="text-align:center;width: 330px;">
				  <iframe style="width:330px;margin-left:10px;display: block;" src="{{url('uploads/teachers/documents/'.$val->files)}}"></iframe>
				  <a href="{{url('uploads/teachers/documents/'.$val->files)}}" target="_blank">Download</a>
				  </div>
				 
				  @endforeach
				</div>
            </div>
		</div>
	</div>

		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

	<!-- Model to send notification -->
	<div id="modal_teacher" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">{{ __('translation.add_teacher') }}</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_notification" id="frm_add_teacher" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">{{ __('translation.First Name') }}:</label>
							<input type="text" class="form-control" id="first_name" name="first_name">
							<input type="hidden" name="teacher_id" id="teacher_id" value="">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Last Name') }}:</label>
							<input type="text" class="form-control" id="last_name" name="last_name">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Email Id') }}:</label>
							<input type="text" class="form-control" id="email" name="email">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Password') }}:</label>
							<input type="text" class="form-control" id="password" name="password">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Status') }}:</label>
							<input type="text" class="form-control" id="status" name="status">
						</div>
						
						<button type="button" class="btn btn-primary" id="btn_save_teacher">{{ __('translation.Submit') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>
	
	<script src="{{ URL::asset('js/custom/jquery.theswitch.min.js') }}"></script>
	<script>
        $(document).ready(function(){
            
			$("#basic-switch").setTheSwitch({
				hsize: 21,
				width: 100,
				height: 25, 
				action: "<?php if($teacherDetails->status == 0){ echo 'off'; } else if($teacherDetails->status == 1){ echo 'on'; }else{ echo 'noset'; } ?>",
                onSet: function(e) {
                    $("#basic-switch").getTheSwitchValue(value=>{
                        
                    });
                },
                onClickOn: function(e) {
                   // var element = $(e).attr('id');
					var teacherid = $(e).attr('data-id');
					//alert(teacherid);
                   // console.log("client basic click on " + element);
                    $("#basic-switch").getTheSwitchValue(value=>{
						if(value == 'on'){
							var status = 1;
						}
						$.ajax({    
							type:"post",
							url:$('meta[name="route"]').attr('content') + '/admin/set-teacher',
							data:{userid:teacherid,status:status,acr:1},
							headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								},
							success:function(result) {
							}
						});
                    });
                    $( "#basic-switch-actions" ).val('on');
                },
                onClickNoSet: function(e) {
                   // var element = $(e).attr('id');
                   // console.log("client basic click noset "  + element);
				   var teacherid = $(e).attr('data-id');
                    $("#basic-switch").getTheSwitchValue(value=>{
                        if(value == 'noset'){
							var status = 2;
						}
						$.ajax({    
							type:"post",
							url:$('meta[name="route"]').attr('content') + '/admin/set-teacher',
							data:{userid:teacherid,status:status,acr:1},
							headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								},
							success:function(result) {
							}
						});
                    });
                    $( "#basic-switch-actions" ).val('noset');
                },
                onClickOff: function(e) {
                   // var element = $(e).attr('id');
                   // console.log("client basic click off "  + element);
				   var teacherid = $(e).attr('data-id');
                    $("#basic-switch").getTheSwitchValue(value=>{
                       if(value == 'off'){
							var status = 0;
						}
						$.ajax({    
							type:"post",
							url:$('meta[name="route"]').attr('content') + '/admin/set-teacher',
							data:{userid:teacherid,status:status,acr:0},
							headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								},
							success:function(result) {
							}
						});
                    });
                    $( "#basic-switch-actions" ).val('off');
                }
            });
        });
	
    </script>
	<script>
		function openTab(evt, tab) {
		  var i, tabcontent, tablinks;
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablinks");
		  for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		  }
		  document.getElementById(tab).style.display = "block";
		  //evt.currentTarget.className += " active";
		}
		
	</script>

@endsection