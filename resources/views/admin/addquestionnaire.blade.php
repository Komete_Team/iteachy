@extends('admin.layouts.app')
@section('title', 'Question')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('questionnaire.Add Question') }}</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>
	        	<form method="post" id="add-questionnaire" autocomplete="off">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
	    						<label for="text">{{ __('questionnaire.Question') }} : <span class="mandatory_field">*</span></label>
	    						<textarea class="form-control" name="question"></textarea>
	    					</div>
						</div>
						
					</div>
					<div class="row anserrow">
						<div class="col-lg-10">
							<div class="form-group">
	    						<label for="text">{{ __('questionnaire.Answer') }} : <span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" name="answer[]">
	    						
	    					</div>
						</div>
						<div class="col-lg-2">
							<div style="margin-top: 30px;"><i title="{{ __('questionnaire.Add Answer') }}" id="add-answer" class="fa fa-plus-circle" aria-hidden="true"></i></div>
						</div>
						
					</div>
					<div id="answer-section">
					</div>
					{!! csrf_field() !!}
					<br>
					<button type="submit" class="btn btn-primary" id="btn_save_school">Invia</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#add-questionnaire").validate({
			rules: {
				question: "required",
				'answer[]': "required"
			},
			messages: {
				question: "Per favore, inserisci la domanda.",
				'answer[]': "Per favore, inserisci la risposta."
			}
		});
		$('#add-answer').click(function(){
			if($('.anserrow').length < 5) {
				var answerHtml = '<div style="margin-top:10px;" class="anserrow row"><div class="col-lg-10" ><input class="form-control" type="text" name="answer[]"></div><div class="col-lg-2"><div><i class="remove-answer fa fa-times" aria-hidden="true"></i></div></div></div>';
				$('#answer-section').append(answerHtml);
			}
		});
		$( "body" ).on( "click", ".remove-answer", function() {
			$(this).parent().parent().parent().remove();
		});
		
	});
</script>
@endsection