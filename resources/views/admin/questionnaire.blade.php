@extends('admin.layouts.app')
@section('title', 'Questionnaires')

@section('content')
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Questionnaires</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<a href="{{ url('admin/questionnaires/add') }}" class="btn btn-primary">Aggiungi</a>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			@if (Session::has('success'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('success') }}</strong>
							</div>
						@endif
						@if (Session::has('error'))
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('error') }}</strong>
							</div>
						@endif
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Domanda</th>
			        				<th>{{ __('translation.Action') }}</th>
			        			</tr>
			        		</thead>
							<tbody>      
								<?php $i=1;?>
								@foreach($questionnaires as $ques)
								   
								<tr class="odd gradeX">
									<td>{{$i}}</td>
									<td>{{ !empty($ques->question)?$ques->question:'NA' }}</td>
									
									<td><a href="{{ url('admin/questionnaires/edit/'.$ques->id) }}" class="edit_sub"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$ques->id}}" class="delete_ques"><i class="fa fa-trash-o"></i></a></td>  
								  
								</tr>
								<?php 
									$i++;
								?>
                                       @endforeach
                                       
                                      
                                    </tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_ques',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "{{ url('admin/deletequestionnaire') }}"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
@endsection