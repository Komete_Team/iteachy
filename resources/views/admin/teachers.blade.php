@extends('admin.layouts.app')
@section('title', 'Teachers')

@section('content')
<?php //echo '<pre>'; print_r($teachers); die; ?>
	<!-- Multiselect dropdown -->
	<link href="{{ URL::asset('/css/multiple-select.css') }}" rel="stylesheet" />
	<script src="{{ URL::asset('/js/multiple-select.js') }}"></script>

	<script>
	$(document).ready(function(){
		$('#users').multipleSelect('destroy');
		$('#users').multipleSelect({
			width: '100%'
		});
	});
	</script>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('translation.teachers') }}</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_teacher_modal">{{ __('translation.add_teacher') }}</button>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table table-striped" id="datatable_teachers" >
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>{{ __('translation.First Name') }}</th>
									<th>{{ __('translation.Last Name') }}</th>
			        				<th>{{ __('translation.Email Id') }}</th>
			        				<th>{{ __('translation.Status') }}</th>
			        				<th>{{ __('translation.Action') }}</th>
			        			</tr>
			        		</thead>
							<tbody>
							@foreach($teachers as $teacher)
			        			<tr>
			        				<td>{{$teacher->id}}</td>
									<td>{{$teacher->first_name}}</td>
									<td>{{$teacher->last_name}}</td>
									<td>{{$teacher->email}}</td>
									<td>{{$teacher->status}}</td>
									<td><a href="{{ url('admin/viewteachers/'.$teacher->id) }}" id="{{$teacher->id}}" class="view_teacher"><i class="fa fa-eye"></i> | <a href="javascript:void(0);" id="{{$teacher->id}}" class="edit_teacher"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$teacher->id}}" class="delete_teacher"><i class="fa fa-trash-o"></i></a></td>
			        			</tr>
							@endforeach	
			        		</tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

	<!-- Model to send notification -->
	<div id="modal_teacher" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">{{ __('translation.add_teacher') }}</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_notification" id="frm_add_teacher" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">{{ __('translation.First Name') }}:</label>
							<input type="text" class="form-control" id="first_name" name="first_name">
							<input type="hidden" name="teacher_id" id="teacher_id" value="">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Last Name') }}:</label>
							<input type="text" class="form-control" id="last_name" name="last_name">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Email Id') }}:</label>
							<input type="text" class="form-control" id="email" name="email">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Password') }}:</label>
							<input type="text" class="form-control" id="password" name="password">
						</div>
						
						<div class="form-group">
							<label for="heading">{{ __('translation.Status') }}:</label>
							<input type="text" class="form-control" id="status" name="status">
						</div>
						
						<button type="button" class="btn btn-primary" id="btn_save_teacher">{{ __('translation.Submit') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection