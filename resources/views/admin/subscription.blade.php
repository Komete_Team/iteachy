@extends('admin.layouts.app')
@section('title', 'Subscriptions')

@section('content')
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Subscriptions</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<a href="{{ url('admin/subscriptions/add') }}" class="btn btn-primary">Aggiungi</a>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			@if (Session::has('success'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('success') }}</strong>
							</div>
						@endif
						@if (Session::has('error'))
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong>{{Session::get('error') }}</strong>
							</div>
						@endif
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Titolo</th>
									<th>Basic</th>
			        				<th>Standard</th>
									<th>Premium</th>
			        				<th>{{ __('translation.Action') }}</th>
			        			</tr>
			        		</thead>
							<tbody>      
								<?php $i=1;?>
								@foreach($subscriptions as $sub)
								   
								<tr class="odd gradeX">
									<td>{{$i}}</td>
									<td>{{ !empty($sub->title)?$sub->title:'NA' }}</td>
									<td>{{ !empty($sub->base)?$sub->base:'NA' }}</td>
									<td>{{ !empty($sub->standard)?$sub->standard:'NA' }}</td>
									<td>{{ !empty($sub->premium)?$sub->premium:'NA' }}</td>
									<td><a href="{{ url('admin/subscriptions/edit/'.$sub->id) }}" class="edit_sub"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$sub->id}}" class="delete_sub"><i class="fa fa-trash-o"></i></a></td>  
								  
								</tr>
								<?php 
									$i++;
								?>
                                       @endforeach
                                       
                                      
                                    </tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_sub',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "{{ url('admin/deletesubscription') }}"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
@endsection