@extends('admin.layouts.app')
@section('title', 'Categories Management')

@section('content')
	<style>
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 54px;
	  height: 28px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 20px;
	  width: 20px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
	</style>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('teacher') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Category Listing</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
			<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_category_modal">Aggiungi categoria</button>
				<button type="button" class="btn btn-primary" id="btn_show_sub_category_modal">Aggiungi sottocategoria</button>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Categoria</th>
									<th>Sotto categoria</th>
			        				<th>{{ __('translation.Action') }}</th> 
			        			</tr>
			        		</thead>
							<tbody>
							@foreach($categories as $category)
			        			<tr>
			        				<td>{{$category->id}}</td>
									<td>{{$category->category_name}}</td>
									<td><a href="{{ url('admin/subcategories/'.$category->id) }}" id="view_subcategory">Vedi sottocategoria</a></td>
									<td><a href="javascript:void(0);" id="{{$category->id}}" class="edit_category"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$category->id}}" class="delete_category"><i class="fa fa-trash-o"></i></a></td>
			        			</tr>
							@endforeach	
			        		</tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<div id="modal_category" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi categoria</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_category" id="frm_add_category" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Nome della categoria:</label>
							<input type="text" class="form-control" id="category_name" name="category_name">
							<input type="hidden" name="category_id" id="category_id" value="">
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_category">Submit</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>
	
	<!-- Model to add sucategory -->
	<div id="modal_sub_category" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi sottocategoria</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_sub_category" id="frm_add_sub_category" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Nome della sottocategoria:</label>
							<input type="text" class="form-control" id="sub_category_name" name="sub_category_name">
							<input type="hidden" name="sub_category_id" id="sub_category_id" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Seleziona il nome della categoria padre:</label>
							<select name="categories[]" id="categories" class="form-control" multiple="">
								@foreach( $categories as $category )
									<option value="{{ $category->id }}">{{ ucwords( strtolower( $category->category_name ) ) }}</option>
								@endforeach
							</select>
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_sub_category">Sottoscrivi</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection