@extends('teacher.layouts.app')
@section('title', 'Admin Bacheca')

@section('content')
	<style>
		.bob_col{width: 46%;
    padding-top: .75rem;
    padding-bottom: .75rem;
    background-color: rgba(86,61,124,.15);
    border: 1px solid rgba(86,61,124,.2);
    flex-basis: 0;
    float: left;
    flex-grow: 1;
    padding: 30px;
    margin-right: 15px;
		}
		.bob_col1{width: 30%;
	min-height:240px;	
    padding-top: .75rem;
    padding-bottom: .75rem;
    background-color: rgba(86,61,124,.15);
    border: 1px solid rgba(86,61,124,.2);
    flex-basis: 0;
    float: left;
    flex-grow: 1;
    padding: 30px;
    margin-right: 15px;
		}
		
.ratting_ul {

    padding-top: 20px;
    text-align: center;

}
	</style>
	<!-- Right side column. Contains the navbar and content of the page -->
	@php $user = Auth::user(); @endphp
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Bacheca
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Bacheca</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">

	        <!-- Small boxes (Stat box) -->
	        <!-- <div class="row">
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-aqua">
	                    <div class="inner">
	                        <h3>
	                            150
	                        </h3>
	                        <p>
	                            New Orders
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-bag"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-green">
	                    <div class="inner">
	                        <h3>
	                            53<sup style="font-size: 20px">%</sup>
	                        </h3>
	                        <p>
	                            Bounce Rate
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-stats-bars"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-yellow">
	                    <div class="inner">
	                        <h3>
	                            44
	                        </h3>
	                        <p>
	                            User Registrations
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-person-add"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-red">
	                    <div class="inner">
	                        <h3>
	                            65
	                        </h3>
	                        <p>
	                            Unique Visitors
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-pie-graph"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	        </div> -->

	        <div class="row">
	            <div class="col-xs-12 connectedSortable">
	                
	            </div>
	        </div>

	        <!-- Main row -->
			<?php 
				$todayslesson = Helper::todayLessons($user->id);
				$nextlecture = Helper::nextLecture($user->id);
				if(!empty($nextlecture)){
					$lecturehour = date('Y-m-d H:i',strtotime($nextlecture->start_date));
					$lecturetime = date('H:i',strtotime($nextlecture->start_date));
					//echo '<br/>';
					$currenthour = date('Y-m-d H:i'); 
					//echo '<br/>';
					
					$starttimestamp = strtotime($currenthour);
					$endtimestamp = strtotime($lecturehour);
					$difference = round(abs($endtimestamp - $starttimestamp)/3600,2);
				}else{
					$difference = '0';
					$lecturetime = '00:00';
				}
				
				//echo $difference; die;
				
				//echo '<pre>'; print_r($nextlecture); die;
			?>
	        <div class="row">
	        	<div class="col-lg-12">
					<div style="width: 100%; display: inline-block;margin-bottom: 50px;text-align: center;font-size: xx-large;">
						<div class="container">
						  <div class="row">
							<div class="col bob_col">
							  <p style="    width: 45%;font-size: 17px;text-transform: uppercase;float: left;"><b>LEZIONI DI OGGI</b></p> | <p style="    width: 45%;font-size: 17px;text-transform: uppercase;float: right;display: table;"><b>{{count($todayslesson)}}</b></p>
							  
							</div>
							<div class="col bob_col">
							  <p style="    width: 45%;font-size: 17px;text-transform: uppercase;float: left;"><b>La prossima lezione e tra {{$difference}} ora</b></p>
							  <p style="    width: 45%;font-size: 17px;text-transform: uppercase;float: right;display: table;"><b style="display:block">Ore {{$lecturetime}}</b>
							  <a href="#" style="margin-top: 15px;" class="btn btn-default btn_custom_li">Collegati Ora</a>
							  </p>
							</div>
						  </div>
						</div>
					</div>
					<div style='clear:both'></div>
					<div style="width: 33%; display: inline-block;float:left;background-color: rgba(86,61,124,.15);margin-right:15px">
					
				<?php $totalreviews = count($teacherreviews); $totalreviews = $totalreviews-1; ?>
				<?php 
					$imgUrl = url('/images/avatar5.png');
					if(!empty($teacherreviews)){
					if( isset( $teacherreviews->profile_image ))
					{
						$imgUrl = url('/images/teacher_logos/' . $teacherreviews->profile_image);
					}
				?>
                <div class="description_box3">
                  <h2>recensioni</h2>
                  <hr>
				  <div style="width:100%;display:block;float: left;padding:10px;">
                  <img src="{{$imgUrl}}" style="width:30%; float:left;display: inline-block;">
				  <div class="reviews {{$teacherreviews->rate}}" style="width:70%;float:left;padding:10px;">
				  <p>{{$teacherreviews->first_name ?? ''.' '.$teacherreviews->last_name ?? ''}}</p>
					<ul class="list-inline">
						@for($i=1; $i<=5; $i++)
						<?php 
						 if($i <= $teacherreviews->rate)
							$cl = 'red';
						 else
							$cl = ' ';
						 ?>
						<li class="{{$cl}}"><a href="javascript:void(0)" ><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
						@endfor
					</ul>
											
				  </div>
				  </div>
                  <div style="padding:10px;">
                  <p class="p_heading">{{$teacherreviews->title ?? ''}}</p>
                  <p class="p_detail">{{$teacherreviews->review_message ?? ''}}</p>
				  </div>
                </div>
				<?php }else{ ?>
					<p>No reviews</p>
				<?php }?>
             
					</div>
					<div id='calendar' style="width: 65%; display: inline-block;"></div>
					<div style='clear:both'></div>
					<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<p></p>
								</div>
							</div>
						</div>
					</div>
					<div style='clear:both'></div>
					
					<?php 
						$totalReceiveNot = Helper::totalNotification($user->id);
						$totalSentNot = Helper::notificationSentDetail($user->id);
						$totalArchiveNot = Helper::notificationArchiveDetail($user->id);
						$totalNewPaymentNot = Helper::newPaymentNotification($user->id);
						//$totalArchiveNot = Helper::updateNotification($user->id);
					?>
					<div style="width: 100%; display: inline-block;margin-top: 50px;">
						<div class="container">
						  <div class="row">
							
							<div class="col bob_col1">
							<h3><a href="{{url('/teacher/all_notifications')}}">Messaggi</a></h3>
							<hr style="border:1px solid #000"></hr>
							<p><b>{{count($totalReceiveNot)}} nuavi messaggi</b></p>
							<p>{{count($totalSentNot)}} messaggi inviati</p>
							<p>{{count($totalArchiveNot)}} messaggi archiviati</p>
							</div>
							<div class="col bob_col1">
							<h3>Lezioni Cancellate</h3>
							<hr style="border:1px solid #000"></hr>
							  @if(count($totalrecords) > 0)
								  @foreach($totalrecords as $val)
								  <p>{{$val->cancel_record}} lezione cancellata | {{$val->date_only}}</p>
								  @endforeach
							  @endif
							</div>
							<div class="col bob_col1">
							  <h3>Pagamenti</h3>
							  <hr style="border:1px solid #000"></hr>
							  <p>{{count($totalNewPaymentNot)}} nuovo pagamento arrivato</p>
							  
							</div>
						  </div>
						</div>
					</div>
					<div style='clear:both'></div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->

	    </section><!-- /.content -->
	</aside>
	<!-- /.right-side -->
	<?php //echo '<pre>'; print_r($teacherAvailable); die;
	
	foreach($teacherAvailable as $teacherAvailableval)
	{
		$arr[] = ['start' => $teacherAvailableval->start_time, 'end' => $teacherAvailableval->end_time, 'dow'=>[$teacherAvailableval->num_days], ' rendering'=>'background'];
		
	}
	
	
	//$json = json_encode($arr);

	//echo $avlobj; die;
	?>
	<script>
	$(document).ready(function() {
		$('#calendar').fullCalendar({
			header:{
				left:'prev,next today',
				center:'title',
			},
			disableDragging: true,
			monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
			monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
			dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
			dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
			allDayText: 'Giornata',
			firstDay: 1,
			buttonText: {
				today: 'oggi',
				month: 'mese',
				week: 'settimana',
				day: 'giorno',

			},
			
			height: 400,
			defaultView: 'agendaWeek',
			slotDuration: '00:30:00',
			slotLabelInterval: 30,
			slotLabelFormat: 'H:mm',
			editable: false,
			axisFormat: 'HH:mm',
			timeFormat: 'HH:mm',
			theme: true,    
			themeSystem:'bootstrap3', 
			close: 'fa-times',
			prev: 'fa-chevron-left',
			next: 'fa-chevron-right',
			prevYear: 'fa-angle-double-left',
			nextYear: 'fa-angle-double-right', 
			dayClick: function(date, jsEvent, view) {
		    	// console.log(date.format('H:m'))
		    	
		    	

		    },  
			//allDay: false,
		    events: "{{ url('/teacher/teacherappointment') }}",
		    selectable:false,
		    selectHelper:false, 
		    eventClick: function(event) {
		    	console.log(event);
		    	var id= event.id; 
		  		//$('#myModal').show();
		  	},
		  	select: function(startDate, endDate, jsEvent, view, resource) {
		  		/* if(view.name=='agendaDay'){
		  			var hoursStart = startDate.format('H:m');
					var hoursEnd = endDate.format('H:m');
					$('#timepicker1').timepicker('setTime', hoursStart);
					$('#timepicker2').timepicker('setTime', hoursEnd);
		      	} */
		    }
		  });	

		});
	</script>		
	
@endsection