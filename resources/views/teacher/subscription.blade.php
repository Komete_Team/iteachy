@extends('teacher.layouts.app')
@section('title', 'Teacher Subscription')

@section('content')
<?php
$paypalDetail = array();
$BusinessEmail= 'testve@yopmail.com';
$paypalUrl=  'https://www.sandbox.paypal.com/cgi-bin/webscr';
$user = Auth::user();
$user_id = $user['id'];
//echo '<pre>'; print_r($user['id']); die;
?>
	<style>
		.comparison .active{background-color: #dff0d8;}
	</style>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('teacher') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('subscription.Teacher Subscription') }}</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <center><h1>{{ __('subscription.Subscription') }}</h1>
	        </center>
	        <div class="comparison">
	        	<table>
	        		<thead>
				      <tr>
				        <th colspan="4" class="qbo">
				          {{ __('subscription.Choose the most suitable plan for you!') }}
				          {{ __('subscription.You can change at any time.') }}
				        </th>
				      </tr>
				      <tr>
				        <th class="compare-heading">{{ __('subscription.time_period') }}</th>
				        <th class="compare-heading">
				          Base
				        </th>
				        <th class="compare-heading">
				          Standard
				        </th>
				        <th class="compare-heading">
				          Premium
				        </th>
				      </tr>
				    </thead>
				    <tbody>
				    	@foreach($subscriptions as $subs)
				      	<tr>
				        	<td></td>
				        	<td colspan="4">&nbsp;</td>
				      	</tr>
				      	<tr class="compare-row">
				        	<td>{{ $subs->title }}</td>
				        	<td price="{{$subs->base}}" class="plan">{{ 'EUR '.$subs->base }}</td>
				        	<td price="{{$subs->standard}}" class="plan">{{ 'EUR '.$subs->standard }}</td>
				        	<td price="{{$subs->premium}}" class="plan">{{ 'EUR '.$subs->premium }}</td>
				      	</tr>
				      	@endforeach
				  	</tbody>
	        	</table>
				<div class="center form-group" style="padding: 10px;">
				<form role="form" method="post" action="{{$paypalUrl}}"  id="demo-form" data-parsley-validate>
					{{ csrf_field() }} 
					<input type="hidden" name="business" value="{{$BusinessEmail}}">  
					<input type="hidden" name="cmd" value="_xclick"> 
					<input type="hidden" name="item_name" value="Subscription">
					<input type="hidden" name="item_number" value="{{$user_id}}">

					<input type="hidden" id="sub_amount" name="amount" value="">
					<input type="hidden" name="currency_code" value="EUR">    
					<input type="hidden" name="custom" id="custom" value="0">                                        
					<input type='hidden' name='cancel_return' value='{{url("teacher/paymentcancel")}}'>
					<input type='hidden' name='return' value='{{url("teacher/paymentsuccess")}}'> 
					<!-- <input type='hidden' name='notify_url' value='{{url('paypal/ipnstatus')}}'> -->
					<input type='hidden' name='notify_url' value='https://www.komete.it'>
					<button id="subscription_pay" class="btn btn-primary disabled">{{ __('translation.pay') }}</button>
				</form>
				</div>
	        </div>
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->

@endsection