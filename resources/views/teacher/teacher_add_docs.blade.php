@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
<style>
.container{
 margin: 0 auto;
}
.content1{
 width: 150px;
 float: left;
 margin-right: 5px;
 border: 1px solid gray;
 border-radius: 3px;
 padding: 5px;
}

/* Delete */
.content1 span{
 border: 2px solid red;
 display: inline-block;
 width: 99%; 
 text-align: center;
 color: red;
}
.content1 span:hover{
 cursor: pointer;
}
img{width:100% !important}
</style>
<script>
$(document).ready(function(){

 $("#but_upload").click(function(){

  var fd = new FormData();
  var files = $('#file')[0].files[0];
  fd.append('file',files);
  fd.append('request',1);

  // AJAX request
  $.ajax({
   url: 'upload_docs/{{$user_id}}',
   type: 'post',
   data: fd,
   contentType: false,
   processData: false,
   headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
   success: function(response){
     if(response != 0){
       var count = $('.container .content1').length;
       count = Number(count) + 1;
		var src = '<?php echo url('/uploads/teachers/documents'); ?>/'+response;
       // Show image preview with Delete button
       $('.container').append("<div class='content1' id='content_"+count+"' ><iframe style='width:100%' src='"+src+"' bob='"+response+"' width='100' height='100'></iframe><span class='delete' id='delete_"+count+"'>Delete</span><input type='hidden' value='"+response+"' name='files[]' /></div>");
     }else{
       alert('file not uploaded');
     }
   }
  });
 });

 // Remove file
 $('.container').on('click','.content1 .delete',function(){
 
  var id = this.id;
  var split_id = id.split('_');
  var num = split_id[1];

  // Get image source
  var imgElement_src = $( '#content_'+num+' img' ).attr("bob");
  var deleteFile = confirm("Do you really want to Delete?");
  if (deleteFile == true) {
      // AJAX request
      $.ajax({
        url: 'upload_docs/{{$user_id}}',
        type: 'post',
	    headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
        data: {path: imgElement_src,request: 2},
        success: function(response){
           // Remove
           if(response == 1){ 
              $('#content_'+num).remove(); 
           } 
        } 
      }); 
   } 
 }); 
});
</script>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Aggiungi documento</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
	    <!-- Main content -->
		 <!-- Main row -->
		<section class="content">
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
				
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
								
	    						<label for="text">{{ __('translation.Document Name') }}:<span class="mandatory_field">*</span></label>
								 <input type="text" class="form-control" id="doc_name" name="doc_name" />
	    						
								@if ($errors->has('doc_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('doc_name') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>	
						
					<div class="row">
							<div class="col-lg-12">
								<div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
									
									<label for="text">{{ __('translation.Upload Certificates') }}:<span class="mandatory_field">*</span></label>
									 
									 <div class="input-group">
										<input type="hidden" id="filename" name="filename" value="">
										<input type="file" id="file" name="file" class="form-control form-control-sm" accept="application/pdf">
										<div class="input-group-btn">
											<input type="button"class="rounded-0 btn btn-primary" value="{{ __('translation.Upload') }}" id="but_upload">
										</div>
									</div>
									
									@if ($errors->has('file'))
										<span class="help-block">
											<strong>{{ $errors->first('file') }}</strong>
										</span>
									@endif
								</div>
								<div class="container"></div>
							</div>
						
					</div>
					<div class="row">	
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('teacher_description') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Write Note') }}:<span class="mandatory_field">*</span></label>
	    						<textarea type="text" class="form-control" id="teacher_description" name="teacher_description">{{ $teacherDetails->teacher_description ?? '' }}</textarea>
								@if ($errors->has('teacher_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('teacher_description') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>

					<button type="submit" class="btn btn-primary" id="btn_save_school">{{ __('translation.Accredited profile') }}</button>
				</form>
	       	</div>
		</section>	
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<div id="modal_teacher_leave" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">modifica permesso</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_leave" id="frm_edit_leave" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Applica congedo:</label>
							<div class="input-group date form_datepicker" data-date="" data-date-format="dd MM yyyy" data-link-field="leave_date" data-link-format="yyyy-mm-dd">
								<input class="form-control" size="16" type="text" value="" id="leave_date" name="leave_date" readonly>
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
							</div>
							
							<input type="hidden" name="leave_id" id="leave_id" value="">
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_leave">{{ __('translation.Accredited profile') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection