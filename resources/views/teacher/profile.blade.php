@extends('teacher.layouts.app')
@section('title', 'Profile Management')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('teacher') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">{{ __('translation.profile') }}</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>		
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-lg-3">
							<label for="school_logo">
							<?php //echo '<pre>'; print_r($teacherDetails); die; ?>
							    <!-- <img src="{{ url('/images/school_default_logo.jpg') }}" height="100px" width="100px"> -->
							    <?php
							    $imgUrl = url('/images/avatar.png');
							    if( $teacherDetails->profile_image != '' )
							    {
							    	
							    		$imgUrl = url('/images/teacher_logos/' . $teacherDetails->profile_image);
							    	
							    }
							    ?>
							    <img src="{{ $imgUrl }}" height="100px" width="100px">
							    <input type="file" id="school_logo" name="teacher_logo" style="display:none;">
								<input type="hidden" value="{{$teacherDetails->profile_image}}" name="old_image" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.First Name') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="first_name" name="first_name" value="{{ $teacherDetails->first_name ?? '' }}">
								@if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Last Name') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="last_name" name="last_name" value="{{ $teacherDetails->last_name ?? '' }}">
								@if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Email Id') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="email" name="email" value="{{ $teacherDetails->email ?? '' }}">
								@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('contact_no') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Contact No') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_no" name="contact_no" value="{{ $teacherDetails->contact_no ?? '' }}">
								@if ($errors->has('contact_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact_no') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Address') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="address" name="address" value="{{ $teacherDetails->address ?? '' }}">
								@if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-6">
	    					<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.City') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="city" name="city" value="{{ $teacherDetails->city ?? '' }}">
								@if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('province') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Province/State') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="province" name="province" value="{{ $teacherDetails->province ?? '' }}">
								@if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Country') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="country" name="country" value="{{ $teacherDetails->country ?? '' }}">
								@if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Category') }}:<span class="mandatory_field">*</span></label>
	    						
								@php $selectedcategories = (array)json_decode($teacherDetails->categories); @endphp	
								@if(count($categories) > 0)
									<select name="categories[]" id="category"  class="form-control" multiple="multiple">
								
									@foreach($categories as $cat)
										 <optgroup label="{{$cat->category_name}}">
										@foreach($subcategories as $subcat)
											@if($cat->id == $subcat->parent_id)
												<option <?php if(!empty($selectedcategories['subcategory'])){ if(in_array($subcat->id, $selectedcategories['subcategory'])){ echo 'selected'; } }?> value="{{$cat->id}}-{{$subcat->id}}">{{$subcat->category_name}}</option>
												
												
											@endif
										@endforeach
										</optgroup>
									@endforeach
				
									
										
									</select>
								@endif
								
									
								
								@if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text">{{ __('translation.online') }}:</label>
	    						<input style="width:auto;" type="radio" class="form-control" id="online" name="online" <?php if($teacherDetails->is_online){ echo 'checked';} ?> value="{{ $teacherDetails->is_online ?? '' }}">
	    					</div>
						</div>
					</div>	
					
					

					

					<button type="submit" class="btn btn-primary" id="btn_save_school">{{ __('translation.Submit') }}</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<script>
		$('#category').multipleSelect({
			width: '100%',
			filter: true,
		});
	</script>
	<!-- /.right-side -->

@endsection