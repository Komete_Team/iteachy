@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('teacher') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Lezioni</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
			<!--<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_lesson_modal">Aggiungi lezione</button>
	    	</div>-->
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table user-data-table" id="datatable_category">
                                    <thead>
                                        <tr>
                                            <th>S-No</th>
                                            <th>Name</th>
                                            <th>Email</th>
											<th>Appointment Start Date</th>
                                            <th>Appointment End Date</th>
											<th>Skype Id</th>
											<th>Meeting Place</th>
                                            <th>Knowledge Level</th>
											<th>Message</th>
											<th>{{ __('translation.Action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $i=1; //echo '<pre>'; print_r($teacherappointments); die;?>
                                        @foreach($teacherappointments as $detail)
                                        <tr class="odd gradeX">
                                            <td>{{$i}}</td>
                                             <td>{{$detail->first_name.' '.$detail->last_name}}</td> 
                                             <td>{{$detail->email ?? ''}}</td>
											 <td>{{date('m-d-Y H:i',strtotime($detail->start_date))}}</td>
											 <td>{{date('m-d-Y H:i',strtotime($detail->end_date))}}</td>
											 <td>{{$detail->skype_id ?? 'N/A'}}</td>
											 <td>{{$detail->meeting_place ?? 'N/A'}}</td>
											 <td>{{$detail->knowledge_level ?? 'N/A'}}</td>
											 <td>{{$detail->message ?? ''}}</td>
											 <td><a href="javascript:void(0);" id="{{$detail->aptid}}" class="edit_appointment"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$detail->aptid}}" class="delete_appointment"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <?php 
                                            $i++;
                                        ?>
                                       @endforeach
                                       
                                      
                                    </tbody>
                                </table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add sucategory -->
	<div id="modal_appointment" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_appointment" id="frm_edit_appointment" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Appointment Start Date:</label>
							<input type="text" class="form-control" name="appointment_start_date" id="appointment_start_date" value="">
							<input type="hidden" name="appointment_id" id="appointment_id" value="">
						</div>
						
						<div class="form-group">
							<label for="heading">Appointment End Date:</label>
							<input type="text" class="form-control" name="appointment_end_date" id="appointment_end_date" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Skype ID:</label>
							<input type="text" class="form-control" name="skype_id" id="skype_id" value="">
						</div>
						<div class="form-group">
							<label for="categories">Meeting Place:</label>
							<input type="text" class="form-control" name="meeting_place" id="meeting_place" value="">
						</div>
						<div class="form-group">
							<label for="categories">Knowledge Level:</label>
							<input type="text" class="form-control" name="knowledge_level" id="knowledge_level" value="">
						</div>
						<div class="form-group">
							<label for="categories">Message:</label>
							<textarea class="form-control" name="message" id="message"></textarea>
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_appointment">Sottoscrivi</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection