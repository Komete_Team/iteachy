@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>{{ __('translation.control_panel') }}</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('teacher') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table user-data-table" id="datatable_category">
                                    <thead>
                                        <tr>
                                            <th>S-No</th>
                                            <th>Foto</th>
                                            <th>Nome</th>
                                             <th>E-mail</th> 
                                            <th>Messaggio</th>
                                             <th>Tipo di utente</th>
											 <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $i='1';?>
                                        @foreach($notifications as $detail)
                                           
                                        <tr class="odd gradeX">
                                            <td>{{$i}}</td>
                                            <td><div class="user-avatar"><a class="chlid" data-id="{{$detail->id}}">
                                                <?php  
												$imagefolder = array(1=>'profile_images/', 2=>'teacher_logos/', 3=>'student_profileimage/');
												if(!empty($detail->profile_image)){
												?>
													<img style="width: 50px;" src="{{url('images/'.$imagefolder[$detail->role].$detail->profile_image)}}">
												<?php }else{?>
													<img style="width: 50px;" src="{{url('images/avatar5.png')}}">	
												<?php } ?>
                                                   

                                              </a></div> </td>
                                             <td>{{$detail->first_name.' '.$detail->last_name}}</td>
                                             <td>{{$detail->email}}</td> 
                                             <td>{{$detail->description}}</td>
											 <td><?php if($detail->role==3){echo 'Student';}else if($detail->role==1){echo 'Admin';}else if($detail->role==2){echo 'Teacher';}?></td> 
											<td>
											<?php if($detail->role==3){ ?>
											<a target="_blank" href="{{url('chat/'.$detail->lecture_id)}}" ><i class="fa fa-reply" aria-hidden="true"></i></a>
											<?php } ?>
											</td>
                                          
                                        </tr>
                                        <?php 
                                            $i++;
                                        ?>
                                       @endforeach
                                       
                                      
                                    </tbody>
                                </table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<!-- Model to add category -->
	<div id="modal_teacher_not" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">modifica permesso</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_leave" id="frm_edit_leave" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">User Name:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Email:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Message:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Message:</label>
							<p id="user_name"></p>
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_leave">{{ __('translation.Accredited profile') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection