@extends('teacher.layouts.app')
@section('title', 'Student Payment Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Payment Info</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 @if (Session::has('success'))
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>
		<section class="content">
			<div>
				<form id="payment-info" method="post">
					<div class="row">
						<div class="col-lg-6">
							{{ __('translation.Type of Payment') }}
						</div>
						<div class="col-lg-6">
							<div><input type="radio" {{ ($payinfo->payment_type ==1)?'checked':''}} name="type_of_payment" value="1"/> {{ __('translation.Paypal') }}</div>
							<div><input {{ ($payinfo->payment_type ==2)?'checked':''}} type="radio" value="2" name="type_of_payment"/> {{ __('translation.Credit Card') }}</div>
						</div>
					</div>
					<div id="card-section" style="{{ ($payinfo->payment_type ==2)?'':'display: none;'}}">
						<?php 
						$cardinfo=[];
						if(!empty($payinfo->pay_info)) {
							$cardinfo = json_decode($payinfo->pay_info);
						}
						?>
						<div class="row">
							<div class="col-lg-6">
								{{ __('translation.Name On Card') }}
							</div>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="name-on-card" name="card[name_on_card]" value="{{isset($cardinfo->name_on_card)?$cardinfo->name_on_card:''}}">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								{{ __('translation.Card Number') }}
							</div>
							<div class="col-lg-6">
								<input type="number" class="form-control" id="card-number" name="card[card_number]" value="{{isset($cardinfo->card_number)?$cardinfo->card_number:''}}">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								{{ __('translation.Expiry Month - Year') }}
							</div>
							<div class="col-lg-6">
								<select name="card[expiryMonth]" id="expiryMonth"
								class="form-control">
								<?php
								for ($i = 1; $i <= 12; $i ++) {
									$monthValue = $i;
									if (strlen($i) < 2){
										$monthValue = "0" . $monthValue;
									}
									?>
								<option {{isset($cardinfo->expiryMonth)?($cardinfo->expiryMonth == $monthValue?'selected':''):''}} value="<?php echo $monthValue; ?>"><?php echo $monthValue; ?></option>
								<?php
								}
								?>
								</select> 
								<select name="card[expiryYear]" id="expiryYear" class="form-control">
								<?php
								for ($i = date("Y"); $i <= date("Y")+10; $i ++) {
									$yearValue = $i;
									?>
									<option {{isset($cardinfo->expiryYear)?($cardinfo->expiryYear == $yearValue?'selected':''):''}} value="<?php echo $yearValue; ?>"><?php echo $i; ?></option>
								<?php
								}
								?>
								</select>
					            
							</div>
						</div>
					</div>
					{!! csrf_field() !!}	
					<button type="submit" class="btn btn-primary" id="btn_save_card-info">invia
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='type_of_payment']").click(function(){
			if($(this).val() == 2) {
				$('#card-section').show();
			} else {
				$('#card-section').hide();
			}
		});
		
		$('#payment-info').validate({
			rules: {
				'card[name_on_card]': "required",
				'card[card_number]': "required"
			},
			messages: {
				'card[name_on_card]': "Inserisci il nome sulla carta.",
				'card[card_number]': "Inserisci il numero della carta."
			}
		});
	});
</script>
@endsection
