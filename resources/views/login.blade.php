@extends('layouts.app')
@section('title', 'Teacher Login')
@section('content')
	<script>
	$(document).ready(function(){
		// Initialize the functionality
		loginObj.init();
	});

	var loginObj = {
		init: function() {
			loginObj.holdFormSubmit();
			
			loginObj.formValidation();
			
			loginObj.loginFunction();
		},
		holdFormSubmit: function() {
			// Form validations
			$('#frm_teacher_login').submit(function(e){
		        e.preventDefault();
		    });
		},
		formValidation: function(){
			$('#frm_teacher_login').validate({
				rules: {
					username: {
						required: true,
						email: true
					},
					password: {
						required: true
					}
				},
				messages: {
					username: {
						required: 'Si prega di inserire il nome utente',
						email: 'per favore inserisci UN nome utente valido'
					},
					password: {
						required: 'Per favore, inserisci la password'
					}
				}
			});
		},
		loginFunction: function() {
			// Login functionality
		    $('#btn_admin_login').click(function(){
		    	// Check the validation
		    	if( $('#frm_teacher_login').valid() )
		    	{
		    		// Hold the button reference
		    		var btn = $(this);

					$('#server_resposne').hide();
		    		$('#server_resposne_msg').html('');

		    		$.ajax({
		    			url: $('meta[name="route"]').attr('content') + '/login',
		    			method: 'post',
		    			data: {
		    				frmData: $('#frm_teacher_login').serialize()
		    			},
		    			beforeSend: function() {
		    				// Disable the button
					        $(btn).attr('disabled', true);
					        
					        $('#loading_spinner').show();
					    },
		    			headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    complete: function()
					    {
					    	// Enable the button
					    	$(btn).attr('disabled', false);

					    	$('#loading_spinner').hide();
					    },
					    success: function(response){
					    	if( response.resCode == 0 )
					    	{
					    		 document.location.href = response.redirectUrl;
					    	}
					    	else
					    	{
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    }
		    		});
		    	}
		    });
		}
	}
	</script>

	<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 10% auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
    label.error {
        color: red;
    }
	</style>
	<div class="login-form">
         @if (Session::has('success'))
                       <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{Session::get('success') }}</strong>
                        </div>
                        @elseif(Session::has('danger'))

                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{Session::get('danger') }}</strong>
                        </div>
         @endif 
        <form autocomplete="off" name="frm_teacher_login" id="frm_teacher_login">
            <h2 class="text-center">Accesso</h2>
            <div class="form-group">
            	<label for="username">Email utente :</label>
                <input type="text" class="form-control" placeholder="" name="username" id="username">
                <input type="hidden" name="source" value="teacher">
            </div>
            <div class="form-group">
            	<label for="username">Password:</label>
                <input type="password" class="form-control" placeholder="" name="password" id="password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" id="btn_admin_login">Accesso</button>
            </div>

            <!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-danger alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>
        </form>
    </div>
@endsection
