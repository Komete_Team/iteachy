@extends('layouts.app')

@section('content')
<style>
.progress-bar{background-color: #C4C4C4;width: 50px;position: absolute;bottom: 22px;}
.stat{width: 50px;height: 200px;background-color: #444;position: absolute;}
</style>

<script src="{{ URL::asset('front/js/custom_listing.js') }}"></script>
<div id="profile_page"> 
<form name="frm1_search_teacher" id="frm1_search_teacher" method="get" action="{{url('getteachers')}}" autocomplete="off" enctype="multipart/form-data">
    <div class="top_search"> 
        <div class="container_filter"> 
            <div class="row">
             <div class="search_section">
					<div class="top_search"> 
						<div class="container_filter"> 
							<div class="row">
							 <div class="search_section">
							  <div class="col-md-4 col-md-offset-2"> 
								<select name="categories[]" id="teachers" class="form-control select2 chosen-select chosen-select-all" multiple  placeholder="seleziona una categoria">
										@if(count($recentSelectedCategories) > 0)	
											<optgroup label="Recent Search">
										@foreach($recentSelectedCategories as $recentval)
											  <option value="{{$recentval->id}}">{{$recentval->category_name}}</option>
										@endforeach	  
											</optgroup>
										@endif	
										@if(count($categories) > 0)
											@foreach($categories as $cat)
								<?php 
									$l=1;
									$totalsubcats = count($subcategories);
									//echo '<pre>'; print_r($totalsubcats); die;
								?>
								@foreach($subcategories as $subcat)
									@if($cat->id == $subcat->parent_id)
										@if($l == 1)
											<optgroup label="{{$cat->category_name}}">	
										@endif
											<option value="{{$subcat->id}}">{{$subcat->category_name}}</option>
										@if($l == $totalsubcats)
											</optgroup>
										@endif
									<?php $l++; ?>	
									@endif
								@endforeach
									
							@endforeach
										@endif
								</select>
								
							  </div>
							  <div class="col-md-4"> 
								<div class="calender_input"> 
								  <div class="input-group" id='datetimepicker2'>
										<span class="input-group-addon">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</span>
										<input type='text' class="form-control" placeholder="Disponibilità" aria-describedby="basic-addon1" id="dtp_input" name="dtp_input" />
								  </div>
								</div>
							  </div>
							 </div>
							</div>
						</div>
					</div>
				
				
             </div>
            </div>
        </div>
    </div>

    <div class="profile_menu"> 
        <div class="container_filter"> 
            <div class="inline_menu"> 
             <ul>  
							  <li><a href="#"> <input type="checkbox" name="accridited" class="form-control" value="1"> Solo professori accreditati</a> </li>
							  <li><a href="#"> <img src="{{ URL::asset('front/images/locationh.png') }}">Tipo di insegnante</a> </li>
							  <li class="clickMe" data-num="3"><a href="#"> <img src="{{ URL::asset('front/images/save.png') }}">Prezzo</a>
								<div id="open-3" class="price-range">
								<div class="slider-labels">
								<div class="caption">
								<span id="slider-range-value1"></span>
								</div>
								<div class="text-right caption">
								<span id="slider-range-value2"></span>
								</div>
								</div>
								<div id="slider-range"></div>
								<input type="hidden" name="min-value" id="min-value" value="">
								<input type="hidden" name="max-value" id="max-value" value="">
								</div>
							</li>
							<li> <button type="Submit" class="btn btn-default btn_custom_li">Ricerca</button></li>
							 </ul>
           </div>
       </div>
    </div>
	</form>

        <div class="prenota1_section">
          <div class="container">
            <div class="row">
             <div class="col-md-8">
              <div class="bg_gray">
                <div class="prenota1_video">
                  <div id="prenota1_video" class="prenota1_tabcontent">
                     <iframe width="100%" height="456" src="{{url('videos/teacher_videos/'.$teacherInfo->file_name)}}"></iframe>
                  </div>
                </div>
                <div class="prenota1_profile_display">
				<?php 
					
					$userCategories = explode("$",$teacherInfo->categories);
							
					$imgUrl = url('/images/avatar5.png');

					if( isset( $teachers_val->profile_image ))
					{

					$imgUrl = url('/images/teacher_logos/' . $teacherInfo->profile_image);

					}
				?>
                <div class="col-md-2">
                  <div class="prenota1_img"> 
                     <img src="{{$imgUrl}}">
                   </div>
                </div>
				<?php //echo '<pre>'; print_r($teachertotalreviews); die; ?>
				
                <div class="col-md-7">
                  <div class="profilemain_user-info"> 
                    <h2>  {{$teacherInfo->first_name.' '.$teacherInfo->last_name}}</h2>
                    <h3 class="info">Insegnante professionista</h3>
                    <div class="prenot_designation">
                    <div class="pre_display1">
                      <p>Vive a</p>
                      <p>{{$teacherInfo->city ?? 'N/A'}}</p>
                    </div>
                    <div class="pre_display2">
                      <p>ROMA</p>
					  @if (count($userCategories) > 0)
						  @foreach($userCategories as $catval)
							 <p>{{$catval}} |</p>
						  @endforeach
					  @endif
                     
                    </div>
                  </div>
                </div>
                </div>

                 <div class="col-md-3">
                    <div class="profilemain_rating">
                      <ul class="profilemain_ratting_ul">  
						@php $totalrates = ceil($teachertotalreviews->rates); @endphp 
						@for($i=1; $i<5; $i++)
							@if($i<=$totalrates)
								<li class="red"> <i class="fa fa-star-o" aria-hidden="true"></i></li> 
							@else
								<li> <i class="fa fa-star-o" aria-hidden="true"></i></li>
							@endif	
                        @endfor 
                      </ul>
                      <p class="p_para1">
						@php $totallesson = count($teacherLessons); @endphp
                        {{$totallesson}} LEZIONI
                      </p>
                      <p class="p_para2">{{$teachertotalreviews->totalusers ?? 0}} STUDENTI</p>
                    </div>
                 </div>
               </div>
                <div class="prenotal_content_div">
                  <h2 class="prenotal_heading"><span class="file_text"><i class="fa fa-file-text-o" aria-hidden="true"></i></span> Informazioni su di me</h2>
                  <hr class="prenotal_hr">
                    <p>{{$teacherInfo->description ?? ''}}</p>
                </div>
                </div>
              </div>
              <div class="col-md-4">
                  <div class="profilemain_side">
                    <div class="tab_first">
                     						 @if(count($teacherLessons) > 0)
						   @foreach($teacherLessons as $lessonval)
							@if($lessonval->type == 0)
								@php $price =  $lessonval->price_per_hour;	@endphp
							@else 
								@php $price =  $lessonval->price_per_hour;	@endphp
							@endif
							@if($lessonval->type == 0)
								
							 <div class="tab_1">
								<div class="profilemain_info pull-left">
								 <p>{{$lessonval->lesson_name}} (Trial)</p>
							   </div>
							   <div class="d_right pull-right">
								 <p>EUR {{$price}}</p>
							   </div>
							 </div>
							@else 
							 <div class="tab_2">
								<div class="profilemain_info pull-left">
								 <p>{{$lessonval->lesson_name}}</p>
							   </div>
							   <div class="d_right pull-right">
								 <p>EUR {{$price}}</p>
							   </div>
							 </div>
							@endif	
						  @endforeach
						@endif
                   </div>
				   

                    
                    <div class="btn_full_width">
                     <div class="btn-group dropup">
                       <a href="{{ url('student/book_lecture/'.$teacherInfo->teacher_id) }}" @if (Auth::guest()) disabled @endif id="singlebutton" name="singlebutton" class="btn btn_custom ">{{ __('translation.book_lesson') }}</a>
                        <button type="button" class="btn btn_custom_small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fa fa-angle-right" aria-hidden="true"></i>
                       <span class="sr-only">Toggle Dropdown</span>
                       </button>
                     </div>
                    </div>
                  </div>
                  <div class="calender_display"> 
                      <h2> <i class="fa fa-calendar" aria-hidden="true"></i> Calendario</h2>
					  <?php
						$month = date('m');
						$months = array('01'=>'Gennaio', '02'=>'Febbraio', '03'=>'Marzo', '04'=>'Aprile', '05'=>'Maggio', '06'=>'Giugno', '07'=>'Luglio', '08'=>'Agosto', '09'=>'Settembre', '10'=>'Ottobre', '11'=>'Novembre', '12'=>'Dicembre');
							
					  ?>
                      <p><i class="fa fa-angle-left" aria-hidden="true"></i> {{$months[$month]}} <i class="fa fa-angle-right" aria-hidden="true"></i></p>
                      <!--<img src="{{url('front/images/calender_side.jpg')}}">-->
					  
					  
						  <div class="search-calendar" id='calendar'></div>
						
					   
                      <div class="calender_btn"> 
                          <div class="btn-group dropup">
							<a href="{{ url('student/book_lecture/'.$teacherInfo->teacher_id) }}" @if (Auth::guest()) disabled @endif id="singlebutton" name="singlebutton" class="btn btn_custom ">{{ __('translation.book_lesson') }}</a> 
                             <button type="button" class="btn btn_custom_small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>  

        <div class="profile_description">
          <div class="container">
           <div class="row">
             <div class="col-md-4">
               <div class="description_box1">
                 <h2>LEZIONI</h2>
                 <hr>
				 @if(count($teacherLessons) > 0)
				   @foreach($teacherLessons as $lessonval)
					@if($lessonval->type == 0)
						@php $price =  $lessonval->price_per_hour;	@endphp
					@else 
						@php $price =  $lessonval->price_per_hour;	@endphp
					@endif
					@if($lessonval->type == 0)
						
					 <div class="description_1">
					   <div class="d_left pull-left">
						 <p>{{$lessonval->lesson_name}} (Trial)</p>
					   </div>
					   <div class="d_right pull-right">
						 <p>EUR {{$price}}</p>
					   </div>
					 </div>
					@else 
					 <div class="description_1">
					   <div class="d_left pull-left">
						 <p>{{$lessonval->lesson_name}}</p>
					   </div>
					   <div class="d_right pull-right">
						 <p>EUR {{$price}}</p>
					   </div>
					 </div>
					@endif	
				  @endforeach
				@endif	
                 
               </div>

             </div>
             <div class="col-md-4">
                <div class="description_box2">
                  <h2>STATISTICHE</h2>
                  <hr>
                  <!--<img src="{{url('front/images/graf.png')}}">-->
				  <?php $i=1; $margin = 0; $total=0; ?>
										@foreach($teacherLessons as $lessonval)
											<?php $total  += $lessonval->total_lessons; ?>
										@endforeach
										<div style="text-align: center;width: 100%;margin: 20px 0px;">
										{{$total}}
										<h3>Lezioni Completate</h3>
										</div>
										@foreach($teacherLessons as $lessonval)
										<?php 
											$percentage  = $lessonval->total_lessons/10;
											
											if($i>1){
												$margin += 80;
											}
											$montth = date("M", strtotime($lessonval->created_at));
											$currentmonth = date("M");
											if($montth == $currentmonth){
												$background = 'background-color:#00B4FF';
											}else{
												$background = 'background-color:#C4C4C4';
											}
											if($i<=3){
										?>
										<div class="stat" style="margin-left:{{$margin}}px">
											<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{$lessonval->total_lessons}}" aria-valuemin="0" aria-valuemax="100" style="height: {{$percentage}}%;{{$background}}">
											<span style="">{{$lessonval->total_lessons}}</span>
											
											</div>
											<span class="stat_month">{{$montth}}</span>
										</div>
											<?php } $i++; ?>
										@endforeach
                </div>
             </div>

             <div class="col-md-4">
				<?php $totalreviews = @count($teacherreviews); $totalreviews = $totalreviews-1; ?>
				<?php 
					$imgUrl = url('/images/avatar5.png');
					if(!empty($teacherreviews)){
					if( isset( $teacherreviews->profile_image ))
					{
						$imgUrl = url('/images/teacher_logos/' . $teacherreviews->profile_image);
					}
				?>
                <div class="description_box3">
                  <h2>recensioni</h2>
                  <hr>
                  <img src="{{$imgUrl}}">
                  <p>{{$teacherreviews->first_name ?? ''.' '.$teacherreviews->last_name ?? ''}}</p>
                  <p class="p_heading">{{$teacherreviews->title ?? ''}}</p>
                  <p class="p_detail">{{$teacherreviews->review_message ?? ''}}</p>
                </div>
				<?php } ?>
             </div>

           </div>
           </div>
        </div>

    <!--profile_page -->
  
  </div>
  <script>
	$(document).ready(function(){
		var teacher_id = "<?php echo $teacherInfo->id; ?>";
		var lctid =1;
		$('#calendar').fullCalendar({
			header: {
	            left: '',
	            center: '',
	            right: ''
	        },
		    views: {
		        agendaThreeDay: {
		            type: 'agenda',
		            duration: { days: 3 }
		        }
		    },
		    defaultView:'agendaThreeDay',
			disableDragging: true,
			monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
			monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
			dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
			dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
			firstDay: 1,
			slotDuration: '03:00:00',
			slotLabelInterval: 180,
			slotLabelFormat: 'H:mm',
			editable: false,
			axisFormat: 'HH:mm',
			timeFormat: 'HH:mm',
			theme: true,    
			themeSystem:'bootstrap3',
			displayEventTime: false,  
			events: $('meta[name="route"]').attr('content') + '/student/teacherappointment/'+teacher_id+'/'+lctid+'?visible=1',
			selectable:false,
			selectHelper:false, 
			eventClick: function(event) {
			//alert(event.className[0]);
			console.log(event);
			var classname = event.className[0];

			var classname2 = event.className[1];
			var id= event.id; 
			//alert(classname);

			//$('#myModal').show();
			}
			});

});
  </script>

@endsection
