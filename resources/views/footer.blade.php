@extends('layouts.app')

@section('content')
<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<div class="log-footer">
						<img src="images/logo.png" alt="">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="footer-link">
						<ul>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">News</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="footer-link">
						<ul>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
							<li><a href="javascript:void(0)">Lorem ipsum</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="social-link">
						<ul>
							<li class="follow">FOLLOW US</li>
							<li><a class="twitter" href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
							<li><a class="facebook" href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
							<li><a class="linkdin" href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
<!-- Footer End-->
@endsection
