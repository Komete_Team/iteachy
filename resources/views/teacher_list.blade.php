@extends('layouts.app')

@section('content')
<script src="{{ URL::asset('front/js/custom_listing.js') }}"></script>
		<div id="profile_page"> 
		<form name="frm1_search_teacher" id="frm1_search_teacher" method="get" action="{{url('getteachers')}}" autocomplete="off" enctype="multipart/form-data">
			<div class="top_search"> 
				<div class="container_filter"> 
					<div class="row">
					 <div class="search_section">
					  <div class="col-md-4 col-md-offset-2"> 
						<select name="categories[]" id="teachers" class="form-control select2 chosen-select chosen-select-all" multiple  placeholder="seleziona una categoria">
								@if(count($recentSelectedCategories) > 0)	
									<optgroup label="Recent Search">
								@foreach($recentSelectedCategories as $recentval)
									  <option value="{{$recentval->id}}">{{$recentval->category_name}}</option>
								@endforeach	  
									</optgroup>
								@endif	
								@if(count($categories) > 0)
									@foreach($categories as $cat)
						<?php 
							$l=1;
							$totalsubcats = count($subcategories);
							//echo '<pre>'; print_r($totalsubcats); die;
						?>
						@foreach($subcategories as $subcat)
							@if($cat->id == $subcat->parent_id)
								@if($l == 1)
									<optgroup label="{{$cat->category_name}}">	
								@endif
									<option value="{{$subcat->id}}">{{$subcat->category_name}}</option>
								@if($l == $totalsubcats)
									</optgroup>
								@endif
							<?php $l++; ?>	
							@endif
						@endforeach
							
					@endforeach
								@endif
						</select>
						
					  </div>
					  <div class="col-md-4"> 
						<div class="calender_input"> 
						  <div class="input-group" id='datetimepicker2'>
								<span class="input-group-addon">
									<i class="fa fa-calendar" aria-hidden="true"></i>
								</span>
								<input type='text' class="form-control" placeholder="Disponibilità" aria-describedby="basic-addon1" id="dtp_input" name="dtp_input" />
						  </div>
						</div>
					  </div>
					 </div>
					</div>
				</div>
			</div>
			<div class="profile_menu"> 
				<div class="container_filter"> 
					<div class="inline_menu"> 
					 <ul>  
					  <li><a href="#"> <input type="checkbox" name="accridited" class="form-control" value="1"> Solo professori accreditati</a> </li>
					  <li><a href="#"> <img src="{{ URL::asset('front/images/locationh.png') }}">Tipo di insegnante</a> </li>
					  <li class="clickMe" data-num="3"><a href="#"> <img src="{{ URL::asset('front/images/save.png') }}">Prezzo</a>
						<div id="open-3" class="price-range">
						<div class="slider-labels">
						<div class="caption">
						<span id="slider-range-value1"></span>
						</div>
						<div class="text-right caption">
						<span id="slider-range-value2"></span>
						</div>
						</div>
						<div id="slider-range"></div>
						<input type="hidden" name="min-value" id="min-value" value="">
						<input type="hidden" name="max-value" id="max-value" value="">
						</div>
					</li>
					<li> <button type="Submit" class="btn btn-default btn_custom_li">Ricerca</button></li>
					 </ul>
				   </div>
				  


			   </div>
			</div>
			
			</form>
		</div>
		
		<div class="profilemain_heading"> 
			<h2>  <?php echo count($teachers); ?> insegnanti trovati</h2>
		</div>
		
		<!--<div class="container_filter"> -->
  <div class="container"> 
     <div class="row"> 
      <div class="cerca_Section"> 
       <div class="col-md-12"> 
           <div class="profile_section"> 
				@php $totalrates = 0; @endphp
				@if(count($teachers) > 0)
				@foreach($teachers as $teachers_val)
				<?php
				$imgUrl = url('/images/avatar5.png');

				if( isset( $teachers_val->profile_image ))
				{

				$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);

				}
				if(!empty($teachers_val->ratings)){
					$ratings = array_sum(explode('$', $teachers_val->ratings));
					$totalusers = count(explode('$', $teachers_val->users));
					$totalrates = ceil($ratings/$totalusers);
				}else{
					$totalrates = 0;
				}
				?>
              <div class="col-md-7" > 
                  <div class="col-md-5 col-sm-6 col-xs-6 left-no_spacing"> 
                      <div class="profile_img"> 
                          <img src="{{$imgUrl}}" width="100%">
						   <ul class="ratting_ul">  
									@for($i=1; $i<=5; $i++)
									<?php 
									 if($i <= $totalrates)
										$cl = 'red';
									 else
										$cl = '';
									 ?>
									<li class="{{$cl}}"><i class="fa fa-star-o" aria-hidden="true"></i></li>
									@endfor	
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-6"> 
                        <div class="profile_info" > 
                            <h2><a href="{{url('teacher_detail/'.$teachers_val->id)}}">{{$teachers_val->first_name ?? ''}}</a></h2>
                            <h3> Insegnante professionista</h3>
                            <ul class="pro_ul_img">  
                                  <li> <img src="{{ URL::asset('front/images/tick.png') }}"></li>
                                  <li> <img src="{{ URL::asset('front/images/home.png') }}"></li>
                                  <li> <img src="{{ URL::asset('front/images/cros.png') }}"></li>
                            </ul>
                            <div class="profession_detaile"> 
                              <p> insegna</p>
                              <h3>  {{$teachers_val->category_name ?? ''}}</h3>
                            </div>
                            <div class="pro_amount_left pull-left"> 
                                <p> Tariffa oraria da </p>
                                <h3>  EUR {{$teachers_val->price_per_hour ?? ''}}</h3>
                            </div>

                            <div class="pro_amount_right pull-right"> 
                                <p> Tariffa prova </p>
                                <h3>  EUR {{$teachers_val->trial_price ?? ''}}</h3>
                            </div>
                        </div>
                  </div>
              </div>

              <div class="col-md-5 col-sm-12 right_spacing"> 
                  <div class="tab">
                    <button class="tablinks active" id="vid_{{$teachers_val->id}}" onclick="show('video_{{$teachers_val->id}}', {{$teachers_val->id}}, 'vid_{{$teachers_val->id}}');"><img src="{{ URL::asset('front/images/video.png') }}"></button>
                     <button class="tablinks" id="des_{{$teachers_val->id}}" onclick="show('text_{{$teachers_val->id}}', {{$teachers_val->id}}, 'des_{{$teachers_val->id}}');"><img src="{{ URL::asset('front/images/letter.png') }}"></button>
                    <button teach-id="{{$teachers_val->id}}" class="calendar-section tablinks" id="ca_{{$teachers_val->id}}" onclick="show('cal_{{$teachers_val->id}}', {{$teachers_val->id}}, 'ca_{{$teachers_val->id}}');"><img src="{{ URL::asset('front/images/calender.png') }}"></button>
                   </div>
                   <div id="video_{{$teachers_val->id}}" class="tabcontent teacherinfo_{{$teachers_val->id}}">
				   @php if($teachers_val->file_name != ''){ @endphp
					  <iframe width="420" height="345" src="{{url('videos/teacher_videos/'.$teachers_val->file_name)}}"></iframe>
				   @php } @endphp  
                   </div>
                   <!-- <div id="video" class="tabcontent">
                    <video width="320" height="240" controls>
                        <source src="movie.mp4" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                      </video>
                  </div> -->

                 <div id="text_{{$teachers_val->id}}" style="display:none;" class="tabcontent teacherinfo_{{$teachers_val->id}}" >
                   <!-- <h3>Paris</h3> -->
                     <p>{{$teachers_val->description}}</p> 
                 </div>

                  <div id="cal_{{$teachers_val->id}}" style="display:none;" class="tabcontent teacherinfo_{{$teachers_val->id}}">
					 <!--<h3 class="text-center">Luglio</h3>-->
					 <div class="calender_img">
					  <div class="search-calendar" id='calendar_{{$teachers_val->id}}'></div>
					 </div>
				   </div>
              </div>
			@endforeach
			@else
			<div style="margin-top: 30px;text-align: center;">There is not found any teacher</div>
			@endif
			<!--/.col-sm-6.col-lg-4--> 
			
          </div>
      </div>
      </div>

      </div>
    </div>
		
<script>
function show(v, id, tab){
$('.tablinks').removeClass('active');
$('.teacherinfo_'+id).hide();
$('#'+v).show();
$('#'+tab).addClass('active');

setTimeout(function(){ $('.fc-today-button').click(); }, 500);
}
</script>		
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
$(".defaultOpen").click();
$(document).ready(function(){
	$('body').on('click','.calendar-section',function(){
		var teacher_id = $(this).attr('teach-id');
		var lctid =1;
		$('#calendar_'+teacher_id).fullCalendar({
			header: {
	            left: '',
	            center: '',
	            right: ''
	        },
		    views: {
		        agendaThreeDay: {
		            type: 'agenda',
		            duration: { days: 3 }
		        }
		    },
		    defaultView:'agendaThreeDay',
			disableDragging: true,
			monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
			monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
			dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
			dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
			firstDay: 1,
			slotDuration: '03:00:00',
			slotLabelInterval: 180,
			slotLabelFormat: 'H:mm',
			editable: false,
			axisFormat: 'HH:mm',
			timeFormat: 'HH:mm',
			theme: true,    
			themeSystem:'bootstrap3',
			displayEventTime: false,  
			events: $('meta[name="route"]').attr('content') + '/student/teacherappointment/'+teacher_id+'/'+lctid+'?visible=1',
			selectable:false,
			selectHelper:false, 
			eventClick: function(event) {
			//alert(event.className[0]);
			console.log(event);
			var classname = event.className[0];

			var classname2 = event.className[1];
			var id= event.id; 
			//alert(classname);

			//$('#myModal').show();
			}
			});
	});
});
</script>
@endsection
