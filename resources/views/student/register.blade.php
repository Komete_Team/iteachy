@extends('layouts.app')
@section('title', 'Student Registration')
@section('content')

	<script>
	$(document).ready(function(){
		// Initialize the functionality
		loginObj.init();
	});

	var loginObj = {
		init: function() {
			loginObj.holdFormSubmit();
			
			loginObj.formValidation();
			
			loginObj.loginFunction();
		},
		holdFormSubmit: function() {
			// Form validations
			$('#frm_student_registration').submit(function(e){
		        e.preventDefault();
		    });
		},
		formValidation: function(){
			$('#frm_student_registration').validate({
				rules: {
					first_name: {
						required: true
					},
					last_name: {
						required: true
					},
					email_id: {
						required: true,
						email: true
					},
					password: {
						required: true
					}
				},
				messages: {
					first_name: {
						required: '<?php echo __('translation.enter_first_name'); ?>'
					},
					last_name: {
						required: '<?php echo __('translation.enter_last_name'); ?>'
					},
					email_id: {
						required: '<?php echo __('translation.enter_email'); ?>',
						email: '<?php echo __('translation.enter_valid_email'); ?>'
					},
					password: {
						required: '<?php echo __('translation.enter_password'); ?>'
					}
				}
			});
		},
		loginFunction: function() {
			// Login functionality
		    $('#btn_student_registration').click(function(e){
				e.preventDefault();
		    	// Check the validation
		    	if( $('#frm_student_registration').valid() )
		    	{
		    		// Hold the button reference
		    		var btn = $(this);

					$('#server_resposne').hide();
		    		$('#server_resposne_msg').html('');

		    		$.ajax({
		    			url: $('meta[name="route"]').attr('content') + '/student/register',
		    			method: 'post',
		    			data: {
		    				frmData: $('#frm_student_registration').serialize()
		    			},
		    			beforeSend: function() {
		    				// Disable the button
					        $(btn).attr('disabled', true);
					        
					        $('#loading_spinner').show();
					    },
		    			headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    complete: function()
					    {
					    	// Enable the button
					    	//$(btn).attr('disabled', false);

					    	$('#loading_spinner').hide();
					    },
					    success: function(response){
					    	if( response.resCode == 0 )
					    	{
					    		// Reset the form
					    		$('#frm_student_registration')[0].reset();

					    		$('#server_resposne').removeClass('alert-danger').addClass('alert-success');
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    	else
					    	{
					    		$('#server_resposne').removeClass('alert-success').addClass('alert-danger');
					    		$('#server_resposne_msg').html(response.resMsg);
					    		$('#server_resposne').show();
					    	}
					    }
		    		});
		    	}
		    });
		}
	}
	</script>

	<style type="text/css">
	.login-form {
		width: 35%;
    	margin: 10% auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
    label.error {
        color: red;
    }
	</style>

	<div class="login-form">
        <form autocomplete="off" name="frm_student_registration" id="frm_student_registration">
		{{ csrf_field() }}
            <h2 class="text-center">{{ __('translation.student_registration') }}</h2>
            <div class="form-group">
            	<label for="full_name">{{ __('translation.First Name') }}:</label>
                <input type="text" class="form-control" name="first_name" id="full_name">
            </div>
			<div class="form-group">
            	<label for="last_name">{{ __('translation.Last Name') }}:</label>
                <input type="text" class="form-control" name="last_name" id="last_name">
            </div>
            <div class="form-group">
            	<label for="email_id">{{ __('translation.Email Id') }}:</label>
                <input type="text" class="form-control" name="email_id" id="email_id">
            </div>
            <div class="form-group">
            	<label for="password">{{ __('translation.Password') }}:</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
			<div class="form-group">
                <input type="checkbox" class="form-control" name="privacy" id="privacy" style="width: 20px;float: left;margin: 0;">
				<label for="privacy">{{ __('translation.accept_privacy') }}:</label>
            </div>
            <div class="form-group">
                <button type="submit"  class="btn btn-primary btn-block disabled" id="btn_student_registration">{{ __('translation.register') }}</button>
            </div>

            <!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>
        </form>
    </div>
	

    <script type="text/javascript">

        $(document).ready(function(){

            $('input[type="checkbox"]').click(function(){

                if($(this).prop("checked") == true){

                    $('#btn_student_registration').removeClass('disabled');

                }

                else if($(this).prop("checked") == false){
					
                    $('#btn_student_registration').addClass('disabled');

                }

            });

        });

    </script>

@endsection