@extends('student.layouts.app')
@section('title', 'Student Bacheca')

@section('content')
<?php //echo '<pre>'; print_r($teacherAvailable); die;
	/* $arr = array();
	foreach($teacherAvailable as $teacherAvailableval)
	{
		$arr[] = ['start' => $teacherAvailableval->start_time, 'end' => $teacherAvailableval->end_time, 'dow'=>[$teacherAvailableval->num_days]];
		
	}
	
	
	$json = json_encode($arr);

	//echo $avlobj; die; */
	?>
	<div class="container">
	<!-- Right side column. Contains the navbar and content of the page -->
	 <form class="form-horizontal" action="{{url('student/saveappointment')}}" method="post">
	 {{ csrf_field() }}
            	<div id="wizard">
            		<!-- SECTION 1 -->
	                <h4></h4>
	                <section>
	                  
	                    <div class="form-row">
						
						<?php //echo '<pre>'; print_r($teacherlectures); die; ?>
	                    	@foreach($teacherlectures as $teacherlectures_val)
								@if($teacherlectures_val->type == 0)
									@php $price =  $teacherlectures_val->price_per_hour;	@endphp
								@else 
									@php $price =  $teacherlectures_val->price_per_hour;	@endphp
								@endif
							<div class="select_lectures" id="{{$teacherlectures_val->id}}-{{$price}}" style="background:#eee;margin-bottom:2px;padding:10px; pointer">
								
								@if($teacherlectures_val->type == 0)
									<span style="">{{$teacherlectures_val->lesson_name}} (Trial)</span>
									<span style="float:right;">&euro;{{$teacherlectures_val->price_per_hour}}</span>
								@else 
									<span style="">{{$teacherlectures_val->lesson_name}}</span>
									<span style="float:right;">&euro;{{$teacherlectures_val->price_per_hour}}</span>
								@endif
								
							</div>
							@endforeach
							<input type="hidden" name="lectureids" id="lectureids" value="" />
							<input type="hidden" name="lectureprice" id="lectureprice" value="" />
							<input type="hidden" name="teacherid" id="teacherid" value="{{$tid}}" />
							<input type="hidden" name="studentid" id="studentid" value="{{$userId}}" />
	                    </div>	
	                  
	                </section>
	                
					<!-- SECTION 2 -->
	                <h4></h4>
	                <section id="cal">
							<div id='calendar1'></div>
							<input type="hidden" value="" name="start_time" id="start_time" />
							<input type="hidden" value="" name="end_time" id="end_time" />
							<input type="hidden" value="" name="start_date" id="start_date" />
							<input type="hidden" value="" name="end_date" id="end_date" />
	                </section>

	                <!-- SECTION 3 -->
	                <h4></h4>
	                <section>
						
							  <div class="col-md-12">
								
								 
								 
								   
									<div class="form-group" style="display:flex;">
									  <label class="col-md-3 control-label" for="email">Skype</label>
									  <div class="col-md-9">
										<select class="form-control" name="skype" id="skype">
											<option>Select Id</option>
											<option value="yes">Skype</option>
										</select>
										<input id="skype_id" name="skype_id" type="text" placeholder="Id:" class="form-control" value="">
									  </div>
									</div>
									
									<div class="form-group" style="display:flex;">
									  <label class="col-md-3 control-label" for="email">luogo preferito per l'incontro</label>
									  <div class="col-md-9">
										<select class="form-control" name="place" id="place" required>
											<option value="">Select Place</option>
											<option value="test1">test1</option>
											<option value="test2">test2</option>
											<option value="test3">test3</option>
										</select>
									  </div>
									</div>
									
									<!-- Email input-->
									<div class="form-group" style="display:flex;">
									  <label class="col-md-3 control-label" for="email">Livello di conoscenza</label>
									  <div class="col-md-9">
										<input id="knowledgelevel" required name="knowledgelevel" min="0" max="5" type="number" placeholder="" class="form-control">
									  </div>
									</div>
							
									<!-- Message body -->
									<div class="form-group" style="display:flex;">
									  <label class="col-md-3 control-label" for="message">il tuo messaggio</label>
									  <div class="col-md-9">
										<textarea class="form-control" required id="message" name="message" placeholder="" rows="5"></textarea>
									  </div>
									</div>
							
									<!-- Form actions -->
									<div class="form-group">
									  <div class="col-md-12 text-right">
										<button type="submit" class="btn btn-primary btn-lg">invia</button>
									  </div>
									</div>
							  
								  
								
							  </div>
						
						
	                </section>

	                
            	</div>
            </form>
	<!-- /.right-side -->
	</div>	
	
	<link rel="stylesheet" href="{{ URL::asset('front/css/step.css') }}" />
	<!-- JQUERY STEP -->
	<script>
		$(document).ready(function(){
			$('.form-horizontal').validate({
				rules: {
					'place': "required",
					'knowledgelevel': "required",
					'message': "required",
				},
				messages: {
					'place': "Seleziona il luogo di incontro preferito.",
					'knowledgelevel': "Seleziona il livello di conoscenza.",
					'message': "Per favore, inserisci il messaggio.",
					
				}
			});
		});
		
		var user_id = '<?php echo $userId; ?>';
		var teacher_id = '<?php echo $tid; ?>';
	</script>

	
	<style>
	.calendar-cell-tempselected{background:#40C7FF;}
	.fc-time-grid-event{background:#40C7FF;}
	//.fc-nonbusiness.fc-bgevent{background:red;}
	</style>
@endsection