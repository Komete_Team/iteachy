@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row">
	        	<div class="col-lg-12">
					  <h2>vuoi eliminare definitivamene l'account</h2>
					  <form method="post" autocomplete="off" enctype="multipart/form-data">
					  {{ csrf_field() }}
						<div class="form-group {{ $errors->has('pwd') ? ' has-error' : '' }}">
						  <label for="pwd">Inserisci password:<span class="mandatory_field">*</span></label>
						  <input type="password" class="form-control" id="pwd" placeholder="Inserisci password" name="pwd">
							@if ($errors->has('pwd'))
								<span class="help-block">
									<strong>{{ $errors->first('pwd') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group {{ $errors->has('pwd2') ? ' has-error' : '' }}">
						  <label for="pwd2">Reinserisci password:<span class="mandatory_field">*</span></label>
						  <input type="password" class="form-control" id="pwd2" placeholder="Reinserisci password" name="pwd2">
						 @if ($errors->has('pwd2'))
							<span class="help-block">
								<strong>{{ $errors->first('pwd2') }}</strong>
							</span>
						 @endif
						</div>
						<button type="submit" class="btn btn-default">{{ __('translation.Submit') }}</button>
					  </form>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add sucategory -->
	<div id="modal_lesson" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_lesson" id="frm_add_lesson" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Nome della lezione:</label>
							<input type="text" class="form-control" name="lesson_name" id="lesson_name" value="">
							<input type="hidden" name="lesson_id" id="lesson_id" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Lezioni totali:</label>
							<input type="text" class="form-control" name="total_lessons" id="total_lessons" value="">
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_lesson">Sottoscrivi</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection