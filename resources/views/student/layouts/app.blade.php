<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="route" content="{{ url('/') }}">
	<!-- font Awesome -->
	<!--<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('website/scripts/rateit.css')}}">-->
	<!-- bootstrap 3.0.2 -->
	 <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::asset('front/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/style.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/multiple-select.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('js/plugins/chosen_v1.8.7/chosen.css') }}" />
	<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ URL::asset('front/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('front/css/step.css') }}">
	 
	<!-- fullCalendar -->
      
	<link href="{{ URL::asset('css/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- jquery here -->
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script src="{{ URL::asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
	<!--<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>-->
	<script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
	<script src="{{ URL::asset('js/multiple-select.js') }}"></script>
	<script src="{{ URL::asset('front/js/owl.carousel.min.js') }}"></script>
	<script src="{{ URL::asset('front/js/custom.js') }}"></script>
	<script type="text/javascript" src="{{URL::asset('website/scripts/jquery.rateit.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('website/scripts/jquery.rateit.min.js')}}"></script>
	<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
	
	 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	
	<script src="{{ URL::asset('front/js/jquery.steps.js') }}"></script>
<script src="{{ URL::asset('front/js/main.js') }}"></script>
	
	@if (Route::currentRouteAction() == 'App\Http\Controllers\ChatController@index')
		<script src="{{ URL::asset('front/js/chat.js') }}"></script>
	@endif

	<!-- fullCalendar -->
	<script type="text/javascript">
		var base_url = '<?php echo url('/') ?>';
	</script>
		<!-- fullCalendar -->
	
	<script type="text/javascript">
	$(document).ready(function(){
		
		$('.owl-carousel').owlCarousel({
			loop:true,
			margin:15,
			responsiveClass:true,
			autoplay:true,
			autoplayTimeout:3000,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:2,
					nav:true
				},
				1000:{
					items:4,
					nav:true,
					loop:true
				}
			}
		});
	});
	</script>
	

	<style type="text/css">
	.login-form {
		width: 35%;
    	margin: 10% auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
    label.error {
        color: red;
    }
	.calendar-cell-leave {
		background-color: #ffffff !important;
		width:100%;
		z-index: 999999 !important;
	}
	.calendar-cell-booked{background:#cccccc !important;
		width:100%;
		z-index: 99 !important;}
		
	.calendar-cell-available{background:#62c208 !important;
		
		z-index: 9 !important;}	
	.calendar-cell-available-selected{background:#00B4FF !important;}	
		
	.calendar-cell-canceled {
		width:100%;
		background-color: #FF0000 !important;
		z-index: 9999 !important;
	}
	</style>
</head>
<body>
	@php $user = Auth::user(); @endphp
    <div id="app">
	<div class="navigation">
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					<a class="navbar-brand" href="{{url('/')}}"><img src="{{ URL::asset('front/images/logo.png') }}" alt=""></a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
						<li><a href="{{ url('/') }}">Cerca un insegnanto</a></li> 
                        <li><a href="{{ url('student/booking_listing') }}">Le mie lezioni</a></li>
						<li><a href="{{ url('contact-us') }}">Preferiti</a></li>
						<?php 
							$notificationdetail= Helper::notificationDetail($user->id);
							$totalnotifications = count($notificationdetail); 
						?>
						 <li class="dropdown notifications-menu">
                             <a href="{{ url('student/notifications')}}" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i>
                                @if($totalnotifications>0)<span class="label label-warning">{{$totalnotifications}}</span> @endif
                            </a> 
                            <ul class="dropdown-menu">
                                <?php /* <li class="header">You have 10 notifications</li> */
                                ?>
                                    @if(!empty($notificationdetail) && count($notificationdetail)> 0)
                                      @foreach($notificationdetail as $k => $notval )
                                                      <li class="unread"><a href="{{ url('teacher/notification').'/'.$notval->id}}">{{$notval->description}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
                                      @endforeach
                                    @else
                                    <li class="read"><a href="javascript:void(0);">nessuna notifica</a></li>  
                                    @endif
									
                                <!--<li class="footer"><a href="#">View all</a></li>-->
                            </ul>
                        </li>
                         @if (Auth::guest())
                            <li><a href="{{ url('login') }}">Accedi</a></li> 
                            <li><a href="{{ url('register') }}">Registrazione insegnanti</a></li>
							<li><a href="{{ url('student/register') }}">Registrazione dello studente</a></li>
							
                        @else
                            <li>
                                <a href="{{ url('student/dashboard') }}" role="button" aria-expanded="false">
                                    {{ Auth::user()->first_name }} <span class="caret"></span>
                                </a>
                                
                            </li>
                            <li> <a href="{{ url('student/logout') }}">{{ __('translation.Sign out') }}</a></li> 
                        @endif
						
						
					</ul>
				</div>
			</div>
		</nav>
	</div>
        @yield('content')
	<div class="footer"> 
        <div class="container"> 
            <div class="row"> 
              <div class="col-md-3"> 
              <div class="foot_1"> 
                 <a href="{{url('/')}}"><img src="{{ URL::asset('front/images/logo.png') }}" alt=""></a>
              </div>
              </div>
              <div class="col-md-2 text-left"> 
              <div class="foot_2"> 
                   <ul> 
                      <li>Cerca lezioni</li>
                      <li>perche</li>
                      <li>Cerca lezioni</li>
                      <li>chi siamo</li>
                      <li>Cerca lezioni</li>
                   </ul>
              </div>
              </div>
              <div class="col-md-2 text-left"> 
              <div class="foot_3"> 
                  <ul> 
                      <li>Assistenza</li>
                      <li>Notelegali</li>
                      <li>pravicy</li>
                      <li>contattaci</li>
                   </ul>
              </div>
              </div>
              <div class="col-md-5 text-left"> 
              <div class="foot_4 "> 
                  <ul> 
                        <li class="social_heading"> <p> FOLLOW US</p></li>
                      <li class="circle_2"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                      <li class="circle_1"><i class="fa fa-facebook" aria-hidden="true"></i></li>
                      <li class="circle_3"><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                      
                   </ul>
              </div>
            </div>
            </div>
        </div>
    </div>
	</div>
</body>
</html>
