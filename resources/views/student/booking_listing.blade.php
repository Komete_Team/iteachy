@extends('layouts.app')
@section('title', 'Admin Bacheca')

@section('content')

	<div class="container">
	
		<?php //echo '<pre>'; print_r($teacherBooked); die; ?>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
			@if(count($teacherBooked)>0)
			<h3 style="background:#c3e8f7; padding:10px;text-align:center">le mie lezione</h3>
			<div class="row" style="padding: 10px;text-align: center;margin-bottom: 5px;">	
					<div class="col-sm-2">
					  <h3></h3>
					</div>
					<div class="col-sm-2">
					  <h3>Tipo di lezione</h3>
					</div>
					<div class="col-sm-2">
					  <h3>A che ora</h3>
					</div>
					<div class="col-sm-2">
					  <h3>Quando</h3>
					</div>
					<div class="col-sm-2">
					  <h3>Cancella</h3>
					</div>
					<div class="col-sm-2">
					  <h3>Vai alla virtualroom</h3>
					</div>
				</div>
			
				@foreach($teacherBooked as $val)
					<?php 
						$i = 0;
						$lecturetime = '';
						$to_time = strtotime($val->end_date);
						$from_time = strtotime($val->start_date);
						$lecturetime = round(abs($to_time - $from_time) / 60,2). " minute";
						$imgUrl = url('/images/avatar5.png');
									
						if( isset( $val->profile_image ))
						{
							
								$imgUrl = url('/images/teacher_logos/' . $val->profile_image);
							
						}
						
						?>
					
						
				<div class="row" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">	
					<div class="col-sm-2">
					  <p><img style="max-width: 150px;" src="{{$imgUrl}}" /></p>
					</div>
					<div class="col-sm-1">
					  <p>{{$val->lesson_name}}</p>
					</div>
					<div class="col-sm-2">
					  <p>{{date('H:i',strtotime($val->start_date))}}</p>
					  <span>{{$lecturetime}}</span>
					</div>
					<div class="col-sm-2">
					  <p>{{date('d M Y',strtotime($val->start_date))}}</p>
					</div>
					<div class="col-sm-1">
					  <p><a href="javascript:void(0);" id="{{$val->id}}" class="delete_appt"><i class='fa fa-trash'></i></a></p>
					</div>
					<div class="col-sm-4">	
					  <a href="{{url('chat/'.$val->lid)}}"><i class='fa fa-comments' style='font-size:30px;color:#337ab7;margin-right: 20px;float:right'></i></a>
					  <a href="#" style="width:100px;float:right;margin-right: 20px;float:right'" class="btn btn-primary center-block disabled">Collegati ora</a> 	
					</div>
				</div>	
				@endforeach
				@else
					<h3 style="padding:10px;text-align:center">Non c'è lezione prenotata</h3>
			@endif
			
			@if(count($teacherPastBooked)>0)
				<h3 style="padding:10px;text-align:center">Lezione Passate</h3>
				@foreach($teacherPastBooked as $val)
					<?php 
						$i = 0;
						$lecturetime = '';
						$to_time = strtotime($val->end_date);
						$from_time = strtotime($val->start_date);
						$lecturetime = round(abs($to_time - $from_time) / 60,2). " minute";
						$imgUrl = url('/images/avatar5.png');
									
						if( isset( $val->profile_image ))
						{
							
								$imgUrl = url('/images/teacher_logos/' . $val->profile_image);
							
						}
						
						?>
					
						
				<div class="row" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">	
					<div class="col-sm-2">
					  <p><img style="max-width: 150px;" src="{{$imgUrl}}" /></p>
					</div>
					<div class="col-sm-1">
					  <p>{{$val->lesson_name}}</p>
					</div>
					<div class="col-sm-2">
					  <p>{{date('H:i',strtotime($val->start_date))}}</p>
					  <span>{{$lecturetime}}</span>
					</div>
					<div class="col-sm-2">
					  <p>{{date('d M Y',strtotime($val->start_date))}}</p>
					</div>
					<div class="col-sm-1">
					  <p><a href="javascript:void(0);" id="{{$val->id}}" class="delete_appt"><i class='fa fa-trash'></i></a></p>
					</div>
					<div class="col-sm-4">
						<a href="{{url('student/questionnaire/'.$val->id)}}"><i class='fa fa-question' style='font-size:30px;color:#337ab7;margin-right: 30px;float:right'></i></a>
					  <a href="{{url('chat/'.$val->lid)}}"><i class='fa fa-comments' style='font-size:30px;color:#337ab7;margin-right: 20px;float:right'></i></a>
					  <a href="#" style="width:100px;float:right;margin-right: 20px;float:right'" class="btn btn-primary center-block disabled">Collegati ora</a> 	
					</div>
				</div>	
				@endforeach
			@endif
			
		 
	
	</div>	
	<!-- Model to add sucategory -->
	<div id="confirm_model" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					
						
						
						<div class="row bobrow" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">
							
						</div>
						
					
					<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>
	
@endsection