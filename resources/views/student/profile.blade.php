@extends('layouts.app')
@section('title', 'Profile Management')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Profile</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>		
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>
	        	<form id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-lg-3">
							<label for="student_image">
							    <?php
							    $imgUrl = url('/images/avatar.png');
							    if( $studentDetails->profile_image != '' )
							    {
							    	
							    		$imgUrl = url('/images/student_profileimage/' . $studentDetails->profile_image);
							    	
							    }
							    ?>
							    <img id="default-student-image" src="{{ $imgUrl }}" height="100px" width="100px">
							    <input type="file" id="student-image" name="student_image" style="display:none;">
								<input type="hidden" value="{{$studentDetails->profile_image}}" name="old_image" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.First Name') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="first_name" name="first_name" value="{{ $studentDetails->first_name ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Last Name') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="last_name" name="last_name" value="{{ $studentDetails->last_name ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Email Id') }}:<span class="mandatory_field">*</span></label>
	    						<input disabled="disabled" type="text" class="form-control" id="email" name="email" value="{{ $studentDetails->email ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('contact_no') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Contact No') }}:<span class="mandatory_field">*</span></label>
	    						<input type="number" class="form-control" id="contact_no" name="contact_no" value="{{ $studentDetails->contact_no ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Address') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="address" name="address" value="{{ $studentDetails->address ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
	    					<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.City') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="city" name="city" value="{{ $studentDetails->city ?? '' }}">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('province') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Province/State') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="province" name="province" value="{{ $studentDetails->province ?? '' }}">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Country') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="country" name="country" value="{{ $studentDetails->country ?? '' }}">
	    					</div>
						</div>
					</div>
					<hr/>
					<?php $communication = explode(':', $studentDetails->social_profile); ?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="text">{{ __('translation.Communication Type') }}:</label>
								<select class="form-control" id="communication-type" name="communication_type">
									<option value="">{{ __('translation.Select') }}</option>
									<option {{ ($communication[0] == 'skype')?'selected':''}} value="skype">Skype</option>
									<option {{ ($communication[0] == 'hangout')?'selected':''}} value="hangout">Hangout</option>
								</select>

							</div>
						</div>
						<div class="col-lg-6">
							<label>&nbsp;</label>
							<input type="text" value="{{ !empty($communication[1])?$communication[1]:''}}" class="form-control" id="communication-id" name="communication_id">
						</div>
					</div>
					<hr size="30"/>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="text">{{ __('translation.Interest') }}:</label>
							</div>
							
						</div>
					</div>
					<div class="row">
						<?php 
						$userInt=[];
						foreach ($userinterest as $int) {
							$userInt[] = $int['category_id'];
						}
						?>
						@foreach($categories as $cat)
							<div class="col-lg-3">
								<input type="checkbox" {{(in_array($cat->id,$userInt))?'checked':''}} name="category[]" value="{{ $cat->id }}"> {{ $cat->category_name }}
							</div>
						@endforeach
					</div>
					<button type="submit" class="btn btn-primary" id="btn_save_school">invia</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<script>
		$(document).ready(function(){
			$('#frm_update_profile').validate({
				rules: {
					first_name: "required",
					last_name: "required",
					contact_no: {
						required:true,
						maxlength: 10,
						minlength: 10
					},
					address: "required",
					city:"required",
					province:"required",
					country:"required"
				},
				messages: {
					first_name: "Per favore inserisci il nome.",
					last_name: "Per favore inserisci il cognome",
					contact_no: {
						required:"Si prega di inserire il numero di contatto.",
						maxlength: "Inserisci un numero di cellulare valido.",
						minlength: "Inserisci un numero di cellulare valido."
					},
					address: "Per favore inserisci l'indirizzo",
					city:"Per favore, inserisci la città.",
					province:"Per favore, inserisci la provenienza.",
					country:"Per favore, inserisci il paese."
				}
			});
			$('#default-student-image').click(function(){
				$('#student-image').click();
			});
			$('#student-image').change(function () {
				var ext = $('#student-image').val().split('.').pop().toLowerCase();
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				   $('#btn_save_school').attr("disabled", true);
				   $('#message-text').html("<div class='alert alert-danger'><strong>Error!</strong>Estensione immagine non disponibile.</div>");
				} else{
					$('#btn_save_school').attr("disabled", false);
					$('#message-text').html('');
				}
			});
		});
	</script>
@endsection