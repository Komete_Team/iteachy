@extends('layouts.app')
@section('title', 'Questionnaire')

@section('content')
<style>
.side_bar [type="radio"]:checked, [type="radio"]:not(:checked) {
    position: unset;
    left: -9999px;
}
</style>
<aside class="container">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Questionario</li>
        </ol>
    </section>
	<section class="content">
		<center><h2>Questionario</h2></center>
		@if (Session::has('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{Session::get('success') }}</strong>
			</div>
		@else
		<form method="post" id="questionnaire" autocomplete="off">
			<div class="form-group">
			@php $counter=1; @endphp
			@foreach($questionnaires as $ques)
				<div class="row question" qid="{{ $ques->id }}">
					<div class="col-lg-12">
						Q{{ $counter.')'.$ques->question }}
					</div>
				</div>
				@php $answers = json_decode($ques->answer); @endphp
				<div class="row">
					<div class="col-lg-12">
					<ul>
						@foreach($answers as $ans)
						<li><label><span for="text">{{ $ans }}</span>
	    				<input class="" type="radio" value="{{ $ans }}" name="answer[{{ $ques->id }}]"></label></li>
						@endforeach
					</ul>	
					</div>
				</div>
				@php $counter++; @endphp
			@endforeach
			{!! csrf_field() !!}
			<button type="submit" class="btn btn-primary" id="btn_save_questionarrie">Invia</button>
			</div>
		</form>
		@endif
	</section>
</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$.extend(jQuery.validator.messages, {
		    required: "Seleziona la tua risposta."
		});
		$("#questionnaire").validate({
			errorPlacement: function (error, element) {
	            if (element.attr("type") == "radio") {
	                error.insertAfter($(element).parents('div').prev($('.question')));
	            } else {
	                // something else
	            }
	        }
		});
		$('.question').each(function(){
			var qid=$(this).attr('qid');
			$("[name='answer["+qid+"]']").rules("add", "required");;
		});
		
	});
</script>
@endsection