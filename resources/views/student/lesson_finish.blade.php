@extends('student.layouts.app')
@section('title', 'Questionnaire')

@section('content')
@php $apptid = 1; @endphp
<aside class="container chatbox">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Post Lecture Questionnaire</li>
        </ol>
    </section>
	<section class="content" style="background:#000; color:#fff;padding: 30px;">
		<center style="padding: 30px;color:#286090"><h2>Lezione Finita!</h2></center>
		<div style="width:700px; margin: 0 auto;">
			<div style="display:inline-block;">
			<p style="width:50%;float:left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<p>
			<p style="width:30%;float:right"><a href="{{url('/student/questionnaire/'.$apptid)}}" class="btn btn-primary center-block">Compila ora</a></p>
			</div>
		</div>
		<hr/>
		<div style="width:700px; margin: 0 auto;">
		<div style="display:inline-block;">
			<p style="width:50%;float:left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<p>
			<p style="width:30%;float:right"><a href="#" class="btn btn-danger center-block">Chiedi rimborsa</a></p>
		</div>
		</div>
	</section>
</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$.extend(jQuery.validator.messages, {
		    required: "Seleziona la tua risposta."
		});
		$("#questionnaire").validate({
			errorPlacement: function (error, element) {
	            if (element.attr("type") == "radio") {
	                error.insertAfter($(element).parents('div').prev($('.question')));
	            } else {
	                // something else
	            }
	        }
		});
		$('.question').each(function(){
			var qid=$(this).attr('qid');
			$("[name='answer["+qid+"]']").rules("add", "required");;
		});
		
	});
</script>
@endsection