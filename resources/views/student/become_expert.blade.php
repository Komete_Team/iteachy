@extends('student.layouts.app')
@section('title', 'Student Payment Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Become Expert</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 @if (Session::has('success'))
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>
		<section class="content">
			<div>
				<form id="become-expert" method="post">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="text">{{ __('translation.Interest') }}:</label>
							</div>
							
						</div>
					</div>
					<div class="row">
						<?php 
						$userInt=[];
						foreach ($userinterest as $int) {
							$userInt[] = $int['category_id'];
						}
						?>
						@foreach($categories as $cat)
							<div class="col-lg-3">
								<input type="checkbox" {{(in_array($cat->id,$userInt))?'checked':''}} name="category[]" value="{{ $cat->id }}"> {{ $cat->category_name }}
							</div>
						@endforeach
					</div>
					<div class="row">
						<label style="display: none;" id="category[]-error" class="error" for="category[]">Per favore, inserisci la password.</label>
					</div>
					
					{!! csrf_field() !!}	
					<button type="submit" class="btn btn-primary" id="btn-become-expert">Submit
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$('#become-expert').validate({
			rules: {
				'category[]': "required"
			},
			messages: {
				'category[]': "Per favore, inserisci la password."
			}
		});
	});
</script>
@endsection
