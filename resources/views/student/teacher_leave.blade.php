@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')

	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
	    <!-- Main content -->
		 <!-- Main row -->
		<section class="content">
	        <div>
	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('dtp_input') ? ' has-error' : '' }}">
								
							<label for="heading">Applica congedo:</label>	
							<div class="input-group date" id='datetimepicker1'>
								<input type='text' class="form-control" id="dtp_input" name="dtp_input" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
							@if ($errors->has('dtp_input'))
								<span class="help-block">
									<strong>{{ $errors->first('dtp_input') }}</strong>
								</span>
							@endif	
	    					</div>
						</div>
					</div>
					

					<button type="submit" class="btn btn-primary" id="btn_save_school">{{ __('translation.Accredited profile') }}</button>
				</form>
	
	       	</div>
		</section>	
	    <section class="content">
			<!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table table-striped" id="datatable_teacher_leave">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>{{ __('translation.Leave Date') }}</th>
			        				<th>{{ __('translation.Action') }}</th> 
			        			</tr>
			        		</thead>
							<tbody>
							@foreach($teacherLeaveDetails as $leave_val)
			        			<tr>
			        				<td>{{$leave_val->id}}</td>
									<td>{{$leave_val->leave_date}}</td>
									<td><a href="javascript:void(0);" id="{{$leave_val->id}}" class="edit_leave" title="Edit Leave"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="{{$leave_val->id}}" class="cancel_leave" title="Cancel Leave"><i class="fa fa-times" style="color:red"></i></a></td>
			        			</tr>
							@endforeach	
			        		</tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<div id="modal_teacher_leave" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">modifica permesso</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_leave" id="frm_edit_leave" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Applica congedo:</label>
							<div class="input-group date form_datepicker">
								<input class="form-control" size="16" type="text" value="" id="leave_date" name="leave_date" readonly>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
							
							<input type="hidden" name="leave_id" id="leave_id" value="">
						</div>
						
						<br/>
						<button type="button" class="btn btn-primary" id="btn_save_leave">{{ __('translation.Accredited profile') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection