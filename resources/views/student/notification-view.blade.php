@extends('layouts.app')
@section('title', 'Admin Bacheca')

@section('content')

	<div class="container">
	
		<?php //echo '<pre>'; print_r($teacherBooked); die; ?>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
			@if(count($notifications)>0)
			<h3 style="background:#c3e8f7; padding:10px;text-align:center">Le mie notifiche</h3>
			
				@foreach($notifications as $val)
					<?php 
						 $i = 0;
						/*$lecturetime = '';
						$to_time = strtotime($val->end_date);
						$from_time = strtotime($val->start_date);
						$lecturetime = round(abs($to_time - $from_time) / 60,2). " minute";*/
						$imgUrl = url('/images/avatar5.png'); 
									
						if( isset( $val->profile_image ))
						{
							
								$imgUrl = url('/images/teacher_logos/' . $val->profile_image);
							
						}
						
						?>
					
						
				<div class="row" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">	
					<div class="col-sm-1">
					  <p><img style="max-width: 150px;width: 60px;" src="{{$imgUrl}}" /></p>
					  
					</div>
					<div class="col-sm-3">
					  <h4>{{$val->first_name.' '.$val->last_name}}</h4>
					  <h5>{{$val->email}}</h5>
					</div>
					<div class="col-sm-5">
					  <p>{{$val->description}}</p>
					</div>
					
					
					<div class="col-sm-2">
					@if($val->type == 2)
					  <p><a href="{{url('/student/questionnaire/'.$val->user_id)}}" class="btn btn-primary center-block">Quesnnair</a></p>
				    @endif
					@if($val->type == 2)
						<p><a href="{{url('/chat/'.$val->user_id)}}" class="btn btn-primary center-block">leggi ora</a></p>
					@endif
					@if($val->type == 1)
						<p style="display: block;float: left;"><a href="{{url('chat/'.$val->lecture_id)}}"><i class='fa fa-comments' style='font-size:30px;color:#337ab7;margin-right: 20px;float:left'></i></a></p>
					@endif
					
					  <p style="float: left;"><a href="#" class="btn btn-primary center-block">Compila Ora</a></p>
					  
					  
					</div>
					
				</div>	
				@endforeach
				@else
					<h3 style="padding:10px;text-align:center">Non c'è un nuovo messaggio</h3>
			@endif
			
			
			
		 
	
	</div>	
	<!-- Model to add sucategory -->
	<div id="confirm_model" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					
						
						
						<div class="row bobrow" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">
							
						</div>
						
					
					<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>
	
@endsection