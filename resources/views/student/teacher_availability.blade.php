@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
<style>
 /* Customize the label (the container) */
.check_container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.check_container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.check_container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.check_container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.check_container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.check_container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
} 
</style>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
		
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
						
						<table class="table table-bordered">
						<thead>
						  <tr>
							<th>#</th>
							<th>{{ __('translation.Days') }}</th>
							<th>{{ __('translation.Available') }}</th>
							<th>{{ __('translation.Start Time') }}</th>
							<th>{{ __('translation.End Time') }}</th>
						  </tr>
						</thead>
						<tbody>
						<?php //echo '<pre>'; print_r($teacherAvailabilities); die; ?>
							@php $i=1; @endphp
							@foreach($availability as $k => $val)
								@php $selectedDay = ''; $st = ''; $starttime = ''; $endtime = ''; @endphp 
								@foreach($teacherAvailabilities as $teacherAvailabilitiesval)
									@if($val->num_days == $teacherAvailabilitiesval->num_days)
										@php 
											$selectedDay = $teacherAvailabilitiesval->num_days;
											$st = $teacherAvailabilitiesval->status;
											$starttime = $teacherAvailabilitiesval->start_time ;
											$endtime = $teacherAvailabilitiesval->end_time;
										@endphp 
									@endif
								@endforeach
							  <tr>
								<td>{{$val->num_days}}</td>
								<td>{{$val->days}}</td>
								<td>
									<label class="check_container"><input type="checkbox" name="days[]" <?php if($st == 1 && $selectedDay == $val->num_days){ echo 'checked'; } ?> value="{{$val->id}}" /><span class="checkmark"></span>
									</label>
								</td>
								<td>
								<div class="input-group date form_time">
									<input type='text' class="form-control" id="dtp1_input{{$i}}" name="start_time[]" value="{{$starttime}}" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
									<br/>
								
								</td>
								<td>
								<div class="input-group date form_time">
									<input type='text' class="form-control" id="dtp2_input{{$i}}" name="end_time[]" value="{{$endtime}}" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
									<br/>
								
								</td>
							  </tr>
							@php $i++; @endphp
						  @endforeach
						  
						</tbody>
					  </table>
					

					<button type="submit" class="btn btn-primary" id="btn_save_school">{{ __('translation.Accredited profile') }}</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

@endsection