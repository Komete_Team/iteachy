@extends('layouts.app')
@section('title', 'Student Payment Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Change Password</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 @if (Session::has('success'))
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>
		<section class="content">
			<div>
				<form id="change-password" method="post">
					<div class="row">
						<div class="col-lg-6">
							{{ __('translation.Password') }}
						</div>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="password" name="password">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							{{ __('translation.Confirm Password') }}
						</div>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="confirm-password" name="confirm_password">
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-6">
							{{ __('translation.New Password') }}
						</div>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="new-password" name="new_password">
						</div>
					</div>
					
					{!! csrf_field() !!}	
					<button type="submit" class="btn btn-primary" id="btn_save_card-info">invia
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$('#change-password').validate({
			rules: {
				'password': "required",
				'new_password': "required",
				'confirm_password': {
					required: true,
      				equalTo: "#password"
    			}
			},
			messages: {
				'password': "Per favore, inserisci la password.",
				'new_password': "Per favore inserisci la nuova password",
				'confirm_password': {
					required: "Inserisci di nuovo la password.",
      				equalTo: "Password e conferma password devono essere uguali."
    			}
			}
		});
	});
</script>
@endsection
