@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
		
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Title') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="title" name="title" value="{{ $teacherDetails->title ?? '' }}">
								@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Description') }}:<span class="mandatory_field">*</span></label>
	    						<textarea class="form-control" id="description" name="description">{{ $teacherDetails->description ?? '' }}</textarea>
								@if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
				
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Price per hour') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="price" name="price" value="{{ $teacherDetails->price ?? '' }}">
								@if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Video') }}:<span class="mandatory_field">*</span></label>
	    						<input type="file" class="form-control" id="video" name="video" value="{{ $teacherDetails->video ?? '' }}">
								@if ($errors->has('video'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
                                @endif
	    					</div>
							<input type="hidden" value="{{$teacherDetails->file_name ?? ''}}" name="old_video" />
							<?php
							    
							    if( isset( $teacherDetails->file_name ) && !is_null( $teacherDetails->file_name ) && ( $teacherDetails->file_name != '' ) )
							    {
							    	
							    		$videoUrl = url('/videos/teacher_videos/' . $teacherDetails->file_name ?? '');
										echo '<iframe src="'.$videoUrl.'"></iframe>';
							    	
							    }
							 ?>
						</div>
					
					
						
					</div>
					

					<button type="submit" class="btn btn-primary" id="btn_save_school">{{ __('translation.Accredited profile') }}</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

@endsection