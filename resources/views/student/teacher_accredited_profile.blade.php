@extends('teacher.layouts.app')
@section('title', 'Teacher Info')

@section('content')
<style>
.container{
 margin: 0 auto;
}
.content1{
 width: 100px;
 float: left;
 margin-right: 5px;
 border: 1px solid gray;
 border-radius: 3px;
 padding: 5px;
}

/* Delete */
.content1 span{
 border: 2px solid red;
 display: inline-block;
 width: 99%; 
 text-align: center;
 color: red;
}
.content1 span:hover{
 cursor: pointer;
}
img{width:100% !important}
</style>
<script>
$(document).ready(function(){

 $("#but_upload").click(function(){

  var fd = new FormData();
  var files = $('#file')[0].files[0];
  fd.append('file',files);
  fd.append('request',1);

  // AJAX request
  $.ajax({
   url: 'upload_docs/{{$user_id}}',
   type: 'post',
   data: fd,
   contentType: false,
   processData: false,
   headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
   success: function(response){
     if(response != 0){
       var count = $('.container .content1').length;
       count = Number(count) + 1;
		var src = '<?php echo url('/uploads/teachers/documents'); ?>/'+response;
       // Show image preview with Delete button
       $('.container').append("<div class='content1' id='content_"+count+"' ><iframe src='"+src+"' bob='"+response+"' width='100' height='100'></iframe><span class='delete' id='delete_"+count+"'>Delete</span><input type='hidden' value='"+response+"' name='files[]' /></div>");
     }else{
       alert('file not uploaded');
     }
   }
  });
 });

 // Remove file
 $('.container').on('click','.content1 .delete',function(){
 
  var id = this.id;
  var split_id = id.split('_');
  var num = split_id[1];

  // Get image source
  var imgElement_src = $( '#content_'+num+' img' ).attr("bob");
  var deleteFile = confirm("Do you really want to Delete?");
  if (deleteFile == true) {
      // AJAX request
      $.ajax({
        url: 'upload_docs/{{$user_id}}',
        type: 'post',
	    headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
        data: {path: imgElement_src,request: 2},
        success: function(response){
           // Remove
           if(response == 1){ 
              $('#content_'+num).remove(); 
           } 
        } 
      }); 
   } 
 }); 
});
</script>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 @if (Session::has('success'))
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('success') }}</strong>
				</div>
			@elseif(Session::has('danger'))

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong>{{Session::get('danger') }}</strong>
				</div>
			@endif 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
			<div>
	    		<a href="{{url('teacher/addteacherdoc')}}" type="button" class="btn btn-primary" id="btn_show_category_modal">{{ __('translation.Add Document') }}</a>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
									<th>{{ __('translation.File Name') }}</th>
			        				<th>{{ __('translation.File') }}</th>
									<th>{{ __('translation.Status') }}</th>
			        				<th>{{ __('translation.Action') }}</th> 
			        			</tr>
			        		</thead>
							<tbody>
							@foreach($teacherDocDetails as $val)
			        			<tr>
			        				<td>{{$val->id}}</td>
									<td>{{$val->doc_name}}</td>
									<td><a target="_blank" href="{{url('/uploads/teachers/documents/'.$val->files)}}">{{ __('translation.view') }}</a></td>
									<td><?php
									$pendingmessage =  __('translation.Pending');
									$approvedmessage =  __('translation.Approved');
									$rejectedmessage =  __('translation.Rejected');
									if($val->status == 0){ echo '<span class="text-primary">'.$pendingmessage.'</span>';}else if($val->status == 1){echo '<span class="text-success">'.$approvedmessage.'</span>';}else{ echo '<span class="text-danger">'.$rejectedmessage.'</span>';} ?></td>
									<td><a href="javascript:void(0);" id="{{$val->id}}" class="delete_teacherdoc"><i class="fa fa-trash-o"></i></a></td>
			        			</tr>
							@endforeach	
			        		</tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<div id="modal_teacher_leave" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">modifica permesso</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_leave" id="frm_edit_leave" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Applica congedo:</label>
							<div class="input-group date form_datepicker" data-date="" data-date-format="dd MM yyyy" data-link-field="leave_date" data-link-format="yyyy-mm-dd">
								<input class="form-control" size="16" type="text" value="" id="leave_date" name="leave_date" readonly>
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
							</div>
							
							<input type="hidden" name="leave_id" id="leave_id" value="">
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_leave">{{ __('translation.Accredited profile') }}</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

@endsection