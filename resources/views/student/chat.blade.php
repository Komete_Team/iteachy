@extends('layouts.app')
@section('title', 'Student Payment Info')

@section('content')
<?php $imgUrl = url('/images/avatar.png');
 ?>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="container chatbox">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Chat</li>
	        </ol>
	    </section>
		<section class="content">
			<div class="col-lg-10 frame">
	            <ul>
	            	@foreach($chats as $chat)
	            		<?php 		
                		if(!empty($chatUsers[$chat->message_from]['profile_image'])) {
							if($chatUsers[$chat->message_from]['role'] == 3){
								$imgUrl = url('/images/student_profileimage/' . $chatUsers[$chat->message_from]['profile_image']);
							}else{
								$imgUrl = url('/images/teacher_logos/' . $chatUsers[$chat->message_from]['profile_image']);
							}
                		
                		}else{
							$imgUrl = url('/images/avatar5.png');
						} ?>
	            		@if ($chat->message_from == $user->id)
	            		<li style="width:100%">
	            			<div class="msj macro">
	                        	<div class="avatar">
	                        		<img class="img-circle" title="" style="width:100%;" src="{{$imgUrl}}" />
	                        	</div>
	                        	<div class="text text-l">
	                        		<p>{{$chat->message}}</p>
	                        		<p><small>{{$chat->date_time}}</small></p>
	                        	</div>
                        	</div>
                    	</li>
                    	@else
                    	<li style="width:100%;">
                    		<div class="msj-rta macro">
                    			<div class="text text-r">
                    				<p>{{$chat->message}}</p>
	                                <p><small>{{$chat->date_time}}</small></p>
	                            </div>
	                        <div class="avatar" style="padding:0px 0px 0px 10px !important">
	                        	<img class="img-circle" style="width:100%;" src="{{$imgUrl}}" />
	                        </div> 
							</div>                            
	                    </li>
                    	@endif
	            	@endforeach
	            </ul>
	            <div>
	                <div class="msj-rta macro">                        
	                    <div class="text text-r" style="background:whitesmoke !important">
	                        <input app-id="{{ $lectureData->id }}" from-user="{{ $user->id }}" to-user="{{ ($user->role==3)?$lectureData->teacher_id:$lectureData->student_id }}" class="mytext form-control" id="text-message" placeholder="scrivi messaggio"/>
	                    </div> 

	                </div>
	                <div style="padding:10px;">
	                    <span class="glyphicon glyphicon-share-alt"></span>
	                </div>                
	            </div>
	        </div>
	        <div class="col-lg-2">
	        	<?php
	        	$roletype = ['2'=>'Teacher', '3'=>'Student'];
			    if( $user->profile_image != '' )
			    {
					if($user->role == 3){
						$imgUrl = url('/images/student_profileimage/' . $user->profile_image);
					}else{
						$imgUrl = url('/images/teacher_logos/' . $user->profile_image);
					}
			    }else{
					$imgUrl = url('/images/avatar5.png');
				}
			    ?>
			    <img id="default-student-image" src="{{ $imgUrl }}" height="100px" width="100px">
	        	<div>
	        		<span>{{ $user->first_name.' '.$user->last_name }}</span><br>
	        		<span>{{ $user->email }}</span><br>
	        		<span>{{ $roletype[$user->role] }}</span>
	        	</div>
	        </div> 
		</section>	
	</aside>
@endsection
