@extends('layouts.app')
@section('title', 'Teacher Registration')
@section('content')
	<div class="login-form">
        <form autocomplete="off" name="frm_teacher_registration" id="frm_teacher_registration">
            <h2 class="text-center">Registrazione insegnanti</h2>
            {{ csrf_field() }}
           
            <div class="form-group">
            	<label for="first_name">{{ __('translation.First Name') }}:</label>
                <input type="text" class="form-control" placeholder="" name="first_name" id="first_name">
            </div>
			
			<div class="form-group">
            	<label for="last_name">{{ __('translation.Last Name') }}:</label>
                <input type="text" class="form-control" placeholder="" name="last_name" id="last_name">
            </div>
			
            <div class="form-group">
            	<label for="email_id">{{ __('translation.Email Id') }}:</label>
                <input type="text" class="form-control" placeholder="" name="email_id" id="email_id">
            </div>
            <div class="form-group">
            	<label for="password">{{ __('translation.Password') }}:</label>
                <input type="password" class="form-control" placeholder="" name="password" id="password">
            </div>

            <div class="form-group">
            	<label for="email_id">{{ __('translation.Category') }}:</label>
				@if(count($categories) > 0)
					<select name="categories[]" id="categories"  class="form-control" multiple="multiple">
					@foreach($categories as $cat)
						 <optgroup label="{{$cat->category_name}}">
						@foreach($subcategories as $subcat)
							@if($cat->id == $subcat->parent_id)
								<option value="{{$cat->id}}-{{$subcat->id}}">{{$subcat->category_name}}</option>
							@endif
						@endforeach
						</optgroup>
					@endforeach
					</select>
				@endif
            </div>
			
			<div class="form-group">
            	<label for="online">{{ __('translation.online') }}:</label>
                <input style="vertical-align: middle;width:auto; display:inline-block;margin-bottom: 5px;" type="radio" class="form-control" placeholder="" name="online" id="online" value="1">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" id="btn_teacher_registration">Registrazione</button>
            </div>

            <!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>
        </form>
    </div>
@endsection