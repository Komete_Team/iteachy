@php $totalrates = 0; @endphp
				@if(count($teachers) > 0)
				@foreach($teachers as $teachers_val)
				<?php
				//echo '<pre>'; print_r($teachers); die;
				$imgUrl = url('/images/avatar5.png');

				if( isset( $teachers_val->profile_image ))
				{

				$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);

				}
				if(!empty($teachers_val->ratings)){
				$ratings = array_sum(explode('$', $teachers_val->ratings));
				$totalusers = count(explode('$', $teachers_val->users));
				$totalrates = ceil($ratings/$totalusers);
				}else{
				$totalrates = 0;
				}

				//echo '<pre>'; print_r($teachers); die;

				?>
              <div class="col-md-7" > 
                  <div class="col-md-5 col-sm-6 col-xs-6 left-no_spacing"> 
                      <div class="profile_img"> 
                          <img src="{{$imgUrl}}" width="100%">
						   <ul class="ratting_ul">  
									@for($i=1; $i<=5; $i++)
									<?php 
									 if($i <= $totalrates)
										$cl = 'red';
									 else
										$cl = '';
									 ?>
									<li class="{{$cl}}"><i class="fa fa-star-o" aria-hidden="true"></i></li>
									@endfor	
                         
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-6"> 
                        <div class="profile_info" > 
                            <h2>{{$teachers_val->first_name ?? ''}}</h2>
                            <h3> Insegnante professionista</h3>
                            <ul class="pro_ul_img">  
                                  <li> <img src="{{ URL::asset('front/images/tick.png') }}"></li>
                                  <li> <img src="{{ URL::asset('front/images/home.png') }}"></li>
                                  <li> <img src="{{ URL::asset('front/images/cros.png') }}"></li>
                            </ul>
                            <div class="profession_detaile"> 
                              <p> insegna</p>
                              <h3>  {{$teachers_val->category_name ?? ''}}</h3>
                            </div>
                            <div class="pro_amount_left pull-left"> 
                                <p> Tariffa oraria da </p>
                                <h3>  EUR {{$teachers_val->price_per_hour ?? ''}}</h3>
                            </div>

                            <div class="pro_amount_right pull-right"> 
                                <p> Tariffa prova </p>
                                <h3>  EUR {{$teachers_val->trial_price ?? ''}}</h3>
                            </div>
                        </div>
                  </div>
              </div>

              <div class="col-md-5 col-sm-12 right_spacing"> 
                  <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'video_{{$teachers_val->id}}')" id="defaultOpen"><img src="{{ URL::asset('front/images/video.png') }}"></button>
                     <button class="tablinks" onclick="openCity(event, 'intro_{{$teachers_val->id}}')"><img src="{{ URL::asset('front/images/letter.png') }}"></button>
                    <button class="tablinks" onclick="openCity(event, 'calender_{{$teachers_val->id}}')"><img src="{{ URL::asset('front/images/calender.png') }}"></button>
                   </div>
                   <div id="video_{{$teachers_val->id}}" class="tabcontent">
					  @php if($teachers_val->file_name != ''){ @endphp
						<iframe width="420" height="345" src="{{url('videos/teacher_videos/'.$teachers_val->file_name)}}"></iframe>
					  @php } @endphp  
                   </div>
                   <!-- <div id="video" class="tabcontent">
                    <video width="320" height="240" controls>
                        <source src="movie.mp4" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                      </video>
                  </div> -->

                 <div id="intro_{{$teachers_val->id}}" class="tabcontent">
                   <!-- <h3>Paris</h3> -->
                     <p>{{$teachers_val->description}}</p> 
                 </div>

                  <div id="calender_{{$teachers_val->id}}" class="tabcontent">
					 <h3 class="text-center">Luglio</h3>
					 <div class="calender_img">
					  <div id='calendar_{{$teachers_val->id}}'></div>
					 </div>
				   </div>
              </div>
			<script>
				var teacher_id = '<?php echo $teachers_val->id; ?>';
			</script>		
			<script>
			$(document).ready(function(){
				//alert('#calendar_'+teacher_id);
			var lctid =1;
			$('#calendar_'+teacher_id).fullCalendar({
			header:{
			left:'prev,next today',
			center:'title',
			},
			height: 700,
			disableDragging: true,
			monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
			monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
			dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
			dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
			allDayText: 'Giornata',
			firstDay: 1,
			buttonText: {
			today: 'oggi',
			month: 'mese',
			week: 'settimana',
			day: 'giorno',

			},
			defaultView: 'agendaWeek',
			slotDuration: '00:30:00',
			slotLabelInterval: 30,
			slotLabelFormat: 'H:mm',
			editable: false,
			axisFormat: 'HH:mm',
			timeFormat: 'HH:mm',
			theme: true,    
			themeSystem:'bootstrap3', 
			close: 'fa-times',
			prev: 'fa-chevron-left',
			next: 'fa-chevron-right',
			prevYear: 'fa-angle-double-left',
			nextYear: 'fa-angle-double-right', 
			dayClick: function(date, jsEvent, view) {
			// console.log(date.format('H:m'))



			},  
			events: $('meta[name="route"]').attr('content') + '/student/teacherappointment/'+teacher_id+'/'+lctid,
			selectable:false,
			selectHelper:false, 
			eventClick: function(event) {
			//alert(event.className[0]);
			console.log(event);
			var classname = event.className[0];

			var classname2 = event.className[1];
			var id= event.id; 
			//alert(classname);

			//$('#myModal').show();
			},
			select: function(startDate, endDate, jsEvent, view, resource) {

			}
			});
			});
			</script>
			@endforeach
			@else
			<div style="margin-top: 30px;text-align: center;">There is not found any teacher</div>
			@endif
			<!--/.col-sm-6.col-lg-4--> 
			<script>
			function show(v, id){
			$('.teacherinfo_'+id).hide();
			$('#'+v).show();
			setTimeout(function(){ $('.fc-today-button').click(); }, 500);
			}
			</script>