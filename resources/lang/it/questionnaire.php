<?php

return [
    'Add Question' => 'Aggiungi domanda',
    'Question' => 'Domanda',
    'Answer' => 'Risposta',
    'Add Answer' => 'Aggiungi risposta',
    'Edit Question' => 'Modifica domanda'
];
