<?php

return [
    'Add Subscription Plan' => 'Aggiungi piano di abbonamento',
    'Title' => 'Titolo',
    'Base' => 'Base',
    'Standard' => 'Standard',
    'Premium' => 'Premium',
    'Edit Subscription Plan' => 'Modifica piano di abbonamento',
    'Teacher Subscription' => 'Abbonamento insegnante',
    'Subscription' => 'Abbonamento',
    'Choose the most suitable plan for you!' => 'Scegli il piano piu addato a te!',
    'You can change at any time.' => 'Puoi cambiare in qualsiasi momento',
	'time_period' => 'periodo di tempo',
];
