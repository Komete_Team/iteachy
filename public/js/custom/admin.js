/* Common js for all administrator related functionalities */

$(document).ready(function(){
    
	$('.form_time').datetimepicker({
       
		locale: 'it',
		format: 'LT'
	
    });

	
	
	$('.form_datepicker').datetimepicker({
		
		locale: 'it',
		format: 'YYYY-MM-DD '
		
    });
	 $('#datetimepicker1').datetimepicker({
		locale: 'it',
		format: 'YYYY-MM-DD '
	 });
	
	
	
	
	/* ---------- Role ---------- */

	// Roles listing
	$.fn.dataTableExt.errMode = 'ignore';
	$('#datatable_roles').dataTable({
	    sServerMethod: "get", 
	    bProcessing: true,
	    bServerSide: true,
	    sAjaxSource: $('meta[name="route"]').attr('content') + '/admin/fetchroles',
	    language: {
          	processing: '<img src="'+ $('meta[name="route"]').attr('content') +'/images/ajax-loader.gif">'
      	},
	    "columnDefs": [
	        { "className": "dt-center", "targets": [0, 4] }
	    ],
	    "aoColumns": [
	        { 'bSortable' : true, "width": "10%" },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : false }
	    ]
	});

	// To add a new role
	$('#btn_show_role_modal').click(function(){
		$('#frm_add_role #role_id').val('');
		$('#modal_role').modal({backdrop: 'static', keyboard: false});
	});
    $('#frm_add_role').submit(function(e){
        e.preventDefault();
    });
    $('#frm_add_role').validate({
        rules: {
            name: {
                required: true
            },
            display_name: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Please enter role keyword'
            },
            display_name: {
                required: 'Please enter role name'
            }
        }
    });

	
	// Save the data
    $('.plan').click(function(){
			var $this = $(this);
			var price = $this.attr('price');
			$('.plan').removeClass('active');
			$this.addClass('active');
			$('#sub_amount').val(price);
			$('#subscription_pay').removeClass('disabled');
    });
	
    // Save the data
    $('#btn_save_role').click(function(){
    	if( $('#frm_add_role').valid() )
    	{
			var $this = $(this);

    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/admin/saveroledetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_role').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
			    	if( response.resCode == 0 )
			    	{
			    		// Close the modal
			    		$('#modal_role').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_role')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_roles').DataTable().ajax.reload();
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });

    // To edit role
    $(document).on('click', '.edit_role', function(){
    	roleId = $(this).attr('id');

    	if( roleId != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/getroledetails',
				method: 'get',
				data: {
					roleId: roleId
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_role #role_id').val(roleId);

			    	$('#frm_add_role #name').val(response.name);
			    	$('#frm_add_role #display_name').val(response.display_name);
			    	$('#frm_add_role #description').val(response.description);

			    	// Show the modal
			    	$('#modal_role').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing service category id');
    	}
    });
	
	$(document).on('click', '.view_user', function(){
    	notid = $(this).attr('id');

    	if( notid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/teacher/getnotdetails',
				method: 'get',
				data: {
					notId: notid
				},
			    success: function(response){
			    	
			    	$('#modal_teacher_not').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing service category id');
    	}
    });
	
	

    // To delete role
    $(document).on('click', '.delete_role', function(){
    	if( confirm('Sei sicuro?') )
    	{
    		roleId = $(this).attr('id');

    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/deleterole',
				method: 'post',
				beforeSend: function() {
    				
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	
			    },
				data: {
					roleId: roleId
				},
			    success: function(response){
			    	// Show the alert
			    	showNotification('Success', response.resMsg);

			    	// Refresh the datatable
			    	$('#datatable_roles').DataTable().ajax.reload();
			    }
			});
    	}
    });

    /* ---------- Role ends ---------- */
	
	$('#datatable_teacher_leave').dataTable({
		"language": {
		"search": "Cerca",
		"lengthMenu": "",
		"infoEmpty": "Nessuna disponibilità",
		"infoFiltered":     "(Filtraggio su tutte le date disponibili)",
		"info": "Mostra _PAGE_ di _PAGES_",
		"emptyTable": "Nessun dato disponibile nella tabella",
		"zeroRecords": "Nessuna corrispondenza trovata",
		"paginate": {
		"previous": "Precedente",
		"next":'Successiva'
		}
		}
	});
	
	$(document).on('click', '.edit_leave', function(){
    	var teacherleaveid = $(this).attr('id');

    	var refer = $(this);
    	if( teacherleaveid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/teacher/getteacherleavedetail',
				method: 'get',
				data: {
					teacherleaveid: teacherleaveid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_edit_leave #leave_id').val(teacherleaveid);
			    	$('#frm_edit_leave #leave_date').val(response.leave_date);
					
			    	// // Show the modal
			    	$('#modal_teacher_leave').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing leave id');
    	}
	});
	
	// Save the sub category data
    $('#btn_save_leave').click(function(){
    	if( $('#frm_edit_leave').valid() ) 
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/teacher/saveteacherleavedetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_edit_leave').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_teacher_leave').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_edit_leave')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_teacher_leave').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	$(document).on('click', '.cancel_leave', function(){
		if( confirm('Sei sicuro?') )
		{
	    	var leaveid = $(this).attr('id');
	    	var refer = $(this);

	    	if( leaveid != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/teacher/cancelleave',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						leaveid: leaveid
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	$('#datatable_teacher_leave').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Missing notification id');
	    	}
		}
	});
	
	
	$.fn.dataTableExt.errMode = 'ignore';
	$('#datatable_category').dataTable({
		"language": {
		"search": "Cerca",
		"lengthMenu": "",
		"infoEmpty": "Nessuna disponibilità",
		"infoFiltered":     "(Filtraggio su tutte le date disponibili)",
		"info": "Mostra _PAGE_ di _PAGES_",
		"emptyTable": "Nessun dato disponibile nella tabella",
		"zeroRecords": "Nessuna corrispondenza trovata",
		"paginate": {
		"previous": "Precedente",
		"next":'Successiva'
		}
		}
	});
	
	
	
	$('#btn_show_category_modal').click(function(){
		// Destroy the previously existing dropdown with single selection (created for edit)
		$('#frm_add_category #category_name').val('');
		$('#frm_add_category #category_id').val('');
		$('#modal_category').modal({backdrop: 'static', keyboard: false});
	});
	
	$('#btn_show_lesson_modal').click(function(){
		// Destroy the previously existing dropdown with single selection (created for edit)
		$('#frm_add_lesson #lesson_name').val('');
		$('#frm_add_lesson #lesson_id').val('');
		$('#frm_add_lesson #total_lessons').val('');
		$('#frm_add_lesson #lesson_time').val('');
		$('#frm_add_lesson #price_per_hour').val('');
		$('#frm_add_lesson #lecture_type').val('');
		
		$('#modal_lesson').modal({backdrop: 'static', keyboard: false});
	});
	
	// Save the sub category data
    $('#btn_save_lesson').click(function(){
    	if( $('#frm_add_lesson').valid() )
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/teacher/savelessons',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_lesson').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_sub_category').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_lesson')[0].reset();

			    		// Show the alert
			    		showNotification('', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_category').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	

	
	$('#btn_show_sub_category_modal').click(function(){
		// Destroy the previously existing dropdown with single selection (created for edit)
		/* $('#categories').multipleSelect('destroy');
		$("#categories").val('');
		$('#categories').multipleSelect({
			width: '100%'
		}); */
		$('#frm_add_sub_category #sub_category_name').val('');
		$('#frm_add_sub_category #sub_category_id').val('');
		$('#modal_sub_category').modal({backdrop: 'static', keyboard: false});
	});
	
	// Save the sub category data
    $('#btn_save_sub_category').click(function(){
    	if( $('#frm_add_sub_category').valid() )
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/admin/savesubcategorydetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_sub_category').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_sub_category').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_sub_category')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_category').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	// Save the category data
    $('#btn_save_category').click(function(){
    	if( $('#frm_add_category').valid() )
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/admin/savecategorydetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_category').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					//alert(response);
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_category').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_category')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_category').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	$('#btn_save_appointment').click(function(){
    	if( $('#frm_edit_appointment').valid() )
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/teacher/saveteacherappointment',
    			method: 'post',
    			data: {
    				frmData: $('#frm_edit_appointment').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_appointment').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_edit_appointment')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_category').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	$(document).on('click', '.edit_appointment', function(){
    	var appointmentid = $(this).attr('id');

    	var refer = $(this);
    	if( appointmentid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/teacher/getappointment',
				method: 'get',
				data: {
					appointmentid: appointmentid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_edit_appointment #appointment_id').val(response.aptid);
			    	$('#frm_edit_appointment #appointment_start_date').val(response.start_date);
					$('#frm_edit_appointment #appointment_end_date').val(response.end_date);
					$('#frm_edit_appointment #skype_id').val(response.skype_id);
					$('#frm_edit_appointment #meeting_place').val(response.meeting_place);
					$('#frm_edit_appointment #knowledge_level').val(response.knowledge_level);
					$('#frm_edit_appointment #message').val(response.message);
					
					
					
			    	// // Show the modal
			    	$('#modal_appointment').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing lesson id');
    	}
	});
	
	$(document).on('click', '.edit_lesson', function(){
    	var lessonid = $(this).attr('id');

    	var refer = $(this);
    	if( lessonid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/teacher/getlessondetails',
				method: 'get',
				data: {
					lessonid: lessonid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_lesson #lesson_id').val(lessonid);
			    	$('#frm_add_lesson #lesson_name').val(response.lesson_name);
					$('#frm_add_lesson #total_lessons').val(response.total_lessons);
					$('#frm_add_lesson #lesson_time').val(response.lesson_time);
					$('#frm_add_lesson #price_per_hour').val(response.price_per_hour);
					$('#frm_add_lesson #lecture_type').val(response.type);
			    	// // Show the modal
			    	$('#modal_lesson').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing lesson id');
    	}
	});
	
	
	
	$(document).on('click', '.delete_appointment', function(){
		var element = this;
		sweetAlert(
		{
			title: "Sei sicuro?",
			text: "Non sarai in grado di recuperarlo!",
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Invia!",
			cancelButtonText: "Cancella!",
			showLoaderOnConfirm: true,
			closeOnCancel: true
		}, 
		function(isConfirm){
			if (isConfirm){
				var appt_id = $(element).attr('id');
				var refer = $(element);
	    	if( appt_id != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/teacher/deleteappointment',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						appt_id: appt_id
					},
				    success: function(response){
				    	// Show the alert
				    	swal("Successo!", response.resMsg, "success");
						e.preventDefault();

				    	// Refresh the datatable
				    	$('#datatable_category').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		swal("Annullata", "Id was missing", "error");
				e.preventDefault();
	    	}
				
			}
		});
	
	});
	
	$(document).on('click', '.delete_lesson', function(){
		
		var element = this;
		sweetAlert(
		{
			title: "Sei sicuro?",
			text: "Non sarai in grado di recuperarlo!",
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Invia!",
			cancelButtonText: "Cancella!",
			showLoaderOnConfirm: true,
			closeOnCancel: true
		}, 
		function(isConfirm){
			if (isConfirm){
				var lesson_id = $(element).attr('id');
				var refer = $(element);
	    	if( lesson_id != '' )
	    	{
	    		// Get the details of the selected role
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/teacher/deletelesson',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						lesson_id: lesson_id
					},
				    success: function(response){
				    	// Show the alert
				    	swal("Successo!", response.resMsg, "success");

				    	// Refresh the datatable
				    	$('#datatable_category').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		swal("Annullata", "Id was missing", "error");
				e.preventDefault();
	    	}
				
			} 
		});
		
	});
	
	$(document).on('click', '.edit_category', function(){
    	var catid = $(this).attr('id');

    	var refer = $(this);
    	if( catid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/getcategorydetails',
				method: 'get',
				data: {
					catid: catid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_category #category_id').val(catid);
			    	$('#frm_add_category #category_name').val(response.category_name);
					
			    	// // Show the modal
			    	$('#modal_category').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing category id');
    	}
	});
	
	$(document).on('click', '.edit_subcategory', function(){
    	var catid = $(this).attr('id');

    	var refer = $(this);
    	if( catid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/getsubcategorydetails',
				method: 'get',
				data: {
					catid: catid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_sub_category #sub_category_id').val(catid);
			    	$('#frm_add_sub_category #sub_category_name').val(response.category_name);
					
			    	// // Show the modal
			    	$('#modal_sub_category').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing sub category id');
    	}
	});
	
	
	
	$(document).on('click', '.delete_subcategory', function(){
		if( confirm('Sei sicuro?') )
		{
	    	var catid = $(this).attr('id');
	    	var refer = $(this);

	    	if( catid != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/admin/deletesubcategory',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						catid: catid
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	$('#datatable_category').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Missing notification id');
	    	}
		}
	});
	
	$(document).on('click', '.delete_category', function(){
		if( confirm('Sei sicuro?') )
		{
	    	var catid = $(this).attr('id');
	    	var refer = $(this);

	    	if( catid != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/admin/deletecategory',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						catid: catid
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	$('#datatable_category').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Missing notification id');
	    	}
		}
	});
	
	$(document).on('click', '.delete_teacherdoc', function(){
		if( confirm('Sei sicuro?') )
		{
	    	var docid = $(this).attr('id');
	    	var refer = $(this);

	    	if( docid != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/teacher/deleteteacherdoc',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						docid: docid
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	$('#datatable_category').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Missing notification id');
	    	}
		}
	});
	
	

    /* ---------- School ---------- */

    // School listing
	$.fn.dataTableExt.errMode = 'ignore';
	$('#datatable_schools').dataTable({
	    sServerMethod: "get", 
	    bProcessing: true,
	    bServerSide: true,
	    sAjaxSource: $('meta[name="route"]').attr('content') + '/admin/fetchschools',
	    language: {
          	processing: '<img src="'+ $('meta[name="route"]').attr('content') +'/images/ajax-loader.gif">'
      	},
	    "columnDefs": [
	        { "className": "dt-center", "targets": [0, 7, 8] }
	    ],
	    "aoColumns": [
	        { 'bSortable' : true, "width": "5%" },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : false },
	        { 'bSortable' : false },
	    ]
	});

	// To change the verification status
	$(document).on('click', '.toggle_verification', function(){
		var schoolId = $(this).attr('id');
		var verificationStatus 	= '0';
		if( $(this).is(':checked') )
		{
			verificationStatus = '1';
		}

		$.ajax({
			url: $('meta[name="route"]').attr('content') + '/admin/verifyschool',
			method: 'post',
			data: {
				schoolId: schoolId,
				verificationStatus: verificationStatus
			},
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			beforeSend: function() {
				$('#loading_spinner').show();	
		    },
		    complete: function()
		    {
		    	$('#loading_spinner').hide();	
		    },
		    success: function(response){
		    	if( response.resCode == 0 )
		    	{
		    		showNotification('Success', response.resMsg);

		    		// Refresh the datatable
		    		$('#datatable_schools').DataTable().ajax.reload();
		    	}
		    	else
		    	{
		    		showNotification('Alert', response.resMsg);
		    	}
		    }
		});
	});

	// To add a new school
    $('#frm_add_school').submit(function(e){
        e.preventDefault();
    });
    $('#frm_add_school').validate({
    	ignore: [],
        rules: {
            school_name: { required: true },
            enrollment_no: { required: true },
            address: { required: true },
            city: { required: true },
            province: { required: true },
            country: { required: true },
            principal_fname: { required: true },
            principal_lname: { required: true },
            principal_email: { required: true, email: true },
            principal_contact_no: { required: true, number: true },
            contact_person_fname: { required: true },
            contact_person_lname: { required: true },
            contact_person_email: { required: true, email: true },
            contact_person_contact_no: { required: true, number: true },
            status: { required: true }
        },
        messages: {
            school_name: { required: 'Please enter school name' },
            enrollment_no: { required: 'Please enter enrollment no' },
            address: { required: 'Please enter address' },
            city: { required: 'Please enter city' },
            province: { required: 'Please enter province/state' },
            country: { required: 'Please enter country' },
            principal_fname: { required: 'Please enter first name' },
            principal_lname: { required: 'Please enter last name' },
            principal_email: { required: 'Please enter email', email: 'Please enter a valid email' },
            principal_contact_no: { required: 'Please enter contact number', number: 'Please enter a valid contact number' },
            contact_person_fname: { required: 'Please enter first name' },
            contact_person_lname: { required: 'Please enter last name' },
            contact_person_email: { required: 'Please enter email', email: 'Please enter a valid email' },
            contact_person_contact_no: { required: 'Please enter contact number', number: 'Please enter a valid contact number' },
            status: { required: 'Please select status' }
        }
    });

    // Save the school details
    $('#btn_save_school').click(function(){
    	if( $('#frm_add_school').valid() )
    	{
    		var $this = $(this);

	        // Create form data object and append the values into it
	        var formData = new FormData();

			// Get the image and append it to form object
	        var school_logo = $('#school_logo').prop('files')[0];
	        if( school_logo != undefined )
	        {
	        	formData.append('school_logo', school_logo);
	        }

	        var documentCount = $('#documents').prop('files').length;
	        for (i = 0; i < documentCount; i++)
	        {
	            formData.append("documents[]", $('#documents').prop('files')[i]);
	        }

	        formData.append('school_id', $('#school_id').val());
	        formData.append('school_name', $('#school_name').val());
	        formData.append('enrollment_no', $('#enrollment_no').val());
	        formData.append('address', $('#address').val());
	        formData.append('city', $('#city').val());
	        formData.append('province', $('#province').val());
	        formData.append('country', $('#country').val());
	        
	        formData.append('principal_fname', $('#principal_fname').val());
	        formData.append('principal_lname', $('#principal_lname').val());
	        formData.append('principal_email', $('#principal_email').val());
	        formData.append('principal_contact_no', $('#principal_contact_no').val());

	        formData.append('contact_person_fname', $('#contact_person_fname').val());
	        formData.append('contact_person_lname', $('#contact_person_lname').val());
	        formData.append('contact_person_email', $('#contact_person_email').val());
	        formData.append('contact_person_contact_no', $('#contact_person_contact_no').val());

	        formData.append('status', $('input[name="status"]:checked').val());

	        $.ajax({
	            url: $('meta[name="route"]').attr('content') + '/admin/saveschooldetails',
	            method: 'post',
	            data: formData,
	            contentType : false,
	            processData : false,
	            beforeSend: function() {
	                // Show the loading button
	                $this.button('loading');
	            },
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            complete: function()
	            {
	                // Change the button to previous
	                $this.button('reset');
	            },
	            success: function(response){
	                if( response.resCode == 0 )
	                {
	                    showNotification('Success', response.resMsg);

	                    // Reset the form
	                    $('#frm_add_school')[0].reset();
	                }
	                else
	                {
	                    showNotification('Alert', response.resMsg);
	                }
	            }
	        });
    	}
    });

    /* ---------- School ends ---------- */
	
	// Teachers listing
	$.fn.dataTableExt.errMode = 'ignore';
	$('#datatable_teachers').dataTable({
		"language": {
		"search": "Cerca",
		"lengthMenu": "",
		"infoEmpty": "Nessun record disponibile",
		"emptyTable": "Nessun dato disponibile nella tabella",
		"zeroRecords": "Nessuna corrispondenza trovata",
		"paginate": {
		"previous": "precedente",
		"next":'Successiva'
		}
		}
	});
	
	$(document).on('click', '.edit_teacher', function(){
    	var teacherid = $(this).attr('id');

    	var refer = $(this);

    	if( teacherid != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/getteacherdetails',
				method: 'get',
				data: {
					teacherid: teacherid
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
					
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_teacher #teacher_id').val(teacherid);
			    	$('#frm_add_teacher #first_name').val(response.first_name);
			    	$('#frm_add_teacher #last_name').val(response.last_name);
			    	$('#frm_add_teacher #email').val(response.email);
					$('#frm_add_teacher #status').val(response.status);
					$('#frm_add_teacher #password').val(response.password);
					
			    	// // Show the modal
			    	$('#modal_teacher').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing teacher id');
    	}
	});
	
	
	
	// Save the teacher data
    $('#btn_save_teacher').click(function(){
    	if( $('#frm_add_teacher').valid() )
    	{
			var $this = $(this);
		
    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/admin/saveteacherdetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_teacher').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
			    	if( response.resCode == 0 )
			    	{
						
			    		// Close the modal
			    		$('#modal_teacher').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_teacher')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_teachers').DataTable().ajax.reload();
						
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	$('#btn_show_teacher_modal').click(function(){
		// Destroy the previously existing dropdown with single selection (created for edit)
		$('#frm_add_teacher #teacher_id').val('');
		$('#frm_add_teacher #first_name').val('');
		$('#frm_add_teacher #last_name').val('');
		$('#frm_add_teacher #email').val('');
		$('#frm_add_teacher #status').val('');
		$('#frm_add_teacher #password').val('');
		$('#modal_teacher').modal({backdrop: 'static', keyboard: false});
	});

	// Notification listing
	$.fn.dataTableExt.errMode = 'ignore';
	$('#datatable_notifications').dataTable({
	    sServerMethod: "get", 
	    bProcessing: true,
	    bServerSide: true,
	    sAjaxSource: $('meta[name="route"]').attr('content') + '/admin/fetchnotifications',
	    language: {
	      	processing: '<img src="'+ $('meta[name="route"]').attr('content') +'/images/ajax-loader.gif">'
	  	},
	    "columnDefs": [
	        { "className": "dt-center", "targets": [0, 6] }
	    ],
	    "aoColumns": [
	        { 'bSortable' : true, "width": "5%" },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : true },
	        { 'bSortable' : false }
	    ]
	});

    // To send a new notificatioon
	$('#btn_show_notification_modal').click(function(){
		// Destroy the previously existing dropdown with single selection (created for edit)
		$('#users').multipleSelect('destroy');
		$("#users").val('');
		$('#users').multipleSelect({
			width: '100%'
		});

		$('#frm_add_notification #notification_id').val('');
		$('#modal_notification').modal({backdrop: 'static', keyboard: false});
	});

    $('#frm_add_notification').submit(function(e){
        e.preventDefault();
    });
    $('#frm_add_notification').validate({
        rules: {
            priority: {
                required: true
            },
            heading: {
                required: true
            },
            description: {
                required: true
            }
        },
        messages: {
          	priority: {
          	    required: 'Please select priority'
          	},
          	heading: {
          	    required: 'Please enter heading'
          	},
          	description: {
          	    required: 'Please enter description'
          	}  
        }
    });

    // Save the data
    $('#btn_save_notification').click(function(){
    	if( $('#frm_add_notification').valid() )
    	{
			var $this = $(this);

    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/admin/savenotificationdetails',
    			method: 'post',
    			data: {
    				frmData: $('#frm_add_notification').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
			    	if( response.resCode == 0 )
			    	{
			    		// Close the modal
			    		$('#modal_notification').modal('hide');
			    		
			    		// Refresh the form
			    		$('#frm_add_notification')[0].reset();

			    		// Show the alert
			    		showNotification('Success', response.resMsg);

			    		// Refresh the datatable
			    		$('#datatable_notifications').DataTable().ajax.reload();
			    	}
			    	else
			    	{
			    		showNotification('Alert', response.resMsg);
			    	}
			    }
    		});
    	}
    });
	
	

	// To edit an existing notification
	$(document).on('click', '.edit_notification', function(){
    	var notificationId = $(this).attr('id');

    	var refer = $(this);

    	if( notificationId != '' )
    	{
    		// Get the details of the selected role
    		$.ajax({
				url: $('meta[name="route"]').attr('content') + '/admin/getnotificationdetails',
				method: 'get',
				data: {
					notificationId: notificationId
				},
				beforeSend: function() {
					$(refer).html('<i class="fa fa-spinner fa-spin"></i> | ');
				},
				complete: function()
				{
					$(refer).html('<i class="fa fa-pencil-square-o"></i> | ');
				},
			    success: function(response){
			    	// Auto-fill the form
			    	$('#frm_add_notification #notification_id').val(notificationId);
			    	$('#frm_add_notification #priority').val(response.priority_id);
			    	$('#frm_add_notification #heading').val(response.heading);
			    	$('#frm_add_notification #description').val(response.description);

			    	if( response.send_email == '1' )
			    	{
			    		$('#email_notification').prop('checked', true);
			    	}
			    	else
			    	{
			    		$('#email_notification').prop('checked', false);	
			    	}

			    	$('#users').multipleSelect('destroy');
			    	
			    	// Set the selection
			    	$("#users").val(response.user_id);

			    	// Re-initialize multipleSelect
			    	$('#users').multipleSelect({
			    		width: '100%',
			    	    single: true
			    	});

			    	// // Show the modal
			    	$('#modal_notification').modal('show');
			    }
			});
    	}
    	else
    	{
    		showNotification('Alert', 'Missing notification id');
    	}
	});

	// To delete notification
	$(document).on('click', '.delete_notification', function(){
		if( confirm('Sei sicuro?') )
		{
	    	var notificationId = $(this).attr('id');
	    	var refer = $(this);

	    	if( notificationId != '' )
	    	{
	    		// Get the details of the selected role
	    		$.ajax({
					url: $('meta[name="route"]').attr('content') + '/admin/deletenotification',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						notificationId: notificationId
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	$('#datatable_notifications').DataTable().ajax.reload();
				    }
				});
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Missing notification id');
	    	}
		}
	});
});

/*
* Function to show notification alerts
* @param string
* @param string
*/

function showNotification(alertType, message)
{
	$('#notification_modal').find('.modal-header').html(alertType);
	$('#notification_modal').find('.modal-body').html(message);
	$('#notification_modal').modal('show');
	//setTimeout(function(){ location.reload(); }, 1000);
	
}