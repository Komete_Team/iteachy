$(function(){
	$("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 500,
        onStepChanging: function (event, currentIndex, newIndex) { 
            if ( newIndex === 1 ) {
                $('.steps ul').addClass('step-2');
            } else {
                $('.steps ul').removeClass('step-2');
            }
            if ( newIndex === 2 ) {
                $('.steps ul').addClass('step-3');
            } else {
                $('.steps ul').removeClass('step-3');
            }

            if ( newIndex === 3 ) {
                $('.steps ul').addClass('step-4');
                $('.actions ul').addClass('step-last');
            } else {
                $('.steps ul').removeClass('step-4');
                $('.actions ul').removeClass('step-last');
            }
            return true; 
        },
        labels: {
            finish: "Place Holder",
            next: "Next",
            previous: "Previous"
        }
    });
	 
	$('.select_lectures').click(function(){
		var array = [];
		$('.bob_button').removeClass('disabled');
		if($(this).hasClass('selected_lecture')){
			var selected_lect = $(this).attr('id');
			 var index = array.indexOf(selected_lect);
 
			if (index > -1) {
			   array.splice(index, 1);
			}
			$('#lectureids').val(array);
			$(this).removeClass('selected_lecture');
		}else{
			 var array = [];
			$('.select_lectures').removeClass('selected_lecture');
			$(this).addClass('selected_lecture');
			var selected_lect = $(this).attr('id');
			array.push(selected_lect);
			var res = selected_lect.split("-");
			//alert(res[1]);
			$('#lectureids').val(res[0]);
			$('#lectureprice').val(res[1]);
		}
		
    });
	
    // Custom Steps Jquery Steps
    $('.wizard > .steps li a').click(function(){
    	$(this).parent().addClass('checked');
		$(this).parent().prevAll().addClass('checked');
		$(this).parent().nextAll().removeClass('checked');
    });
    // Custom Button Jquery Steps
    $('.forward').click(function(){
    	$("#wizard").steps('next');
		
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
		
		
		
    })
    // Checkbox
    $('.checkbox-circle label').click(function(){
        $('.checkbox-circle label').removeClass('active');
        $(this).addClass('active');
    })
})
