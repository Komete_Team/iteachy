var me = {};


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time) {
    if (time === undefined){
        time = 0;
    }
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        control = '<li style="width:100%">' +
                        '<div class="msj macro">' +
                        '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ me.avatar +'" /></div>' +
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="'+you.avatar+'" /></div>' +                                
                  '</li>';
    }
    setTimeout(
        function(){                        
            $(".chatbox").find('ul').append(control).scrollTop($("ul").prop('scrollHeight'));
        }, time);
    
}

function resetChat(){
    $(".chatbox").find('ul').empty();
}
$(document).ready(function(){
    $(".chatbox ul").animate({ scrollTop: ($('li').length)*50 }, 1000);	  
    var imgedef= $("#default-student-image").attr('src'); 
    me.avatar = imgedef;
    $("#text-message").on("keydown", function(e){
        if (e.which == 13){
            var message = $(this).val();
            var element=this;
            var appid=$(this).attr('app-id');
            var fromuser=$(this).attr('from-user');
            var touser=$(this).attr('to-user');
            if (message !== "") {
                $.ajax({
                    type:"GET",
                    url:base_url+'/savedata',
                    data:{'appid':appid,'fromuser':fromuser,'touser':touser,'message':message},
                    success: function(response){
                        if(response=='success')
                        {
                            insertChat("me", message);   
							$(".chatbox ul").animate({ scrollTop: ($('li').length)*50 });
                            $(element).val('');
                        }
                    }
                });
            }
        }
    });
});
//resetChat();
/*insertChat("me", "Hello Tom...", 0);  
insertChat("you", "Hi, Pablo", 1500);*/