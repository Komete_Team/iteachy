/* Common js for all frontend related functionalities */

			
			

$(document).ready(function(){
	$(document).on('click', '.search_teacher', function(){
		// Get the teacher categories
		var refer = $(this);
		$.ajax({
			url: $('meta[name="route"]').attr('content') + '/get_teacher_category',
			method: 'post',
			beforeSend: function() {
				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
			},
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			complete: function()
			{
				$(refer).html('<i class="fa fa-trash-o"></i>');
			},
			data: {
			},
			success: function(response){
				alert(response);
			}
		});
	});
	
    $('#review_form').validate({
        rules: {
            headline: {
                required: true
            },
            review: {
                required: true
            }
        },
        messages: {
            headline: {
                required: 'Please enter title for review'
            },
            review: {
                required: 'Please enter message for review'
            }
        }
    });
	
	$(document).on('click', '#reviewbutton', function(){
		// Get the teacher categories
		var refer = $(this);
		$.ajax({
			url: $('meta[name="route"]').attr('content') + '/saveteacherreview',
			method: 'post',
			beforeSend: function() {
				$('#reviewbutton').html('loading..');
			},
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			complete: function()
			{
				//$(refer).html('<i class="fa fa-trash-o"></i>');
				$('#reviewbutton').html('Sottoscrivi');
				$('#headline').val('');
				$('#review').val('');
				
				
			},
			data: {
				frmData: $('#review_form').serialize()
			},
			success: function(response){
				if( response.resCode == 0 )
				{
					$('#review_form').append('<div class="alert alert-success" role="alert" style="margin-top:10px;">La recensione è stata inviata</div>');
				}
			}
		});
	});
	
	
	

  // SlideToggle

  $('.clickMe').click(function(){
	let holdNum = $(this).attr("data-num");
	$('#open-'+holdNum).toggle(300);
  })
  
  

   

   
   $('.action').change(function(){
	  var refer = $(this);
	  if($(this).val() != '')
	  {
	   var catid = $(this).val();
	   if(catid != ''){
		   $.ajax({
				url: $('meta[name="route"]').attr('content') + '/get_teacher_subcategory',
				method: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				
				data: {
					frmData:catid
				},
				success: function(response){
					$('#subcategory').html(response);
					   $('#subcategory').multipleSelect({
							width: '100%',
							filter: true,
						});
				}
			});
	   }
	   
	  }
	});
	
	// Save the data
    $('#btn_search_teacher').click(function(){
			var $this = $(this);

    		$.ajax({
    			url: $('meta[name="route"]').attr('content') + '/get_teacher',
    			method: 'post',
    			data: {
    				frmData: $('#frm_search_teacher').serialize()
    			},
    			beforeSend: function() {
    				// Show the loading button
			        $this.button('loading');
					var im = "<div style='text-align:center;'><img style='width: 53px;margin-top: 20%;' src='"+$('meta[name="route"]').attr('content') + "/images/ajax-loader.gif' /></div>";
					$('.member-card-box').html(im);
			    },
    			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    complete: function()
			    {
			    	// Change the button to previous
			    	$this.button('reset');
			    },
			    success: function(response){
					//console.log(response.allteachers);
			    	$('.member-card-box').html(response);
					
			    		
			    	
			    	
			    }
    		});
    });
	
	$('#categories').multipleSelect({
		width: '100%',
		filter: true,
	});
	
	
	$('body').on('click','.delete_appt1', function() {
		$('#frm_delete_appt').submit();
	});
	$('.delete_appt').on('click', function() {
		
		$('#confirm_model').modal({backdrop: 'static', keyboard: false});
		
		
	    	var appointmentid = $(this).attr('id');
	    	var refer = $(this);

	    	if( appointmentid != '' )
	    	{
				$.ajax({
					url: $('meta[name="route"]').attr('content') + '/student/getappointment',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						appointmentid: appointmentid
					},
				    success: function(response){
				    	// Show the alert
				    	//showNotification('Success', response.resMsg);
						if( response.resCode == 0 )
						{	
							//alert(response.resMsg);
							$('.bobrow').html(response.resMsg);
						}
				    	// Refresh the datatable
				    	//$('#datatable_notifications').DataTable().ajax.reload();
				    }
				});
				
	    		// Get the details of the selected role
	    		/* $.ajax({
					url: $('meta[name="route"]').attr('content') + '/student/deleteappointment',
					method: 'post',
	    			beforeSend: function() {
	    				$(refer).html('<i class="fa fa-spinner fa-spin"></i>');
				    },
	    			headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				    complete: function()
				    {
				    	$(refer).html('<i class="fa fa-trash-o"></i>');
				    },
					data: {
						appointmentid: appointmentid
					},
				    success: function(response){
				    	// Show the alert
				    	showNotification('Success', response.resMsg);

				    	// Refresh the datatable
				    	//$('#datatable_notifications').DataTable().ajax.reload();
				    }
				}); */
	    	}
	    	else
	    	{
	    		showNotification('Alert', 'Something went wrong');
	    	}
		
	});
	
	
	

});

	function getPaymentType(payment_type){

	//	alert(payment_type);
		if(payment_type == 'card'){
			$('#carddetail').show();
			$('#paypal_div').hide();
			$('#payment-form').hide();
		}
		if(payment_type == 'paypal'){
			$('#carddetail').hide();
			$('#paypal_div').show();
			$('#payment-form').hide();
		}
		if(payment_type == 'stripe'){
			$('#carddetail').hide();
			$('#paypal_div').hide();
			$('#payment-form').show();
		}
	}

