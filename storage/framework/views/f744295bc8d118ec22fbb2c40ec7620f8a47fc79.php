<?php $__env->startSection('title', 'Teacher Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Teacher Info</li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table user-data-table" id="datatable_category">
                                    <thead>
                                        <tr>
                                            <th>S-No</th>
                                            <th>Foto</th>
                                            <th>Nome</th>
                                             <th>E-mail</th> 
                                            <th>Messaggio</th>
                                             <th>Tipo di utente</th>
											 <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $i='1';?>
                                        <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           
                                        <tr class="odd gradeX">
                                            <td><?php echo e($i); ?></td>
                                            <td><div class="user-avatar"><a class="chlid" data-id="<?php echo e($detail->id); ?>">
                                                <?php  
												$imagefolder = array(1=>'profile_images/', 2=>'teacher_logos/', 3=>'student_profileimage/');
												if(!empty($detail->profile_image)){
												?>
													<img style="width: 50px;" src="<?php echo e(url('images/'.$imagefolder[$detail->role].$detail->profile_image)); ?>">
												<?php }else{?>
													<img style="width: 50px;" src="<?php echo e(url('images/avatar5.png')); ?>">	
												<?php } ?>
                                                   

                                              </a></div> </td>
                                             <td><?php echo e($detail->first_name.' '.$detail->last_name); ?></td>
                                             <td><?php echo e($detail->email); ?></td> 
                                             <td><?php echo e($detail->description); ?></td>
											 <td><?php if($detail->role==3){echo 'Student';}else if($detail->role==1){echo 'Admin';}else if($detail->role==2){echo 'Teacher';}?></td> 
											<td>
											<?php if($detail->role==3){ ?>
											<a target="_blank" href="<?php echo e(url('chat/'.$detail->lecture_id)); ?>" ><i class="fa fa-reply" aria-hidden="true"></i></a>
											<?php } ?>
											</td>
                                          
                                        </tr>
                                        <?php 
                                            $i++;
                                        ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
                                </table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add category -->
	<!-- Model to add category -->
	<div id="modal_teacher_not" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">modifica permesso</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_leave" id="frm_edit_leave" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">User Name:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Email:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Message:</label>
							<p id="user_name"></p>
						</div>
						<div class="form-group">
							<label for="heading">Message:</label>
							<p id="user_name"></p>
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_leave"><?php echo e(__('translation.Accredited profile')); ?></button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>