<?php $__env->startSection('title', 'School Management'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active"><?php echo e(__('subscription.Edit Subscription Plan')); ?></li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>
	        	<form method="post" id="edit-subscription" autocomplete="off">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text"><?php echo e(__('subscription.Title')); ?> : <span class="mandatory_field">*</span></label>
	    						<input value="<?php echo e($subscriptionData->title); ?>" type="text" class="form-control" name="title">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text"><?php echo e(__('subscription.Base')); ?> : <span class="mandatory_field">*</span></label>
	    						<input value="<?php echo e($subscriptionData->base); ?>" type="number" class="form-control" name="base">
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text"><?php echo e(__('subscription.Standard')); ?> : <span class="mandatory_field">*</span></label>
	    						<input value="<?php echo e($subscriptionData->standard); ?>" type="number" class="form-control" name="standard">
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text"><?php echo e(__('subscription.Premium')); ?> : <span class="mandatory_field">*</span></label>
	    						<input value="<?php echo e($subscriptionData->premium); ?>" type="number" class="form-control" name="premium">
	    					</div>
						</div>
					</div>
					<?php echo csrf_field(); ?>

					<button type="submit" class="btn btn-primary" id="btn_save_school">Submit</button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#edit-subscription").validate({
			rules: {
				title: "required",
				base: "required",
				standard: "required",
				premium: "required"
			},
			messages: {
				title: "Per favore, inserisci il titolo.",
				base: "Inserisci il prezzo del piano di base.",
				standard: "Inserisci il prezzo del piano di standard.",
				premium: "Inserisci il prezzo del piano di premium."
			}
		});
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>