<?php $__env->startSection('title', 'Teacher Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">impostazioni</li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row">
	        	<div class="col-lg-12">
					  <h2>Vuoi eliminare definitivamene l'account</h2>
					  <form method="post" autocomplete="off" enctype="multipart/form-data">
					  <?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('pwd') ? ' has-error' : ''); ?>">
						  <label for="pwd">Inserisci password:<span class="mandatory_field">*</span></label>
						  <input type="password" class="form-control" id="pwd" placeholder="Inserisci password" name="pwd">
							<?php if($errors->has('pwd')): ?>
								<span class="help-block">
									<strong><?php echo e($errors->first('pwd')); ?></strong>
								</span>
							<?php endif; ?>
						</div>
						<div class="form-group <?php echo e($errors->has('pwd2') ? ' has-error' : ''); ?>">
						  <label for="pwd2">Reinserisci password:<span class="mandatory_field">*</span></label>
						  <input type="password" class="form-control" id="pwd2" placeholder="Reinserisci password" name="pwd2">
						 <?php if($errors->has('pwd2')): ?>
							<span class="help-block">
								<strong><?php echo e($errors->first('pwd2')); ?></strong>
							</span>
						 <?php endif; ?>
						</div>
						<button type="submit" class="btn btn-default"><?php echo e(__('translation.Submit')); ?></button>
					  </form>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add sucategory -->
	<div id="modal_lesson" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_lesson" id="frm_add_lesson" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Nome della lezione:</label>
							<input type="text" class="form-control" name="lesson_name" id="lesson_name" value="">
							<input type="hidden" name="lesson_id" id="lesson_id" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Lezioni totali:</label>
							<input type="text" class="form-control" name="total_lessons" id="total_lessons" value="">
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_lesson">Sottoscrivi</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>