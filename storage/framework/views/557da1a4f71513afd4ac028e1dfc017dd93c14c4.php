<?php $__env->startSection('title', 'Student Payment Info'); ?>

<?php $__env->startSection('content'); ?>
<?php $imgUrl = url('/images/avatar.png');
 ?>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	<aside class="container chatbox">
		<section class="content">
			<div class="col-lg-10 frame">
	            <ul>
	            	<?php $__currentLoopData = $chats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	            		<?php 		
                		if(!empty($chatUsers[$chat->message_from]['profile_image'])) {
							if($chatUsers[$chat->message_from]['role'] == 3){
								$imgUrl = url('/images/student_profileimage/' . $chatUsers[$chat->message_from]['profile_image']);
							}else{
								$imgUrl = url('/images/teacher_logos/' . $chatUsers[$chat->message_from]['profile_image']);
							}
                		
                		}else{
							$imgUrl = url('/images/avatar5.png');
						} ?>
	            		<?php if($chat->message_from == $user->id): ?>
	            		<li style="width:100%">
	            			<div class="msj macro">
	                        	<div class="avatar">
	                        		<img class="img-circle" title="" style="width:100%;" src="<?php echo e($imgUrl); ?>" />
	                        	</div>
	                        	<div class="text text-l">
	                        		<p><?php echo e($chat->message); ?></p>
	                        		<p><small><?php echo e($chat->date_time); ?></small></p>
	                        	</div>
                        	</div>
                    	</li>
                    	<?php else: ?>
                    	<li style="width:100%;">
                    		<div class="msj-rta macro">
                    			<div class="text text-r">
                    				<p><?php echo e($chat->message); ?></p>
	                                <p><small><?php echo e($chat->date_time); ?></small></p>
	                            </div>
	                        <div class="avatar" style="padding:0px 0px 0px 10px !important">
	                        	<img class="img-circle" style="width:100%;" src="<?php echo e($imgUrl); ?>" />
	                        </div>                             
	                    </li>
                    	<?php endif; ?>
	            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            </ul>
	            <div>
	                <div class="msj-rta macro">                        
	                    <div class="text text-r" style="background:whitesmoke !important">
	                        <input app-id="<?php echo e($lectureData->id); ?>" from-user="<?php echo e($user->id); ?>" to-user="<?php echo e(($user->role==3)?$lectureData->teacher_id:$lectureData->student_id); ?>" class="mytext form-control" id="text-message" placeholder="scrivi messaggio"/>
	                    </div> 

	                </div>
	                <div style="padding:10px;">
	                    <span class="glyphicon glyphicon-share-alt"></span>
	                </div>                
	            </div>
	        </div>
	        <div class="col-lg-2">
	        	<?php
	        	$roletype = ['2'=>'Teacher', '3'=>'Student'];
			    if( $user->profile_image != '' )
			    {
					if($user->role == 3){
						$imgUrl = url('/images/student_profileimage/' . $user->profile_image);
					}else{
						$imgUrl = url('/images/teacher_logos/' . $user->profile_image);
					}
			    }else{
					$imgUrl = url('/images/avatar5.png');
				}
			    ?>
			    <img id="default-student-image" src="<?php echo e($imgUrl); ?>" height="100px" width="100px">
	        	<div>
	        		<span><?php echo e($user->first_name.' '.$user->last_name); ?></span><br>
	        		<span><?php echo e($user->email); ?></span><br>
	        		<span><?php echo e($roletype[$user->role]); ?></span>
	        	</div>
	        </div> 
		</section>	
	</aside>
	</aside>	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>