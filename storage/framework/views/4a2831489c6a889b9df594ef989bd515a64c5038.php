<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="route" content="<?php echo e(url('/')); ?>">

	<title>School Registration</title>

	<!-- css here -->
	<link rel="stylesheet" href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>" />

	<!-- jquery here -->
	<script src="<?php echo e(URL::asset('js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('js/jquery.validate.min.js')); ?>"></script>

	

	<style type="text/css">
	.login-form {
		width: 35%;
    	margin: 10% auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
    label.error {
        color: red;
    }
	</style>
</head>
<body>
	<?php //echo '<pre>'; print_r($user); die; ?>

<div>
<p>Hello <?php echo $user['first_name'].' '.$user['last_name'].','; ?></p>
<p>Thank you for register on ITEACHY. Please click on below link to activate your account</p>
<p><a href="<?php echo url('teacher/varify/'.$user['_token']); ?>">Confirmation</a></p>
<p></p>
<p></p>
<p>Thank you</p>
<p>Iteachy</p>
</div>

</body>
</html>