<?php $__env->startSection('title', 'Questionnaires'); ?>

<?php $__env->startSection('content'); ?>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Questionnaires Answers</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			<?php if(Session::has('success')): ?>
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('success')); ?></strong>
							</div>
						<?php endif; ?>
						<?php if(Session::has('error')): ?>
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('error')); ?></strong>
							</div>
						<?php endif; ?>
						<h3>Questionnaires Answers</h3>
			        	<table class="table table-striped" id="datatable_category">
							<tbody>      
								<?php $i=1; //echo '<pre>'; print_r($questionnaires); die; ?>
								<?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								 <?php  
								 $answers = $val->answer; 
								 $answersarr = json_decode($answers, true);
								 //echo '<pre>'; print_r($answersarr); die;
								  ?>
								<tr class="odd gradeX">
									<td>
										<div class="accordion" id="accordionExample">
										  <div class="card">
											<div class="card-header" id="headingOne">
											  <h2 class="mb-0">
												<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo e($val->appointment_id); ?>" aria-expanded="true" aria-controls="collapseOne">
												 Appointment #<?php echo e($val->appointment_id); ?>

												</button>
											  </h2>
											</div>

											<div id="<?php echo e($val->appointment_id); ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
											  <div class="card-body">
												<?php  $i=1;  ?>
												<?php $__currentLoopData = $questionnaires; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ques): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													 <h3>Q<?php echo e($i); ?> <?php echo e($ques->question); ?></h3>
													 <?php $__currentLoopData = $answersarr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $ansval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<?php if($ques->id == $k): ?>
															<?php echo e($ansval); ?>

														<?php endif; ?>	
													 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														<p></p>
												<?php  $i++  ?>	
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											  </div>
											</div>
										  </div>
										</div>
									</td>
									 
								  
								</tr>
								<?php 
									$i++;
								?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
			        	</table>
						<div class="center" style="text-align: center;"><?php echo e($teachers->links()); ?></div>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_ques',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "<?php echo e(url('admin/deletequestionnaire')); ?>"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>