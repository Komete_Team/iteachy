<?php $__env->startSection('title', 'Admin Bacheca'); ?>

<?php $__env->startSection('content'); ?>
<?php
$paypalDetail = array();
$BusinessEmail= 'testve@yopmail.com';
$paypalUrl=  'https://www.sandbox.paypal.com/cgi-bin/webscr';
?>
	<div class="container">
	<!-- Right side column. Contains the navbar and content of the page -->
	
            	
            	
							<div class="col-md-4">
								<div class="row">
									<div class="col-lg-4">
										<label for="school_logo">
										<?php //echo '<pre>'; print_r($teacherDetails); die; ?>
											<!-- <img src="<?php echo e(url('/images/school_default_logo.jpg')); ?>" height="100px" width="100px"> -->
											<?php
											$imgUrl = url('/images/avatar.png');
											if( $teacher_detail->profile_image != '' )
											{
												
													$imgUrl = url('/images/teacher_logos/' . $teacher_detail->profile_image);
												
											}
											?>
											<img src="<?php echo e($imgUrl); ?>" height="100px" width="100px">
										</label>
									</div>
									<div class="col-lg-8">
									<p><?php echo e($teacher_detail->first_name.' '.$teacher_detail->last_name); ?></p>
									<p><?php echo e($teacherlectures->lesson_name ?? ''); ?></p>
									<p><?php echo e(date('F d, Y', strtotime($lectdetail['start_date']))); ?></p>
									<p><?php echo e($skype_id ?? ''); ?></p>
									<p><b><?php echo e($price ?? ''); ?> EURO</b></p>
									<?php //echo '<pre>'; print_r(); die; ?>
									</div>
									
								</div>
							</div>
							  <div class="col-md-8">
								<h3>Tipo di pagamento</h3>
								  
								 
								   
									<!-- Email input-->
									<div class="form-group">
										
										<input id="paypal" onClick="getPaymentType('paypal');" value="paypal" name="payment_type" type="radio" placeholder="" class="form-control" style="width: 10px;float: left;margin-right: 10px;">
										
										<label class="control-label" for="paypal">Paypal</label>
									</div>

								
									
									<div class="form-group">
										
										<input id="card" onClick="getPaymentType('stripe');" name="payment_type" type="radio" placeholder="" class="form-control" style="width: 10px;float: left;margin-right: 10px;">
										
										<label class="control-label" for="card">Card</label>
									</div>
									<div id="carddetail" style="display:none;">
									<!-- Email input-->
										<div class="container">
											<div class="row">
												<div class="col-xs-12 col-md-8">
													<div class="panel">
														<form class="form-horizontal" action="<?php echo e(url('student/saveappointment')); ?>" method="post">
														<?php echo e(csrf_field()); ?>

														<div class="panel-body">
															
																<div class="row">
																	<div class="col-xs-12">
																		<div class="form-group">
																			<label>CARD NUMBER</label>
																			<div class="input-group">
																				<input type="tel" class="form-control" placeholder="Valid Card Number" />
																				<span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-7 col-md-7">
																		<div class="form-group">
																			<label><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
																			<input type="tel" class="form-control" placeholder="MM / YY" />
																		</div>
																	</div>
																	<div class="col-xs-5 col-md-5 pull-right">
																		<div class="form-group">
																			<label>CV CODE</label>
																			<input type="tel" class="form-control" placeholder="CVC" />
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12">
																		<div class="form-group">
																			<label>CARD OWNER</label>
																			<input type="text" class="form-control" placeholder="Card Owner Names" />
																		</div>
																	</div>
																</div>
															
														</div>
														<div class="panel-footer">
															<div class="row">
																<div class="col-xs-12">
																	<button class="btn btn-warning btn-lg btn-block">Process payment</button>
																</div>
															</div>
														</div>
														</form>
													</div>
												</div>
											</div>
										</div>




									</div>
									<div id="paypal_div" style="display:none">
									
										<form role="form" method="post" action="<?php echo e($paypalUrl); ?>"  id="demo-form" data-parsley-validate>
                                         <?php echo e(csrf_field()); ?>                                       

                                        <input type="hidden" name="business" value="<?php echo e($BusinessEmail); ?>">  
                                        <input type="hidden" name="cmd" value="_xclick"> 
                                        <input type="hidden" name="item_name" value="<?php echo e(!empty($user_detail)?$user_detail->first_name:''); ?>">
                                        <input type="hidden" name="item_number" value="<?php echo e(!empty($user_detail)?$user_detail->id:''); ?>">
										
                                        <input type="hidden" id="amount" name="amount" value="<?php echo e($price ?? ''); ?>">
                                        <input type="hidden" name="currency_code" value="EUR">    
                                        <input type="hidden" name="custom" id="custom" value="<?php echo e($tid.'$'.$appt_id); ?>">                                        
                                        <input type='hidden' name='cancel_return' value='<?php echo e(url("paypal/cancel")); ?>'>
                                        <input type='hidden' name='return' value='<?php echo e(url("paypal/getresponse")); ?>'> 
                                       <!-- <input type='hidden' name='notify_url' value='<?php echo e(url('paypal/ipnstatus')); ?>'> -->
                                        <input type='hidden' name='notify_url' value='https://www.komete.it'>
                                         

                                    
										<div class="panel-footer">
											<div class="row">
												<div class="col-xs-12">
													<input type="submit" class="btn btn-warning btn-lg btn-block" value="Pay with paypal">
												</div>
											</div>
										</div>
										</form> 
									</div>
									<!-- Form actions -->
									


							<!-- stripe payments-->		
									<?php if(Session::has('success')): ?>
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p><?php echo e(Session::get('success')); ?></p>
                        </div>
                    <?php endif; ?>
  
                    <form role="form" action="<?php echo e(route('stripe.post')); ?>" method="post" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="<?php echo e(env('STRIPE_KEY')); ?>"
                                                    id="payment-form" style="display:none;">
                        @csrf
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' size='4' type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text' minlength="16"  maxlength="16">
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'

 
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
						<input type="hidden" name="business" value="<?php echo e($BusinessEmail); ?>"> 
						<input type="hidden" name="teacher_id" id="custom" value="<?php echo e($tid); ?>">   
						<input type="hidden" name="appointment_id" id="custom" value="<?php echo e($appt_id); ?>">   
						<input type="hidden" id="amount" name="amount" value="<?php echo e($price ?? ''); ?>">
						<input type="hidden" name="item_name" value="<?php echo e(!empty($user_detail)?$user_detail->first_name:''); ?>">
                                        <input type="hidden" name="item_number" value="<?php echo e(!empty($user_detail)?$user_detail->id:''); ?>">
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now (<?php echo e($price ?? ''); ?> EURO)</button>
                            </div>
                        </div>
                          
                    </form>

					<!-- stripe payment end-->
								   
								
							  </div>
						

          
	<!-- /.right-side -->
	</div>	
	

<style>
    .cc-img {
        margin: 0 auto;
    }
</style>
	
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
  <script type="text/javascript">
  $(function() {
	  var $form         = $(".require-validation");
	$('form.require-validation').bind('submit', function(e) {
	  var $form         = $(".require-validation"),
		  inputSelector = ['input[type=email]', 'input[type=password]',
						   'input[type=text]', 'input[type=file]',
						   'textarea'].join(', '),
		  $inputs       = $form.find('.required').find(inputSelector),
		  $errorMessage = $form.find('div.error'),
		  valid         = true;
		  $errorMessage.addClass('hide');
   
		  $('.has-error').removeClass('has-error');
	  $inputs.each(function(i, el) {
		var $input = $(el);
		if ($input.val() === '') {
		  $input.parent().addClass('has-error');
		  $errorMessage.removeClass('hide');
		  e.preventDefault();
		}
	  });
	
	  if (!$form.data('cc-on-file')) {
		e.preventDefault();
		Stripe.setPublishableKey($form.data('stripe-publishable-key'));
		Stripe.createToken({
		  number: $('.card-number').val(),
		  cvc: $('.card-cvc').val(),
		  exp_month: $('.card-expiry-month').val(),
		  exp_year: $('.card-expiry-year').val()
		}, stripeResponseHandler);
	  }
	
	});
	
	function stripeResponseHandler(status, response) {
		  if (response.error) {
			  $('.error')
				  .removeClass('hide')
				  .find('.alert')
				  .text(response.error.message);
		  } else {
			  // token contains id, last4, and card type
			  var token = response['id'];
			  // insert the token into the form so it gets submitted to the server
			  $form.find('input[type=text]').empty();
			  $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
			  $form.get(0).submit();
		  }
	  }
	
  });
  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('student.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>