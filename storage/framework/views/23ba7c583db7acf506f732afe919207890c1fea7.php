<?php $__env->startSection('title', 'Questionnaire'); ?>

<?php $__env->startSection('content'); ?>
<style>
.side_bar [type="radio"]:checked, [type="radio"]:not(:checked) {
    position: unset;
    left: -9999px;
}
</style>
<aside class="container">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Questionnaire</li>
        </ol>
    </section>
	<section class="content">
		<center><h2>Questionnaire</h2></center>
		<?php if(Session::has('success')): ?>
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong><?php echo e(Session::get('success')); ?></strong>
			</div>
		<?php else: ?>
		<form method="post" id="questionnaire" autocomplete="off">
			<div class="form-group">
			<?php  $counter=1;  ?>
			<?php $__currentLoopData = $questionnaires; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ques): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="row question" qid="<?php echo e($ques->id); ?>">
					<div class="col-lg-12">
						Q<?php echo e($counter.')'.$ques->question); ?>

					</div>
				</div>
				<?php  $answers = json_decode($ques->answer);  ?>
				<div class="row">
					<div class="col-lg-12">
					<ul>
						<?php $__currentLoopData = $answers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><label><span for="text"><?php echo e($ans); ?></span>
	    				<input class="" type="radio" value="<?php echo e($ans); ?>" name="answer[<?php echo e($ques->id); ?>]"></label></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>	
					</div>
				</div>
				<?php  $counter++;  ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php echo csrf_field(); ?>

			<button type="submit" class="btn btn-primary" id="btn_save_questionarrie">Invia</button>
			</div>
		</form>
		<?php endif; ?>
	</section>
</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$.extend(jQuery.validator.messages, {
		    required: "Seleziona la tua risposta."
		});
		$("#questionnaire").validate({
			errorPlacement: function (error, element) {
	            if (element.attr("type") == "radio") {
	                error.insertAfter($(element).parents('div').prev($('.question')));
	            } else {
	                // something else
	            }
	        }
		});
		$('.question').each(function(){
			var qid=$(this).attr('qid');
			$("[name='answer["+qid+"]']").rules("add", "required");;
		});
		
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>