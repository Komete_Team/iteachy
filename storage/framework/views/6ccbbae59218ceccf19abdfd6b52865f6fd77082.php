<?php $__env->startSection('title', 'Admin Bacheca'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Bacheca
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Bacheca</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">

	        <!-- Small boxes (Stat box) -->
	        <!-- <div class="row">
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-aqua">
	                    <div class="inner">
	                        <h3>
	                            150
	                        </h3>
	                        <p>
	                            New Orders
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-bag"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-green">
	                    <div class="inner">
	                        <h3>
	                            53<sup style="font-size: 20px">%</sup>
	                        </h3>
	                        <p>
	                            Bounce Rate
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-stats-bars"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-yellow">
	                    <div class="inner">
	                        <h3>
	                            44
	                        </h3>
	                        <p>
	                            User Registrations
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-person-add"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-red">
	                    <div class="inner">
	                        <h3>
	                            65
	                        </h3>
	                        <p>
	                            Unique Visitors
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-pie-graph"></i>
	                    </div>
	                    <a href="#" class="small-box-footer">
	                        More info <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	        </div> -->

	        <div class="row">
	            <div class="col-xs-12 connectedSortable">
	                
	            </div>
	        </div>

	        <!-- Main row -->
	        <div class="row">
	        	<div class="col-lg-12">
		        	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
		        </div>
	       	</div>
	        <!-- /.row (main row) -->

	    </section><!-- /.content -->
	</aside>
	<!-- /.right-side -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>