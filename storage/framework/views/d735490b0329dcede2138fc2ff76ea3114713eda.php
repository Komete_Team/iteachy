<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <meta name="route" content="<?php echo e(url('/')); ?>">
        
        <title><?php echo $__env->yieldContent('title'); ?></title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
       <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <!-- font Awesome -->
        <link href="<?php echo e(URL::asset('css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo e(URL::asset('css/ionicons.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo e(URL::asset('css/morris/morris.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo e(URL::asset('css/jvectormap/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
      
		<link href="<?php echo e(URL::asset('css/fullcalendar/fullcalendar.min.css')); ?>" rel="stylesheet" type="text/css" />
		
        <!-- Daterange picker -->
        <link href="<?php echo e(URL::asset('css/daterangepicker/daterangepicker-bs3.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo e(URL::asset('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo e(URL::asset('css/AdminLTE.css')); ?>" rel="stylesheet" type="text/css" />

		<link href="<?php echo e(URL::asset('css/multiple-select.css')); ?>" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo e(URL::asset('css/timepicker/bootstrap-timepicker.css')); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo e(URL::asset('css/timepicker/bootstrap-timepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
		
		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- Datatable style -->
        <link href="<?php echo e(URL::asset('css/datatables/jquery.dataTables.min.css')); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo e(URL::asset('js/jquery-ui-1.10.3.min.js')); ?>"></script>
        <!-- Bootstrap -->
        <script src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

		<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        
        <script src="<?php echo e(URL::asset('js/plugins/morris/morris.min.js')); ?>"></script>
        <!-- Sparkline -->
        <script src="<?php echo e(URL::asset('js/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
        <!-- jvectormap -->
        <script src="<?php echo e(URL::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
        <script src="<?php echo e(URL::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
        <!-- fullCalendar -->
        <script src="<?php echo e(URL::asset('js/plugins/fullcalendar/fullcalendar.min.js')); ?>"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo e(URL::asset('js/plugins/jqueryKnob/jquery.knob.js')); ?>"></script>
        <!-- daterangepicker -->
        <script src="<?php echo e(URL::asset('js/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo e(URL::asset('js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
        
        <!-- iCheck -->
        <!-- <script src="<?php echo e(URL::asset('js/plugins/iCheck/icheck.min.js')); ?>"></script> -->

        <!-- AdminLTE App -->
        <!-- <script src="<?php echo e(URL::asset('js/AdminLTE/app.js')); ?>"></script> -->
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo e(URL::asset('js/AdminLTE/dashboard.js')); ?>"></script>


        <!-- Datatable -->
        <script src="<?php echo e(URL::asset('js/plugins/datatables/jquery.dataTables.js')); ?>"></script>

        <!-- Datatable -->
        <script src="<?php echo e(URL::asset('js/jquery.validate.min.js')); ?>"></script>

        
		<script src="<?php echo e(URL::asset('js/multiple-select.js')); ?>"></script>
		
		<script src="<?php echo e(URL::asset('js/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('js/plugins/timepicker/bootstrap-timepicker.js')); ?>"></script>
		
		
		<!-- Admin functionality -->
        <script src="<?php echo e(URL::asset('js/custom/admin.js')); ?>"></script>
		<?php if(Route::currentRouteAction() == 'App\Http\Controllers\ChatController@index'): ?>
		<script src="<?php echo e(URL::asset('front/js/chat.js')); ?>"></script>
		<?php endif; ?>
		<script type="text/javascript">
			var base_url = '<?php echo url('/') ?>';
		</script>
		

        <script>
        // To manage the left nav submenu open/close
        $(document).ready(function(){
        	if( $('.treeview').hasClass('active') )
        	{
        		// Show the open icon on selected menu
        		$('.treeview').find('.navicon').removeClass('fa-angle-left').addClass('fa-angle-down');
        	}

        	$('.treeview').click(function(){
        		if( $(this).hasClass('active') )
        		{
        			$(this).removeClass('active');
        			$(this).find('.navicon').removeClass('fa-angle-down').addClass('fa-angle-left');
        			$(this).find('.treeview-menu').css('display', 'none');
        		}
        		else
        		{
        			$(this).addClass('active');
        			$(this).find('.navicon').removeClass('fa-angle-left').addClass('fa-angle-down');
        			$(this).find('.treeview-menu').css('display', 'block');
        		}
        	});
        });
        </script>

        <style type="text/css">
        label.error {
        	color: red;
        }
        .mandatory_field {
        	color: red;
        }

        /* For loading spinner */
        #loading_spinner {
          background: #ffffff;
          color: #666666;
          position: fixed;
          height: 100%;
          width: 100%;
          z-index: 5000;
          top: 0;
          left: 0;
          float: left;
          text-align: center;
          padding-top: 25%;
          opacity: .80;
        }
        .spinner {
            margin: 0 auto;
            height: 64px;
            width: 64px;
            animation: rotate 0.8s infinite linear;
            border: 5px solid firebrick;
            border-right-color: transparent;
            border-radius: 50%;
        }
        @keyframes  rotate {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
		.alert {margin:20px;}
		.fc-today
	{
		background-color:inherit !important;
	}
	

	
        </style>
		<style type="text/css">
	
	label.error {
		color: red;
	}
    
	.calendar-cell-leave {
		background-color: #ffffff !important;
		width:100%;
		z-index: 999999 !important;
	}
	.calendar-cell-booked{background:#cccccc !important;
		width:100%;
		z-index: 99 !important;}
		
	.calendar-cell-available{background:#62c208 !important;
		
		z-index: 9 !important;}	
	.calendar-cell-available-selected{background:#00B4FF !important;}	
		
	.calendar-cell-canceled {
		width:100%;
		background-color: #FF0000 !important;
		z-index: 9999 !important;
	}
	</style>
    </head>
    <body class="skin-black">
	<?php  $user = Auth::user();  ?>
    	<!-- Loading spinner -->
    	<div id="loading_spinner" style="display:none;">
    	    <div class="spinner"></div>
    	</div>
    	<!-- Loading spinner ends -->

        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?php echo e(url('teacher')); ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Insegnanti Bacheca
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                
                <!-- Sidebar toggle button-->
                <!-- <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a> -->
                
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">4</span>
                            </a> -->
                            <ul class="dropdown-menu">
                                <li class="header">You have 4 messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo e(url('images/avatar3.png')); ?>" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo e(url('images/avatar2.png')); ?>" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    AdminLTE Design Team
                                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo e(url('images/avatar.png')); ?>" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Developers
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo e(url('images/avatar2.png')); ?>" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Sales Department
                                                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo e(url('images/avatar.png')); ?>" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
						<?php 
							$notificationdetail= Helper::notificationDetail($user->id);
							$totalnotifications = count($notificationdetail); 
						?>
                        <li class="dropdown notifications-menu">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i>
                                <?php if($totalnotifications>0): ?><span class="label label-warning"><?php echo e($totalnotifications); ?></span> <?php endif; ?>
                            </a> 
                            <ul class="dropdown-menu">
                                <?php /* <li class="header">You have 10 notifications</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users warning"></i> 5 new members joined
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-cart success"></i> 25 sales made
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-person danger"></i> You changed your username
                                            </a>
                                        </li>
                                    </ul>
                                </li> */ ?>
                                    <?php if(!empty($notificationdetail) && count($notificationdetail)> 0): ?>
                                      <?php $__currentLoopData = $notificationdetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $notval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if($notval->type == 2): ?>
                                                      <li class="unread"><a href="<?php echo e(url('teacher/notification').'/'.$notval->id); ?>"><?php echo e($notval->description); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
													<?php endif; ?>  
													<?php if($notval->type == 1): ?>
													  <li class="unread"><a href="<?php echo e(url('teacher/all_notifications')); ?>"><?php echo e($notval->description); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
													  <?php endif; ?> 
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <li class="read"><a href="javascript:void(0);">nessuna notifica</a></li>  
                                    <?php endif; ?>
									
                                <!--<li class="footer"><a href="#">View all</a></li>-->
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-danger">9</span>
                            </a> -->
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <h3>
                                                    Design some buttons
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li><!-- end task item -->
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <h3>
                                                    Create a nice theme
                                                    <small class="pull-right">40%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">40% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li><!-- end task item -->
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <h3>
                                                    Some task I need to do
                                                    <small class="pull-right">60%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li><!-- end task item -->
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <h3>
                                                    Make beautiful transitions
                                                    <small class="pull-right">80%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">80% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li><!-- end task item -->
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo e($user->first_name); ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo e(url('images/avatar3.png')); ?>" class="img-circle" alt="User Image" />
                                    <p>
                                    	
                                        <?php echo e($user->name ?? ""); ?> - <?php echo e($role[0]->display_name ?? ""); ?>

                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li> -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
									<?php if($user->is_accridited == 1): ?>
                                    <div class="pull-left">
                                        <a href="<?php echo e(url('teacher/profile')); ?>" class="btn btn-default btn-flat"><?php echo e(__('translation.Profile')); ?></a>
                                    </div>
									<?php endif; ?>
                                    <div class="pull-right">
                                        <a href="<?php echo e(url('teacher/logout')); ?>" class="btn btn-default btn-flat"><?php echo e(__('translation.Sign out')); ?></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo e(url('images/avatar3.png')); ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo e(__('translation.hello')); ?>, <?php echo e($user->first_name ?? ""); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo e(__('translation.online')); ?></a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
						
						<?php if($user->is_accridited == 1): ?>
                        <li class="<?php echo e(Request::is('teacher/dashboard') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/dashboard')); ?>">
                                <i class="fa fa-dashboard"></i> <span>Bacheca</span>
                            </a>
                        </li>
						<li class="<?php echo e(Request::is('teacher/teacher_accredited_profile') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/teacher_accredited_profile')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.Accredited profile')); ?></span>
                            </a>
                        </li>
						<li class="<?php echo e(Request::is('teacher/teacher_info') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/teacher_info')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.Public Information')); ?></span>
                            </a>
                        </li>
						
						<li class="<?php echo e(Request::is('teacher/teacher_availability') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/teacher_availability')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.Availability')); ?></span>
                            </a>
                        </li>
						
						<li class="<?php echo e(Request::is('teacher/teacher_leave') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/teacher_leave')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.Leave Calendar')); ?></span>
                            </a>
                        </li>		
                        	
						
						<li class="<?php echo e(Request::is('teacher/lessons') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/lessons')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.lessons')); ?></span>
                            </a>
                        </li>
						
						<li class="<?php echo e(Request::is('teacher/appointments') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/appointments')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.appointments')); ?></span>
                            </a>
                        </li>
						
						<li class="<?php echo e(Request::is('teacher/settings') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/settings')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.settings')); ?></span>
                            </a>
                        </li>
						
						<li class="<?php echo e(Request::is('teacher/subscription') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/subscription')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.subscribe')); ?></span>
                            </a>
                        </li>
						
						<!--<li class="<?php echo e(Request::is('teacher/questionnaire') ? 'active' : ''); ?>">
                            <a href="<?php echo e(url('teacher/questionnaire')); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo e(__('translation.questionnaire')); ?></span>
                            </a>
                        </li>-->
						
						
                        <!-- <li>
                            <a href="pages/widgets.html">
                                <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Charts</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/charts/morris.html"><i class="fa fa-angle-double-right"></i> Morris</a></li>
                                <li><a href="pages/charts/flot.html"><i class="fa fa-angle-double-right"></i> Flot</a></li>
                                <li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>UI Elements</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
                                <li><a href="pages/UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                                <li><a href="pages/UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                                <li><a href="pages/UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                                <li><a href="pages/UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Forms</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                                <li><a href="pages/forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                                <li><a href="pages/forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                                <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="pages/calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="badge pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="pages/mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="badge pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>
                                <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul>
                        </li> -->
						<?php endif; ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Pagewise content -->
            <div>
            	<?php echo $__env->yieldContent('content'); ?>
            </div>

        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        <!-- Server response modal -->
        <div id="notification_modal" class="modal fade">
        	<div class="modal-dialog">
        		<!-- Modal content-->
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
        				<h4 class="modal-title"></h4>
        			</div>
        			<div class="modal-body"></div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        			</div>
        		</div>
        	</div>
        </div>

    </body>
</html>