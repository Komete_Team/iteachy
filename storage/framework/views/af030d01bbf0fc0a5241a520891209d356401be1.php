<?php $__env->startSection('title', 'Profile Management'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active"><?php echo e(__('translation.profile')); ?></li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>		
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

					<div class="row">
						<div class="col-lg-3">
							<label for="school_logo">
							<?php //echo '<pre>'; print_r($teacherDetails); die; ?>
							    <!-- <img src="<?php echo e(url('/images/school_default_logo.jpg')); ?>" height="100px" width="100px"> -->
							    <?php
							    $imgUrl = url('/images/avatar.png');
							    if( $teacherDetails->profile_image != '' )
							    {
							    	
							    		$imgUrl = url('/images/teacher_logos/' . $teacherDetails->profile_image);
							    	
							    }
							    ?>
							    <img src="<?php echo e($imgUrl); ?>" height="100px" width="100px">
							    <input type="file" id="school_logo" name="teacher_logo" style="display:none;">
								<input type="hidden" value="<?php echo e($teacherDetails->profile_image); ?>" name="old_image" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.First Name')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo e($teacherDetails->first_name ?? ''); ?>">
								<?php if($errors->has('first_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Last Name')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo e($teacherDetails->last_name ?? ''); ?>">
								<?php if($errors->has('last_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Email Id')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="email" name="email" value="<?php echo e($teacherDetails->email ?? ''); ?>">
								<?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('contact_no') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Contact No')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="contact_no" name="contact_no" value="<?php echo e($teacherDetails->contact_no ?? ''); ?>">
								<?php if($errors->has('contact_no')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('contact_no')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('address') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Address')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="address" name="address" value="<?php echo e($teacherDetails->address ?? ''); ?>">
								<?php if($errors->has('address')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-6">
	    					<div class="form-group <?php echo e($errors->has('city') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.City')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="city" name="city" value="<?php echo e($teacherDetails->city ?? ''); ?>">
								<?php if($errors->has('city')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('city')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('province') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Province/State')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="province" name="province" value="<?php echo e($teacherDetails->province ?? ''); ?>">
								<?php if($errors->has('province')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('province')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Country')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="country" name="country" value="<?php echo e($teacherDetails->country ?? ''); ?>">
								<?php if($errors->has('country')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('country')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group <?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Category')); ?>:<span class="mandatory_field">*</span></label>
	    						
								<?php  $selectedcategories = (array)json_decode($teacherDetails->categories);  ?>	
								<?php if(count($categories) > 0): ?>
									<select name="categories[]" id="category"  class="form-control" multiple="multiple">
								
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										 <optgroup label="<?php echo e($cat->category_name); ?>">
										<?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php if($cat->id == $subcat->parent_id): ?>
												<option <?php if(!empty($selectedcategories['subcategory'])){ if(in_array($subcat->id, $selectedcategories['subcategory'])){ echo 'selected'; } }?> value="<?php echo e($cat->id); ?>-<?php echo e($subcat->id); ?>"><?php echo e($subcat->category_name); ?></option>
												
												
											<?php endif; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</optgroup>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				
									
										
									</select>
								<?php endif; ?>
								
									
								
								<?php if($errors->has('category')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('category')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
	    						<label for="text"><?php echo e(__('translation.online')); ?>:</label>
	    						<input style="width:auto;" type="radio" class="form-control" id="online" name="online" <?php if($teacherDetails->is_online){ echo 'checked';} ?> value="<?php echo e($teacherDetails->is_online ?? ''); ?>">
	    					</div>
						</div>
					</div>	
					
					

					

					<button type="submit" class="btn btn-primary" id="btn_save_school"><?php echo e(__('translation.Submit')); ?></button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<script>
		$('#category').multipleSelect({
			width: '100%',
			filter: true,
		});
	</script>
	<!-- /.right-side -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>