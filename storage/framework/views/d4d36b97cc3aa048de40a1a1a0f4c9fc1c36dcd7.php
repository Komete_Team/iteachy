<?php $__env->startSection('title', 'Questionnaires'); ?>

<?php $__env->startSection('content'); ?>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Questionnaires</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<a href="<?php echo e(url('admin/questionnaires/add')); ?>" class="btn btn-primary">Aggiungi</a>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			<?php if(Session::has('success')): ?>
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('success')); ?></strong>
							</div>
						<?php endif; ?>
						<?php if(Session::has('error')): ?>
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('error')); ?></strong>
							</div>
						<?php endif; ?>
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Domanda</th>
			        				<th>Modifica</th>
			        			</tr>
			        		</thead>
							<tbody>      
								<?php $i=1;?>
								<?php $__currentLoopData = $questionnaires; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ques): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								   
								<tr class="odd gradeX">
									<td><?php echo e($i); ?></td>
									<td><?php echo e(!empty($ques->question)?$ques->question:'NA'); ?></td>
									
									<td><a href="<?php echo e(url('admin/questionnaires/edit/'.$ques->id)); ?>" class="edit_sub"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="<?php echo e($ques->id); ?>" class="delete_ques"><i class="fa fa-trash-o"></i></a></td>  
								  
								</tr>
								<?php 
									$i++;
								?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_ques',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "<?php echo e(url('admin/deletequestionnaire')); ?>"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>