<?php $__env->startSection('content'); ?>
	<!-- banner -->
    <div class="banner"> 
      <div class="banner_form">
        <div class="form_content pull-left"> 
            <h2>Cerca lezioni</h2>
        </div>
         <div class="form_right pull-right">
			 
			  <form class="example" style="margin:auto;width:550px" name="frm_search_teacher" id="frm_search_teacher" method="get" action="<?php echo e(url('teachers')); ?>" autocomplete="off" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>	
				<div class="search-box">
					<div class="input-field"> 
						<?php if(count($categories) > 0): ?>
							<select name="categories[]" id="category"  class="form-control" required>
								<option value="">Seleziona categoria</option>
								<optgroup label="All Categories">
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->category_name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	 
								</optgroup>
								<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php 
										$l=1;
										$totalsubcats = count($subcategories);
										//echo '<pre>'; print_r($totalsubcats); die;
									?>
									<?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($cat->id == $subcat->parent_id): ?>
											<?php if($l == 1): ?>
												<optgroup label="<?php echo e($cat->category_name); ?>">	
											<?php endif; ?>
												<option value="<?php echo e($subcat->id); ?>"><?php echo e($subcat->category_name); ?></option>
											<?php if($l == $totalsubcats): ?>
												</optgroup>
											<?php endif; ?>
										<?php $l++; ?>	
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						<?php endif; ?>
						<input type="hidden" value="1" name="accridited" />
						<input type="hidden" value="0" name="min-value" />
						<input type="hidden" value="10000000" name="max-value" />
						
					</div>
					<div class="search"> <button type="submit"><img src="<?php echo e(url('front/images/search.png')); ?>"></button> </div>
				</div>
			</form>	
         </div>
       </div>
    </div>
	<!-- imges-gallery -->
    <div class="images_gallery"> 
      <div class="images_heading"> 
        <h2>  Trova il tuo insegnante<br> tra i nostri XXXX professionisti.</h2>
        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore <br>et dolore magna aliqua.</p>
      </div>
	<?php //echo '<pre>'; print_r($teachers); die; ?>
      <div class="images_section"> 
        <div class="container"> 
            <div class="row"> 
              <div class="box_1"> 
				<?php if(count($teachers) > 0): ?>
					<?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teachers_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					  <div class="col-md-3 col-sm-2 col-xs-1"> 
						  <div class="img_box1"> 
							<div>
							<ul class="ul_img">  
								 <li> <img src="<?php echo e(url('front/images/tick.png')); ?>"></li>
								<li><img src="<?php echo e(url('front/images/home.png')); ?>"></li>
								<li><img src="<?php echo e(url('front/images/cros.png')); ?>"></li>
							</ul> 
							<?php  
							$imgUrl = url('/images/avatar5.png');
							if( isset( $teachers_val->profile_image ))
							{
								$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);
							}
							 ?>
							   <a href="<?php echo e(url('teacher_detail/'.$teachers_val->id)); ?>"><img class="teacher_img gel_img1" src="<?php echo e($imgUrl); ?>" width="100%"></a>
							</div>
							<div class="img_incontent"> 
							<?php $substr = Helper::sub_string($teachers_val->first_name.' '.$teachers_val->last_name, 0, 20); ?>
								<h3><a href="<?php echo e(url('teacher_detail/'.$teachers_val->id)); ?>"><?php echo e($substr); ?></a></h3>
								<p><?php echo e(substr($teachers_val->description, 0, 100)); ?></p>
							</div> 
						  </div>
					  </div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
              </div>
            </div>
        </div>
      </div>
    </div>
	<div class="container text-center"> 
          <div class="btn-group dropup custom_line">
            <button type="button" class="btn btn_custom">trova l’insegnante adatto a te</button>
            <button type="button" class="btn btn_custom_small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
        </div>
      </div>
	  
	<!-- services -->
    <div class="services"> 
        <div class="container"> 
            <div class="row"> 
              <h2 class="text-center">  Come funziona</h2>
                <div class="col-md-4"> 
                    <div class="service_box1 text-center"> 
                      <div class="service_circle">
                      <div class="service_circle_border">
                        <img src="<?php echo e(URL::asset('front/images/services_1.png')); ?>">
                      </div>
                      </div>
                        <h3>Cerca <br> un insegnante  </h3>
                        <hr class="services_hr">  
                        <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </P>
                    </div>
                </div>
                <div class="col-md-4"> 
                    <div class="service_box1 text-center"> 
                      <div class="service_circle">
                        <div class="service_circle_border">
                        <img src="<?php echo e(URL::asset('front/images/services_2.png')); ?>">
                      </div>
                      </div>
                        <h3>Prenota <br> la tua lezione </h3>
                        <hr class="services_hr">  
                        <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </P>
                    </div>
                </div>
                <div class="col-md-4"> 
                    <div class="service_box1 text-center"> 
                      <div class="service_circle">
                        <div class="service_circle_border">
                        <img src="<?php echo e(URL::asset('front/images/services_3.png')); ?>">
                      </div>
                      </div>
                        <h3>Inizia <br> il tuo percorso  </h3>
                        <hr class="services_hr">  
                        <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </P>
                    </div>
                </div>
            </div>
             </div>
              <div class="bg_blue">
               </div>
        </div>
		<div class="Perch_section">
        <div class="container"> 
            <div class="row"> 
                <div class="col-md-4"> 
                    <div class="Perch_1 text-center"> 
                      <h2>  Perchè iTeachy</h2>
                    </div>
                  </div>

                  <div class="col-md-8"> 
                    <div class="Perch_2 text-center"> 
                      <p class="para_1"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                      <p class="para_2">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    </div>
                  </div>
            </div>
        </div>
      </div> 
	<!-- testimonial -->
    <div class="testimonial_section">
      <div class="container">
         <h2 class="testimonial_heading">Reviews</h2>
            <div id="testimonial" class="owl-carousel owl-theme">
              <div class="item">
                  <div class="item_1"> 
                   <div class="text-left"> 
                     <img src="<?php echo e(URL::asset('front/images/face_3.jpg')); ?>">
                   </div>
                   <div class="user-info"> 
                     <h3>  Andy</h3>
                     <h3 class="info"> Imparando FRANCESE</h3>
                   </div>
               
               <div class="detaile"> 
                   <p class="para_1"> La risposta che stavo cercando.</p>
                   <p class="para_2"> Ho completato cinque lezioni e sono stupito di quanto ho già imparato. Non solo il mio insegnante conversa con me in italiano, ma mi invia anche fogli di lavoro e piccole clip audio su cui lavoro al mio ritmo.</p>
               </div>
             </div>
              </div>

              <div class="item">
                  <div class="item_1"> 
                                     
                   <div class="text-left"> 
                     <img src="<?php echo e(URL::asset('front/images/face_1.jpg')); ?>">
                   </div>
                   <div class="user-info"> 
                     <h3>  Andy</h3>
                     <h3 class="info"> Imparando FRANCESE</h3>
                   </div>
               
               <div class="detaile"> 
                   <p class="para_1"> La risposta che stavo cercando.</p>
                   <p class="para_2"> Ho completato cinque lezioni e sono stupito di quanto ho già imparato. Non solo il mio insegnante conversa con me in italiano, ma mi invia anche fogli di lavoro e piccole clip audio su cui lavoro al mio ritmo.</p>
               </div>
             </div>
              </div>

              <div class="item">
                  <div class="item_1"> 
                                     
                   <div class="text-left"> 
                     <img src="<?php echo e(URL::asset('front/images/face_2.jpg')); ?>">
                   </div>
                   <div class="user-info"> 
                     <h3>  Andy</h3>
                     <h3 class="info"> Imparando FRANCESE</h3>
                   </div>
               
               <div class="detaile"> 
                   <p class="para_1"> La risposta che stavo cercando.</p>
                   <p class="para_2"> Ho completato cinque lezioni e sono stupito di quanto ho già imparato. Non solo il mio insegnante conversa con me in italiano, ma mi invia anche fogli di lavoro e piccole clip audio su cui lavoro al mio ritmo.</p>
               </div>
             </div>
              </div>

              <div class="item">
                  <div class="item_1"> 
                                     
                   <div class="text-left"> 
                     <img src="<?php echo e(URL::asset('front/images/face_3.jpg')); ?>">
                   </div>
                   <div class="user-info"> 
                     <h3>  Andy</h3>
                     <h3 class="info"> Imparando FRANCESE</h3>
                   </div>
               
               <div class="detaile"> 
                   <p class="para_1"> La risposta che stavo cercando.</p>
                   <p class="para_2"> Ho completato cinque lezioni e sono stupito di quanto ho già imparato. Non solo il mio insegnante conversa con me in italiano, ma mi invia anche fogli di lavoro e piccole clip audio su cui lavoro al mio ritmo.</p>
               </div>
             </div>
              </div>




             </div>

      </div>
    </div>
    <!-- testimonial-end -->
	<!-- last_banner -->

      <div class="bg_img"> 

          <div class="container-fluid"> 
          <div class="row"> 
           <div class="col-md-6"> 
           </div>
           <div class="col-md-6" style="    padding-right: 0;"> 
            <div class="last_bg_content"> 
                <h3> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h3>
                <p> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                <div class="btn-group dropup">
                     <button type="button" class="btn btn_custom">INIZIA ORA</button>
                     <button type="button" class="btn btn_custom_small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span class="sr-only">Toggle Dropdown</span>
                     </button>
                 </div>
            </div>
           </div>
          </div>
          </div>
      </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>