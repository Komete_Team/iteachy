<?php $__env->startSection('title', 'Notifications'); ?>

<?php $__env->startSection('content'); ?>

	<!-- Multiselect dropdown -->
	<link href="<?php echo e(URL::asset('/css/multiple-select.css')); ?>" rel="stylesheet" />
	<script src="<?php echo e(URL::asset('/js/multiple-select.js')); ?>"></script>

	<script>
	$(document).ready(function(){
		$('#users').multipleSelect('destroy');
		$('#users').multipleSelect({
			width: '100%'
		});
	});
	</script>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Notifiche</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_notification_modal">Invia notifica</button>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Foto</th>
									<th>Nome</th>
			        				<th>Titolo</th>
									<th>Priorità</th>
			        				<!--<th>Email Notification Choosed</th>-->
			        				<th>Status</th>
			        				<th>Azione</th>
			        			</tr>
			        		</thead>
							<tbody>      
								<?php $i=1;?>
								<?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								   
								<tr class="odd gradeX">
									<td><?php echo e($i); ?></td>
									<td><div class="user-avatar"><a class="chlid" data-id="<?php echo e($detail->id); ?>">
										<?php  if(!empty($detail->profile_image)){?>
											<img style="width: 50px;" src="<?php echo e(url('images/profile_images/'.$detail->profileimage)); ?>">
										<?php }else{?>
											<img style="width: 50px;" src="<?php echo e(url('images/avatar5.png')); ?>">	
										<?php } ?>
										   

									  </a></div> </td>
									<td><?php echo e($detail->first_name.' '.$detail->last_name); ?></td>
									<td><?php echo e($detail->heading); ?></td>
									<td>
										<?php $__currentLoopData = $priorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $priority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php if($priority->id == $detail->priority_id){ echo $priority->priority_type;
											} ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</td>
									<!--<td><?php if($detail->send_email == '1'): ?> Yes <?php else: ?> No <?php endif; ?></td> -->
									<td><?php if($detail->is_read == '1'): ?> Received <?php else: ?> Not Received <?php endif; ?> </td>
									<td><a href="javascript:void(0);" id="<?php echo e($detail->id); ?>" class="edit_teacher"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="<?php echo e($detail->id); ?>" class="delete_teacher"><i class="fa fa-trash-o"></i></a></td>  
								  
								</tr>
								<?php 
									$i++;
								?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

	<!-- Model to send notification -->
	<div id="modal_notification" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Notification Details</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_notification" id="frm_add_notification" autocomplete="off">
						<div class="form-group">
							<label for="users">All Users:</label>
							<select name="users[]" id="users" class="form-control" multiple="">
								<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($user->id); ?>"><?php echo e(ucwords( strtolower( $user->first_name.' '.$user->last_name ) )); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
						<div class="form-group">
							<label for="priority">Priority:</label>
							<select name="priority" id="priority" class="form-control">
								<option value="">Select</option>
								<?php $__currentLoopData = $priorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $priority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($priority->id); ?>"><?php echo e(ucwords( strtolower( $priority->priority_type ) )); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
						<div class="form-group">
							<label for="heading">Titolo:</label>
							<input type="text" class="form-control" id="heading" name="heading">
							<input type="hidden" name="notification_id" id="notification_id" value="">
						</div>
						<div class="form-group">
							<label for="description">Description:</label>
							<textarea name="description" id="description" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label for="description">Send Email:</label>
							<div class="checkbox">
							  	<label><input type="checkbox" value="1" name="email_notification" id="email_notification">Send notification on email also</label>
							</div>
						</div>
						<button type="submit" class="btn btn-primary" id="btn_save_notification">Submit</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>