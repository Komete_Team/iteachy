<?php $__env->startSection('content'); ?>
<script src="<?php echo e(URL::asset('front/js/custom_listing.js')); ?>"></script>
		<div id="profile_page"> 
		<form name="frm1_search_teacher" id="frm1_search_teacher" method="get" action="<?php echo e(url('getteachers')); ?>" autocomplete="off" enctype="multipart/form-data">
			<div class="top_search"> 
				<div class="container_filter"> 
					<div class="row">
					 <div class="search_section">
					  <div class="col-md-4 col-md-offset-2"> 
						<select name="categories[]" id="teachers" class="form-control select2 chosen-select chosen-select-all" multiple  placeholder="seleziona una categoria">
								<?php if(count($recentSelectedCategories) > 0): ?>	
									<optgroup label="Recent Search">
								<?php $__currentLoopData = $recentSelectedCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recentval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									  <option value="<?php echo e($recentval->id); ?>"><?php echo e($recentval->category_name); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	  
									</optgroup>
								<?php endif; ?>	
								<?php if(count($categories) > 0): ?>
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php 
							$l=1;
							$totalsubcats = count($subcategories);
							//echo '<pre>'; print_r($totalsubcats); die;
						?>
						<?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($cat->id == $subcat->parent_id): ?>
								<?php if($l == 1): ?>
									<optgroup label="<?php echo e($cat->category_name); ?>">	
								<?php endif; ?>
									<option value="<?php echo e($subcat->id); ?>"><?php echo e($subcat->category_name); ?></option>
								<?php if($l == $totalsubcats): ?>
									</optgroup>
								<?php endif; ?>
							<?php $l++; ?>	
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
						</select>
						
					  </div>
					  <div class="col-md-4"> 
						<div class="calender_input"> 
						  <div class="input-group" id='datetimepicker2'>
								<span class="input-group-addon">
									<i class="fa fa-calendar" aria-hidden="true"></i>
								</span>
								<input type='text' class="form-control" placeholder="Disponibilità" aria-describedby="basic-addon1" id="dtp_input" name="dtp_input" />
						  </div>
						</div>
					  </div>
					 </div>
					</div>
				</div>
			</div>
			<div class="profile_menu"> 
				<div class="container_filter"> 
					<div class="inline_menu"> 
					 <ul>  
					  <li><a href="#"> <input type="checkbox" name="accridited" class="form-control" value="1"> Solo professori accreditati</a> </li>
					  <li><a href="#"> <img src="<?php echo e(URL::asset('front/images/locationh.png')); ?>">Tipo di insegnante</a> </li>
					  <li class="clickMe" data-num="3"><a href="#"> <img src="<?php echo e(URL::asset('front/images/save.png')); ?>">Prezzo</a>
						<div id="open-3" class="price-range">
						<div class="slider-labels">
						<div class="caption">
						<span id="slider-range-value1"></span>
						</div>
						<div class="text-right caption">
						<span id="slider-range-value2"></span>
						</div>
						</div>
						<div id="slider-range"></div>
						<input type="hidden" name="min-value" id="min-value" value="">
						<input type="hidden" name="max-value" id="max-value" value="">
						</div>
					</li>
					<li> <button type="Submit" class="btn btn-default btn_custom_li">Ricerca</button></li>
					 </ul>
				   </div>
				  


			   </div>
			</div>
			
			</form>
		</div>
		
		<div class="profilemain_heading"> 
			<h2>  <?php echo count($teachers); ?> insegnanti trovati</h2>
		</div>
		
		<!--<div class="container_filter"> -->
  <div class="container"> 
     <div class="row"> 
      <div class="cerca_Section"> 
       <div class="col-md-12"> 
           <div class="profile_section"> 
				<?php  $totalrates = 0;  ?>
				<?php if(count($teachers) > 0): ?>
				<?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teachers_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
				//echo '<pre>'; print_r($teachers); die;
				$imgUrl = url('/images/avatar5.png');

				if( isset( $teachers_val->profile_image ))
				{

				$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);

				}
				if(!empty($teachers_val->ratings)){
				$ratings = array_sum(explode('$', $teachers_val->ratings));
				$totalusers = count(explode('$', $teachers_val->users));
				$totalrates = ceil($ratings/$totalusers);
				}else{
				$totalrates = 0;
				}

				//echo '<pre>'; print_r($teachers); die;

				?>
              <div class="col-md-7" > 
                  <div class="col-md-5 col-sm-6 col-xs-6 left-no_spacing"> 
                      <div class="profile_img"> 
                          <img src="<?php echo e($imgUrl); ?>" width="100%">
						   <ul class="ratting_ul">  
									<?php for($i=1; $i<=5; $i++): ?>
									<?php 
									 if($i <= $totalrates)
										$cl = 'red';
									 else
										$cl = '';
									 ?>
									<li class="<?php echo e($cl); ?>"><i class="fa fa-star-o" aria-hidden="true"></i></li>
									<?php endfor; ?>	
                         
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-6"> 
                        <div class="profile_info" > 
                            <h2><a href="<?php echo e(url('teacher_detail/'.$teachers_val->id)); ?>"><?php echo e($teachers_val->first_name ?? ''); ?></a></h2>
                            <h3> Insegnante professionista</h3>
                            <ul class="pro_ul_img">  
                                  <li> <img src="<?php echo e(URL::asset('front/images/tick.png')); ?>"></li>
                                  <li> <img src="<?php echo e(URL::asset('front/images/home.png')); ?>"></li>
                                  <li> <img src="<?php echo e(URL::asset('front/images/cros.png')); ?>"></li>
                            </ul>
                            <div class="profession_detaile"> 
                              <p> insegna</p>
                              <h3>  <?php echo e($teachers_val->category_name ?? ''); ?></h3>
                            </div>
                            <div class="pro_amount_left pull-left"> 
                                <p> Tariffa oraria da </p>
                                <h3>  EUR <?php echo e($teachers_val->price_per_hour ?? ''); ?></h3>
                            </div>

                            <div class="pro_amount_right pull-right"> 
                                <p> Tariffa prova </p>
                                <h3>  EUR <?php echo e($teachers_val->trial_price ?? ''); ?></h3>
                            </div>
                        </div>
                  </div>
              </div>

              <div class="col-md-5 col-sm-12 right_spacing"> 
                  <div class="tab">
                    <button class="tablinks active" id="vid_<?php echo e($teachers_val->id); ?>" onclick="show('video_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>, 'vid_<?php echo e($teachers_val->id); ?>');"><img src="<?php echo e(URL::asset('front/images/video.png')); ?>"></button>
                     <button class="tablinks" id="des_<?php echo e($teachers_val->id); ?>" onclick="show('text_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>, 'des_<?php echo e($teachers_val->id); ?>');"><img src="<?php echo e(URL::asset('front/images/letter.png')); ?>"></button>
                    <button teach-id="<?php echo e($teachers_val->id); ?>" class="calendar-section tablinks" id="ca_<?php echo e($teachers_val->id); ?>" onclick="show('cal_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>, 'ca_<?php echo e($teachers_val->id); ?>');"><img src="<?php echo e(URL::asset('front/images/calender.png')); ?>"></button>
                   </div>
                   <div id="video_<?php echo e($teachers_val->id); ?>" class="tabcontent teacherinfo_<?php echo e($teachers_val->id); ?>">
				   <?php  if($teachers_val->file_name != ''){  ?>
					  <iframe width="420" height="345" src="<?php echo e(url('videos/teacher_videos/'.$teachers_val->file_name)); ?>"></iframe>
				   <?php  }  ?>  
                   </div>
                   <!-- <div id="video" class="tabcontent">
                    <video width="320" height="240" controls>
                        <source src="movie.mp4" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                      </video>
                  </div> -->

                 <div id="text_<?php echo e($teachers_val->id); ?>" style="display:none;" class="tabcontent teacherinfo_<?php echo e($teachers_val->id); ?>" >
                   <!-- <h3>Paris</h3> -->
                     <p><?php echo e($teachers_val->description); ?></p> 
                 </div>

                  <div id="cal_<?php echo e($teachers_val->id); ?>" style="display:none;" class="tabcontent teacherinfo_<?php echo e($teachers_val->id); ?>">
					 <!--<h3 class="text-center">Luglio</h3>-->
					 <div class="calender_img">
					  <div class="search-calendar" id='calendar_<?php echo e($teachers_val->id); ?>'></div>
					 </div>
				   </div>
              </div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php else: ?>
			<div style="margin-top: 30px;text-align: center;">There is not found any teacher</div>
			<?php endif; ?>
			<!--/.col-sm-6.col-lg-4--> 
			
          </div>
      </div>
      </div>

      </div>
    </div>
		
<script>
function show(v, id, tab){
$('.tablinks').removeClass('active');
$('.teacherinfo_'+id).hide();
$('#'+v).show();
$('#'+tab).addClass('active');

setTimeout(function(){ $('.fc-today-button').click(); }, 500);
}
</script>		
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
$(".defaultOpen").click();
$(document).ready(function(){
	$('body').on('click','.calendar-section',function(){
		var teacher_id = $(this).attr('teach-id');
		var lctid =1;
		$('#calendar_'+teacher_id).fullCalendar({
			header: {
	            left: '',
	            center: '',
	            right: ''
	        },
		    views: {
		        agendaThreeDay: {
		            type: 'agenda',
		            duration: { days: 3 }
		        }
		    },
		    defaultView:'agendaThreeDay',
			disableDragging: true,
			monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
			monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
			dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
			dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
			firstDay: 1,
			slotDuration: '03:00:00',
			slotLabelInterval: 180,
			slotLabelFormat: 'H:mm',
			editable: false,
			axisFormat: 'HH:mm',
			timeFormat: 'HH:mm',
			theme: true,    
			themeSystem:'bootstrap3',
			displayEventTime: false,  
			events: $('meta[name="route"]').attr('content') + '/student/teacherappointment/'+teacher_id+'/'+lctid+'?visible=1',
			selectable:false,
			selectHelper:false, 
			eventClick: function(event) {
			//alert(event.className[0]);
			console.log(event);
			var classname = event.className[0];

			var classname2 = event.className[1];
			var id= event.id; 
			//alert(classname);

			//$('#myModal').show();
			}
			});
	});
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>