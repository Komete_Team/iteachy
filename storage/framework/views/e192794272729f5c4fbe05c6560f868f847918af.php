							
<?php  $totalrates = 0;  ?>
<?php if(count($teachers) > 0): ?>
<?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teachers_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php
//echo '<pre>'; print_r($teachers); die;
$imgUrl = url('/images/avatar5.png');

if( isset( $teachers_val->profile_image ))
{

$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);

}
if(!empty($teachers_val->ratings)){
$ratings = array_sum(explode('$', $teachers_val->ratings));
$totalusers = count(explode('$', $teachers_val->users));
$totalrates = ceil($ratings/$totalusers);
}else{
$totalrates = 0;
}

//echo '<pre>'; print_r($teachers); die;

?>
<div class="row">
<div class="col-sm-6 col-lg-4">
<div class="member-card">
<div class="membar-review">
<div class="member-pix"> <img style="min-height: 105px;" src="<?php echo e($imgUrl); ?>" alt=""> </div>
<div class="text">
	<ul class="review">
		<?php for($i=1; $i<=5; $i++): ?>
		<?php 
		 if($i <= $totalrates)
			$cl = 'color';
		 else
			$cl = ' ';
		 ?>
		<li><a href="javascript:void(0)" class="<?php echo e($cl); ?>"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
		<?php endfor; ?>
	</ul>
	<h3><?php echo e($teachers_val->first_name ?? ''); ?></h3>
	<h4><?php echo e($teachers_val->address ?? ''); ?></h4>
</div>
</div>
<div class="detail-bx">
<div class="check_list">
	<ul>
		<li><a href="javascript:void(0);" onclick="show('video_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>);">Video</a></li>
		<li><a href="javascript:void(0);" onclick="show('text_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>);">Text</a></li>
		<li><a href="javascript:void(0);" onclick="show('cal_<?php echo e($teachers_val->id); ?>', <?php echo e($teachers_val->id); ?>);">Calendar</a></li>
	</ul>
</div>
<div class="text-name"> <small>insegna</small>
	<h3><?php echo e($teachers_val->category_name ?? ''); ?></h3>
</div>
<div class="text-name"> <small>Tariffa oraria da</small>
	<h3>EUR <?php echo e($teachers_val->price_per_hour ?? ''); ?></h3>
</div>
<div>
	<ul>
		<li style="display:table-cell"><img src="<?php echo e(url('front/images/right-tick.png')); ?>"></li>
		<li style="display:table-cell"><img src="<?php echo e(url('front/images/home-tick.png')); ?>"></li>
		<li style="display:table-cell"><img src="<?php echo e(url('front/images/cross-tick.png')); ?>"></li>
	</ul>
</div>
<div class="view-more"><a href="<?php echo e(url('teacher_detail/'.$teachers_val->id)); ?>">Prenota lezione <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
</div>

</div>
</div>
<div class="col-sm-10 col-lg-6">
	<div class="teacherinfo_<?php echo e($teachers_val->id); ?>" id="video_<?php echo e($teachers_val->id); ?>">
		<iframe src="<?php echo e(url('videos/teacher_videos/'.$teachers_val->file_name)); ?>"></iframe>
	</div>
	<div class="teacherinfo_<?php echo e($teachers_val->id); ?>" id="text_<?php echo e($teachers_val->id); ?>" style="display:none;">
		<p><?php echo e($teachers_val->description); ?></p>
	</div>
	<div class="teacherinfo_<?php echo e($teachers_val->id); ?>" id="cal_<?php echo e($teachers_val->id); ?>" style="display:none;">
		<div id='calendar'></div>
	</div>
</div>
</div>
<script>
		
									var teacher_id = '<?php echo $teachers_val->id; ?>';
								</script>		
							<script>
								$(document).ready(function(){
									var lctid =1;
								$('#calendar').fullCalendar({
										header:{
											left:'prev,next today',
											center:'title',
										},
										height: 700,
										disableDragging: true,
										monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
										monthNamesShort: ['genn','febbr','mar','apr','magg','giugno','luglio','ag','sett','ott','nov','dic'],
										dayNames: ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
										dayNamesShort: ['dom','lun','mar','mer','gio','ven','sab'],
										allDayText: 'Giornata',
										firstDay: 1,
										buttonText: {
											today: 'oggi',
											month: 'mese',
											week: 'settimana',
											day: 'giorno',

										},
										defaultView: 'agendaWeek',
										slotDuration: '00:30:00',
										slotLabelInterval: 30,
										slotLabelFormat: 'H:mm',
										editable: false,
										axisFormat: 'HH:mm',
										timeFormat: 'HH:mm',
										theme: true,    
										themeSystem:'bootstrap3', 
										close: 'fa-times',
										prev: 'fa-chevron-left',
										next: 'fa-chevron-right',
										prevYear: 'fa-angle-double-left',
										nextYear: 'fa-angle-double-right', 
										dayClick: function(date, jsEvent, view) {
											// console.log(date.format('H:m'))
											
											

										},  
										events: $('meta[name="route"]').attr('content') + '/student/teacherappointment/'+teacher_id+'/'+lctid,
										selectable:false,
										selectHelper:false, 
										eventClick: function(event) {
											//alert(event.className[0]);
											console.log(event);
											var classname = event.className[0];
											
											var classname2 = event.className[1];
											var id= event.id; 
											//alert(classname);
											
											//$('#myModal').show();
										},
										select: function(startDate, endDate, jsEvent, view, resource) {
											
										}
									  });
									   });
								</script>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div style="margin-top: 30px;text-align: center;">There is not found any teacher</div>
<?php endif; ?>
<!--/.col-sm-6.col-lg-4--> 
<script>
function show(v, id){
	$('.teacherinfo_'+id).hide();
	$('#'+v).show();
	setTimeout(function(){ $('.fc-today-button').click(); }, 500);
}
</script>							
					
					
