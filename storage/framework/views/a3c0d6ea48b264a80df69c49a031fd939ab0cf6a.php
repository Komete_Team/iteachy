<?php $__env->startSection('title', 'Teacher Subscription'); ?>

<?php $__env->startSection('content'); ?>
<?php
$paypalDetail = array();
$BusinessEmail= 'testve@yopmail.com';
$paypalUrl=  'https://www.sandbox.paypal.com/cgi-bin/webscr';
$user = Auth::user();
$user_id = $user['id'];
//echo '<pre>'; print_r($user['id']); die;
?>
	<style>
		.comparison .active{background-color: #dff0d8;}
	</style>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small>Control panel</small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active"><?php echo e(__('subscription.Teacher Subscription')); ?></li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
	        <center><h1><?php echo e(__('subscription.Subscription')); ?></h1>
	        </center>
	        <div class="comparison">
	        	<table>
	        		<thead>
				      <tr>
				        <th colspan="4" class="qbo">
				          <?php echo e(__('subscription.Choose the most suitable plan for you!')); ?>

				          <?php echo e(__('subscription.You can change at any time.')); ?>

				        </th>
				      </tr>
				      <tr>
				        <th class="compare-heading"><?php echo e(__('subscription.time_period')); ?></th>
				        <th class="compare-heading">
				          Base
				        </th>
				        <th class="compare-heading">
				          Standard
				        </th>
				        <th class="compare-heading">
				          Premium
				        </th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				      	<tr>
				        	<td></td>
				        	<td colspan="4">&nbsp;</td>
				      	</tr>
				      	<tr class="compare-row">
				        	<td><?php echo e($subs->title); ?></td>
				        	<td price="<?php echo e($subs->base); ?>" class="plan"><?php echo e('EUR '.$subs->base); ?></td>
				        	<td price="<?php echo e($subs->standard); ?>" class="plan"><?php echo e('EUR '.$subs->standard); ?></td>
				        	<td price="<?php echo e($subs->premium); ?>" class="plan"><?php echo e('EUR '.$subs->premium); ?></td>
				      	</tr>
				      	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				  	</tbody>
	        	</table>
				<div class="center form-group" style="padding: 10px;">
				<form role="form" method="post" action="<?php echo e($paypalUrl); ?>"  id="demo-form" data-parsley-validate>
					<?php echo e(csrf_field()); ?> 
					<input type="hidden" name="business" value="<?php echo e($BusinessEmail); ?>">  
					<input type="hidden" name="cmd" value="_xclick"> 
					<input type="hidden" name="item_name" value="Subscription">
					<input type="hidden" name="item_number" value="<?php echo e($user_id); ?>">

					<input type="hidden" id="sub_amount" name="amount" value="">
					<input type="hidden" name="currency_code" value="EUR">    
					<input type="hidden" name="custom" id="custom" value="0">                                        
					<input type='hidden' name='cancel_return' value='<?php echo e(url("teacher/paymentcancel")); ?>'>
					<input type='hidden' name='return' value='<?php echo e(url("teacher/paymentsuccess")); ?>'> 
					<!-- <input type='hidden' name='notify_url' value='<?php echo e(url('paypal/ipnstatus')); ?>'> -->
					<input type='hidden' name='notify_url' value='https://www.komete.it'>
					<button id="subscription_pay" class="btn btn-primary disabled"><?php echo e(__('translation.pay')); ?></button>
				</form>
				</div>
	        </div>
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>