<?php $__env->startSection('title', 'Subscriptions'); ?>

<?php $__env->startSection('content'); ?>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Subscriptions</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	    	<div>
	    		<a href="<?php echo e(url('admin/subscriptions/add')); ?>" class="btn btn-primary">Aggiungi</a>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
	        			<?php if(Session::has('success')): ?>
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('success')); ?></strong>
							</div>
						<?php endif; ?>
						<?php if(Session::has('error')): ?>
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button> 
								<strong><?php echo e(Session::get('error')); ?></strong>
							</div>
						<?php endif; ?>
			        	<table class="table table-striped" id="datatable_category">
			        		<thead>
			        			<tr>
			        				<th>#</th>
			        				<th>Titolo</th>
									<th>Basic</th>
			        				<th>Standard</th>
									<th>Premium</th>
			        				<th>Modifica</th>
			        			</tr>
			        		</thead>
							<tbody>      
								<?php $i=1;?>
								<?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								   
								<tr class="odd gradeX">
									<td><?php echo e($i); ?></td>
									<td><?php echo e(!empty($sub->title)?$sub->title:'NA'); ?></td>
									<td><?php echo e(!empty($sub->base)?$sub->base:'NA'); ?></td>
									<td><?php echo e(!empty($sub->standard)?$sub->standard:'NA'); ?></td>
									<td><?php echo e(!empty($sub->premium)?$sub->premium:'NA'); ?></td>
									<td><a href="<?php echo e(url('admin/subscriptions/edit/'.$sub->id)); ?>" class="edit_sub"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="<?php echo e($sub->id); ?>" class="delete_sub"><i class="fa fa-trash-o"></i></a></td>  
								  
								</tr>
								<?php 
									$i++;
								?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
			        	</table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click','.delete_sub',function(){
				var subid=$(this).attr('id');
				var r = confirm("Sei sicuro. Vuoi cancellare?");
				if (r == true) {
				  	$.ajax({
				        type: "GET",
				        url: "<?php echo e(url('admin/deletesubscription')); ?>"+'/'+subid,
				        success: function(response) {
				            if (response == 'success') {
				            	location.reload();
				            }
				        }
				    });
				} 
			});
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>