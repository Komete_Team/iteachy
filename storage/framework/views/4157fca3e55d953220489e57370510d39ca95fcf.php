<?php $__env->startSection('title', 'Student Payment Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Delete Profile</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 <?php if(Session::has('success')): ?>
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>
		<section class="content">
			<div>
				<form id="delete-profile" method="post">
					<div class="row">
						<div class="col-lg-6">
							<?php echo e(__('translation.Password')); ?>

						</div>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="password" name="password">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<?php echo e(__('translation.Confirm Password')); ?>

						</div>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="confirm-password" name="confirm_password">
						</div>
					</div>
					
					<?php echo csrf_field(); ?>	
					<button type="submit" class="btn btn-primary" id="btn_save_card-info">Submit
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$('#delete-profile').validate({
			rules: {
				'password': "required",
				'confirm_password': {
					required: true,
      				equalTo: "#password"
    			}
			},
			messages: {
				'password': "Per favore, inserisci la password.",
				'confirm_password': {
					required: "Inserisci di nuovo la password.",
      				equalTo: "Password e conferma password devono essere uguali."
    			}
			}
		});
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('student.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>