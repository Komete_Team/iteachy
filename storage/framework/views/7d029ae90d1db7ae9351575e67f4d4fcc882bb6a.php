<?php $__env->startSection('title', 'Admin Bacheca'); ?>

<?php $__env->startSection('content'); ?>

	<div class="container">
	
		<?php //echo '<pre>'; print_r($teacherBooked); die; ?>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
			<?php if(count($notifications)>0): ?>
			<h3 style="background:#c3e8f7; padding:10px;text-align:center">Le mie notifiche</h3>
			
				<?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php 
						 $i = 0;
						/*$lecturetime = '';
						$to_time = strtotime($val->end_date);
						$from_time = strtotime($val->start_date);
						$lecturetime = round(abs($to_time - $from_time) / 60,2). " minute";*/
						$imgUrl = url('/images/avatar5.png'); 
									
						if( isset( $val->profile_image ))
						{
							
								$imgUrl = url('/images/teacher_logos/' . $val->profile_image);
							
						}
						
						?>
					
						
				<div class="row" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">	
					<div class="col-sm-1">
					  <p><img style="max-width: 150px;width: 60px;" src="<?php echo e($imgUrl); ?>" /></p>
					  
					</div>
					<div class="col-sm-3">
					  <h4><?php echo e($val->first_name.' '.$val->last_name); ?></h4>
					  <h5><?php echo e($val->email); ?></h5>
					</div>
					<div class="col-sm-5">
					  <p><?php echo e($val->description); ?></p>
					</div>
					
					
					<div class="col-sm-2">
					<?php if($val->type == 2): ?>
					  <p><a href="<?php echo e(url('/student/questionnaire/'.$val->user_id)); ?>" class="btn btn-primary center-block">Quesnnair</a></p>
				    <?php endif; ?>
					<?php if($val->type == 2): ?>
						<p><a href="<?php echo e(url('/chat/'.$val->user_id)); ?>" class="btn btn-primary center-block">leggi ora</a></p>
					<?php endif; ?>
					<?php if($val->type == 1): ?>
						<p style="display: block;float: left;"><a href="<?php echo e(url('chat/'.$val->lecture_id)); ?>"><i class='fa fa-comments' style='font-size:30px;color:#337ab7;margin-right: 20px;float:left'></i></a></p>
					<?php endif; ?>
					
					  <p style="float: left;"><a href="#" class="btn btn-primary center-block">Compila Ora</a></p>
					  
					  
					</div>
					
				</div>	
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php else: ?>
					<h3 style="padding:10px;text-align:center">Non c'è un nuovo messaggio</h3>
			<?php endif; ?>
			
			
			
		 
	
	</div>	
	<!-- Model to add sucategory -->
	<div id="confirm_model" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					
						
						
						<div class="row bobrow" style="background: #eee;padding: 10px;text-align: center;margin-bottom: 5px;">
							
						</div>
						
					
					<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>