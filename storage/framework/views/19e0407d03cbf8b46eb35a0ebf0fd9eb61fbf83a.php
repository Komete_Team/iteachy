<?php $__env->startSection('title', 'Student Payment Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Become Expert</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 <?php if(Session::has('success')): ?>
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>
		<section class="content">
			<div>
				<form id="become-expert" method="post">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="text"><?php echo e(__('translation.Interest')); ?>:</label>
							</div>
							
						</div>
					</div>
					<div class="row">
						<?php 
						$userInt=[];
						foreach ($userinterest as $int) {
							$userInt[] = $int['category_id'];
						}
						?>
						<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-lg-3">
								<input type="checkbox" <?php echo e((in_array($cat->id,$userInt))?'checked':''); ?> name="category[]" value="<?php echo e($cat->id); ?>"> <?php echo e($cat->category_name); ?>

							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
					<div class="row">
						<label style="display: none;" id="category[]-error" class="error" for="category[]">Per favore, inserisci la password.</label>
					</div>
					
					<?php echo csrf_field(); ?>	
					<button type="submit" class="btn btn-primary" id="btn-become-expert">Submit
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$('#become-expert').validate({
			rules: {
				'category[]': "required"
			},
			messages: {
				'category[]': "Per favore, inserisci la password."
			}
		});
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('student.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>