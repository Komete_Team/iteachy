<?php $__env->startSection('content'); ?>
	<div class="login-form">
        <form autocomplete="off" name="frm_teacher_registration" id="frm_teacher_registration">
            <h2 class="text-center"><?php echo e(__('translation.about_us')); ?></h2>
            <?php echo e(csrf_field()); ?>

           
            <div class="form-group">
            	<label for="first_name">Nome:</label>
                <input type="text" class="form-control" placeholder="" name="name" id="name">
            </div>
			
			
            <div class="form-group">
            	<label for="email_id">Email:</label>
                <input type="text" class="form-control" placeholder="" name="email_id" id="email_id">
            </div>
            
			<div class="form-group">
            	<label for="email_id">Message:</label>
                <textarea class="form-control" placeholder="" name="message" id="message"></textarea>
            </div>

           

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" id="btn_teacher_registration">Invia</button>
            </div>

            
        </form>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>