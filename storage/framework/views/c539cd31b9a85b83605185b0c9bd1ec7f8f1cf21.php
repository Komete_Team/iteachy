<?php $__env->startSection('title', 'Teacher Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Lezioni</li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
			<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_lesson_modal">Aggiungi lezione</button>
	    	</div>
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table user-data-table" id="datatable_category">
                                    <thead>
                                        <tr>
                                            <th>S-No</th>
                                            <th>Nome della lezione</th>
                                            <th>Numero di lezion</th>
											<th>Azioni</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $i=1;?>
                                        <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo e($i); ?></td>
                                             <td><?php echo e($detail->lesson_name ?? ''); ?></td> 
                                             <td><?php echo e($detail->total_lessons ?? ''); ?></td>
											 <td><a href="javascript:void(0);" id="<?php echo e($detail->id); ?>" class="edit_lesson"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="<?php echo e($detail->id); ?>" class="delete_lesson"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <?php 
                                            $i++;
                                        ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
                                </table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add sucategory -->
	<div id="modal_lesson" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					<form name="frm_add_lesson" id="frm_add_lesson" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Nome della lezione:</label>
							<input type="text" class="form-control" name="lesson_name" id="lesson_name" value="">
							<input type="hidden" name="lesson_id" id="lesson_id" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Lezioni totali:</label>
							<input type="text" class="form-control" name="total_lessons" id="total_lessons" value="">
						</div>
						<div class="form-group">
							<label for="categories">Intervallo di lezione (in minuti):</label>
							<input type="number" step="30" class="form-control" name="lesson_time" id="lesson_time" value="">
						</div>
						<div class="form-group">
							<label for="categories"><?php echo e(__('translation.Price per hour')); ?>:</label>
							<input type="text" class="form-control" name="price_per_hour" id="price_per_hour" value="">
						</div>
						
						<div class="form-group">
							<label for="categories"><?php echo e(__('translation.lecture_type')); ?>:</label>
							<select name="lecture_type" id="lecture_type" class="form-control">
								<option value="">Seleziona</option>
								<option value="1">normale </option>
								<option value="0">Prova</option>
							</select>
						</div>
					
						
						
						<button type="button" class="btn btn-primary" id="btn_save_lesson">invia</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>