<?php $__env->startSection('title', 'Teacher Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active"><?php echo e(__('translation.teacher_info')); ?></li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
	    <!-- Main content -->
	    <section class="content">
	        <!-- Main row -->
		
	        <div>

	        	<form name="frm_update_profile" id="frm_update_profile" method="post" autocomplete="off" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Title')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="title" name="title" value="<?php echo e($teacherDetails->title ?? ''); ?>">
								<?php if($errors->has('title')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Description')); ?>:<span class="mandatory_field">*</span></label>
	    						<textarea class="form-control" id="description" name="description"><?php echo e($teacherDetails->description ?? ''); ?></textarea>
								<?php if($errors->has('description')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
						</div>
				
						<?php /* <div class="col-lg-12">
							<div class="form-group {{ $errors->has('price_per_hour') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Price per hour') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="price_per_hour" name="price_per_hour" value="{{ $teacherDetails->price_per_hour ?? '' }}">
								@if ($errors->has('price_per_hour'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_per_hour') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
	    						<label for="text">{{ __('translation.Price') }}:<span class="mandatory_field">*</span></label>
	    						<input type="text" class="form-control" id="price" name="price" value="{{ $teacherDetails->price ?? '' }}">
								@if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
	    					</div>
						</div> */ ?>
						
						<div class="col-lg-12">
							<div class="form-group <?php echo e($errors->has('video') ? ' has-error' : ''); ?>">
	    						<label for="text"><?php echo e(__('translation.Video')); ?>:<span class="mandatory_field">*</span></label>
	    						<input type="file" class="form-control" id="video" name="video" value="<?php echo e($teacherDetails->video ?? ''); ?>">
								<?php if($errors->has('video')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('video')); ?></strong>
                                    </span>
                                <?php endif; ?>
	    					</div>
							<input type="hidden" value="<?php echo e($teacherDetails->file_name ?? ''); ?>" name="old_video" />
							<?php
							    
							    if( isset( $teacherDetails->file_name ) && !is_null( $teacherDetails->file_name ) && ( $teacherDetails->file_name != '' ) )
							    {
							    	
							    		$videoUrl = url('/videos/teacher_videos/' . $teacherDetails->file_name ?? '');
										echo '<iframe src="'.$videoUrl.'"></iframe>';
							    	
							    }
							 ?>
						</div>
					
					
						
					</div>
					

					<button type="submit" class="btn btn-primary" id="btn_save_school"><?php echo e(__('translation.Accredited profile')); ?></button>
				</form>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->

	</aside>
	<!-- /.right-side -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('teacher.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>