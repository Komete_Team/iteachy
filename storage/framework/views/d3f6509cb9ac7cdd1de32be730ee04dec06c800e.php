<?php $__env->startSection('title', 'Admin Bacheca'); ?>

<?php $__env->startSection('content'); ?>
	<div class="container">
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Bacheca</li>
	        </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">

	        <!-- Small boxes (Stat box) -->
	         <div style="padding: 20px;background: #71D5FF;float: left;width: 100%;">
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-aqua" style="float:left; margin-left:10px; width:30%">
	                    <label for="school_logo">
							<?php //echo '<pre>'; print_r($teacherInfo); die; ?>
							    <!-- <img src="<?php echo e(url('/images/school_default_logo.jpg')); ?>" height="100px" width="100px"> -->
							    <?php
							    $imgUrl = url('/images/avatar.png');
							    if( $studentInfo->profile_image != '' )
							    {
							    	
							    		$imgUrl = url('/images/student_profileimage/' . $studentInfo->profile_image);
							    	
							    }
							    ?>
							    <img src="<?php echo e($imgUrl); ?>" height="100px" width="100px">
						</label>
	                </div>
					<div>
						<h4><?php echo e($studentInfo->first_name.''.$studentInfo->last_name); ?></h4>
						<p><?php echo e($studentInfo->email); ?></p>
						<a href="<?php echo e(url('student/profile')); ?>">Modify Profile</a> |
						<a href="<?php echo e(url('student/changepassword')); ?>">Change Password</a>
					</div>
	            </div>
				<div class="col-lg-2 col-xs-6" style="background: #fff;padding: 20px;border: 1px solid #ccc;margin-right: 10px;">
	                <div class="small-box bg-green">
	                    <div class="inner text-center">
	                        <h3>
							<?php echo e($total_booked_appt); ?>

	                        </h3>
	                        <p>
	                            Prossime lezioni
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-stats-bars"></i>
	                    </div>
	                   
	                </div>
	            </div>
	            <div class="col-lg-2 col-xs-6" style="background: #fff;padding: 20px;border: 1px solid #ccc;margin-right: 10px;">
	                <div class="small-box bg-green">
	                    <div class="inner text-center">
	                        <h3>
	                            0
	                        </h3>
	                        <p>
	                            Azione richiesta
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-stats-bars"></i>
	                    </div>
	                    
	                </div>
	            </div>
	            <div class="col-lg-2 col-xs-6" style="background: #fff;padding: 20px;border: 1px solid #ccc;margin-right: 10px;">
	                <div class="small-box bg-yellow">
	                    <div class="inner text-center">
	                        <h3>
	                            0
	                        </h3>
	                        <p>
	                            Pacchetti attivi
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-person-add"></i>
	                    </div>
	                    
	                </div>
	            </div>
	            <div class="col-lg-2 col-xs-6" style="background: #fff;padding: 20px;border: 1px solid #ccc;margin-right: 10px;">
	                <div class="small-box bg-red">
	                    <div class="inner text-center">
	                        <h3>
	                            0
	                        </h3>
	                        <p>
	                            euro
	                        </p>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-pie-graph"></i>
	                    </div>
	                   
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-xs-12 connectedSortable member_grid">
	                <div class="full-width padd-global padd-global-bt">
						<div class="members">
							<div class="button-box">
									<a href="javascript:void(0)"><span>Lorem ipsum dolor sit</span> <span class="arrow"><i class="fa fa-angle-right"></i></span></a>
								</div>
							<div class="member-person">
								<div class="owl-carousel owl-theme">
								<?php //echo '<pre>'; print_r($teachers); die; ?>
									<?php  $totalrates = 0;   ?>
						<?php if(count($teachers) > 0): ?>
							<?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teachers_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php
								$imgUrl = url('/images/avatar5.png');
								
								if( isset( $teachers_val->profile_image ))
								{
									
										$imgUrl = url('/images/teacher_logos/' . $teachers_val->profile_image);
									
								}
								$ratings = array_sum(explode('$', $teachers_val->ratings));
								$totalusers = count(explode('$', $teachers_val->users));
								$totalrates = ceil($ratings/$totalusers);
								
								
							?>
								<div class="">
									<div class="member-card">
										<div class="membar-review">
											<div class="member-pix"> <img style="min-height: 105px;" src="<?php echo e($imgUrl); ?>" alt=""> </div>
											<div class="text" style="width:100%">
												<ul class="review">
													<?php for($i=1; $i<=5; $i++): ?>
													<?php 
													 if($i <= $totalrates)
														$cl = 'color';
													 else
														$cl = ' ';
													 ?>
													<li style="padding-right:5px;float:left"><a href="javascript:void(0)" class="<?php echo e($cl); ?>"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													<?php endfor; ?>
												</ul>
												<h3><?php echo e($teachers_val->first_name ?? ''); ?></h3>
												<h4><?php echo e($teachers_val->address ?? ''); ?></h4>
											</div>
										</div>
										<div class="detail-bx">
											<div class="check_list">
												<ul>
													<li><img src="<?php echo e(url('front/images/right-tick.png')); ?>"></li>
													<li><img src="<?php echo e(url('front/images/home-tick.png')); ?>"></li>
													<li><img src="<?php echo e(url('front/images/cross-tick.png')); ?>"></li>
												</ul>
											</div>
											<div class="text-name"> <small>insegna</small>
												<h3><?php echo e($teachers_val->category_name ?? ''); ?></h3>
											</div>
											<div class="text-name"> <small>Tariffa oraria da</small>
												<h3>EUR <?php echo e($teachers_val->price_per_hour ?? ''); ?></h3>
											</div>
											<div class="view-more"><a href="<?php echo e(url('teacher_detail/'.$teachers_val->id)); ?>">Prenota lezione <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
										</div>
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
								</div>
								
								<div class="full-width" style="padding: 30px;">
									<div class="banner-section">
										<form name="frm_search_teacher" id="frm_search_teacher" method="get" action="<?php echo e(url('teachers')); ?>" autocomplete="off" enctype="multipart/form-data">
												<?php echo e(csrf_field()); ?>	
											<div class="search-box" style="position: unset;">
												<div class="heading">Cerca un insegnanto</div>
												<div class="input-field"> 
													<?php if(count($categories) > 0): ?>
														<select name="categories" id="category"  class="form-control" required>
															<option value="">Please select at least one category</option>
															<optgroup label="All Categories">
																<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->category_name); ?></option>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	 
															</optgroup>
															<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php 
																	$l=1;
																	$totalsubcats = count($subcategories);
																	//echo '<pre>'; print_r($totalsubcats); die;
																?>
																<?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<?php if($cat->id == $subcat->parent_id): ?>
																		<?php if($l == 1): ?>
																			<optgroup label="<?php echo e($cat->category_name); ?>">	
																		<?php endif; ?>
																			<option value="<?php echo e($cat->id); ?>-<?php echo e($subcat->id); ?>"><?php echo e($subcat->category_name); ?></option>
																		<?php if($l == $totalsubcats): ?>
																			</optgroup>
																		<?php endif; ?>
																	<?php $l++; ?>	
																	<?php endif; ?>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</select>
													<?php endif; ?>
												</div>
												<div class="search"> <button type="submit"> <i class="fa fa-search"></i> </button> </div>
											</div>
										</form>	
									</div>
								</div>
							
							</div>
						</div>
					</div>
	            </div>
	        </div>

	        <!-- Main row -->
	        <div class="row">
	        	<div class="col-lg-12">
					
		        </div>
	       	</div>
	        <!-- /.row (main row) -->

	    </section><!-- /.content -->
	</aside>
	<!-- /.right-side -->
	</div>	
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>