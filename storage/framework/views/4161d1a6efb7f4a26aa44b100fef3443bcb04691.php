<?php $__env->startSection('title', 'Teacher Registration'); ?>
<?php $__env->startSection('content'); ?>
	<div class="login-form">
        <form autocomplete="off" name="frm_teacher_registration" id="frm_teacher_registration">
            <h2 class="text-center">Registrazione insegnanti</h2>
            <?php echo e(csrf_field()); ?>

           
            <div class="form-group">
            	<label for="first_name"><?php echo e(__('translation.First Name')); ?>:</label>
                <input type="text" class="form-control" placeholder="" name="first_name" id="first_name">
            </div>
			
			<div class="form-group">
            	<label for="last_name"><?php echo e(__('translation.Last Name')); ?>:</label>
                <input type="text" class="form-control" placeholder="" name="last_name" id="last_name">
            </div>
			
            <div class="form-group">
            	<label for="email_id"><?php echo e(__('translation.Email Id')); ?>:</label>
                <input type="text" class="form-control" placeholder="" name="email_id" id="email_id">
            </div>
            <div class="form-group">
            	<label for="password"><?php echo e(__('translation.Password')); ?>:</label>
                <input type="password" class="form-control" placeholder="" name="password" id="password">
            </div>

            <div class="form-group">
            	<label for="email_id"><?php echo e(__('translation.Category')); ?>:</label>
				<?php if(count($categories) > 0): ?>
					<select name="categories[]" id="categories"  class="form-control" multiple="multiple">
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						 <optgroup label="<?php echo e($cat->category_name); ?>">
						<?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($cat->id == $subcat->parent_id): ?>
								<option value="<?php echo e($cat->id); ?>-<?php echo e($subcat->id); ?>"><?php echo e($subcat->category_name); ?></option>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</optgroup>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				<?php endif; ?>
            </div>
			
			<div class="form-group">
            	<label for="online"><?php echo e(__('translation.online')); ?>:</label>
                <input style="vertical-align: middle;width:auto; display:inline-block;margin-bottom: 5px;" type="radio" class="form-control" placeholder="" name="online" id="online" value="1">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" id="btn_teacher_registration">Registrazione</button>
            </div>

            <!-- Loading button -->
            <div class="spinner-border" id="loading_spinner" style="display: none;"></div>

            <!-- server response -->
          	<div class="alert alert-dismissible text-center" id="server_resposne" style="display: none;">
            	<button type="button" class="close"></button>
            	<span id="server_resposne_msg"></span>
          	</div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>