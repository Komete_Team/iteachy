<?php $__env->startSection('title', 'Teacher Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	        <h1>
	            Dashboard
	            <small><?php echo e(__('translation.control_panel')); ?></small>
	        </h1>
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('teacher')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Lezioni</li>
	        </ol>
	    </section>
		<section>
			 <?php if(Session::has('success')): ?>
			   <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>

				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>	
		
		 <!-- Main content -->
	    <section class="content">
			<!--<div>
	    		<button type="button" class="btn btn-primary" id="btn_show_lesson_modal">Aggiungi lezione</button>
	    	</div>-->
	        <!-- Main row -->
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-lg-12">
	        		<div class="table-responsive">
			        	<table class="table user-data-table" id="datatable_category">
                                    <thead>
                                        <tr>
                                            <th>S-No</th>
                                            <th>Name</th>
                                            <th>Email</th>
											<th>Appointment Start Date</th>
                                            <th>Appointment End Date</th>
											<th>Skype Id</th>
											<th>Meeting Place</th>
                                            <th>Knowledge Level</th>
											<th>Message</th>
											<th>Azioni</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php $i=1; //echo '<pre>'; print_r($teacherappointments); die;?>
                                        <?php $__currentLoopData = $teacherappointments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo e($i); ?></td>
                                             <td><?php echo e($detail->first_name.' '.$detail->last_name); ?></td> 
                                             <td><?php echo e($detail->email ?? ''); ?></td>
											 <td><?php echo e(date('m-d-Y H:i',strtotime($detail->start_date))); ?></td>
											 <td><?php echo e(date('m-d-Y H:i',strtotime($detail->end_date))); ?></td>
											 <td><?php echo e($detail->skype_id ?? 'N/A'); ?></td>
											 <td><?php echo e($detail->meeting_place ?? 'N/A'); ?></td>
											 <td><?php echo e($detail->knowledge_level ?? 'N/A'); ?></td>
											 <td><?php echo e($detail->message ?? ''); ?></td>
											 <td><a href="javascript:void(0);" id="<?php echo e($detail->aptid); ?>" class="edit_appointment"><i class="fa fa-pencil-square-o"></i> | <a href="javascript:void(0);" id="<?php echo e($detail->aptid); ?>" class="delete_appointment"><i class="fa fa-trash-o"></i></a> | <a href="<?php echo e(url('/admin/getquesans')); ?>" id="<?php echo e($detail->aptid); ?>" title="list Feedbacks" class="show_appointment_feedback"><i class="fa fa-list-alt"></i></a></td>
                                        </tr>
                                        <?php 
                                            $i++;
                                        ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                      
                                    </tbody>
                                </table>
		        	</div>
		        </div>
	       	</div>
	        <!-- /.row (main row) -->
	    </section><!-- /.content -->
	    <!-- Main content -->
		 <!-- Main row -->
	</aside>
	<!-- /.right-side -->
	
	<!-- Model to add sucategory -->
	<div id="modal_appointment" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Aggiungi lezione</h4>
				</div>
				<div class="modal-body">
					<form name="frm_edit_appointment" id="frm_edit_appointment" autocomplete="off">
						
						
						<div class="form-group">
							<label for="heading">Appointment Start Date:</label>
							<input type="text" class="form-control" name="appointment_start_date" id="appointment_start_date" value="">
							<input type="hidden" name="appointment_id" id="appointment_id" value="">
						</div>
						
						<div class="form-group">
							<label for="heading">Appointment End Date:</label>
							<input type="text" class="form-control" name="appointment_end_date" id="appointment_end_date" value="">
						</div>
						
						<div class="form-group">
							<label for="categories">Skype ID:</label>
							<input type="text" class="form-control" name="skype_id" id="skype_id" value="">
						</div>
						<div class="form-group">
							<label for="categories">Meeting Place:</label>
							<input type="text" class="form-control" name="meeting_place" id="meeting_place" value="">
						</div>
						<div class="form-group">
							<label for="categories">Knowledge Level:</label>
							<input type="text" class="form-control" name="knowledge_level" id="knowledge_level" value="">
						</div>
						<div class="form-group">
							<label for="categories">Message:</label>
							<textarea class="form-control" name="message" id="message"></textarea>
						</div>
						
						
						<button type="button" class="btn btn-primary" id="btn_save_appointment">Sottoscrivi</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Vicina</button>
				</div> -->
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>