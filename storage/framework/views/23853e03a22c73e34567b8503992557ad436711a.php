<?php $__env->startSection('title', 'Student Payment Info'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class=" container">
	    <section class="content-header">
	        <ol class="breadcrumb">
	            <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	            <li class="active">Payment Info</li>
	        </ol>
	    </section>
		<section>
			<label id="message-text"></label>
			 <?php if(Session::has('success')): ?>
			    <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('success')); ?></strong>
				</div>
			<?php elseif(Session::has('danger')): ?>
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
						<strong><?php echo e(Session::get('danger')); ?></strong>
				</div>
			<?php endif; ?> 	
		</section>
		<section class="content">
			<div>
				<form id="payment-info" method="post">
					<div class="row">
						<div class="col-lg-6">
							<?php echo e(__('translation.Type of Payment')); ?>

						</div>
						<div class="col-lg-6">
							<div><input type="radio" <?php echo e(($payinfo->payment_type ==1)?'checked':''); ?> name="type_of_payment" value="1"/> <?php echo e(__('translation.Paypal')); ?></div>
							<div><input <?php echo e(($payinfo->payment_type ==2)?'checked':''); ?> type="radio" value="2" name="type_of_payment"/> <?php echo e(__('translation.Credit Card')); ?></div>
						</div>
					</div>
					<div id="card-section" style="<?php echo e(($payinfo->payment_type ==2)?'':'display: none;'); ?>">
						<?php 
						$cardinfo=[];
						if(!empty($payinfo->pay_info)) {
							$cardinfo = json_decode($payinfo->pay_info);
						}
						?>
						<div class="row">
							<div class="col-lg-6">
								<?php echo e(__('translation.Name On Card')); ?>

							</div>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="name-on-card" name="card[name_on_card]" value="<?php echo e(isset($cardinfo->name_on_card)?$cardinfo->name_on_card:''); ?>">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<?php echo e(__('translation.Card Number')); ?>

							</div>
							<div class="col-lg-6">
								<input type="number" class="form-control" id="card-number" name="card[card_number]" value="<?php echo e(isset($cardinfo->card_number)?$cardinfo->card_number:''); ?>">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<?php echo e(__('translation.Expiry Month - Year')); ?>

							</div>
							<div class="col-lg-6">
								<select name="card[expiryMonth]" id="expiryMonth"
								class="form-control">
								<?php
								for ($i = 1; $i <= 12; $i ++) {
									$monthValue = $i;
									if (strlen($i) < 2){
										$monthValue = "0" . $monthValue;
									}
									?>
								<option <?php echo e(isset($cardinfo->expiryMonth)?($cardinfo->expiryMonth == $monthValue?'selected':''):''); ?> value="<?php echo $monthValue; ?>"><?php echo $monthValue; ?></option>
								<?php
								}
								?>
								</select> 
								<select name="card[expiryYear]" id="expiryYear" class="form-control">
								<?php
								for ($i = date("Y"); $i <= date("Y")+10; $i ++) {
									$yearValue = $i;
									?>
									<option <?php echo e(isset($cardinfo->expiryYear)?($cardinfo->expiryYear == $yearValue?'selected':''):''); ?> value="<?php echo $yearValue; ?>"><?php echo $i; ?></option>
								<?php
								}
								?>
								</select>
					            
							</div>
						</div>
					</div>
					<?php echo csrf_field(); ?>	
					<button type="submit" class="btn btn-primary" id="btn_save_card-info">invia
				</form>
			</div>
		</section>	
	</aside>
<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='type_of_payment']").click(function(){
			if($(this).val() == 2) {
				$('#card-section').show();
			} else {
				$('#card-section').hide();
			}
		});
		
		$('#payment-info').validate({
			rules: {
				'card[name_on_card]': "required",
				'card[card_number]': "required"
			},
			messages: {
				'card[name_on_card]': "Inserisci il nome sulla carta.",
				'card[card_number]': "Inserisci il numero della carta."
			}
		});
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('student.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>