<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
	Route::get('locale/{locale}', function ($locale){
		Session::put('locale', $locale);
		return redirect()->back();
	});

	Route::match(['get','post'],'/teacher/varify/{token}', 'TeacherController@index');
	Route::match(['get','post'],'/getteachers','SiteController@searchTeacher');
	Route::get('/','SiteController@index');
	Route::match(['get','post'],'/get_teacher_category','SiteController@getTeacherCategory');
	Route::match(['get','post'],'/teachers','SiteController@searchTeacher');
	Route::get('/login', 'TeacherController@index');
	Route::get('/register', 'TeacherController@register');
	Route::any('user-resetpassword/{token}','SiteController@ResetPassword');
	
	Route::post('/login', 'LoginController@login');
	// To save the teacher review
	Route::post('/saveteacherreview', 'TeacherController@saveTeacherReview');
	
	// To save the teacher detail
	Route::post('/registerteacher', 'TeacherController@registerTeacher');
	Route::post('/registeruser', 'SiteController@registerUser');
	Route::match(['get','post'],'/user-forgotpassword', 'SiteController@userForgotpassword');
	
	// To get the teacher sub category
	Route::match(['get','post'],'/get_teacher_subcategory', 'SiteController@getTeacherSubcategory');
	
	// To get the teacher sub category
	Route::match(['get','post'],'/get_teacher', 'SiteController@getTeacherByFilter');
	Route::match(['get','post'],'/teacher_detail/{id}', 'SiteController@teacherDetail');
	Route::match(['get','post'],'/about-us', 'SiteController@aboutUs');
	Route::match(['get','post'],'/contact-us', 'SiteController@contactUs');
	
	
	
	
	
	

	// site login page
	/* Route::match(['get','post'],'/login', 'LoginController@login');
	Route::match(['get','post'],'/login', 'SiteController@login');
	// site registration page
	Route::match(['get','post'],'/register', 'SiteController@register');
	Route::match(['get','post'],'/registerteacher', 'SiteController@registerTeacher'); */
	
	/*
 ----------------
| School Routes |
----------------
*/



// To be accessed without logged-in
Route::group(['prefix' => 'teacher', 'middleware' => 'auth'], function() {

	// School login page
	Route::get('/', 'TeacherController@index');
	Route::get('/testemail', 'TeacherController@testemail');
	//Route::get('/varify/{token}', 'TeacherController@index');
	// login page
	//Route::post('/login', 'LoginController@login');

	// registration page
	//Route::get('/register', 'TeacherController@register');
	
	// Teacher profile page
	Route::match(['get','post'],'/profile', 'TeacherController@profile');
	Route::match(['get','post'],'/teacher_info', 'TeacherController@teacherInfo');
	Route::match(['get','post'],'/teacher_availability', 'TeacherController@teacherAvailability');
	Route::match(['get','post'],'/teacher_leave', 'TeacherController@teacherLeave');
	Route::get('/getteacherleavedetail', 'TeacherController@getTeacherLeaveDetail');
	Route::post('/saveteacherleavedetails', 'TeacherController@saveTeacherLeaveDetail');
	Route::post('/cancelleave', 'TeacherController@cancelLeave');
	Route::match(['get','post'],'/teacher_accredited_profile', 'TeacherController@teacherAccreditedProfile');
	Route::match(['get','post'],'/upload_docs/{id}', 'TeacherController@uploadDocs');
	Route::match(['get','post'],'/addteacherdoc', 'TeacherController@teacherAddDoc');
	
	
	// To delete teacher document
	Route::post('/deleteteacherdoc', 'TeacherController@deleteDoc');
	// To delete teacher appointment
	Route::post('/deleteappointment', 'TeacherController@deleteAppointment');
	
	Route::get('/notifications', 'TeacherController@notifications');
	Route::get('/all_notifications', 'TeacherController@AllNotification');
	
	Route::get('notification/{id?}','TeacherController@Notification');
	// Teacher user dashboard page
	Route::get('/dashboard', 'TeacherController@dashboard');
	
	// teacher logout
	Route::get('/logout', 'TeacherController@logout');
	Route::get('/getnotdetails', 'TeacherController@getNotDetails');
	
	Route::get('/appointments', 'TeacherController@getAppointments');
	
	Route::get('/getappointment', 'TeacherController@getAppointment');
	
	
	Route::get('/lessons', 'TeacherController@teacherLesson');
	// teacher appointment
	Route::get('/teacherappointment', 'TeacherController@teacherAppointment');
	// save teacher lesson
	Route::post('/savelessons', 'TeacherController@saveLessons');
	
	Route::post('/saveteacherappointment', 'TeacherController@saveTeacherAppointment');
	
	// edit teacher lesson
	Route::get('/getlessondetails', 'TeacherController@getLessonDetails');
	// To delete teacher lesson
	Route::post('/deletelesson', 'TeacherController@deleteLesson');
	// Teacher Settings
	Route::match(['get','post'],'/settings', 'TeacherController@settings');
	Route::match(['get','post'],'/subscription', 'SubscriptionController@subscription');
	Route::match(['get','post'],'/paymentsuccess', 'SubscriptionController@paymentsuccess');
	Route::match(['get','post'],'/paymentcancel', 'SubscriptionController@paymentcancel');
	
	
	

});

// To be accessed without logged-in
Route::group(['prefix' => 'student'], function() {
	Route::get('/teacherappointment/{id}/{lid}', 'StudentController@teacherAppointment');
});

Route::group(['prefix' => 'student', 'middleware' => 'auth'], function() {
	// School login page
	Route::get('/', 'StudentController@index');
	//Route::get('/profile', 'StudentController@profile');
	Route::get('/book_lecture/{id}', 'StudentController@bookLecture');
	
	
	Route::post('/savetimeslot', 'StudentController@saveTimeSlot');
	Route::post('/deletetimeslot', 'StudentController@deleteTimeSlot');
	Route::post('/saveappointment', 'StudentController@saveAppointment');
	

	// School login page
	Route::post('/login', 'LoginController@login');
   
   	// student logout
	Route::get('/logout', 'StudentController@logout');
	// School registration page
	Route::get('/register', 'StudentController@register');

	// To save the school detail
	Route::post('/register', 'StudentController@registerStudent');
	Route::post('/deleteappointment/{id}', 'StudentController@deleteAppointment');
	Route::match(['get','post'],'/getappointment', 'StudentController@getAppointment');
	
	// ALI user dashboard page
	Route::get('/dashboard', 'StudentController@dashboard');
	
	
	
	Route::get('/booking_listing', 'StudentController@booking_listing');
	Route::match(['get','post'],'/changepassword', 'StudentController@changePassword');
	Route::match(['get','post'],'/profile', 'StudentController@profile');
	Route::match(['get','post'],'/paymentinfo', 'StudentController@paymentinfo');
	Route::match(['get','post'],'/deleteprofile', 'StudentController@deleteprofile');
	Route::match(['get','post'],'/becomeexpert', 'StudentController@becomeexpert');
	Route::match(['get','post'],'/questionnaire/{id}', 'QuestionnaireController@questionnaire');
	Route::match(['get','post'],'/notifications', 'StudentController@Notification');
	Route::match(['get','post'],'/lesson_finish', 'StudentController@lesson_finish');
	
	
	

});

Route::group(['prefix' => 'chat'], function() {
	Route::get('/{id}', 'ChatController@index');
});
Route::get('/savedata', 'ChatController@savedata');

Route::group(['prefix'=>'paypal'],function(){

Route::match(['get','post'],'cancel','StudentController@CancelStatus');	

Route::match(['get','post'],'success/{tx}','StudentController@SuccessStatus');

Route::match(['get','post'],'ipnstatus','StudentController@Ipnstatus');

Route::match(['get','post'],'getresponse','StudentController@GetPaymentResponse');


Route::match(['get','post'],'getdigitalcard','StudentController@GetDigitCardPaymentResponce');

});	
	
/*
 ----------------
| School Routes |
----------------
*/

// To be accessed without logged-in
Route::group(['prefix' => 'school'], function() {

	// School login page
	Route::get('/', 'SchoolController@index');

	// School login page
	Route::post('/login', 'LoginController@login');

	// School registration page
	Route::get('/register', 'SchoolController@register');

	// To save the school detail
	Route::post('/register', 'SchoolController@registerSchool');

	// ALI user dashboard page
	Route::get('/dashboard', 'SchoolController@dashboard');

});

/*
 ---------------
| Admin Routes |
---------------
*/

// To be accessed without logged-in
Route::group(['prefix' => 'admin'], function() {

	// Admin login page
	Route::get('/', 'LoginController@index');

	// Admin login
	Route::post('/login', 'LoginController@login');

});

// To be accessed after logged-in
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

	// Admin dashboard page
	Route::get('/dashboard', 'AdminController@dashboard');
	
	// Admin teachers page
	Route::get('/teachers', 'AdminController@teachers');
	
	Route::get('/categories', 'AdminController@categories');
	
	Route::get('/subcategories/{id}', 'AdminController@subCategories');
	
	Route::get('/viewteachers/{id}', 'AdminController@viewTeacher');
	
	Route::post('/set-teacher', 'AdminController@setTeacherStatus');

	// Admin logout
	Route::get('/logout', 'AdminController@logout');

	// To return user role page
	Route::get('/roles', 'AdminController@roles');

	// To fetch the roles listing
	Route::get('/fetchroles', 'AdminController@fetchRoles');

	// To save role details
	Route::post('/saveroledetails', 'AdminController@saveRoleDetails');

	// To fetch the selected role details
	Route::get('/getroledetails', 'AdminController@getRoleDetails');

	// To delete role
	Route::post('/deleterole', 'AdminController@deleteRole');

	// To return school listing page
	Route::get('/schools', 'AdminController@schools');

	// To update the school verification status
	Route::post('/verifyschool', 'AdminController@verifySchool');

	// To return add/edit school page
	Route::get('/school/{id?}', 'AdminController@addSchool');

	// To save school details
	Route::post('/saveschooldetails', 'AdminController@saveSchoolDetails');

	// To fetch the schools listing
	Route::get('/fetchschools', 'AdminController@fetchSchools');

	// To return calender events page
	Route::get('/calendarevents', 'AdminController@calendarEvents');

	// To save calender event details
	Route::post('/saveeventdetails', 'AdminController@saveEventDetails');

	// To return event details
	Route::get('/geteventdetails', 'AdminController@getEventDetails');

	// To return notification page
	Route::get('/notifications', 'AdminController@notifications');
	
	Route::get('notification/{id?}','AdministratorController@Notification');
	
	// To save Teacher details
	Route::post('/saveteacherdetails', 'AdminController@saveTeacherDetails');
	
	Route::post('/savecategorydetails', 'AdminController@saveCategoryDetails');
	
	Route::post('/savesubcategorydetails', 'AdminController@saveSubCategoryDetails');
	
	
	
	// To save notification details
	Route::post('/savenotificationdetails', 'AdminController@saveNotificationDetails');

	// To fetch the notification listing
	Route::get('/fetchnotifications', 'AdminController@fetchNotifications');

	// To get notification details
	Route::get('/getnotificationdetails', 'AdminController@getNotificationDetails');
	
	// To get teacher details
	Route::get('/getteacherdetails', 'AdminController@getTeacherDetails');
	
	Route::get('/getcategorydetails', 'AdminController@getCategoryDetails');
	
	Route::get('/getsubcategorydetails', 'AdminController@getSubCategoryDetails');
	
	Route::post('/deletesubcategory', 'AdminController@deleteSubCategory');
	
	Route::post('/deletecategory', 'AdminController@deleteCategory');
	
	// To delete notification
	Route::post('/deletenotification', 'AdminController@deleteNotification');
	Route::get('/questionnaires', 'QuestionnaireController@index');
	Route::get('/getquesans', 'QuestionnaireController@getAnswers');
	
	Route::match(['get','post'],'/questionnaires/add', 'QuestionnaireController@add');
	Route::match(['get','post'],'/questionnaires/edit/{id}', 'QuestionnaireController@edit');
	Route::get('/deletequestionnaire/{id}', 'QuestionnaireController@delete');
	Route::get('/subscriptions', 'SubscriptionController@index');
	Route::match(['get','post'],'/subscriptions/add', 'SubscriptionController@add');
	Route::match(['get','post'],'/subscriptions/edit/{id}', 'SubscriptionController@edit');
	Route::get('/deletesubscription/{id}', 'SubscriptionController@delete');
	Route::match(['get','post'],'/questionnaire', 'AdminController@questionnaires');
	Route::get('/appointments', 'AdminController@getAppointments');

});

	// To fetch the images from storage and return it
	Route::get('/images/{path}/{filename}', function ($path, $filename)
	{
		// Replace all '-' dash to '/' slash and make the complete path
		$path = str_replace('-', '/', $path);
		
		$filePath = storage_path() . '/' . $path. '/' . $filename;

		if(!File::exists($filePath)) abort(404);

		$file = File::get($filePath);
		$type = File::mimeType($filePath);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);
		return $response;
	});
	Route::get('/lectureReminderNotification/{reminderTime}','CronjobController@lectureReminderNotification');